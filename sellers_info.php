<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
?>

<?php

if(isset($_REQUEST['submit']))
{

	$name = isset($_POST['name']) ? $_POST['name'] : '';
	$desc = isset($_POST['desc']) ? $_POST['desc'] : '';
	$country = isset($_POST['country']) ? $_POST['country'] : '';
	$address = isset($_POST['address']) ? $_POST['address'] : '';
	$person = isset($_POST['person']) ? $_POST['person'] : '';
	$lang = isset($_POST['lang']) ? $_POST['lang'] : '';
	$esb = isset($_POST['esb']) ? $_POST['esb'] : '';
	$owner = isset($_POST['owner']) ? $_POST['owner'] : '';
	$payment = isset($_POST['payment']) ? $_POST['payment'] : '';
	$tem = isset($_POST['tem']) ? $_POST['tem'] : '';
	$active = 1;
	
	

	$fields = array(
		'company_name' => mysql_real_escape_string($name),
		'description' => mysql_real_escape_string($desc),
		'country' => mysql_real_escape_string($country),
		'address' => mysql_real_escape_string($address),
		'person_incharge' => mysql_real_escape_string($person),
		'language' => mysql_real_escape_string($lang),
		'year_of_est' => mysql_real_escape_string($esb),
		'owner_type' => mysql_real_escape_string($owner),
		'payment' => mysql_real_escape_string($payment),
		'total_emp' => mysql_real_escape_string($tem));


		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	  $editQuery = "UPDATE `sellers_info` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";

		if (mysql_query($editQuery)) {
		
		if($_FILES['image']['tmp_name']!='')
		{
		$target_path="../upload/sellerimg/";
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$img_name =time().$userfile_name;
		$img=$target_path.$img_name;
		move_uploaded_file($userfile_tmp, $img);
		
		$image =mysql_query("UPDATE `sellers_info` SET `image`='".$img_name."' WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'");
		}
		
		
			$_SESSION['msg'] = "Info Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Category";
		}

		header('Location:sellers_info.php?id=1&action=edit');
		exit();
	
	 }
	 else
	 {
	 
	 }
				
				
}

if($_REQUEST['action']=='edit')
{
$row = mysql_fetch_array(mysql_query("SELECT * FROM `sellers_info` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

}
?>



<!DOCTYPE html>
<html>
    
    <head>
        <title>Seller Info</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
       
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
         <?php include('includes/header.php');?>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal" method="post" action="sellers_info.php" enctype="multipart/form-data">
                                     <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                                     <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                                      <fieldset>
                                        <legend></legend>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Company Name</label>
                                          <div class="controls">
       <input name="name" class="input-xlarge focused" required  id="focusedInput" type="text" value="<?php echo $row['company_name'];?>">
                                          </div>
                                        </div>
										
										
										
										
                                         
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Description</label>
                                        <div class="controls">
                                        <?php /*?><input name="description" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['description'];?>"><?php */?>
		<textarea name="desc" style="width:300px; height:100px;"><?php echo stripslashes($row['description']);?></textarea>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Country </label>
                                        <div class="controls">
            <input name="country" class="input-xlarge focused" required  id="focusedInput" type="text" value="<?php echo $row['country'];?>">                   
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Address</label>
                                        <div class="controls">
        <textarea name="address" style="width:300px; height:100px;"><?php echo stripslashes($row['address']);?></textarea>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Person Incharge</label>
                                        <div class="controls">
         <input name="person" class="input-xlarge focused" type="text" value="<?php echo $row['person_incharge'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Language</label>
                                        <div class="controls">
         <input name="lang" class="input-xlarge focused" type="text" value="<?php echo $row['language'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Year of Establishment</label>
                                        <div class="controls">
           <input name="esb" class="input-xlarge focused" type="text" value="<?php echo $row['year_of_est'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Owner Type</label>
                                        <div class="controls">
          <input name="owner" class="input-xlarge focused" type="text" value="<?php echo $row['owner_type'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Payment</label>
                                        <div class="controls">
        <input name="payment" class="input-xlarge focused" type="text" value="<?php echo $row['payment'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Total Employee</label>
                                        <div class="controls">
        <input name="tem" class="input-xlarge focused" type="text" value="<?php echo $row['total_emp'];?>">
                                        </div>
                                        </div>
										
										
										
										
									
										
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Main Image</label>
                                        <div class="controls">
                                        <input type="file" name="image" > <?php if($row['image']!=''){?><br><a href="../upload/sellerimg/<?php echo $row['image'];?>" target="_blank">View</a><?php }?>
                                        </div>
                                        </div>



                                        
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary"  name="submit">Save changes</button>
                                          <button type="reset" class="btn" onClick="location.href='list_car.php'">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                

                </div>
            </div>
            <hr>
             <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>
        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />
        
        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>


        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>