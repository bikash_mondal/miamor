<?php
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");


if ($_POST['submit']) {

    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $cnfpass = isset($_POST['cnfpass']) ? $_POST['cnfpass'] : '';

    $dbpass = md5($password);



    $fields = array(
        'password' => mysql_real_escape_string($dbpass),
    );



    $fieldsList = array();
    foreach ($fields as $field => $value) {
        $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
    }

    if ($password == $cnfpass) {
        $editQuery = "UPDATE `dateing_user` SET " . implode(', ', $fieldsList)
                . " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";

        mysql_query($editQuery);

        $_SESSION['MSG'] = "Password Updated";
        header('Location:change_user_pass.php?id=' . $_REQUEST['id'] . '&action=chnagepass');
        exit();
    } else {
        $_SESSION['MSG'] = "Wrong pssword";
        header('Location:change_user_pass.php?id=' . $_REQUEST['id'] . '&action=chnagepass');
        exit();
    }
}
if ($_REQUEST['action'] == 'edit') {

    $categoryRowset = mysql_fetch_array(mysql_query("select * from `dateing_user` where `id`='" . $_REQUEST['id'] . "'"));
    $dobselect = explode('-', $categoryRowset['dob']);
    $preyear = $dobselect[0];
    $premonth = $dobselect[1];
    $predate = $dobselect[2];
}
?>
<!DOCTYPE html>
<html>

    <head>
        <title>Change Password</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <style type="text/css">
            .input-xlarge { width:380px; }
        </style>
    </head>

    <body>
        <?php include('includes/header.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php include('includes/left_panel.php'); ?>
                <!--/span-->
                <div class="span9" id="content">
                    <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?>  Password </div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">

                                    <?php
                                    if ($_SESSION['MSG'] != '') {
                                        echo $_SESSION['MSG'];
                                        $_SESSION['MSG'] = '';
                                    }
                                    ?>

                                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                        <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />
                                        <input type="hidden" name="action" value="<?php echo $_REQUEST['action']; ?>" />
                                        <fieldset>
                                            <legend><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> Password</legend>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">New Password</label>
                                                <div class="controls">
                                                    <input type="password" name="password" class="input-xlarge focused" placeholder="New Password" required/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Confirm Password</label>
                                                <div class="controls">
                                                    <input type="password" name="cnfpass" id="focusedInput" class="input-xlarge focused" placeholder="Confirm Password" required/>
                                                </div>
                                            </div>






                                            <div class="form-actions">
                                                <!--                                                <button type="submit" class="btn btn-primary"  name="submit">Save changes</button>-->
                                                <input type="submit" name="submit" value="Save changes" class="btn btn-primary">
                                                <button type="reset" class="btn" onClick="location.href = 'list_user.php'">Cancel</button>
                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>



                </div>
            </div>
            <hr>
            <?php include('includes/footer.php'); ?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>
        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />

        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>


        <script src="assets/scripts.js"></script>
        <script>
                                                    $(function () {
                                                        $(".datepicker").datepicker();
                                                        $(".uniform_on").uniform();
                                                        $(".chzn-select").chosen();
                                                        $('.textarea').wysihtml5();

                                                        $('#rootwizard').bootstrapWizard({onTabShow: function (tab, navigation, index) {
                                                                var $total = navigation.find('li').length;
                                                                var $current = index + 1;
                                                                var $percent = ($current / $total) * 100;
                                                                $('#rootwizard').find('.bar').css({width: $percent + '%'});
                                                                // If it's the last tab then hide the last button and show the finish instead
                                                                if ($current >= $total) {
                                                                    $('#rootwizard').find('.pager .next').hide();
                                                                    $('#rootwizard').find('.pager .finish').show();
                                                                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                                                                } else {
                                                                    $('#rootwizard').find('.pager .next').show();
                                                                    $('#rootwizard').find('.pager .finish').hide();
                                                                }
                                                            }});
                                                        $('#rootwizard .finish').click(function () {
                                                            alert('Finished!, Starting over!');
                                                            $('#rootwizard').find("a[href*='tab1']").trigger('click');
                                                        });
                                                    });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>