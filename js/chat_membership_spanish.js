/*

Copyright (c) 2009 Anant Garg (anantgarg.com | inscripts.com)

This script may be used for non-commercial purposes only. For any
commercial purposes, please contact the author at 
anant.garg@inscripts.com

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

*/

var windowFocus = true;
var username;
var chatHeartbeatCount = 0;
var minChatHeartbeat = 9000;
var maxChatHeartbeat = 33000;
var chatHeartbeatTime = minChatHeartbeat;
var originalTitle;
var blinkOrder = 0;
var chatusername='';

var chatboxFocus = new Array();
var newMessages = new Array();
var newMessagesWin = new Array();
var chatBoxes = new Array();

$(document).ready(function(){
	originalTitle = document.title;
	startChatSession();

	$([window, document]).blur(function(){
		windowFocus = false;
	}).focus(function(){
		windowFocus = true;
		document.title = originalTitle;
	});

    $(document).on('click','.chat_emotion_div .css-emoticon',function(){
        console.log($(this).html());
        chattextbx = $(this).closest('.chatboxinput').find('.chatboxtextarea');
        text_val =  chattextbx.val();
        chattextbx.val(text_val+ ' ' + $(this).html());
        //assign_emotions();
    })
    
    $(document).on('click','.chat_emotion_opener',function(){
        $('.chat_emotion_div').toggle();
        //assign_emotions();
    })
    
    $(document).on('click','.chat_emotion_div .css-emoticon',function(){
        console.log($(this).html());
        chattextbx = $(this).closest('.chatboxinput').find('.chatboxtextarea');
        text_val =  chattextbx.val();
        chattextbx.val(text_val+ ' ' + $(this).html());
        //assign_emotions();
    })
    
    $(document).on('click','.chat_emotion_opener',function(){
        $('.chat_emotion_div').toggle();
        //assign_emotions();
    })    
    
    
	/*$('.chatboxmessagecontent').emoticonize({
		delay: 800,
		animate: true,
		//exclude: 'pre, code, .no-emoticons'
	});*/
});

function restructureChatBoxes() {
	align = 0;
	for (x in chatBoxes) {
		chatboxtitle = chatBoxes[x];

		if ($("#chatbox_"+chatboxtitle).css('display') != 'none') {
			if (align == 0) {
				$("#chatbox_"+chatboxtitle).css('right', '20px');
			} else {
				width = (align)*(225+7)+20;
				$("#chatbox_"+chatboxtitle).css('right', width+'px');
			}
			align++;
		}
	}
}

function chatWith(chatuserid,chatuser) {
    
        chatusername=chatuser; 
        $('#online').slideUp();
        if($("#chatbox_"+chatuserid+" .chatboxtextarea").length)
        {
            //console.log('hi112');
            //$("#chatbox_"+chatuserid).show();
            //document.getElementById('chatbox_' +  chatuserid).style.display = 'block';
            //$('#chatbox_'+chatuserid).css('display','block');
            $('#chatbox_'+chatuserid).removeClass('no-display-chat');
            restructureChatBoxes();
//            $("#chatbox_"+chatuserid).css('bottom', '0px');
//	
//            chatBoxeslength = 0;
//
//            for (x in chatBoxes) {
//                    if ($("#chatbox_"+chatBoxes[x]).hasClass('no-display-chat')) {
//                            chatBoxeslength++;
//                    }
//            }
//
//            if (chatBoxeslength == 0) {
//                    $("#chatbox_"+chatuserid).css('right', '20px');
//            } else {
//                    width = (chatBoxeslength)*(225+7)+20;
//                    $("#chatbox_"+chatuserid).css('right', width+'px');
//            }
        }
        else
        {
            console.log('hielse');
            createChatBox(chatuserid);
        }
         
        $.post("http://107.170.152.166/miamor/useronline.php", {chatuserid: chatuserid,chatuser: chatuser}, function(data){
	  console.log(data);
	}); 
	$("#chatbox_"+chatuserid+" .chatboxtextarea").focus();
}
function assign_emotions()
{
    $('.chatboxmessagecontent,.chat_emotion_div').emoticonize({
			delay: 800,
			animate: true,
			//exclude: 'pre, code, .no-emoticons'
	      });
}

function createChatBox(chatboxtitle,minimizeChatBox) {
	if ($("#chatbox_"+chatboxtitle).length > 0) {
	   if ($("#chatbox_"+chatboxtitle).css('display') == 'none') {
		$("#chatbox_"+chatboxtitle).css('display','block'); 
		restructureChatBoxes();
	   }
	   $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
	   return;
	}
        chat_emotions = ":-) :) :o) :c) :^) :-D :-( :-9 ;-) :-P :-p :-Þ :-b :-O :-/ :-X :-# :'( B-) 8-) :-\\ ;*( :-* :] :&gt; =] =) 8) :} :D 8D XD xD =D :( :&lt; :[ :{ =( ;) ;] ;D :P :p =P =p :b :Þ :O 8O :/ =/ :S :#";
        
	$(" <div />" ).attr("id","chatbox_"+chatboxtitle)
	.addClass("chatbox draggable")
	.html('<div class="chatboxhead"><div class="chatboxtitle">'+chatusername+'</div><div class="chatboxoptions"><img src="http://miamor.com.co/images/device_camera_recorder_video_-128.png" width="16" height="16" style="margin-right:10px;cursor:pointer;" onclick="javascript:window.location.href=\'http://107.170.152.166/miamor/OpenTokSDK/app/OpenTok/web/index1.php?id='+chatboxtitle+'&fname='+chatusername+'\'"/><a href="javascript:void(0)" onclick="javascript:toggleChatBoxGrowth(\''+chatboxtitle+'\')">-</a> <a href="javascript:void(0)" onclick="javascript:closeChatBox(\''+chatboxtitle+'\')">X</a></div><br clear="all"/></div><div class="chatboxcontent" style="overflow-x: hidden;position:relative;"><div style="position:absolute;top:100px;left: 83px;">Loading...</div></div><div class="chatboxinput"><div class="chat_emotion_div">' + chat_emotions + '</div><img class="chat_emotion_opener" src="images/smiley.png"><textarea class="chatboxtextarea" onkeydown="javascript:return checkChatBoxInputKey(event,this,\''+chatboxtitle+'\');" id="chtbx'+chatboxtitle+'" style="border: 1px solid #8B8888;"></textarea><input type="button" onclick="gotopackage()" value="Purchase Translate package" style="border-radius: 5px;color: #fff;float: left;font-family: arial;font-size: 13px;height: 25px;width: 190px;margin-left: 20px;background:#0099FF;border:0px;margin-top:7px;cursor:pointer"><div style="clear:both"></div></div>')
	.appendTo($( "body" ));
			   
	$("#chatbox_"+chatboxtitle).css('bottom', '0px');
	
	chatBoxeslength = 0;

	for (x in chatBoxes) {
		if ($("#chatbox_"+chatBoxes[x]).css('display') != 'none') {
			chatBoxeslength++;
		}
	}

	if (chatBoxeslength == 0) {
		$("#chatbox_"+chatboxtitle).css('right', '20px');
	} else {
		width = (chatBoxeslength)*(225+7)+20;
		$("#chatbox_"+chatboxtitle).css('right', width+'px');
	}
	
	chatBoxes.push(chatboxtitle);

	if (minimizeChatBox == 1) {
		minimizedChatBoxes = new Array();

		if ($.cookie('chatbox_minimized')) {
			minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
		}
		minimize = 0;
		for (j=0;j<minimizedChatBoxes.length;j++) {
			if (minimizedChatBoxes[j] == chatboxtitle) {
				minimize = 1;
			}
		}

		if (minimize == 1) {
			$('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
			$('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
		}
	}

	chatboxFocus[chatboxtitle] = false;

	$("#chatbox_"+chatboxtitle+" .chatboxtextarea").blur(function(){
		chatboxFocus[chatboxtitle] = false;
		$("#chatbox_"+chatboxtitle+" .chatboxtextarea").removeClass('chatboxtextareaselected');
	}).focus(function(){
		chatboxFocus[chatboxtitle] = true;
		newMessages[chatboxtitle] = false;
		$('#chatbox_'+chatboxtitle+' .chatboxhead').removeClass('chatboxblink');
		$("#chatbox_"+chatboxtitle+" .chatboxtextarea").addClass('chatboxtextareaselected');
	});

	$("#chatbox_"+chatboxtitle).click(function() {
		if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') != 'none') {
			$("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
		}
	});

	$("#chatbox_"+chatboxtitle).show();
	//--------------------------------------------------//
	  setTimeout(function(){
	   $.post("http://miamor.com.co/get_previous_chat.php", {chatuserid: chatboxtitle}, function(data){

		
	      var datastr=$.parseJSON(data);
              //console.log(datastr);
	      $('#chatbox_'+chatboxtitle+' .chatboxcontent').html(datastr);
	      assign_emotions();
	   });
	   $('#chatbox_'+chatboxtitle+' .chatboxcontent').animate({ scrollTop: $('#chatbox_'+chatboxtitle+' .chatboxcontent')[0].scrollHeight}, 1000);
	  },2000);
          
          setTimeout(function(){
               $.post("http://miamor.com.co/get_online_status.php", {chatuserid: chatboxtitle}, function(data){
                   var datastr=$.parseJSON(data);
                   if(datastr)
                   {
                       
                       $('#chatbox_'+chatboxtitle+' .chatboxinput .chatboxinput').remove;
                   }
                   else
                   {
                       offspan = '<span class="offsspan">Este usuario no está en línea ahora.</span>';
                       $('#chatbox_'+chatboxtitle+' .chatboxinput').prepend(offspan);
                   }
               })
          },3000);
	//--------------------------------------------------//
}

function translate_spanish(chtboxid)
{
     var text = $('#chtbx'+chtboxid).val();
     console.log('translate.php?source=en&target=sp&val='+text);
     $.post('translate.php?source=en&target=es&val='+text, function(data){
     if(data!=''){
      }
    });
}

function chatHeartbeat(){

	var itemsfound = 0;
	
	if (windowFocus == false) {
 
		var blinkNumber = 0;
		var titleChanged = 0;

		for (x in newMessagesWin) {
			if (newMessagesWin[x] == true) {
				++blinkNumber;
				if (blinkNumber >= blinkOrder) {
                                $.ajax({
                                    url: "chathead.php",
                                    data: {id : x},
                                    type: "POST",
                                    success: function (resp) {
                                        //alert(resp);
                                        document.title = resp;
                                    }
                                });                    
                                //document.title = x + 'says....' ;
					titleChanged = 1;
					break;	
				}
			}
		}
		
		if (titleChanged == 0) {
			document.title = originalTitle;
			blinkOrder = 0;
		} else {
			++blinkOrder;
		}

	} else {
		for (x in newMessagesWin) {
			newMessagesWin[x] = false;
		}
	}

	for (x in newMessages) {
		if (newMessages[x] == true) {

			if (chatboxFocus[x] == false) {
				//FIXME: add toggle all or none policy, otherwise it looks funny
				$('#chatbox_'+x+' .chatboxhead').toggleClass('chatboxblink');
			}
		}
	}
	
	$.ajax({
	  url: "chat.php?action=chatheartbeat",
	  cache: false,
	  dataType: "json",
	  success: function(data) {

		$.each(data.items, function(i,item){
			if (item)	{ // fix strange ie bug

				chatboxtitle = item.id;

				if ($("#chatbox_"+chatboxtitle).length <= 0) {

					chatusername=item.f;
					createChatBox(chatboxtitle);
				}
				if ($("#chatbox_"+chatboxtitle).css('display') == 'none') {
					$("#chatbox_"+chatboxtitle).css('display','block');
					restructureChatBoxes();
				}
				
				if (item.s == 1) {
					item.f = chatusername;
				}

				if (item.s == 2) {
					$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo">'+item.m+'</span></div>');
				} else {
					newMessages[chatboxtitle] = true;
					newMessagesWin[chatboxtitle] = true;
					/*$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+item.f+'[' + item.date + ']:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+item.m+'</span></div>');*/
					$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+item.f+' :&nbsp;</span><span class="chatboxmessagecontent">'+item.m+'</span></div>');
                                        var audio = new Audio();
                                        audio.src = "images/hangouts_message.ogg";
                                        audio.play();
				}
				$('.chatboxmessagecontent').emoticonize({
					delay: 800,
					animate: true,
					//exclude: 'pre, code, .no-emoticons'
			       });


				$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
				itemsfound += 1;
			}
		});

		chatHeartbeatCount++;

		if (itemsfound > 0) {
			chatHeartbeatTime = minChatHeartbeat;
			chatHeartbeatCount = 1;
		} else if (chatHeartbeatCount >= 10) {
			chatHeartbeatTime *= 2;
			chatHeartbeatCount = 1;
			if (chatHeartbeatTime > maxChatHeartbeat) {
				chatHeartbeatTime = maxChatHeartbeat;
			}
		}
		
		setTimeout('chatHeartbeat();',chatHeartbeatTime);
	}});
}

function closeChatBox(chatboxtitle) {
        //$('#chatbox_'+chatboxtitle).remove();
	//$('#chatbox_'+chatboxtitle).css('cssText','display:none !important');
        $('#chatbox_'+chatboxtitle).addClass('no-display-chat');
	restructureChatBoxes();
       
        
	$.post("chat.php?action=closechat", { chatbox: chatboxtitle} , function(data){
             $.post("http://miamor.com.co/closed_user.php", { userid: chatboxtitle} , function(data){
                 
                // window.location.reload();
	});
	});

}

function toggleChatBoxGrowth(chatboxtitle) {
	if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') == 'none') {  
		
		var minimizedChatBoxes = new Array();
		
		if ($.cookie('chatbox_minimized')) {
			minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
		}

		var newCookie = '';

		for (i=0;i<minimizedChatBoxes.length;i++) {
			if (minimizedChatBoxes[i] != chatboxtitle) {
				newCookie += chatboxtitle+'|';
			}
		}

		newCookie = newCookie.slice(0, -1)


		$.cookie('chatbox_minimized', newCookie);
		$('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','block');
		$('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','block');
		$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
	} else {
		
		var newCookie = chatboxtitle;

		if ($.cookie('chatbox_minimized')) {
			newCookie += '|'+$.cookie('chatbox_minimized');
		}


		$.cookie('chatbox_minimized',newCookie);
		$('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
		$('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
	}
	
}


function checkChatBoxInputKey(event,chatboxtextarea,chatboxtitle) {
	 
	if(event.keyCode == 13 && event.shiftKey == 0)  {
		message = $(chatboxtextarea).val();
		message = message.replace(/^\s+|\s+$/g,"");

		$(chatboxtextarea).val('');
		$(chatboxtextarea).focus();
		$(chatboxtextarea).css('height','44px');
		if (message != '') {
			$.post("chat.php?action=sendchat", {to: chatboxtitle, message: message, time: getDateTime()} , function(data){
				message = message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
				/*$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+username+' [' + new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(:[\d]{2})(.*)/, "$1$3") +']:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+message+'</span></div>');*/

				$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+username+' :&nbsp;</span><span class="chatboxmessagecontent">'+message+'</span></div>');
				
				$('.chatboxmessagecontent').emoticonize({
					delay: 800,
					animate: true,
					//exclude: 'pre, code, .no-emoticons'
			       });
			       $("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
			});
		}
		chatHeartbeatTime = minChatHeartbeat;
		chatHeartbeatCount = 1;

		return false;
	}

	var adjustedHeight = chatboxtextarea.clientHeight;
	var maxHeight = 94;

	if (maxHeight > adjustedHeight) {
		adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
		if (maxHeight)
			adjustedHeight = Math.min(maxHeight, adjustedHeight);
		if (adjustedHeight > chatboxtextarea.clientHeight)
			$(chatboxtextarea).css('height',adjustedHeight+8 +'px');
	} else {
		$(chatboxtextarea).css('overflow','auto');
	}
	 chatHeartbeat();
	
}

function startChatSession(){  
	$.ajax({
	  url: "chat.php?action=startchatsession",
	  cache: false,
	  dataType: "json",
	  success: function(data) {

		username = data.username;

		$.each(data.items, function(i,item){
			if (item)	{ // fix strange ie bug

				chatboxtitle = item.f;

				if ($("#chatbox_"+chatboxtitle).length <= 0) {
					createChatBox(chatboxtitle,1);
				}
				
				if (item.s == 1) {
					item.f = chatusername;
				}

				if (item.s == 2) {
					$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo">'+item.m+'</span></div>');
				} else {
					$("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+item.f+':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+item.m+'</span></div>');
				}
			}
		});
		
		for (i=0;i<chatBoxes.length;i++) {
			chatboxtitle = chatBoxes[i];
			$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
			setTimeout('$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);', 100); // yet another strange ie bug
		}
	
	setTimeout('chatHeartbeat();',chatHeartbeatTime);
		
	}});
}


function getDateTime() {
    var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds(); 
    if(month.toString().length == 1) {
        var month = '0'+month;
    }
    if(day.toString().length == 1) {
        var day = '0'+day;
    }   
    if(hour.toString().length == 1) {
        var hour = '0'+hour;
    }
    if(minute.toString().length == 1) {
        var minute = '0'+minute;
    }
    if(second.toString().length == 1) {
        var second = '0'+second;
    }   
    var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;   
     return dateTime;
}

function gotopackage(){
location.href='purchase_translation.php';	
}

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    
    
};

