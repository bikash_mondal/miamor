<?php
ob_start();
session_start();
include('administrator/includes/config.php');
if ($_SESSION['user_id'] == '') {
    header('location:index.php');
    exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <title>Search Members</title>
        <meta name="" content="">
        <link rel="stylesheet" href="css/style.css">
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    </head>
    <body style="background: url(images/bg-main.jpg) center top no-repeat fixed; margin: 0 0 15px 0;">
        <?php include('includes/header.php'); ?>
        <div class="container">
            <div class="profile_body">
                <?php include('includes/left-panel.php'); ?>
                <div class="right_menu">
                    <h2><?php echo YOURSEARCH ?></h2>
                    <div class="tab_based search_results">

                        <?php
                        $sql = "SELECT * FROM `dateing_user` WHERE `id`!='" . $_SESSION['user_id'] . "'";
                        if ($_REQUEST['search_name'] != '') {
                            $sql .=" AND `fname` Like '%" . $_REQUEST['search_name'] . "%'";
                        }
                        if ($_REQUEST['name'] != '') {
                            $sql .=" OR `dpname` Like '%" . $_REQUEST['name'] . "%'";
                        }
                        if ($_REQUEST['sex'] != '') {
                            $sql .=" AND `sex`='" . $_REQUEST['sex'] . "'";
                        }
                        if (!empty($_REQUEST['age'])) {
                            $current_user = mysql_fetch_array(mysql_query("SELECT *,FLOOR(DATEDIFF (NOW(), dob)/365) as age FROM `dateing_user` WHERE `id`='" . $_SESSION['user_id'] . "'"));
                            //echo "age - ".$current_user['age'];
                            //exit;
                            //if($_REQUEST['age'])
                            if ($_REQUEST['age'] == '14 AND 17' && $current_user['age'] > 17) {
                                $sql .=" AND FLOOR(DATEDIFF (NOW(), dob)/365) NOT BETWEEN " . $_REQUEST['age'];
                            } else {
                                $sql .=" AND FLOOR(DATEDIFF (NOW(), dob)/365) BETWEEN " . $_REQUEST['age'];
                            }
                        }
                        if ($_REQUEST['height_from'] != '') {
                            $sql .=" AND `height` >= '" . $_REQUEST['height_from'] . "'";
                        }
                        if ($_REQUEST['height_to'] != '') {
                            $sql .=" AND `height` <= '" . $_REQUEST['height_to'] . "'";
                        }
                        if (isset($_REQUEST['religion']) && $_REQUEST['religion'] != '') {
                            $reg = '';
                            foreach ($_REQUEST['religion'] as $v) {
                                $reg.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(religion, '" . trim($reg, ',') . "')";
                        }
                        if (isset($_REQUEST['education']) && $_REQUEST['education'] != '') {
                            $edu = '';
                            foreach ($_REQUEST['education'] as $v) {
                                $edu.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(education, '" . trim($edu, ',') . "')";
                        }
                        if (isset($_REQUEST['bodytype']) && $_REQUEST['bodytype'] != '') {
                            $bdy = '';
                            foreach ($_REQUEST['bodytype'] as $v) {
                                $bdy.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(bodytype, '" . trim($bdy, ',') . "')";
                        }
                        if (isset($_REQUEST['smoking']) && $_REQUEST['smoking'] != '') {
                            $smk = '';
                            foreach ($_REQUEST['smoking'] as $v) {
                                $smk.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(smoking, '" . trim($smk, ',') . "')";
                        }
                        if (isset($_REQUEST['drinking']) && $_REQUEST['drinking'] != '') {
                            $drnk = '';
                            foreach ($_REQUEST['drinking'] as $v) {
                                $drnk.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(drinking, '" . trim($drnk, ',') . "')";
                        }
                        if (isset($_REQUEST['language1']) && $_REQUEST['language1'] != '') {

                            $lang = '';
                            foreach ($_REQUEST['language1'] as $v) {
                                $lang.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(language, '" . trim($lang, ',') . "')";
                        }
                        if (isset($_REQUEST['relation']) && $_REQUEST['relation'] != '') {
                            $rltn = '';
                            foreach ($_REQUEST['relation'] as $v) {
                                $rltn.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(relation, '" . trim($rltn, ',') . "')";
                        }
                        if (isset($_REQUEST['child']) && $_REQUEST['child'] != '') {
                            $chld = '';
                            foreach ($_REQUEST['child'] as $v) {
                                $chld.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(child, '" . trim($chld, ',') . "')";
                        }
                        if (isset($_REQUEST['ethnicity']) && $_REQUEST['ethnicity'] != '') {
//					 $ethn='';
//					 foreach($_REQUEST['ethnicity'] as $v)
//					 {
//                                            $ethn.=$v.',';
//					 }
//					 $sql .=" AND FIND_IN_SET(ethnicity, '".trim($ethn,',')."')";

                            $sql .= " AND ethnicity IN(" . implode(',', $_REQUEST['ethnicity']) . ")";
                        }
                        if (isset($_REQUEST['appearence']) && $_REQUEST['appearence'] != '') {
                            $appear = '';
                            foreach ($_REQUEST['appearence'] as $v) {
                                $appear.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(appearence, '" . trim($appear, ',') . "')";
                        }
                        if (isset($_REQUEST['relocate']) && $_REQUEST['relocate'] != '') {
                            $reloc = '';
                            foreach ($_REQUEST['relocate'] as $v) {
                                $reloc.=$v . ',';
                            }
                            $sql .=" AND FIND_IN_SET(relocate, '" . trim($reloc, ',') . "')";
                        }
                        $sql .=" order by fb_varify DESC , is_phone_verified DESC";

                        $res2 = mysql_query($sql);
                        $tot = mysql_num_rows($res2);
                        if ($tot > 0) {
                            while ($res = mysql_fetch_array($res2)) {
                                $dobselect = explode('-', $res['dob']);
                                $preyear = $dobselect[0];
                                $premonth = $dobselect[1];
                                $predate = $dobselect[2];
                                $cdate = date("Y");
                                $myage = $cdate - $preyear;

                                $isblockedbyother = mysql_num_rows(mysql_query("SELECT * FROM `dating_block_user` WHERE `block_to`='" . $_SESSION['user_id'] . "' AND `user_id`='" . $res['id'] . "'"));



                                if ($res['sex'] == 'male') {
                                    $sexy = MALE;
                                }
                                if ($res['sex'] == 'female') {
                                    $sexy = FEMALE;
                                }



                                if ($res['lookfor'] == "penpal") {
                                    $lookFor = PENPAL;
                                } if ($res['lookfor'] == "a friend") {
                                    $lookFor = AFRIEND;
                                } if ($res['lookfor'] == "casual fun") {
                                    $lookFor = CASUAL;
                                } if ($user['lookfor'] == "relationship") {
                                    $lookFor = ARELATION;
                                } if ($res['lookfor'] == "marriage") {
                                    $lookFor = MARRIGE;
                                } if ($res['lookfor'] == "sugar daddy") {
                                    $lookFor = SUGAR;
                                }



                                if ($isblockedbyother == 0) {

                                    if ($res['fname'] != '') {
                                        ?>
                                        <div id="f1_container">
                                            <div id="f1_card" class="shadow">
                                                <div class="front face" style="background:url(<?php echo (($res['image'] != '') ? 'upload/userimage/' . $res['image'] : 'upload/userimage/noimage.Jpeg') ?>);background-size: 100% 100%;background-repeat: no-repeat;">
                                                    <div class="profinfodiv" >
                                                        <h4 style="font-size: 17px; color:#fff; font-weight: bold !important;"><?php echo (!empty($res['dpname']) ? $res['dpname'] : $res['fname']); ?> 
                                                            <?php
                                                            if ($res['is_loggedin'] == 1) {
                                                                ?>
                                                                <span class="online_span" style="float:right"><i></i></span>
                                                            <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="back face center">
                                                    <?php
                                                    $getfavourite = mysql_fetch_array(mysql_query("select * from `dateing_favourite` where `from_id`='" . $_SESSION['user_id'] . "' AND `to_id`='" . $res['id'] . "' AND status=1 ORDER by id DESC limit 1"));
                                                    $requests = mysql_fetch_array(mysql_query("select * from `dating_friend_requests` where `from_id`='" . $_SESSION['user_id'] . "' AND `to_id`='" . $res['id'] . "' ORDER by id DESC limit 1"));

                                                    $getrequests = mysql_fetch_array(mysql_query("select * from `dating_friend_requests` where `from_id`='" . $res['id'] . "' AND `to_id`='" . $_SESSION['user_id'] . "' ORDER by id DESC limit 1"));
                                                    ?>
                                                    <div class="backimgdiv">
                                                        <img src="<?php echo (($res['image'] != '') ? 'upload/userimage/' . $res['image'] : 'upload/userimage/noimage.Jpeg') ?>"/>
                                                    </div>
                                                    <div class="backinfodiv">
                                                        <h4><?php
                                                            echo (!empty($res['dpname']) ? $res['dpname'] : $res['fname']);
                                                            #echo ((strlen($res['fname'])>8)?ucwords(substr($res['fname'],0,8)).'..':ucwords($res['fname'])); 
                                                            ?></h4>
                                                        <?php if (isset($res['sex']) && $res['sex'] != '') { ?>
                                                            <p><?php echo ucwords($sexy); ?></p>
                                                        <?php } ?>
                                                        <p><span><?php echo AGEDET ?>:</span> <?php echo $myage; ?> <?php echo YEARS_OLD ?> </p>
                                                        <?php if (isset($res['lookfor']) && $res['lookfor'] != '') { ?>
                                                            <p><span><?php echo LOOKING ?>:</span> <?php echo ucwords($lookFor); ?></p>
                                                        <?php } else { ?>
                                                            <p><span><?php echo LOOKING ?>:</span> N/A</p>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="backaboutmediv">
                                                        <p><span><?php echo ABOUTME; ?>:</span> <?php echo ((isset($res['about_me']) && $res['about_me'] != '') ? nl2br(substr($res['about_me'], 0, 230)) . '...' : 'N/A'); ?></p>
                                                    </div>

                                                    <div class="backfavdiv">
                                                        <span><?php if (!empty($getfavourite)) { ?><img src="images/rate.png" alt="favourite" id="favic" title="Favourite" style="margin-top: -4px;"><?php } ?> <img src="images/rate.png" alt="favourite" id="favic<?php echo $res['id'] ?>" style="display:none;margin-top: -4px;"></span>

                                                        <?php
                                                        if (!empty($getrequests)) {
                                                            if ($getrequests['accept'] == 1) {
                                                                ?>
                                                                <span><img src="images/frnd.png" alt="Friend" style="width:22px;height:22px;margin-right: 4px;margin-bottom: 6px;" title="Friend"/></span>
                                                                <?php
                                                            }
                                                        } else {
                                                            if (!empty($requests)) {
                                                                if ($requests['accept'] == 1) {
                                                                    ?>
                                                                    <span><img src="images/frnd.png" alt="Friend" style="width:22px;height:22px;margin-right: 4px;margin-bottom: 6px;" title="Friend"/></span>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </div>

                                                    <div class="profinfodivback three_bttn">
                                                        <?php
                                                        $getfavourite = mysql_fetch_array(mysql_query("select * from `dateing_favourite` where `from_id`='" . $_SESSION['user_id'] . "' AND `to_id`='" . $res['id'] . "' AND status=1 ORDER by id DESC limit 1"));
                                                        if (empty($getfavourite)) {
                                                            ?>
                                                            <input type="button" value="<?php echo LANG_FAVOURITE; ?>" id="favourite<?php echo $res['id'] ?>" onclick="make_fav(<?php echo $res['id'] ?>)"> 
                                                        <?php } ?>

                                                        <input type="button" value="<?php echo DETAILS; ?>" onClick="location.href = 'details.php?id=<?php echo $res['id'] ?>'"/>
                                                        <?php
                                                        if (!empty($getrequests)) {
                                                            if ($getrequests['accept'] == 1) {
                                                                if (has_access('send_messages') == true) {
                                                                    if (is_permitted($res['id'], 'send_messages') == true) {
                                                                        ?>
                                                                        <input type="button" value="<?php echo CHATDT ?>" onclick="javascript:chatWith('<?php echo $res['id'] ?>', '<?php echo (!empty($res['dpname']) ? $res['dpname'] : $res['fname']); ?>')">
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if (!empty($requests)) {
                                                                if ($requests['accept'] == 1) {
                                                                    if (has_access('send_messages') == true) {
                                                                        if (is_permitted($res['id'], 'send_messages') == true) {
                                                                            ?>
                                                                            <input type="button" value="<?php echo CHATDT ?>" onclick="javascript:chatWith('<?php echo $res['id'] ?>', '<?php echo (!empty($res['dpname']) ? $res['dpname'] : $res['fname']); ?>')">
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            ?>
                            <div class="tab_based search_results" style="text-align:center">
                                <h1 style="color:red;font-size:22px;font-weight:100;margin:20px 0px">
                                    <?php echo LANG_NO_SEARCH_RESULT; ?>
                                </h1>
                            </div>
                        <?php }
                        ?>
                        <!--------------------------------------->

                    </div>
                </div>
                <div class="clearfix"></div>
                <?php include('includes/footer.php'); ?>
            </div>
        </div>
    <style>
        .online_span i {
            width: 12px;
            height: 12px;
            background: #41c439;
            border-radius: 100%;
            z-index: 999999;
            margin-top:2px;margin-right:9px;
            display:block;
        }
        .three_bttn input[type="button"]{float:none;margin-left:0px;margin-top:13px}
        #f1_container {
            position: relative;
            margin: 13px 0px 13px 13px;
            width: 270px;
            height: 270px;
            z-index: 1;
            float:left;
        }
        #f1_container {
            perspective: 1000;
        }
        #f1_card {
            width: 100%;
            height: 100%;
            transform-style: preserve-3d;
            transition: all 1.0s linear;
            border: 2px solid #FFF;
        }
        #f1_container:hover #f1_card {
            transform: rotateY(180deg);
            #box-shadow: -5px 5px 5px #aaa;
        }
        .face {
            position: absolute;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
        }
        .face.back {
            display: block;
            transform: rotateY(180deg);
            box-sizing: border-box;
            #padding: 10px;
            color: #000;
            text-align: center;
            background-color: #ECE8E8;
        }
        .profinfodiv
        {
            width:99.7%;
            height:31px;
            border:0px solid red;
            margin-top:239px;
            background:url(images/trans_new.png) repeat;
            font-family:arial;
        }
        .profinfodivback
        {
            width:99.7%;
            height:45px;
            border:0px solid red;
            background:url(images/trans_new.png) repeat;
            margin-top:225px;
            font-family:arial;
        }
        .backimgdiv{float:left;height:70px;width:70px;margin:7px 0 0 7px}
        .backimgdiv img{height:70px;width:70px;border-radius:100%;border:2px solid #0099FF;}
        .profinfodiv h4{color:#6CC1FA;text-align:left;padding:7px 0 7px 10px}
        .profinfodiv .descdiv,.profinfodivback .descdiv{float:left}
        .profinfodiv .descdiv h4,.profinfodivback .descdiv h4{padding:2px 0 2px 10px}
        .profinfodiv .descdiv h4 span,.profinfodivback .descdiv h4 span{font-size:12px !important;padding:2px 4px 2px 0px !important;}
        .backinfodiv h4{color:#6CC1FA;text-align:left;padding:2px 0 7px 10px;font-size:20px}
        .backinfodiv p{text-align: left;padding: 0 0 0 10px;font-size: 13px;margin-bottom:8px;color:#525355}
        .backinfodiv p span{font-weight:bold}
        .backinfodiv{float:left;height:100px;width:172px;margin:7px 0 0 10px;}

        .backfavdiv{float:left;height:30px;width:70px;margin:10px 0 0 10px;position:absolute;top:0px;right:0}

        .backaboutmediv{float:left;width:90%;margin-left:10px} 
        .backaboutmediv p{text-align: justify;font-size: 13px;margin-bottom:8px;color:#525355}
        .backaboutmediv p span{font-weight:bold}
    </style>
    <script>
        $(document).ready(function () {
            //When page loads...
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content

            //On Click Event
            $("ul.tabs li").click(function () {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });
        });
        function make_fav(user_id) {
            $.ajax({
                type: "post",
                url: "function/ajax.php?action=favourite",
                data: 'from_id=<?php echo $_SESSION['user_id']; ?>&to_id=' + user_id + '&message=',
                success: function (msg) {
                    if (msg.trim() == 'suc') {
                        //$('.thank_u'+user_id).html('Added to Favourite..').show('slow');
                        //$('#message_text').html('');
                        //$("#msg").hide();
                        $('#favourite' + user_id).hide();
                        $('#favic' + user_id).show();
                    }
                }
            });
        }
    </script>
</body>
</html>

























































