<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
if(isset($_REQUEST['submit']))
{
	$title = isset($_POST['title']) ? $_POST['title'] : '';
	$title_translated = isset($_POST['title_translated']) ? $_POST['title_translated'] : '';
	$description = isset($_POST['description']) ? $_POST['description'] : '';
	$description_translated = isset($_POST['description_translated']) ? $_POST['description_translated'] : '';
        $duration_time=  isset($_REQUEST['duration_time'])?$_REQUEST['duration_time']:'';
        
        
	$price = isset($_POST['price']) ? $_POST['price'] : '';
	$for = isset($_POST['for']) ? $_POST['for'] : 'M';
        $duration = isset($_POST['duration']) ? $_POST['duration'] : 0 ;
	$fields = array(
		'title' => mysql_real_escape_string($title),
		'title_translated' => mysql_real_escape_string($title_translated),
		'description' => mysql_real_escape_string($description),
		'description_translated' => mysql_real_escape_string($description_translated),
		'price' => mysql_real_escape_string($price),
                'duration_time'=>mysql_real_escape_string($duration_time),
		'for' => mysql_real_escape_string($for),
                'duration' => $duration
		);
		$fieldsList = array();
		foreach ($fields as $field => $value)
		{
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
		$editQuery = "UPDATE `dateing_package` SET " . implode(', ', $fieldsList). " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
		if (mysql_query($editQuery))
		{
			$_SESSION['msg'] = "Subscription Package Updated Successfully";
		}
		else
		{
			$_SESSION['msg'] = "Error occuried while updating Subscription Package";
		}
		//header('Location:social.php');
		//exit();
}
	$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `dateing_package` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

?>

<!DOCTYPE html>

<html>

    

    <head>

        <title>Edit Subscription Package</title>

        <!-- Bootstrap -->

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">

        <link href="assets/styles.css" rel="stylesheet" media="screen">

       

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    </head>

    

    <body>

         <?php include('includes/header.php');?>

        <div class="container-fluid">

            <div class="row-fluid">

                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Edit Subscription Package</div>
                            </div>
<div class="block-content collapse in">
	<div class="span12">
		<!--subscription_package_edit.php-->
		<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Title</label>
					<div class="controls">
					<input name="title" class="input-xlarge focused" required  id="focusedInput" type="text" value="<?php echo $categoryRowset['title'];?>">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Title Translated</label>
					<div class="controls">
					<input name="title_translated" class="input-xlarge focused" required type="text" value="<?php echo $categoryRowset['title_translated'];?>">
					</div>
				</div>
				
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Description</label>
					<div class="controls">
					<textarea name="description" class="input-xlarge focused" required><?php echo $categoryRowset['description'];?></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Description Translated</label>
					<div class="controls">
					<textarea name="description_translated" class="input-xlarge focused" required><?php echo $categoryRowset['description_translated'];?></textarea>
					</div>
				</div>
				
				
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Price</label>
					<div class="controls">
						<input name="price" class="input-xlarge focused" <?php if($_REQUEST['id']==1){echo 'readonly';}else{echo 'required';}?>  type="number" value="<?php if($_REQUEST['id']==1){echo 0;}else{echo $categoryRowset['price'];}?>">
					</div>
				</div>
                                <?php
                                if($_REQUEST['id']!=2)
                                {    
                                ?>
                                <div class="control-group">
					<label class="control-label" for="focusedInput">Duration (Months)</label>
					<div class="controls">
						<input name="duration" class="input-xlarge focused" required  type="number" value="<?php echo (isset($categoryRowset['duration'])?$categoryRowset['duration']:''); ?>">
					</div>
				</div>
                                <?php } else{?>
                            
                            <div class="control-group">
					<label class="control-label" for="focusedInput">Duration (Minute's)</label>
					<div class="controls">
						<input name="duration_time" class="input-xlarge focused" required  type="number" value="<?php echo (isset($categoryRowset['duration_time'])?$categoryRowset['duration_time']:''); ?>">
					</div>
				</div>
                                <?php }?>
                            
				<?php
				if($_REQUEST['id'] != 1)
				{
				?>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Price</label>
					<div class="controls">
						<label><input name="for" type="radio" value="M" <?php if($categoryRowset['for'] == 'M')echo 'checked="checked"';?>><span>For Male</span></label>
						<label><input name="for" type="radio" value="F" <?php if($categoryRowset['for'] == 'F')echo 'checked="checked"';?>><span>For Female</span></label>
					</div>
				</div>
				<?php
				}
				?>
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary"  name="submit">Save changes</button>
					<button type="reset" class="btn" onClick="location.href='subscription_package_management.php'">Cancel</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>

            <hr>

             <?php include('includes/footer.php');?>

        </div>

        <!--/.fluid-container-->

<style>
.form-horizontal .controls label
{
	float: left;
	height: 22px;
	margin-bottom: 5px;
	margin-top: 5px;
}
.form-horizontal .controls input[type="radio"]
{
	float: left;
}
.form-horizontal .controls span
{
	float: left;
	margin: 0px 7px;
}
.form-horizontal .controls label:first-child
{
	margin-right: 30px;
}
.form-horizontal .controls label:second-child
{
	margin-left: 30px;
}
</style>
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">

        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">

        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">



        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">



        <script src="vendors/jquery-1.9.1.js"></script>

        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="vendors/jquery.uniform.min.js"></script>

        <script src="vendors/chosen.jquery.min.js"></script>

        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>

        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />

        

        <script type="text/javascript" src="js/colorpicker.js"></script>

        <script type="text/javascript" src="js/eye.js"></script>

        <script type="text/javascript" src="js/utils.js"></script>

        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>





        <script src="assets/scripts.js"></script>

        <script>

        $(function() {

            $(".datepicker").datepicker();

            $(".uniform_on").uniform();

            $(".chzn-select").chosen();

            $('.textarea').wysihtml5();



            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {

                var $total = navigation.find('li').length;

                var $current = index+1;

                var $percent = ($current/$total) * 100;

                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead

                if($current >= $total) {

                    $('#rootwizard').find('.pager .next').hide();

                    $('#rootwizard').find('.pager .finish').show();

                    $('#rootwizard').find('.pager .finish').removeClass('disabled');

                } else {

                    $('#rootwizard').find('.pager .next').show();

                    $('#rootwizard').find('.pager .finish').hide();

                }

            }});

            $('#rootwizard .finish').click(function() {

                alert('Finished!, Starting over!');

                $('#rootwizard').find("a[href*='tab1']").trigger('click');

            });

        });

        </script>

        <script type="text/javascript" src="js/jquery.js"></script>

                <script type="text/javascript" src="js/chat.js"></script>
    </body>



</html>
