<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");

if(isset($_GET['id']) && $_GET['id']!='')
{
   $queryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `dating_report` WHERE `id`='".mysql_real_escape_string($_GET['id'])."'"));
}
?><!DOCTYPE html>
<html>
    
    <head>
        <title>Report Details</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
         <?php include('includes/header.php');?>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">User Report Details</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal">
                                      <fieldset>
                                        <legend>User Report Details</legend>
                                        <?php

	                                $touserDetails=mysql_fetch_array(mysql_query("SELECT * FROM `dateing_user` WHERE `id`='".$queryRowset['report_for']."'"));

									 $fromuserDetails=mysql_fetch_array(mysql_query("SELECT * FROM `dateing_user` WHERE `id`='".$queryRowset['user_id']."'"));
									?>
                                       <div class="control-group">
                                          <label class="control-label" for="focusedInput">Report From</label>
                                          <div class="controls">
                                           <a href="user_details.php?id=<?php echo $fromuserDetails['id'];?>&action=details&bk=4"><?php echo ucwords($fromuserDetails['fname']);?></a>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label">Report Against</label>
                                          <div class="controls">
                                            <span class="input-xlarge"><a href="user_details.php?id=<?php echo $touserDetails['id'];?>&action=details&bk=4"><?php echo ucwords($touserDetails['fname']);?></a></span>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label" for="disabledInput">Message</label>
                                          <div class="controls">
                                            <?php echo nl2br($queryRowset['message']);?>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label" for="disabledInput">Report Date</label>
                                          <div class="controls">
                                            <?php echo date('d M, Y H:i:s',strtotime($queryRowset['date']));?>
                                          </div>
                                        </div>

							

									<?php 

									 $all_reports = mysql_query("SELECT * FROM `dating_report` WHERE  `report_for`=".mysql_real_escape_string($touserDetails['id'])." and id!=".$_GET['id']." ");
									
									if (mysql_num_rows($all_reports)>0) { ?>

										<legend>Other Report ( old )</legend>
										

									<?php
									while ($reports=mysql_fetch_array($all_reports)) {

									   $fromuserDetails=mysql_fetch_array(mysql_query("SELECT * FROM `dateing_user` WHERE `id`='".$reports['user_id']."'"));
							
                                        ?>

                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Report From</label>
                                          <div class="controls">
                                           <a href="user_details.php?id=<?php echo $fromuserDetails['id'];?>&action=details&bk=4"><?php echo ucwords($fromuserDetails['fname']);?></a>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label">Report Against</label>
                                          <div class="controls">
                                            <span class="input-xlarge"><a href="user_details.php?id=<?php echo $touserDetails['id'];?>&action=details&bk=4"><?php echo ucwords($touserDetails['fname']);?></a></span>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label" for="disabledInput">Message</label>
                                          <div class="controls">
                                            <?php echo nl2br($reports['message']);?>
                                          </div>
                                        </div>

                                        <div class="control-group">
                                          <label class="control-label" for="disabledInput">Report Date</label>
                                          <div class="controls">
                                            <?php echo date('d M, Y H:i:s',strtotime($reports['date']));?>
                                          </div>
                                        </div>
									<?php } } ?>
                                        
                                        <div class="form-actions">
                                        
                                         <button type="reset" class="btn" onClick="window.location.href='list_report.php'">Back</button>
                                        
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                

                </div>
            </div>
            <hr>
             <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>
