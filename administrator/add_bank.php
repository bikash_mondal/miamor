<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");


if(isset($_REQUEST['submit']))
{

	

		
					 
	 if($_REQUEST['action']=='edit')
	  {
             $id = $_REQUEST['id'];
             unset($_POST['action']);
            unset($_POST['submit']);
            unset($_POST['id']);
            $fieldsList = array();
		foreach ($_POST as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
	  $editQuery = "UPDATE `dateing_bank` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($id) . "'";

		if (mysql_query($editQuery)) {
		
		
		
		
			$_SESSION['msg'] = "Category Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Category";
		}
                
		header('Location:list_banks.php');
		exit();
	
	 }
	 else
	 {
	 unset($_POST['action']);
         unset($_POST['submit']);
	 $addQuery = "INSERT INTO `dateing_bank` (`" . implode('`,`', array_keys($_POST)) . "`)"
			. " VALUES ('" . implode("','", array_values($_POST)) . "')";
		
		mysql_query($addQuery);
		$last_id=mysql_insert_id();
		
		 
		if (mysql_query($addQuery)) {
		
			$_SESSION['msg'] = "Category Added Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while adding Category";
		}
		
		header('Location:list_banks.php'); 
		exit();
	
	 }
				
				
}

if($_REQUEST['action']=='edit')
{
$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `dateing_bank` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

}
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Add Bank Details</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
       
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

		<style type="text/css">
			.input-xlarge { width:380px; }
		</style>
    </head>
    
    <body>
         <?php include('includes/header.php');?>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?>  Bank Details </div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal" method="post" action="add_bank.php" enctype="multipart/form-data">
                                     <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                                     <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                                      <fieldset>
                                          <legend>Currency</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Currency</label>
                                          <div class="controls">
                      <input name="currency" class="input-xlarge focused" required  id="currency" type="text" value="<?php echo $categoryRowset['currency'];?>">
                                          </div>
                                        </div>
                                        
                                        <legend>Intermediary Bank</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Bank Name</label>
                                          <div class="controls">
                      <input name="ib_name" class="input-xlarge focused" required  id="ib_name" type="text" value="<?php echo $categoryRowset['ib_name'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">ABA Code</label>
                                          <div class="controls">
                      <input name="aba_code" class="input-xlarge focused"  id="aba_code" type="text" value="<?php echo $categoryRowset['aba_code'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">IBAN</label>
                                          <div class="controls">
                      <input name="iban" class="input-xlarge focused"  id="iban" type="text" value="<?php echo $categoryRowset['iban'];?>">
                                          </div>
                                        </div>
                                        
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">SWIFT</label>
                                          <div class="controls">
                      <input name="swift" class="input-xlarge focused" required  id="swift" type="text" value="<?php echo $categoryRowset['swift'];?>">
                                          </div>
                                        </div>
                                        
                                        <legend>Receiving Bank</legend>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Bank Name</label>
                                          <div class="controls">
                      <input name="rb_name" class="input-xlarge focused" required  id="rb_name" type="text" value="<?php echo $categoryRowset['rb_name'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Account</label>
                                          <div class="controls">
                      <input name="account" class="input-xlarge focused" required  id="account" type="text" value="<?php echo $categoryRowset['account'];?>">
                                          </div>
                                        </div>
                                        
                                        <legend>Beneficiary</legend>
                                        
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Name of Account</label>
                                          <div class="controls">
                      <input name="account_name" class="input-xlarge focused" required  id="account_name" type="text" value="<?php echo $categoryRowset['account_name'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Account Number</label>
                                          <div class="controls">
                      <input name="account_number" class="input-xlarge focused" required  id="account_number" type="text" value="<?php echo $categoryRowset['account_number'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">City</label>
                                          <div class="controls">
                      <input name="city" class="input-xlarge focused" required  id="city" type="text" value="<?php echo $categoryRowset['city'];?>">
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Country</label>
                                          <div class="controls">
                      <input name="country" class="input-xlarge focused" required  id="country" type="text" value="<?php echo $categoryRowset['country'];?>">
                                          </div>
                                        </div>
                                        



                                        
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary"  name="submit">Save changes</button>
                                          <button type="reset" class="btn" onClick="location.href='list_banks.php'">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                

                </div>
            </div>
            <hr>
             <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>
        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />
        
        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>


        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>