<?php
include_once('includes/session.php');
//include_once("includes/config.php");
//include_once("includes/functions.php");
session_start();
$_SESSION['user_name'];
?>


<!DOCTYPE html>
<html>

    <head>
        <title>Dashboard</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
            $(function () {
                // Easy pie charts
                $('.chart').easyPieChart({animate: 1000});
            });
        </script>

        <script type="text/javascript" src="js/chat.js"></script>

        <?php
        //$date = date('Y-m-d');
        $date = date('Y-m-d', strtotime("-30 days"));
        $recent_user = mysql_query("select * from `dateing_user` WHERE 1 order by `last_login` DESC");
        $recntuser = mysql_num_rows($recent_user);










        $totuser_sql = mysql_query("select * from `dateing_user` where id!='' ");
        $totuser = mysql_num_rows($totuser_sql);


        $defaulted_sql = mysql_query("select * from `dateing_user` where subscription_id='1'");
        $dfltuser = mysql_num_rows($defaulted_sql);

        $acvuser_sql = mysql_query("select * from `dateing_user` where status='1'");
        $acvuser = mysql_num_rows($acvuser_sql);

        $blkuser_sql = mysql_query("SELECT * FROM `dateing_user` AS `USER` INNER JOIN `dating_block_reason` AS `BLOCKED` ON USER.id = BLOCKED.user_id  where USER.status='0'");
        $blkeduser = mysql_num_rows($blkuser_sql);

        $muser_sql = mysql_query("select * from `dateing_user` where gender='M'");
        $muser = mysql_num_rows($muser_sql);

        $fuser_sql = mysql_query("select * from `dateing_user` where gender='F'");
        $fuser = mysql_num_rows($fuser_sql);

        $subs_sql = mysql_query("select * from `dateing_user` where subscription_id!='1'");
        $subsuser = mysql_num_rows($subs_sql);

        $ref_sql = mysql_query("select * from `dating_reference`  where ref_id<>''");
        $refuser = mysql_num_rows($ref_sql);

//            $active_users=(($acvuser*100)/$totuser);
//
//            $blkeduser=(($blkeduser*100)/$totuser);
//
//            $male_user=(($muser*100)/$totuser);
//
//            $female_user=(($fuser*100)/$totuser);
        ?>

    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <?php include_once('includes/header.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php include_once('includes/left_panel.php'); ?>
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">

                        <div class="navbar">
                            <div class="navbar-inner">
                                <ul class="breadcrumb">
                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                                    <li>
                                        <a href="#">Dashboard</a> <span class="divider">/</span>	
                                    </li>
                                    <li>
                                        <a href="settings.php">Settings</a> <span class="divider">/</span>	
                                    </li>
                                    <li class="active">Tools</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Defaulted Users</div>
                                    <div class="pull-right">
                                        <span class="badge badge-info"><?php echo mysql_num_rows($defaulted_sql); ?></span>
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($dfltuser > 0) {
                                                $c = 0;
                                                while ($tuser = mysql_fetch_array($defaulted_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $tuser['fname']; ?></td>
                                                        <td><?php echo $tuser['email']; ?></td>

                                                    </tr>
                                                <?php }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Blocked Users</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php
                                            echo mysql_num_rows($blkuser_sql);
                                            ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($acvuser > 0) {
                                                $c = 0;
                                                while ($auser = mysql_fetch_array($blkuser_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $auser['fname']; ?></td>
                                                        <td><?php echo $auser['email']; ?></td>

                                                    </tr>
                                                <?php }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Male Users </div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $muser; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>

                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($muser > 0) {
                                                $c = 0;
                                                while ($mluser = mysql_fetch_array($muser_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $mluser['fname']; ?></td>
                                                        <td><?php echo $mluser['email']; ?></td>

                                                    </tr>
    <?php }
} else {
    ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"> Female Users</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $fuser; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($fuser > 0) {
                                                $c = 0;
                                                while ($fmuser = mysql_fetch_array($fuser_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $fmuser['fname']; ?></td>
                                                        <td><?php echo $fmuser['email']; ?></td>

                                                    </tr>
    <?php }
} else {
    ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>


                    </div>


                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"> Subscribed Users</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $subsuser; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($subsuser > 0) {
                                                $c = 0;
                                                while ($fmuser = mysql_fetch_array($subs_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $fmuser['fname']; ?></td>
                                                        <td><?php echo $fmuser['email']; ?></td>

                                                    </tr>
    <?php }
} else {
    ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"> Referred Users</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo mysql_num_rows($ref_sql); ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Ref#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($refuser > 0) {
                                                $c = 0;
                                                while ($fmuser = mysql_fetch_array($ref_sql)) {
                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    $refferedusersql = mysql_query("SELECT * FROM `dateing_user` WHERE id='" . $fmuser['user_id'] . "'");

                                                    if (mysql_num_rows($refferedusersql) > 0) {
                                                        $reffereduser = mysql_fetch_array($refferedusersql);
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $c; ?></td>
                                                            <td><?php echo $reffereduser['fname']; ?></td>
                                                            <td><?php echo $reffereduser['email']; ?></td>
                                                            <td><?php echo $reffereduser['ref_id']; ?></td>

                                                        </tr>
        <?php
        }
    }
} else {
    ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>



                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"> Recent Users</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $recntuser; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($recntuser > 0) {
                                                $c = 0;
                                                while ($row = mysql_fetch_assoc($recent_user)) {

                                                    $c++;
                                                    if ($c == 5) {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td><?php echo $row['fname']; ?></td>
                                                        <td><?php echo $row['email']; ?></td>
                                                    </tr>
    <?php }
} else { ?>
                                                <tr>
                                                    <td colspan="3">
                                                        No records found
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>                    








                </div>
            </div>
            <hr>
<?php include_once('includes/footer.php'); ?>
        </div>
        <!--/.fluid-container-->


    </body>

</html>