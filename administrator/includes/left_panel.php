<?php
$pagename = @end(explode('/', $_SERVER['REQUEST_URI']));
/*
  $pagename_array=@end(explode('/',$_SERVER['REQUEST_URI']));
  end($pagename_array);
  $pagename_url = prev($pagename_array);
 */
?>

<div class="span3" id="sidebar">

    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        <li>
            <a href="javascript:history.back();"><i class="icon-chevron-right"></i> Go To Previous</a>
        </li> 
        <li <?php if ($pagename == 'dashboard.php') { ?>  class="active" <?php } ?>>
            <a href="dashboard.php"><i class="icon-chevron-right"></i> Dashboard</a>
        </li>  
        <!--<li <?php if ($pagename == 'bank_details.php') { ?>  class="active" <?php } ?>>
            <a href="bank_details.php"><i class="icon-chevron-right"></i> Bank Details</a>
        </li>-->
        <li <?php if ($pagename == 'list_banks.php') { ?>  class="active" <?php } ?>>
            <a href="list_banks.php"><i class="icon-chevron-right"></i> Bank Details</a>
        </li>
        <li <?php if ($pagename == 'list_user.php') { ?>  class="active" <?php } ?>>
            <a href="list_user.php"><i class="icon-chevron-right"></i>  Users</a>
        </li>
        <li <?php if ($pagename == 'list_subscribeuser.php') { ?>  class="active" <?php } ?> >
            <a href="list_subscribeuser.php"><i class="icon-chevron-right"></i>  Subscribed Users</a>
        </li>
        <li <?php if ($pagename == 'list_user_trans.php') { ?>  class="active" <?php } ?> >
            <a href="list_user_trans.php"><i class="icon-chevron-right"></i>  Assign Translation Package To Users</a>
        </li>
        <li <?php if ($pagename == 'add_sms_package.php') { ?>  class="active" <?php } ?>>
            <a href="add_sms_package.php"><i class="icon-chevron-right"></i> Add Translation Package</a>
        </li>

        <li <?php if ($pagename == 'list_sms_package.php') { ?>  class="active" <?php } ?>>                        
            <a href="list_sms_package.php"><i class="icon-chevron-right"></i>List Translation Package</a>
        </li>
        <li <?php if ($pagename == 'refference_user.php') { ?>  class="active" <?php } ?>>
            <a href="refference_user.php"><i class="icon-chevron-right"></i> Referred Users</a>
        </li>
        <li <?php if ($pagename == 'list_user_approval.php') { ?>  class="active" <?php } ?>>
            <a href="list_user_approval.php"><i class="icon-chevron-right"></i> Verified Users</a>
        </li>
        <li <?php if ($pagename == 'list_report.php') { ?>  class="active" <?php } ?>>
            <a href="list_report.php"><i class="icon-chevron-right"></i> User Report Management</a>
        </li>
        <li <?php if ($pagename == 'blocked_user.php') { ?>  class="active" <?php } ?>>
            <a href="blocked_user.php"><i class="icon-chevron-right"></i> Blocked Users</a>
        </li>
        <li <?php if ($pagename == 'query.php') { ?>  class="active" <?php } ?>>
            <a href="query.php"><i class="icon-chevron-right"></i> Users Query</a>
        </li>                        
        <li <?php if ($pagename == 'advertise.php') { ?>  class="active" <?php } ?>>
            <a href="advertise.php"><i class="icon-chevron-right"></i>Users Advertise</a>
        </li>
        <li <?php if ($pagename == 'homecms.php') { ?>  class="active" <?php } ?>>
            <a href="homecms.php"><i class="icon-chevron-right"></i> Manage Home Page</a>
        </li>
        <li <?php if ($pagename == 'homeimages.php') { ?>  class="active" <?php } ?>>
            <a href="homeimages.php"><i class="icon-chevron-right"></i> Manage Home Images</a>
        </li>
        <li <?php if ($pagename == 'cms.php') { ?>  class="active" <?php } ?>>
            <a href="cms.php"><i class="icon-chevron-right"></i> Manage CMS</a>
        </li>
        <li <?php if ($pagename == 'template.php') { ?>  class="active" <?php } ?>>
            <a href="template.php"><i class="icon-chevron-right"></i> Manage Email Template</a>
        </li>

        <li <?php if ($pagename == 'config_email_temp.php') { ?>  class="active" <?php } ?>>
            <a href="config_email_temp.php"><i class="icon-chevron-right"></i>Configure Email Templates</a>
        </li>

        <li <?php if ($pagename == 'compatibility_set.php') { ?>  class="active" <?php } ?>>
            <a href="compatibility_set.php"><i class="icon-chevron-right"></i>Compatibility</a>
        </li>                        
        <li <?php if ($pagename == 'send_news.php') { ?>  class="active" <?php } ?>>
            <a href="send_news.php"><i class="icon-chevron-right"></i>Send News</a>
        </li>
        <li <?php if ($pagename == 'smscontent.php?id=9') { ?>  class="active" <?php } ?>>
            <a href="smscontent.php?id=9"><i class="icon-chevron-right"></i>SMS Content</a>
        </li>
        <li <?php if ($pagename == 'sendsms.php') { ?>  class="active" <?php } ?>>
            <a href="sendsms.php"><i class="icon-chevron-right"></i>Send SMS</a>
        </li>
        <li <?php if ($pagename == 'add_limoadd.php') { ?>  class="active" <?php } ?>>
            <a href="add_limoadd.php"><i class="icon-chevron-right"></i> Add advertisement</a>
        </li>

        <li <?php if ($pagename == 'list_limoadd.php') { ?>  class="active" <?php } ?>>
            <a href="list_limoadd.php"><i class="icon-chevron-right"></i> List advertisement</a>
        </li>
        <li <?php if ($pagename == 'add_pages_list.php') { ?>  class="active" <?php } ?>>
            <a href="add_pages_list.php"><i class="icon-chevron-right"></i> List of add pages</a>
        </li>
        <li <?php if ($pagename == 'blogs.php') { ?>  class="active" <?php } ?>>
            <a href="blogs.php"><i class="icon-chevron-right"></i> Blogs</a>
        </li>
        <li <?php if ($pagename == 'list_story.php') { ?>  class="active" <?php } ?>>
            <a href="list_story.php"><i class="icon-chevron-right"></i> List Story</a>
        </li>
        <li <?php if ($pagename == 'change_quickpics.php') { ?>  class="active" <?php } ?>>
            <a href="change_quickpics.php"><i class="icon-chevron-right"></i>Manage quick pics</a>
        </li>
        <li <?php if ($pagename == 'social.php') { ?>  class="active" <?php } ?>>
            <a href="social.php"><i class="icon-chevron-right"></i> Manage Social Media</a>
        </li>
        <li <?php if ($pagename == 'social_sharing.php') { ?>  class="active" <?php } ?>>
            <a href="social_sharing.php"><i class="icon-chevron-right"></i> Manage Social Sharing</a>
        </li>
        <li <?php if ($pagename == 'subscription_package_management.php') { ?>  class="active" <?php } ?>>
            <a href="subscription_package_management.php"><i class="icon-chevron-right"></i> Subscription Package Management</a>
        </li>
        <li <?php if ($pagename == 'subscription_package_management_women.php') { ?>  class="active" <?php } ?>>
            <a href="subscription_package_management_women.php"><i class="icon-chevron-right"></i> Subscription Package Women</a>
        </li>
        
        <li <?php if ($pagename == 'default_payment.php') { ?>  class="active" <?php } ?>>
            <a href="default_payment.php"><i class="icon-chevron-right"></i>Defaulted payments</a>
        </li>

        <li <?php if ($pagename == 'es_language_managent.php') { ?>  class="active" <?php } ?>>
            <a href="es_language_managent.php"><i class="icon-chevron-right"></i>Language Management </a>
        </li>
        <li>
            <a href="logout.php">Logout</a>
        </li>

    </ul>

</div>
