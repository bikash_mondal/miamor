<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/notifybar/jquery.notifyBar.js"></script>
<link href="js/notifybar/notifyBar.css" rel="stylesheet" type="text/css" />
<script>
<?php if (!empty($_SESSION['NOTIFY_MESSAGE'])) {
    ?>
        showSuccess('<?php echo $_SESSION['NOTIFY_MESSAGE']; ?>');
    <?php
    unset($_SESSION['NOTIFY_MESSAGE']);
}
?>
    
<?php if (!empty($_SESSION['msg'])) {
    ?>
        showSuccess('<?php echo $_SESSION['msg']; ?>');
    <?php
    unset($_SESSION['msg']);
}
?>    
    
    
</script>
<footer>
    <p>&copy; Dating <?php echo date('Y'); ?></p>
</footer>