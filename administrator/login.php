<?php
ob_start();
session_start();
include_once("includes/config.php");

if(isset($_POST['submit']))
{
	$un=$_REQUEST['un'];
	$pass=$_REQUEST['pass'];
	$sql="SELECT * FROM `dateing_tbladmin` WHERE `admin_username`='".mysql_real_escape_string($un)."' and `admin_password`='".mysql_real_escape_string($pass)."'";
	//exit;
	$rs=mysql_query($sql) or die(mysql_error());
	if($row=mysql_fetch_object($rs))
	{
		
		$_SESSION['username']=$row->admin_username;
		$_SESSION['admin_id']=$row->id;
		$_SESSION['myy']=$row->id;
		$_SESSION['user_name']='administrator';
		header("location:dashboard.php");
	}
	else
	{
		$msg="Invalid Username or Password.";
	}
}

?><!DOCTYPE html>
<html>
  <head>
    <title>Admin Login</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<style>
#forgot_password_ul li { list-style-type:none;}
</style>
<script >
function send_forgot_password()
{
  var forgot_email=$('#forgot_email').val();
  if(forgot_email=='')
  {
    $('#email_err').html('<font color="red">Please enter your email.</font>');
  }
  else
  {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(forgot_email)) 
    {
            $('#email_err').html('Please wait...');
            $.post('ajax_forgot.php?email='+forgot_email, function(data){
                if(data=='success'){
	              $('#email_err').html('<font color="green" style="float:left;text-align:left">A new password has been sent to your mail. Please check your mail.</font>');
                } else if(data=='fail'){ 
	            $('#email_err').html('<font color="red">Email id not exist.</font>');
                }
             });
     }
     else
     {
        $('#email_err').html('<font color="red">Please enter valid email.</font>');
     }
  }
  
}

</script>
  </head>
  <body id="login">
    <div class="container">

      <form class="form-signin" action="login.php" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" required class="input-block-level" placeholder="Username" name="un">
        <input type="password" required class="input-block-level" placeholder="Password" name="pass">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
	<label class="checkbox">
		<a href="javascript:void(0)" onclick="$('#iforgot_password').toggle('slow');">Forgot Your password?</a>
	</label>
        <button class="btn btn-large btn-primary" type="submit" name="submit">Sign in</button>
      </form>

<div id="iforgot_password" style="display:none;">
<form class="form-signin" name="frm" method="POST" action="">

  <ul id="forgot_password_ul" >
							 <li class="left_form_text">Forgot Password</li>
							 <li>
							 <input type="email" name="login_email" class="left_form_text_box" placeholder="<?php echo EMAIL_ADDRESS ?>" id="forgot_email"/>
							 <span id="email_err" style="font-size:16px;float:left"></span>
							 </li>
							 <li>&nbsp;</li>
							 <li><input type="button" name="login" value="Submit" class="btn btn-large btn-primary" onclick="send_forgot_password()" style="padding:0px;"/></li>
							</ul>
   </form>                         
</div>


    </div> <!-- /container -->
    <script src="vendors/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
