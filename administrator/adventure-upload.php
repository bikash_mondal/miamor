<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
require('includes/XLSXReader.php');

if(isset($_REQUEST['submit']))
{

		$uid=$_REQUEST['uid'];
		$imageName = $_FILES['file']['name'];
		$full_image_path = 'uploaded_xml/'.strtotime(date('y-m-d h:i:s')).$imageName;
		move_uploaded_file($_FILES['file']['tmp_name'],$full_image_path);
		$csv_file = $full_image_path; 
		$xlsx = new XLSXReader($csv_file);
		$sheetNames = $xlsx->getSheetNames();
		foreach($sheetNames as $sheetName) {
		$sheet = $xlsx->getSheet($sheetName);
		$data[] = $sheet->getData();
		}
		
		
		
		if(!empty($data[0]))
		{
			foreach($data[0] as $k=> $inv_data)
			{
			  if($k!=0) 
			  {
				
				$user_id=$uid;
				$cat_id=$inv_data[0];
				$title=$inv_data[1];
				$description=$inv_data[2];
				$adv_date=date('Y-m-d',strtotime($inv_data[3]));
				$location=$inv_data[4];
				$state=$inv_data[5];
				$zip=$inv_data[6];
				$country=$inv_data[7];
				$abcd=get_lat_log($location,$zip);
				$latitude=$abcd['latitude'];
				$longitude=$abcd['longitude'];
				$price=$inv_data[8];
				$is_active=$inv_data[9];
				$vid_link=$inv_data[10];
				
				$addQuery = "INSERT INTO adventure_circle_adventure     
			(user_id,cat_id,title,description,adv_date,location,state,zip,country,latitude,longitude,price,is_active)
VALUES ('$user_id','$cat_id','$title','$description','$adv_date','$location','$state','$zip','$country','$latitude','$longitude','$price','$is_active')";
                  
			mysql_query($addQuery);
			
			$record=mysql_query("SELECT MAX(id) as MaximumID FROM `adventure_circle_adventure`");
			
			$max_id=mysql_fetch_row($record);
			
			$max_id=$max_id[0];
			
			$video_link=$vid_link;
			
			$date_added=date('y-m-d h:i:s');
			
			mysql_query("INSERT INTO `adventure_circle_adventurevideo` 
			
			(user_id,adv_id,title,description,video_link,date_added)  
			
			VALUES ('$user_id','$max_id','','','$video_link','$date_added')"); 
			
			}
			}
			
			header('Location:list_adventure.php');
			exit();
		
}

}
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Bulk Adventure Upload</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
         <?php include('includes/header.php');?>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Bulk Adventure Upload</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                    
                                     
                                      <fieldset>
                                        <legend>Adventure Upload</legend>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Select user</label>
                                          <div class="controls">
                                            <select name="uid" required>
                                            <option value="">Select User</option>
                                            <option value="0">ADMIN</option>
                                            <?php
                                             $sql="select * from dateing_user  where 1";
											 $record=mysql_query($sql);
											 while($row=mysql_fetch_object($record))
											 {
											?>
                                            <option value="<?=$row->id?>"><?php echo $row->fname;?> <?php echo $row->lname;?></option>
                                            <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Select Xlsx/xls File</label>
                                          <div class="controls">
                       <input type="file" name="file" id="xml_upload" required>
                                          </div>
                                        </div>
                                         
                                        
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary"  name="submit">Upload</button>
                                          <button type="reset" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                

                </div>
            </div>
            <hr>
             <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            
			
			$('#xml_upload').change(function() {
			var ext = $('#xml_upload').val().split(".").pop().toLowerCase();
			if($.inArray(ext, ["xlsx","xls"]) == -1) {
			alert('Invalid File Format');
			$('#xml_upload').val('');
			return false;			
			}
			else{
			return true;
			}
			});
			
			
			$(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>