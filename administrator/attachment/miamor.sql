-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 17, 2015 at 01:55 AM
-- Server version: 5.5.41-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `miamor`
--

-- --------------------------------------------------------

--
-- Table structure for table `dateing_add`
--

CREATE TABLE IF NOT EXISTS `dateing_add` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `dateing_add`
--

INSERT INTO `dateing_add` (`id`, `title`, `description`, `url`, `image`) VALUES
(6, 'add2', 'add2', NULL, 'printer70.png'),
(7, 'By Bik', 'Lorem Ipsum Doller Sit Amet', 'http://team3.nationalitsolution.co.in', 'wings_rightt.png'),
(8, 'Add With URL', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'adds1.jpg'),
(9, 'For Top', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'wpid-REAL-ESTATE-PHOTOGRAPHY.jpg'),
(10, 'For Bottom', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'Real-Estate1.jpg'),
(11, 'jane image', 'Test', 'http://team3.nationalitsolution.co.in/journal', '20150410_145647.jpg'),
(12, 'Test 1', 'Laura', '', 'Laura Archbold.jpg'),
(14, 'test 3', '', '', 'i_love_my_colombian_girlfriend_sweatshirt_dark.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_add_pages`
--

CREATE TABLE IF NOT EXISTS `dateing_add_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `pagename` varchar(255) DEFAULT NULL,
  `no_add` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dateing_add_pages`
--

INSERT INTO `dateing_add_pages` (`id`, `description`, `pagename`, `no_add`, `amount`) VALUES
(3, 'Profile Page', 'profile.php', 3, '1.00'),
(4, 'Search Page', 'search_results.php', 3, '1.00'),
(5, 'Edit Profile Page', 'edit_profile.php', 3, '1.00'),
(7, 'Interest Page', 'interest.php', 6, '2.00'),
(8, 'Gallery Page', 'my_gallery.php', 6, '5.00'),
(9, 'Whose online Page', 'whose_online.php', 6, '2.00'),
(10, 'Message Page', 'message.php', 6, '3.00'),
(11, 'Friend Page', 'my_friends.php', 6, '1.00'),
(12, 'Wink Page', 'wink.php', 6, '10.00'),
(13, 'Kiss Page', 'kiss.php', 6, '20.00'),
(14, 'Compatibility Page', 'compatibility_user.php', 6, '10.00'),
(15, 'Favourite Page', 'favourite_member.php', 6, '10.00');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_advertise`
--

CREATE TABLE IF NOT EXISTS `dateing_advertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `company` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dateing_advertise`
--

INSERT INTO `dateing_advertise` (`id`, `name`, `email`, `telephone`, `company`, `message`, `date`) VALUES
(1, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', '919876543210', 'NITS', 'NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS NITS ', '2015-06-18 06:50:53'),
(2, 'damon de berry', 'damon@optimumstudio.com', '123456', 'miamor', 'test', '2015-06-20 12:42:21'),
(3, 'fsdfsdf', 'dsfsdf@gmail.com', 'sdfsdf', 'sdfsdf', 'dsfsdf', '2015-06-22 08:09:54'),
(4, 'Abhijit Roy', 'nits.santanu@gmail.com', '919876543210', 'NITS', 'sfdsfdsf', '2015-06-22 09:58:12'),
(5, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', '919876543210', 'NITS', 'fsdfsdfsdfdsfdsfdsfsdfd', '2015-06-22 10:21:57'),
(6, 'damon de berry', 'damon@optimumstudio.com', '01234567', 'me', 'test', '2015-06-22 15:50:09'),
(7, 'damon de berry', 'damon@optimumstudio.com', '0', 'n', 'n', '2015-06-22 15:50:55'),
(8, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', '919876543210', 'sdfsdf', 'sddfgfdgfdgdf', '2015-06-24 12:25:50'),
(9, 'damon de berry', 'damon@optimumstudio.com', '123456', '1', 'test', '2015-06-24 13:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_assign_ads`
--

CREATE TABLE IF NOT EXISTS `dateing_assign_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_id` int(11) NOT NULL,
  `addpage_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0' COMMENT '0:Right, 1:Left, 2:Bottom, 3:Top ',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `dateing_assign_ads`
--

INSERT INTO `dateing_assign_ads` (`id`, `add_id`, `addpage_id`, `position`) VALUES
(38, 7, 3, 0),
(39, 14, 5, 1),
(26, 12, 5, 2),
(18, 11, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_banner`
--

CREATE TABLE IF NOT EXISTS `dateing_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(200) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `status_2` (`status`),
  KEY `status_3` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dateing_banner`
--

INSERT INTO `dateing_banner` (`id`, `user_id`, `name`, `description`, `image`, `link`, `status`) VALUES
(1, 0, 'Lorem ipsum dolor sit amet consectetur', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider1.jpg', 'http://www.google.com', 0),
(2, 0, 'Image 2', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider2.jpg', 'http://www.google.com', 0),
(3, 0, 'Image 3', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider3.jpg', 'http://www.google.com', 0),
(8, 0, '', '', '1059388609_banner.jpg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_billing`
--

CREATE TABLE IF NOT EXISTS `dateing_billing` (
  `billing_id` int(8) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `billing_fname` varchar(100) NOT NULL,
  `billing_lname` varchar(100) NOT NULL,
  `billing_add` varchar(100) NOT NULL,
  `billing_city` varchar(100) NOT NULL,
  `billing_zip` int(8) NOT NULL,
  `billing_state` varchar(256) NOT NULL,
  `billing_ephone` bigint(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `shiping_fname` varchar(100) NOT NULL,
  `shiping_lname` varchar(100) NOT NULL,
  `shiping_add` varchar(100) NOT NULL,
  `shiping_city` varchar(50) NOT NULL,
  `shiping_zip` int(8) NOT NULL,
  `shiping_state` varchar(256) NOT NULL,
  `shiping_ephone` bigint(20) NOT NULL,
  `email1` varchar(256) NOT NULL,
  PRIMARY KEY (`billing_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `dateing_billing`
--

INSERT INTO `dateing_billing` (`billing_id`, `orderid`, `billing_fname`, `billing_lname`, `billing_add`, `billing_city`, `billing_zip`, `billing_state`, `billing_ephone`, `email`, `shiping_fname`, `shiping_lname`, `shiping_add`, `shiping_city`, `shiping_zip`, `shiping_state`, `shiping_ephone`, `email1`) VALUES
(11, '6', 'Avik', 'Ghosh', 'kolkata', 'kolkata', 123123, 'HI', 12312321, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'kolkata', 'kolkata', 123123, 'HI', 0, ''),
(12, '', 'Avik', 'Ghosh', 'Kolkata', 'kolkata', 700017, 'AE', 1234567890, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'Kolkata', 'kolkata', 700017, 'AE', 0, ''),
(13, '', 'aaa', 'aaa', 'aaa', 'aaaa', 700111, 'AA', 1111111111, 'aa@gmail.com', 'aaaa', 'aaaa', 'aaaa', 'aaaaa', 7001111, 'ID', 0, ''),
(14, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1231231231, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(15, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(16, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(17, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(18, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(19, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(20, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(21, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(22, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(23, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(24, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(25, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(26, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(27, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(28, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(29, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(30, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(31, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(32, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(33, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(34, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(35, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_blogs`
--

CREATE TABLE IF NOT EXISTS `dateing_blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `titletranslated` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text NOT NULL,
  `description_translated` text CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dateing_blogs`
--

INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(1, 'The Sexiest Nationalities', 'Las nacionalidades más sexys', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">While traditional courting rituals and dating customs have been evolving over the years, re-entering the dating arena still seems somewhat bland, and disheartening. It&rsquo;s not fun to embark on a journey to couple-hood when the pickings are slim, especially if you are truly limited to a small population of single people in your corner of the world. MiAmor.com.co presents a variety of new options to expand your, er, dating palette.</span></p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">When you limit yourse<span class="text_exposed_show">lf to dating domestically, depending on where you live, that could mean you are only dated like-minded, similar looking people. But when you realize that there are a whole world of single people out there, the possibilities become endless and exciting. Recently we decided to conduct a poll amongst our American members to discover which nationalities top your list of the sexiest nationalities on the planet. </span></span></p>\r\n<div class="text_exposed_show">\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">The Top Ten Sexiest Nationalities for Women:</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian Famous Face: Shakira<br />\r\n		Brazilian Famous Face: Camila Alves<br />\r\n		American Famous Face: Jennifer Lawrence<br />\r\n		Spanish Famous Face: Penelope Cruz<br />\r\n		Russian Famous Face: Anne Vyalitsyna<br />\r\n		Dutch Famous Face: Famke Janssen<br />\r\n		French Famous Face: Eva Green<br />\r\n		Bulgarian Famous Face: Nina Dobrev<br />\r\n		Swedish Famous Face: Malin Akerman<br />\r\n		Italian Famous Face: Carla Bruni</span></p>\r\n	<p>\r\n		<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Shakira-shakira-37240_1024_768-659x350.jpg" style="width: 1100px; height: 584px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span title="While traditional courting rituals and dating customs have been evolving over the years, re-entering the dating arena still seems somewhat bland, and disheartening.">Mientras rituales de cortejo y costumbres tradicionales de citas han ido evolucionando con los a&ntilde;os, volver a entrar en el escenario de citas todav&iacute;a parece un poco soso, y desalentador. </span><span title="It''s not fun to embark on a journey to couple-hood when the pickings are slim, especially if you are truly limited to a small population of single people in your corner of the world.">No es divertido para embarcarse en un viaje para pareja de campana cuando las ganancias son escasas, especialmente si usted est&aacute; realmente limitado a una peque&ntilde;a poblaci&oacute;n de personas solteras en su rinc&oacute;n del mundo. </span>MiAmor.com.co <span title="Dating abroad presents a variety of new options to expand your, er, dating palette.\r\n\r\n">presenta una variedad de nuevas opciones para ampliar su paleta&nbsp; y&nbsp;&nbsp; data.</span><br />\r\n	<br />\r\n	<span title="When you limit yourself to dating domestically, depending on where you live, that could mean you are only dated like-minded, similar looking people.">Cuando usted se limita a salir con el pa&iacute;s, dependiendo de donde usted vive, eso podr&iacute;a significar que s&oacute;lo son anticuadas ideas afines, personas de aspecto similar. </span><span title="But when you realize that there are a whole world of single people out there, the possibilities become endless and exciting.">Pero cuando te das cuenta de que hay todo un mundo de personas solteras por ah&iacute;, las posibilidades se vuelven infinitas y emocionante. </span><span title="Recently we decided to conduct a poll amongst our American members to discover which nationalities top your list of the sexiest nationalities on the planet.\r\n\r\n">Recientemente decidimos realizar una encuesta entre nuestros miembros americanos para descubrir qu&eacute; nacionalidades arriba su lista de las nacionalidades m&aacute;s sexy del planeta.</span><br />\r\n	<br />\r\n	&nbsp; <span title="The Top Ten Sexiest Nationalities for Women:\r\n\r\n">las m&aacute;s sexy Nacionalidades Top 10&nbsp;&nbsp; Mujer:</span><br />\r\n	<br />\r\n	<span title="Colombian Famous Face: Shakira\r\n">Cara famoso colombiano: Shakira</span><br />\r\n	<span title="Brazilian Famous Face: Camila Alves\r\n">Cara famoso brasile&ntilde;o: Camila Alves</span><br />\r\n	<span title="American Famous Face: Jennifer Lawrence\r\n">Americana cara famosa: Jennifer Lawrence</span><br />\r\n	<span title="Spanish Famous Face: Penelope Cruz\r\n">Espa&ntilde;ol Rostro Famoso: Pen&eacute;lope Cruz</span><br />\r\n	<span title="Russian Famous Face: Anne Vyalitsyna\r\n">Rusia cara famosa: Anne Vyalitsyna</span><br />\r\n	<span title="Dutch Famous Face: Famke Janssen\r\n">Holand&eacute;s cara famosa: Famke Janssen</span><br />\r\n	<span title="French Famous Face: Eva Green\r\n">Franc&eacute;s Cara famoso: Eva Green</span><br />\r\n	<span title="Bulgarian Famous Face: Nina Dobrev\r\n">B&uacute;lgaro cara famosa: Nina Dobrev</span><br />\r\n	<span title="Swedish Famous Face: Malin Akerman\r\n">Sueco cara famosa: Malin Akerman</span><br />\r\n	<span title="Italian Famous Face: Carla Bruni">Italiano cara famosa: Carla Bruni</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Shakira-shakira-37240_1024_768-659x350(1).jpg" style="width: 1100px; height: 584px;" /></p>\r\n', 1, '2015-06-29 21:44:44'),
(2, 'Discover Colombia', 'Descubra Colombia', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Let&#39;s face it, you&#39;ve done the Caribbean, you&#39;ve been through Europe, the states are boring and you need something new. Flying to the far East sounds great but is way too expensive. You&#39;re looking for that sense of adventure that you can&#39;t find on a cruise ship or another boring mega-resort and you don&#39;t want to spend thousands of dollars and fly halfway across the world.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">You want a place close by with the culture of Europe, the adventure of Costa Rica, the party atmosphere of Greece, the beauty of Alaska, the beaches of the Caribbean, lots of activities, and the friendliest, happiest, best looking people in South America.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">The answer is Colombia. This rediscovered gem is right in our backyard and is quickly becoming one of the fastest growing destinations for young tourists, eco-lovers, and foreign culture enthusiasts.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>Here are ten reasons why you need to get down to Colombia now.</strong></span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>1.</strong> <strong>It&#39;s not as dangerous as you think.</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">It&#39;s amazing at how much the negative/dangerous stereotype about colombia exists throughout the world. When I first went to Colombia is 2002, my friends and family couldn&#39;t believe it. They thought I was crazy for going into a militarized danger zone and I was definitely going to get kidnapped, shot, robbed, or sold into the drug trade.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Fortunately for me, that wasn&#39;t the case. Although, Colombia has had a shaky past, things are different now. Kidnappings are down by half, murder and crime rates have dropped significantly, and there has been a huge decrease in violence. The dramatic change can be credited to former President Alvaro Uribe and very motivated officials who have gone through great measures to fight crime and make Colombia a safe place for tourists.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Now American and foreign tourists can be seen everywhere in Colombia and locals are happy to finally have a blossoming tourism industry.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>2. The People &amp; Hospitality</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombia has a unique mix of ethnic cultures, ranging from indigenous African roots to Spanish settlers. This combination creates some of the nicest and best looking people on the planet. Locals are friendly and usually very accepting of foreign tourists. Just a short time ago, Colombia was shunned by the tourism world for being too dangerous. Nowadays, the people of Colombia compensate for the shaky past by creating a positive, friendly, and safe environment for tourists. Colombians foster their new and growing tourism industry and you can be sure to meet some very interesting and friendly people. The Women will be quick to take you out on the dancefloor for a salsa session and the Men will be quick to quench your thirst with a shot of aguardiente.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>3. The Beaches</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Tayrona National Park BeachVirgin Beach in Tayrona National Park<br />\r\n		Colombia has over 300 beaches along its caribbean and pacific coasts. There are beaches for all types of beach-goers. You have your relaxed resort-style beaches on the caribbean coast in and around Cartagena. You also have untouched, tropical paradise on the island of San Andres and Providencia.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Are you more of the adventure beacher? Try Tayrona National Park. This eco-lovers dream has untouched beaches surrounded by rainforests, hundreds of species of rare animals, and lush tropical landscapes. Be sure to stay in an Eco-Cabana (ecohab), a small cabin high up, with spectacular views of the ocean. Like surfing? Try Nuqui on the Western pacific side.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>4.</strong> <strong>The Scenery</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Mountain ChurchLas Lajas Sanctuary Nestled in the Mountains<br />\r\n		Having Pacific and Caribbean coastlines, lush rainforests, and rolling mountains really create a tremendous backdrop for a vacation. If you are into sightseeing, hiking, eco-tourism, rare species of wild animals, and untouched paradises, Colombia has it all. The beaches on the Northern Caribbean coast boast white sandy beaches and island paradises. The beaches on the Western Pacific side are a little more geared towards the adventure traveller with lush green forests descending into the virgin beaches with exciting campgrounds and surf spots.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">The Andes mountains bring about some of the highest points in Colombia and some of the nicest scenery as well. Snow capped mountains line the horizons, while active volcano&#39;s create some breathtaking views and pictures.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">The cities have their own distinct architecture which is not to be missed. The Walled City of Cartagena is some of the most original and colorful architecture on the planet and a walking tour is a must. Each city has it&#39;s own unique flair and the country as a whole is a great place of scenery watching, and people watching.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>5.</strong> <strong>The Coffee</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian CoffeeColombian Coffee Beans<br />\r\n		Visiting a coffee plantation in Colombia is like visiting a winery in Napa Valley, only without the hangover.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombia has perfect weather conditions that allow it to grow the best coffee in the world. The coffee region or &quot;Eje Cafetero&quot; is south of medellin, has a pleasant climate and here you will experience lush green landscapes, and rolling mountains and the best coffee in the world. Take a few days and experience life as a coffee farmer in Colombia. Learn how this very popular product is produced, take tours of the facilities, and mingle with some local farmers.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Coffee tourism has become a very popular attraction in Colombia. Don&#39;t miss the Parque Nacional del Cafe, which is an amusement park based on coffee. The kids will love it. Just don&#39;t give them too much coffee.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>6.</strong> <strong>The Festivals</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Carnaval de BarranquillaCarnaval in Barranquilla<br />\r\n		Attending a festival in Colombia is an absolute necessity if you want to experience the people and the culture. The Carnaval de Barranquilla leads the pack as the best one in the country and is only rivaled by the festival in Rio de Janeiro as the best festival in the world. Also experience the Carnaval de Bogota and the Carnival of Blacks and Whites in Cali.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">During these festivals, the entire city shuts down for a few days and you see all races, colors, classes, and ages come together in an extravagant celebration with drinking, dancing, flamboyant and colorful parades, music, energy, and great times. If you are lucky enough to be in Colombia during one of these festivals, I highly recommend being a part of one of these cultural events.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>7. The Food</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Bandeja PaisaBandeja Paisa - Delicious<br />\r\n		Colombia has some rare and delicious cuisine that you can&#39;t find anywehere else in the world. The arepas, corn patties filled with meats and chesses are a must everywhere you go and are as common as french fries.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Ajiaco and sancocho are traditional soup dishes made from meats and vegetables which change depending on the region you are in. Being a meat lover, my personal favorite is the Bandeja Paisa. This hearty dish is not for vegatarians as it contains different meats such as beef, chicken, pork, sausage, fried eggs, rice, beans, and plantains. This dish varies depending on where you are in Colombia depending on the local specialty. If you are adventurous and find yourself in the Santander region, try a plate of Hormigas Culonas, &quot;Large-Bottomed Ants&quot; which are salty and crunchy just like a peanut.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombians take pride in their food and you will find that dishes are always fresh with limited preservatives or over-the-top dressings or seasonings typically found in American foods. Rare, exotic fruits are in abundance all over the country and typically complement well-cooked meals and desserts. You will find that meals do not bloat you or make you tired, and always leave you wanting more.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>8.</strong> <strong>The Nightlife</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombia is known for its extravagant nightlife, bars, clubs, and restaurants. The entertainment is top-notch, the people are friendly, and the Women are amazingly beautiful. The Colombian people have a natural flair for dancing and partying, and they do it well. Cali is known as the Nightlife capital of Colombia and it is easy to see why. Beautiful people are accompanied by a wide range of reataurants, live music, drinking, dancing, and good vibes overall.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">If you find yourself in Bogot&aacute;, be sure to check out Andr&eacute;s Carne de Res. One of my personal favorite restaurants/nightclubs.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>9. The Women</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">A Colombian Woman, A Group of Pretty Colombian Girls<br />\r\n		If you are looking to experience true exotic beauty, derived from the eccentric mix of African and Spanish cultures, the Colombian people are the ones. On the other hand, If you are a single guy, looking to mingle with some of the most beautiful women in the world, Colombia is definitely where you want to be.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian people, Women especially take great pride in their appearance. Women are very well kept and are never shy when flaunting their beauty. They learn early on how to dance and mingle and what it takes to be stunningly gorgeous. It is no coincidence that there are beautiful women all over the place.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian women tend to be very polite and approachable. They enjoy drinking, dancing, talking, meeting new people, and are typically great companions on a night out. A Colombian woman, although always polite, will let you know if she is not interested in your advances, so be polite yourself, and make every effort to be genuine and friendly. Oh, and learn some Spanish Gringo! It is easy to meet and discover lots of beautiful Colombian women on MiAmor, so before you head to Colombia make the connections here.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><strong>10. Location</strong></span></p>\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">People fly all over the world to see and experience new things. You can get history and culture in Italy, beaches and resorts in Hawaii, Eco tourism and outdoor activities in Costa Rica, and friendly outgoing cultures in Greece.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Why not go to a place that has it all, AND is right in your backyard. Colombia is very close to the United States and Canada and is a new and exciting opportunity for any traveler. Flights to Colombia are generally inexpensive and are only a couple hours from most North American International airports. Prices and accommodations are usually cheaper than their foreign counterparts, and there are always friendly people to show you around.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Study in Colombia_0.jpg" style="width: 1100px; height: 735px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span title="Let''s face it, you''ve done the Caribbean, you''ve been through Europe, the states are boring and you need something new.">Seamos realistas, que ha hecho el Caribe, que ha pasado por Europa, los estados son aburridos y&nbsp; necesitan algo nuevo. </span><span title="Flying to the far East sounds great but is way too expensive.">Volando hacia el lejano Oriente suena muy bien, pero es demasiado caro. </span><span title="You''re looking for that sense of adventure that you can''t find on a cruise ship or another boring mega-resort and you don''t want to spend thousands of dollars and fly halfway across the world.\r\n\r\n">Usted est&aacute; en busca de ese sentido de la aventura que no se puede encontrar en un crucero o en otro aburrido mega-resort y no quieren gastar miles de d&oacute;lares y volar al otro lado del mundo.</span><br />\r\n	<br />\r\n	<span title="You want a place close by with the culture of Europe, the adventure of Costa Rica, the party atmosphere of Greece, the beauty of Alaska, the beaches of the Caribbean, lots of activities, and the friendliest, happiest, best looking people in South">&iquest;Quieres un lugar cercano con la cultura de Europa, la aventura de Costa Rica, el ambiente de fiesta de Grecia, la belleza de Alaska, las playas del Caribe, un mont&oacute;n de actividades, y los m&aacute;s felices, mejor, buscan gente m&aacute;s amable del Sur </span><span title="America.\r\n\r\n">Am&eacute;rica.</span><br />\r\n	<br />\r\n	<span title="The answer is Colombia.">La respuesta es Colombia. </span><span title="This rediscovered gem is right in our backyard and is quickly becoming one of the fastest growing destinations for young tourists, eco-lovers, and foreign culture enthusiasts.\r\n\r\n">Esta joya redescubierta es justo en nuestro patio trasero y se est&aacute; convirtiendo r&aacute;pidamente en uno de los destinos de m&aacute;s r&aacute;pido crecimiento para los turistas j&oacute;venes, eco-amantes y entusiastas de la cultura extranjera.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><strong><span title="Here are ten reasons why you need to get down to Colombia now.\r\n\r\n">Aqu&iacute; hay diez razones por las que necesitas ir a Colombia ahora.</span></strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="1. It''s not as dangerous as you think.\r\n"><strong>1.</strong> <strong>No es tan peligroso como parece.</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="It''s amazing at how much the negative/dangerous stereotype about colombia exists throughout the world.">Es incre&iacute;ble lo mucho que existe el estereotipo negativo / peligroso de Colombia en todo el mundo. </span><span title="When I first went to Colombia is 2002, my friends and family couldn''t believe it.">Cuando fui por primera vez a Colombia es 2002, mis amigos y mi familia no se lo pod&iacute;an creer. </span><span title="They thought I was crazy for going into a militarized danger zone and I was definitely going to get kidnapped, shot, robbed, or sold into the drug trade.\r\n\r\n">Pensaron que estaba loco por entrar en una zona de peligro militarizada y yo&nbsp; sin duda iba ser secuestrado, robado o vendido en el comercio de la droga.</span><br />\r\n	<br />\r\n	<span title="Fortunately for me, that wasn''t the case.">Afortunadamente para m&iacute;, no fue el caso. </span><span title="Although, Colombia has had a shaky past, things are different now.">Aunque, Colombia ha tenido un pasado inestable, las cosas son diferentes ahora. </span><span title="Kidnappings are down by half, murder and crime rates have dropped significantly, and there has been a huge decrease in violence.">Los secuestros han disminuido en las tasas de medio, de asesinato y el crimen han bajado considerablemente, y ha habido una gran disminuci&oacute;n de la violencia. </span><span title="The dramatic change can be credited to former President Alvaro Uribe and very motivated officials who have gone through great measures to fight crime and make Colombia a safe place for tourists.\r\n\r\n">El cambio dram&aacute;tico puede ser acreditado con el ex presidente &Aacute;lvaro Uribe y funcionarios que han pasado por grandes medidas para combatir la delincuencia y hacer de Colombia un lugar seguro para los turistas muy motivada.</span><br />\r\n	<br />\r\n	<span title="Now American and foreign tourists can be seen everywhere in Colombia and locals are happy to finally have a blossoming tourism industry.\r\n\r\n">Ahora los turistas estadounidenses y extranjeros pueden verse por todas partes en Colombia y los lugare&ntilde;os est&aacute;n contentos de finalmente tener una industria tur&iacute;stica floreciente.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="2. The People &amp; Hospitality\r\n"><strong>2. La Gente &amp; Hospitalidad</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Colombia has a unique mix of ethnic cultures, ranging from indigenous African roots to Spanish settlers.">Colombia tiene una mezcla &uacute;nica de culturas &eacute;tnicas, que van desde las ra&iacute;ces africanas ind&iacute;genas a los colonos espa&ntilde;oles. </span><span title="This combination creates some of the nicest and best looking people on the planet.">Esta combinaci&oacute;n crea algunas de las personas m&aacute;s bonitas y mejor aspecto en el planeta. </span><span title="Locals are friendly and usually very accepting of foreign tourists.">La gente es amable y normalmente muy bien acogidas por los turistas extranjeros. </span><span title="Just a short time ago, Colombia was shunned by the tourism world for being too dangerous.">Hace poco tiempo, Colombia fue rechazado por el mundo del turismo por ser demasiado peligroso. </span><span title="Nowadays, the people of Colombia compensate for the shaky past by creating a positive, friendly, and safe environment for tourists.">Hoy en d&iacute;a, la gente de Colombia compensan el pasado inestable mediante la creaci&oacute;n de un ambiente positivo, amable y seguro para los turistas. </span><span title="Colombians foster their new and growing tourism industry and you can be sure to meet some very interesting and friendly people.">Colombianos fomentan su nueva y creciente industria del turismo y usted puede estar seguro de conocer a algunas personas muy interesantes y amigables. </span><span title="The Women will be quick to take you out on the dancefloor for a salsa session and the Men will be quick to quench your thirst with a shot of aguardiente.\r\n\r\n">Las mujeres se apresuran para llevarlo a cabo en la pista para una sesi&oacute;n de salsa y los hombres ser&aacute;n r&aacute;pida para saciar su sed con un trago de aguardiente.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="3. The Beaches\r\n"><strong>3.</strong> <strong>Las Playas</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Tayrona National Park BeachVirgin Beach in Tayrona National Park\r\n">Parque Tayrona Playa Virgen en el Parque Nacional Tayrona</span><br />\r\n	<span title="Colombia has over 300 beaches along its caribbean and pacific coasts.">Colombia cuenta con m&aacute;s de 300 playas a lo largo de sus costas Caribe y el Pac&iacute;fico. </span><span title="There are beaches for all types of beach-goers.">Hay playas para todos los tipos de visitantes. </span><span title="You have your relaxed resort-style beaches on the caribbean coast in and around Cartagena.">Usted tiene sus playas estilo relajado, resort en la costa caribe en los alrededores de Cartagena. </span><span title="You also have untouched, tropical paradise on the island of San Andres and Providencia.\r\n\r\n">Usted tambi&eacute;n tiene intacto para&iacute;so tropical en la isla de San Andr&eacute;s y Providencia.</span><br />\r\n	<br />\r\n	<span title="Are you more of the adventure beacher?">&iquest;Es usted m&aacute;s de la beacher aventura? </span><span title="Try Tayrona National Park.">Pruebe Parque Nacional Tayrona. </span><span title="This eco-lovers dream has untouched beaches surrounded by rainforests, hundreds of species of rare animals, and lush tropical landscapes.">Este sue&ntilde;o eco-amantes tiene playas v&iacute;rgenes rodeadas de selvas tropicales, cientos de especies de animales raros, y exuberantes paisajes tropicales. </span><span title="Be sure to stay in an Eco-Cabana (ecohab), a small cabin high up, with spectacular views of the ocean.">Aseg&uacute;rese de mantenerse en un Eco-Cabana (ecohab), una peque&ntilde;a caba&ntilde;a en lo alto, con espectaculares vistas del oc&eacute;ano. </span><span title="Like surfing?">Al igual que el surf? </span><span title="Try Nuqui on the Western pacific side.\r\n\r\n">Trate Nuqui en el lado del Pac&iacute;fico Occidental.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="4. The Scenery\r\n"><strong>4.</strong> <strong>El Paisaje</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Mountain ChurchLas Lajas Sanctuary Nestled in the Mountains\r\n">Las Lajas Santuario enclavado en las monta&ntilde;as</span><br />\r\n	<span title="Having Pacific and Caribbean coastlines, lush rainforests, and rolling mountains really create a tremendous backdrop for a vacation.">Tener las costas del Pac&iacute;fico y del Caribe, exuberantes selvas y monta&ntilde;as ondulantes realmente crean un enorme tel&oacute;n de fondo para unas vacaciones. </span><span title="If you are into sightseeing, hiking, eco-tourism, rare species of wild animals, and untouched paradises, Colombia has it all.">Si usted est&aacute; en hacer turismo, senderismo, ecoturismo, especies raras de animales salvajes, y para&iacute;sos v&iacute;rgenes, Colombia tiene todo. </span><span title="The beaches on the Northern Caribbean coast boast white sandy beaches and island paradises.">Las playas de la costa norte del Caribe cuentan con playas de arena blanca y para&iacute;sos de la isla. </span><span title="The beaches on the Western Pacific side are a little more geared towards the adventure traveller with lush green forests descending into the virgin beaches with exciting campgrounds and surf spots.\r\n\r\n">Las playas en el lado del Pac&iacute;fico Occidental son un poco m&aacute;s orientado hacia el viajero de aventura con exuberantes bosques verdes que descienden hacia las playas v&iacute;rgenes con campamentos emocionantes y lugares para practicar surf.</span><br />\r\n	<br />\r\n	<span title="The Andes mountains bring about some of the highest points in Colombia and some of the nicest scenery as well.">Las monta&ntilde;as de los Andes lograr algunos de los puntos m&aacute;s altos de Colombia y algunos de los paisajes m&aacute;s bonitos tambi&eacute;n. </span><span title="Snow capped mountains line the horizons, while active volcano''s create some breathtaking views and pictures.\r\n\r\n">Cumbres nevadas alinean los horizontes, mientras que el volc&aacute;n&nbsp; activos Crear unas vistas impresionantes y fotos.</span><br />\r\n	<br />\r\n	<span title="The cities have their own distinct architecture which is not to be missed.">Las ciudades tienen su propia arquitectura distinta que no se puede perder. </span><span title="The Walled City of Cartagena is some of the most original and colorful architecture on the planet and a walking tour is a must.">La ciudad amurallada de Cartagena es algo de la arquitectura m&aacute;s original y colorido en el planeta y un recorrido a pie es una necesidad. </span><span title="Each city has it''s own unique flair and the country as a whole is a great place of scenery watching, and people watching.\r\n\r\n">Cada ciudad tiene su propio estilo &uacute;nico y el pa&iacute;s en su conjunto es un gran lugar de observaci&oacute;n de paisajes, y observar a la gente.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="5. The Coffee\r\n"><strong>5.</strong> <strong>El Caf&eacute;</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Colombian CoffeeColombian Coffee Beans\r\n">Caf&eacute; de Colombia</span><br />\r\n	<span title="Visiting a coffee plantation in Colombia is like visiting a winery in Napa Valley, only without the hangover.\r\n\r\n">Visitar una plantaci&oacute;n de caf&eacute; en Colombia es como visitar una bodega en Napa Valley, s&oacute;lo que sin la resaca.</span><br />\r\n	<br />\r\n	<span title="Colombia has perfect weather conditions that allow it to grow the best coffee in the world.">Colombia tiene condiciones clim&aacute;ticas perfectas que permiten que crezca el mejor caf&eacute; del mundo. </span><span a="" and="" best="" climate="" coffee="" eje="" experience="" green="" has="" here="" in="" is="" lush="" mountains="" of="" pleasant="" rolling="" south="" the="" title="The coffee region or " will="" you="">La regi&oacute;n cafetera o &quot;Eje Cafetero&quot; es al sur de Medell&iacute;n, cuenta con un clima agradable y aqu&iacute; usted experimentar&aacute; exuberantes paisajes verdes y monta&ntilde;as y el mejor caf&eacute; del mundo rodando. </span><span title="Take a few days and experience life as a coffee farmer in Colombia.">T&oacute;mese unos d&iacute;as y la experiencia de la vida como un productor de caf&eacute; en Colombia. </span><span title="Learn how this very popular product is produced, take tours of the facilities, and mingle with some local farmers.\r\n\r\n">Aprenda c&oacute;mo se produce este producto muy popular, tomar tours de las instalaciones, y se mezclan con algunos agricultores locales.</span><br />\r\n	<br />\r\n	<span title="Coffee tourism has become a very popular attraction in Colombia.">Turismo caf&eacute; se ha convertido en una atracci&oacute;n muy popular en Colombia. </span><span title="Don''t miss the Parque Nacional del Cafe, which is an amusement park based on coffee.">No te pierdas el Parque Nacional del Caf&eacute;, que es un parque de atracciones a base de caf&eacute;. </span><span title="The kids will love it.">A los ni&ntilde;os les encantar&aacute;. </span><span title="Just don''t give them too much coffee.\r\n\r\n">Simplemente no les damos demasiado caf&eacute;.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="6. The Festivals\r\n"><strong>6. Las Fiestas</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Carnaval de BarranquillaCarnaval in Barranquilla\r\n">Carnaval de Barranquilla</span><br />\r\n	<span title="Attending a festival in Colombia is an absolute necessity if you want to experience the people and the culture.">Asistir a un festival en Colombia es una necesidad absoluta si quieres experimentar la gente y la cultura. </span><span title="The Carnaval de Barranquilla leads the pack as the best one in the country and is only rivaled by the festival in Rio de Janeiro as the best festival in the world.">El Carnaval de Barranquilla lidera el grupo como uno de los mejores en el pa&iacute;s y s&oacute;lo se ve igualado por el festival en R&iacute;o de Janeiro como el mejor festival del mundo. </span><span title="Also experience the Carnaval de Bogota and the Carnival of Blacks and Whites in Cali.\r\n\r\n">Tambi&eacute;n experimentar el Carnaval de Bogot&aacute; y el Carnaval de Negros y Blancos en Pasto.</span><br />\r\n	<br />\r\n	<span title="During these festivals, the entire city shuts down for a few days and you see all races, colors, classes, and ages come together in an extravagant celebration with drinking, dancing, flamboyant and colorful parades, music, energy, and great times.">Durante estas fiestas, la ciudad entera se apaga durante unos d&iacute;as y ver todas las razas, colores, clases y edades se dan cita en una celebraci&oacute;n extravagante con la bebida, el baile, extravagante y coloridos desfiles, m&uacute;sica, energ&iacute;a y grandes momentos. </span><span title="If you are lucky enough to be in Colombia during one of these festivals, I highly recommend being a part of one of these cultural events.\r\n\r\n">Si tienes la suerte de estar en Colombia durante una de estas fiestas, le recomiendo ser parte de uno de estos eventos culturales.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="7. The Food\r\n"><strong>7</strong>.<strong> La Comida</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Bandeja PaisaBandeja Paisa - Delicious\r\n">La Bandeja Paisa - Delicioso</span><br />\r\n	<span title="Colombia has some rare and delicious cuisine that you can''t find anywehere else in the world.">Colombia tiene alguna rara y deliciosa cocina que no se puede encontrar en ninguna otra parte del mundo. </span><span title="The arepas, corn patties filled with meats and chesses are a must everywhere you go and are as common as french fries.\r\n\r\n">Las arepas, empanadas de ma&iacute;z rellenas de carnes y pandebono o almojabana con quesos son una necesidad donde quiera que vaya y son tan comunes como las papas fritas.</span><br />\r\n	<br />\r\n	<span title="Ajiaco and sancocho are traditional soup dishes made from meats and vegetables which change depending on the region you are in. Being a meat lover, my personal favorite is the Bandeja Paisa.">Ajiaco y sancocho son platos tradicionales sopas hechas con carnes y vegetales que cambian dependiendo de la regi&oacute;n donde se encuentra. Siendo un amante de la carne, mi favorita es la bandeja paisa. </span><span title="This hearty dish is not for vegatarians as it contains different meats such as beef, chicken, pork, sausage, fried eggs, rice, beans, and plantains.">Este suculento plato no es para vegatarians ya que contiene diferentes carnes como carne de res, pollo, cerdo, salchichas, huevos fritos, arroz, frijoles y pl&aacute;tanos. </span><span title="This dish varies depending on where you are in Colombia depending on the local specialty.">Este plato var&iacute;a dependiendo de d&oacute;nde se encuentre en Colombia seg&uacute;n la especialidad local. </span><span a="" and="" are="" crunchy="" just="" large-bottomed="" like="" peanut.="" salty="" title="If you are adventurous and find yourself in the Santander region, try a plate of Hormigas Culonas, " which="">Si le gusta la aventura y se encuentra en la regi&oacute;n de Santander, pruebe un plato de Hormigas Culonas, &quot;-Large Bottomed Hormigas&quot;, que son salado y crujiente como un cacahuete.</span><br />\r\n	<br />\r\n	<span title="Colombians take pride in their food and you will find that dishes are always fresh with limited preservatives or over-the-top dressings or seasonings typically found in American foods.">Los colombianos se enorgullecen de su comida y usted encontrar&aacute; que los platos son siempre frescos con conservantes limitados o over-the-top aderezos o condimentos encuentran t&iacute;picamente en los alimentos estadounidenses. </span><span title="Rare, exotic fruits are in abundance all over the country and typically complement well-cooked meals and desserts.">Raras, frutas ex&oacute;ticas se encuentran en abundancia en todo el pa&iacute;s y por lo general se complementan las comidas y postres bien cocidos. </span><span title="You will find that meals do not bloat you or make you tired, and always leave you wanting more.\r\n\r\n">Usted encontrar&aacute; que las comidas no te hinchan o te hacen cansado, y siempre te dejan con ganas de m&aacute;s.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="8. The Nightlife\r\n"><strong>8.</strong> <strong>La Noche</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Colombia is known for its extravagant nightlife, bars, clubs, and restaurants.">Colombia es conocida por su vida nocturna extravagante, bares, clubes y restaurantes. </span><span title="The entertainment is top-notch, the people are friendly, and the Women are amazingly beautiful.">El entretenimiento es de primera categor&iacute;a, la gente es amable, y las mujeres son incre&iacute;blemente hermoso. </span><span title="The Colombian people have a natural flair for dancing and partying, and they do it well.">El pueblo colombiano tienen un don natural para el baile y la fiesta, y lo hacen bien. </span><span title="Cali is known as the Nightlife capital of Colombia and it is easy to see why.">Cali es conocida como la capital de la vida nocturna de Colombia y es f&aacute;cil ver por qu&eacute;. </span><span title="Beautiful people are accompanied by a wide range of reataurants, live music, drinking, dancing, and good vibes overall.\r\n\r\n">Gente guapa se acompa&ntilde;an de una amplia gama de reataurants, m&uacute;sica en vivo, beber, bailar y buen rollo general.</span><br />\r\n	<br />\r\n	<span title="If you find yourself in Bogotá, be sure to check out Andrés Carne de Res.">Si usted se encuentra en Bogot&aacute;, aseg&uacute;rate de revisar Andr&eacute;s Carne de Res. </span><span title="One of my personal favorite restaurants/nightclubs.\r\n\r\n">Uno de mis restaurantes favoritos / discotecas personales.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="9. The Women\r\n"><strong>9.</strong> <strong>Las Mujeres</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="A Colombian Woman, A Group of Pretty Colombian Girls\r\n">Una mujer colombiana, un grupo de chicas guapas colombianas</span><br />\r\n	<span title="If you are looking to experience true exotic beauty, derived from the eccentric mix of African and Spanish cultures, the Colombian people are the ones.">Si usted est&aacute; buscando para experimentar la verdadera belleza ex&oacute;tica, derivado de la mezcla exc&eacute;ntrica de las culturas africanas y espa&ntilde;olas, los colombianos son los m&aacute;s. </span><span title="On the other hand, If you are a single guy, looking to mingle with some of the most beautiful women in the world, Colombia is definitely where you want to be.\r\n\r\n">Por otro lado, si usted es un hombre soltero, mirando a mezclarse con algunas de las mujeres m&aacute;s bellas del mundo, Colombia es sin duda donde quieres estar.</span><br />\r\n	<br />\r\n	<span title="Colombian people, Women especially take great pride in their appearance.">Pueblo colombiano, especialmente mujeres se enorgullecen de su apariencia. </span><span title="Women are very well kept and are never shy when flaunting their beauty.">Las mujeres est&aacute;n muy bien cuidados y nunca son t&iacute;midas cuando haciendo alarde de su belleza. </span><span title="They learn early on how to dance and mingle and what it takes to be stunningly gorgeous.">Ellos aprenden pronto a bailar y se mezclan y lo que se necesita para ser incre&iacute;blemente hermosa. </span><span title="It is no coincidence that there are beautiful women all over the place.\r\n\r\n">No es casualidad que hay mujeres hermosas por todo el lugar.</span><br />\r\n	<br />\r\n	<span title="Colombian women tend to be very polite and approachable.">Mujeres colombianas tienden a ser muy educadas y atentas. </span><span title="They enjoy drinking, dancing, talking, meeting new people, and are typically great companions on a night out.">Ellos disfrutan de beber, bailar, hablar, conocer gente nueva, y suelen ser grandes compa&ntilde;eros en una noche de fiesta. </span><span title="A Colombian woman, although always polite, will let you know if she is not interested in your advances, so be polite yourself, and make every effort to be genuine and friendly.">Una mujer colombiana, aunque siempre educada, le permitir&aacute; saber si ella no est&aacute; interesada en sus avances, as&iacute; que sea educado a s&iacute; mismo, y hacer todo lo posible para ser aut&eacute;ntico y amable. </span><span title="Oh, and learn some Spanish Gringo!">Ah, y aprender un poco de espa&ntilde;ol Gringo! </span><span title="It is easy to meet and discover lots of beautiful Colombian women on MiAmor.com.co, so before you head to Colombia make the connections here.\r\n\r\n">Es f&aacute;cil conocer y descubrir un mont&oacute;n de hermosas mujeres colombianas en MiAmor, as&iacute; que antes de ir a Colombia a haga las conexiones aqu&iacute;.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="10. Location\r\n"><strong>10.</strong> <strong>Ubicaci&oacute;n</strong></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="People fly all over the world to see and experience new things.">La gente vuela por todo el mundo para ver y experimentar cosas nuevas. </span><span title="You can get history and culture in Italy, beaches and resorts in Hawaii, Eco tourism and outdoor activities in Costa Rica, and friendly outgoing cultures in Greece.\r\n\r\n">Usted puede obtener la historia y la cultura de Italia, playas y centros tur&iacute;sticos en Hawai, el turismo ecol&oacute;gico y actividades al aire libre en Costa Rica, y culturas salientes amistosos en Grecia.</span><br />\r\n	<br />\r\n	<span title="Why not go to a place that has it all, AND is right in your backyard.">&iquest;Por qu&eacute; no ir a un lugar que lo tiene todo, y est&aacute; justo en su patio trasero. </span><span title="Colombia is very close to the United States and Canada and is a new and exciting opportunity for any traveler.">Colombia est&aacute; muy cerca de los Estados Unidos y Canad&aacute;, y es una nueva y emocionante oportunidad para cualquier viajero. </span><span title="Flights to Colombia are generally inexpensive and are only a couple hours from most North American International airports.">Vuelos a Colombia son generalmente de bajo costo y son s&oacute;lo un par de horas de la mayor&iacute;a de los aeropuertos internacionales de Am&eacute;rica del Norte. </span><span title="Prices and accommodations are usually cheaper than their foreign counterparts, and there are always friendly people to show you around.">Los precios y las habitaciones son generalmente m&aacute;s baratos que sus hom&oacute;logos extranjeros, y siempre hay gente amable que le muestre todo.</span></span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Study in Colombia_0(1).jpg" style="width: 1100px; height: 735px;" /></p>\r\n', 1, '2015-06-29 21:45:37');
INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(6, 'Colombian Women', 'Mujeres Colombianas', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian women are known to be loyal and infidelity is not really part of their vocabulary. This idea is reflected by the fact that Colombia has one of the lowest divorce rates in the world. When dating a Colombian girl, it is vital to win the approval of her family, especially if you want the relationship for long term. In fact, most Colombian women stay with their immediate family until they get married. Then, they stay with their spouse until the end. Such practice is a huge part of their culture, as I said, they are highly traditional and conventional. They really value the quality of family relationship.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Colombian women are the type of ladies that men are deeply after to, their qualities are indeed mesmerizing. It is important for Colombian women to satisfy the needs of her husband. They see to it that their family is intact. Colombian girls are independent and very determine. Colombian girls are known for their open-minded outlook, heartwarming laughter, and caring attitude. They always want to exhibit their womanhood. Most men find them to be very physically attractive, and they are often substantially thinner than their American counterparts.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">A lot of foreign men are seeing and hearing all the good qualities of Colombian women. However, there is no end to a list of positive things about these women. They don&#39;t value luxurious things or a glamorous lifestyle. They simply want to be loved. They do not expect gifts, and they are even excited when a man pays for their cab ride or buys them dinner. Being a man of honor is a quality that Colombian girls are deepy attracted to.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Thus, many men want the chance to meet Colombian women. Some men tend to prefer other girls such as Asian or Latinas. With effort and confidence, a man will not have a hard time to date a Colombian girl.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">The great thing about Colombian women is that they have the inclination for foreign men. Most Colombian women feel like Colombian men act like Casanovas. They are tired of these attitudes, and they want something different. Colombian girls are addicted to foreign movies and shows. Watching these movies, they have learned about romance. They want a man who can give them this kind of romance. MiAmor.com.co has a large database of Colombian women who want to find a foreign partner.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">They regard American and other foreign men as the ideal partners for life.. Many Colombian girls leave their country to seek a man in the United States. By following these tips, you may be able to date one of these beautiful women. If you want a Colombian girl to consider dating you, simply be romantic and affectionate. Colombian women are still traditional when it comes to dating routines. By being sweet and romantic, it will not be hard for you to make her fall in love. Once a Colombian woman believes this, she will stay devoted to you forever.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">make sure to present yourself in a neat way, this will surely attract Colombian women into you. However, you should have a few conversation topics ready to impress these women. Colombian women love music. If possible, you should be prepared to talk about a few of your favorite albums. You may even wish to listen to a little Colombian music before your first date. Attracting Colombian women can be fairly easy. You need to make sure that you look reasonably clean and attractive. Then you will definitely make a Colombian girl fall for you.</span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Colom35620611acolom356g.jpg" style="width: 1100px; height: 825px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span title="Colombian women are known to be loyal and infidelity is not really part of their vocabulary.">Mujeres colombianas son conocidos por ser leal y la infidelidad no es realmente parte de su vocabulario. </span><span title="This idea is reflected by the fact that Colombia has one of the lowest divorce rates in the world.">Esta idea se refleja en el hecho de que Colombia tiene una de las tasas de divorcio m&aacute;s bajas del mundo. </span><span title="When dating a Colombian girl, it is vital to win the approval of her family, especially if you want the relationship for long term.">Al salir con una chica colombiana, es vital para ganar la aprobaci&oacute;n de su familia, especialmente si desea que la relaci&oacute;n de largo plazo. </span><span title="In fact, most\r\n">De hecho, la mayor parte de </span><span title="Colombian women stay with their immediate family until they get married.">Mujeres colombianas permanecen con su familia inmediatamente hasta que se casan. </span><span title="Then, they stay with their spouse until the end.">A continuaci&oacute;n, se quedan con su c&oacute;nyuge hasta el final. </span><span title="Such practice is a huge part of their culture, as I said, they are highly traditional and conventional.">Esta pr&aacute;ctica es una gran parte de su cultura, como he dicho, son muy tradicional y convencional. </span><span title="They really value the quality of family relationship.\r\n\r\n">Ellos valoran mucho la calidad de la relaci&oacute;n familiar.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Colombian women are the type of ladies that men are deeply after to, their qualities are indeed mesmerizing.">Mujeres colombianas son el tipo de mujeres que hombres est&aacute;n profundamente despu&eacute;s de sus cualidades son realmente fascinante. </span><span title="It is important for Colombian women to satisfy the needs of her husband.">Es importante que las mujeres colombianas para satisfacer las necesidades de su marido. </span><span title="They see to it that their family is intact.">Se preocupan de que su familia est&aacute; intacto. </span><span title="Colombian girls are independent and very determine.">Ni&ntilde;as colombianas son independientes y muy determinadas. </span><span title="Colombian girls are known for their open-minded outlook, heartwarming laughter, and caring attitude.">Ni&ntilde;as colombianas son conocidos por su actitud de mente abierta, la risa tierna, y la actitud solidaria. </span><span title="They always want to exhibit their womanhood.">Siempre quieren exhibir su condici&oacute;n de mujer. </span><span title="Most men find them to be very physically attractive, and they are often substantially thinner than their American counterparts.\r\n\r\n">La mayor&iacute;a de los hombres encuentran que sean muy atractivas f&iacute;sicamente, y que a menudo son considerablemente m&aacute;s delgado que sus hom&oacute;logos estadounidenses.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="A lot of foreign men are seeing and hearing all the good qualities of Colombian women.">Una gran cantidad de hombres extranjeros est&aacute;n viendo y oyendo todas las buenas cualidades de la mujer colombiana. </span><span title="However, there is no end to a list of positive things about these women.">Sin embargo, no hay fin a una lista de cosas positivas sobre estas mujeres. </span><span title="They don''t value luxurious things or a glamorous lifestyle.">No valoran las cosas de lujo o un estilo de vida glamoroso. </span><span title="They simply want to be loved.">Ellos simplemente quieren ser amados. </span><span title="They do not expect gifts, and they are even excited when a man pays for their cab ride or buys them dinner.">No esperan regalos, y&nbsp; son a&uacute;n emocionales cuando un hombre paga por su viaje en taxi o les compra la cena. </span><span title="Being a man of honor is a quality that Colombian girls are deepy attracted to.\r\n\r\n">Siendo un hombre de honor es una cualidad que las ni&ntilde;as colombianas son Deepy atra&iacute;do.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="Thus, many men want the chance to meet Colombian women.">Por lo tanto, muchos hombres quieren la oportunidad de conocer a la mujer colombiana. </span><span title="Some men tend to prefer other girls such as Asian or Latinas.">Algunos hombres tienden a preferir otras chicas tales como Asia o latinas. </span><span title="With effort and confidence, a man will not have a hard time to date a Colombian girl.\r\n\r\n">Con esfuerzo y confianza, un hombre no va a tener un tiempo dif&iacute;cil salir con una chica colombiana.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="The great thing about Colombian women is that they have the inclination for foreign men.">La gran cosa sobre las mujeres colombianas es que tienen la inclinaci&oacute;n de los hombres extranjeros. </span><span title="Most Colombian women feel like Colombian men act like Casanovas.">La mayor&iacute;a de las mujeres colombianas sienten como hombres colombianos act&uacute;an como Casanovas. </span><span title="They are tired of these attitudes, and they want something different.">Est&aacute;n cansados &#8203;&#8203;de estas actitudes, y quieren algo diferente. </span><span title="Colombian girls are addicted to foreign movies and shows.">Ni&ntilde;as colombianas son adictas a las pel&iacute;culas y programas extranjeros. </span><span title="Watching these movies, they have learned about romance.">Viendo estas pel&iacute;culas, que han aprendido sobre el romance. </span><span title="They want a man who can give them this kind of romance.">Ellas quieren un hombre que les puede dar este tipo de romance. </span><span title="MiAmor.com.co has a large database of Colombian women who want to find a foreign partner.\r\n\r\n">MiAmor.com.co tiene una gran base de datos de las mujeres colombianas que quieren encontrar una pareja extranjero.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="They regard American and other foreign men as the ideal partners for life.. Many Colombian girls leave their country to seek a man in the United States.">Consideran que los hombres estadounidenses y de otros como los socios ideales para la vida .. Muchas mujeres colombianas abandonan su pa&iacute;s en busca de un hombre en los Estados Unidos. </span><span title="By following these tips, you may be able to date one of these beautiful women.">Siguiendo estos consejos, usted puede ser capaz de salir con una de estas hermosas mujeres. </span><span title="If you want a Colombian girl to consider dating you, simply be romantic and affectionate.">Si quieres una chica colombiana a considerar salir contigo, simplemente ser rom&aacute;ntico y cari&ntilde;oso. </span><span title="Colombian women are still traditional when it comes to dating routines.">Mujeres colombianas siguen siendo tradicional cuando se trata de rutinas de citas. </span><span title="By being sweet and romantic, it will not be hard for you to make her fall in love.">Al ser dulce y rom&aacute;ntico, no va a ser dif&iacute;cil para usted para hacer que se enamore. </span><span title="Once a Colombian woman believes this, she will stay devoted to you forever.\r\n\r\n">Una vez que una mujer colombiana cree esto, ella se quedar&aacute; enamorada de ti para siempre.</span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="es"><span title="make sure to present yourself in a neat way, this will surely attract Colombian women into you.">aseg&uacute;rese de presentarse de una manera ordenada, esto seguramente atraer&aacute; a las mujeres colombianas en usted. </span><span title="However, you should have a few conversation topics ready to impress these women.">Sin embargo, usted debe tener un par de temas de conversaci&oacute;n listos para impresionar a estas mujeres. </span><span title="Colombian women love music.">Mujeres colombianas les encanta la m&uacute;sica. </span><span title="If possible, you should be prepared to talk about a few of your favorite albums.">Si es posible, usted debe estar preparado para hablar de algunos de sus discos favoritos. </span><span title="You may even wish to listen to a little Colombian music before your first date.">Incluso puedes escuchar un poco de m&uacute;sica colombiana antes de su primera cita. </span><span title="Attracting Colombian women can be fairly easy.">Atraer a las mujeres colombianas puede ser bastante f&aacute;cil. </span><span title="You need to make sure that you look reasonably clean and attractive.">Usted necesita asegurarse de que usted parece bastante limpio y atractivo. </span><span title="Then you will definitely make a Colombian girl fall for you.">Entonces definitivamente va a hacer que una chica colombiana cae para usted.</span></span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span lang="es"><span title="Then you will definitely make a Colombian girl fall for you."><img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Colom35620611acolom356g(1).jpg" style="width: 1100px; height: 825px;" /></span></span></p>\r\n', 1, '2015-06-29 21:46:16'),
(7, 'Why do Colombian women prefer foreigners?', '¿Por qué las mujeres los prefieren extranjeros?', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="en"><span title="Ojos azules, acento distinto.">Blue eyes, different accents. </span><span title="Mujeres enloquecidas.">Crazed women. </span><span title="¿Qué tan cierto es el imaginario de que las mujeres, a la hora de escoger entre locales y este tipo de especímenes, se decantan sin pensar por los segundos?">How true is the imagination of the Colombian women, when choosing between local and this type of specimens, without thinking opt for the latter? </span><span title="¿Mejor posición económica?">Better off? </span><span title="¿Trato igualitario?">Equal treatment? </span><span title="¿Estatus social, o amor?\r\n\r\n">Social status, or love?</span><br />\r\n	<br />\r\n	<span title="Quizás todas y cada una de las anteriores.">Maybe each and every one of the above. </span><span title="Es decir, la tara cultural ha estado muy arraigada desde el comienzo de nuestra historia.">That is, the cultural shortcoming that has been well established since the beginning. </span><span title="Según el libro de referencia del genetista colombiano Emilio Yunis, ''¿Por qué somos así?'', se refleja en la enseñanza repentina y continuada de que todo aquel con facciones europeas resultaba ser superior por antonomasia.">According to the reference book of the Colombian geneticist Emilio Yunis, &#39;Why are we like this?&#39;, Reflected in the sudden and continued teaching that everyone with European features proved to be superior par excellence. </span><span title="Y por ende, estar con ellos era recibir favores e incluso atenciones.\r\n\r\n">And therefore, being with them your going to be receiving favors and even attention.</span><br />\r\n	<br />\r\n	<span title="Esclavas afroamericanas e indígenas, fuese por relaciones forzadas y abusivas o por voluntad propia, buscaban un “mejor futuro” con los extranjeros.">African American slaves and Indians, through forced and abusive relationships or by choice, seeked a &quot;better future&quot; with foreigners. </span><span title="Claro, no sucede en todos los casos.\r\n\r\n">Of course, not true in all cases.</span><br />\r\n	<br />\r\n	<span title="De igual modo, tiene mucho que ver en el sentimiento de inferioridad racial y complejos que han tenido muchas etnias no blancas en Latinoamérica.">Similarly, it has much to do with the feeling of racial inferiority complexes that have had many non-white ethnic groups in Latin America. </span><span title="Sus estadistas procuraron favorecer la inmigración de extranjeros, preferiblemente cristianos y blancos, para “mejorar la raza”.">Seekt to encourage the immigration of foreigners, preferably Christians and white, to &quot;improve the race.&quot; </span><span title="Y en muchos casos, mejorar la posición económica y ver que, en comparación con los hombres de otros lados, los locales tenían mucho que perder.\r\n\r\n">And in many cases, improve the economic position and wait and see, compared with men from other places, locals had a lot to lose.</span><br />\r\n	<br />\r\n	<span title="Esto se puede ver en columnas como la de Héctor Abad Faciolince llamada ''El dimorfismo sexual colombiano”, donde los hombres de aquel país no salían bien parados en comparación con la popular belleza de sus compatriotas femeninas.">This can be seen in columns like H&eacute;ctor Abad Faciolince called &#39;The Colombian sexual dimorphism, &quot;where the men of the country do not stand out well compared to the popular beauty of their female compatriots. </span><span title="También se puede ver en la cantidad de mujeres que acuden a agencias matrimoniales por un hombre extranjero, ya que se quejan del machismo e infidelidad de los hombres locales.">It can also be seen in the number of women seeking to date online to meet a foreign man, Damon de Berry of MiAmor.com.co says Colombian women complain of machismo and infidelity of local men. </span><span title="Y no pasa solo en Latinoamérica.\r\n\r\n">This doesn&#39;t only happens in Latin America though</span><br />\r\n	<br />\r\n	<span title="En Colombia, por ejemplo, existe una agencia que muestra en su página testimonios de enlaces exitosos entre extranjeros y mujeres nacionales.">In MiAmor, for example, there is are page testimonies of successful links between foreign and domestic women. </span><span title="Explican el por qué de la ''deseabilidad'' de las colombianas, que son más hogareñas y menos frías que las anglosajonas, y resumen las ''ventajas'' de los norteamericanos así: “Estos hombres tienen experiencia, son educados, profesionales y pueden ofrecerte un mejor">Which describe the &#39;desirability&#39; of Colombian women, who are more homey and less cold than the Anglo-Saxon&#39;s, and the testimonies summarize the &#39;advantages&#39; of the American way: &quot;These men are experienced, polite, professional and can offer a better </span><span title="nivel de vida que la mayoría de los prospectos a nivel local”, aseguran.\r\n\r\n">standard of living than most local prospects, &quot;they say.</span><br />\r\n	<br />\r\n	<span title="“Las diferencias con los hombres de mi país, al menos en mi experiencia personal, es que ellos no son personas de confiar, te mienten mucho y no les importa lo que sientes.">&quot;The differences with the men of my country, at least in my personal experience is that they are not people to trust, they lie a lot and do not care how you feel. </span><span title="Con él las cosas son muy distintas, sabemos que la honestidad es una clave para que nuestra relación sea sólida y no tiene miedo en mostrarme sus sentimientos, tanto su lado fuerte como su lado débil.">With my current boyfriend things are very different, we know that honesty is the key to our relationship, we are not afraid to show our feelings both strong and the weak side. </span><span title="Eso en una cultura como la mía se ve mal entre los mismos hombres”, afirma la periodista Natalia Torres, quien es novia de un inglés.">That in my culture,makes men look bad, &quot;says journalist Natalia Torres, who is the girlfriend of an Englishman. </span><span title="Natalia aclara, sin embargo, que no la enamoró el hecho de que fuese extranjero, sino sus intereses comunes, cosa que no encontró con ningún hombre de su nación.\r\n\r\n">Natalia is clear though that she did not fall for him because he was a foreigner, but their common interests, which isn&#39;t found with any men of her nation.</span><br />\r\n	<br />\r\n	<span title="No todo es lo que parece…\r\n\r\n">Not everything is what it seems ...</span><br />\r\n	<br />\r\n	<span title="Pero hombres malos hay en todas partes.">But evil men are everywhere. </span><span title="Prueba de eso es que algunas mujeres no encuentran su “paraíso soñado” con el extranjero que les prometió el cielo.\r\n\r\n">Proof of this is that some women do not find their &quot;dream paradise&quot; with the foreigner who promised heaven.</span><br />\r\n	<br />\r\n	<span title="También sucede que hay cuestiones culturales que muchas mujeres no tienen en cuenta a la hora de entablar una relación y querer salir de su país a toda costa.\r\n\r\n">It also happens that there are cultural issues that many women do not consider when building a relationship and wanting to leave their country at all costs.</span><br />\r\n	<br />\r\n	<span title="Como quien dice, no todo lo que brilla es oro, y hay muchos factores que llevan a las mujeres a relacionarse con hombres de otro país.">As they say, all that glitters is not gold, and there are many factors that arrive for women who try and relate to people from another country. </span><span title="Y no todos los casos son iguales pero el más importante es que usted construye una relación sólida y realmente conocer a su Extranjero antes de decidirse a correr y fugarse\r\n\r\n">And not all the cases are the same but the most important is that you build a solid relationship and really know your foreigner before deciding to run off and escape</span><br />\r\n	<br />\r\n	<span title="Miamor tiene muy estrictas directrices acoso y comunicación para mantener a todas las mujeres a gusto con su introducción extranjero y experiencias de dating">Miamor harassment has very strict guidelines with communication to keep all women at ease with foreign introductions and dating experiences</span></span></span></p>\r\n<p>\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/A1912.jpg" style="width: 1100px; height: 730px;" /></p>\r\n', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Ojos azules, acento distinto. Mujeres enloquecidas. &iquest;Qu&eacute; tan cierto es el imaginario de que las mujeres, a la hora de escoger entre locales y este tipo de espec&iacute;menes, se decantan sin pensar por los segundos? &iquest;Mejor posici&oacute;n econ&oacute;mica? &iquest;Trato igualitario? &iquest;Estatus social, o amor?</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Quiz&aacute;s todas y cada una de las anteriores. Es decir, la tara cultural ha estado muy arraigada desde el comienzo de nuestra historia. Seg&uacute;n el libro de referencia del genetista colombiano Emilio Yunis, &lsquo;&iquest;Por qu&eacute; somos as&iacute;?&rsquo;, se refleja en la ense&ntilde;anza repentina y continuada de que todo aquel con facciones europeas resultaba ser superior por antonomasia. Y por ende, estar con ellos era recibir favores e incluso atenciones.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Esclavas afroamericanas e ind&iacute;genas, fuese por relaciones forzadas y abusivas o por voluntad propia, buscaban un &ldquo;mejor futuro&rdquo; con los extranjeros. Claro, no sucede en todos los casos.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">De igual modo, tiene mucho que ver en el sentimiento de inferioridad racial y complejos que han tenido muchas etnias no blancas en Latinoam&eacute;rica. Sus estadistas procuraron favorecer la inmigraci&oacute;n de extranjeros, preferiblemente cristianos y blancos, para &ldquo;mejorar la raza&rdquo;. Y en muchos casos, mejorar la posici&oacute;n econ&oacute;mica y ver que, en comparaci&oacute;n con los hombres de otros lados, los locales ten&iacute;an mucho que perder.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span class="hps">Esto se puede ver</span> <span class="hps">en las columnas</span> <span class="hps">como</span> <span class="hps">H&eacute;ctor</span> <span class="hps">Abad</span> <span class="hps">Faciolince</span> <span class="hps atn">llama &#39;</span><span>El</span> <span class="hps">dimorfismo</span> <span class="hps">sexual</span> <span class="hps">en Colombia</span><span>,</span> <span class="hps atn">&quot;</span><span>donde los hombres</span> <span class="hps">del pa&iacute;s</span> <span class="hps">no se destacan</span> <span class="hps">bien en comparaci&oacute;n con</span> <span class="hps">la belleza</span> <span class="hps">popular de</span> <span class="hps">sus</span> <span class="hps">compatriotas femeninas</span><span>.</span> <span class="hps">Tambi&eacute;n</span> <span class="hps">se puede ver en</span> <span class="hps">el n&uacute;mero de mujeres</span> <span class="hps">que buscan</span> <span class="hps">hasta la fecha</span> <span class="hps">en l&iacute;nea para</span> <span class="hps">conocer a un</span> <span class="hps">hombre extranjero</span><span>,</span> <span class="hps">Damon</span> <span class="hps">de Berry</span> <span class="hps">de</span> <span class="hps">MiAmor.com.co</span> <span class="hps">dice que las mujeres</span> <span class="hps">colombianas</span> <span class="hps">se quejan</span> <span class="hps">del machismo</span> <span class="hps">y la infidelidad</span> <span class="hps">de los hombres</span> <span class="hps">locales.</span> <span class="hps">Esto no</span> <span class="hps">s&oacute;lo ocurre</span> <span class="hps">en Am&eacute;rica Latina</span><span>, aunque</span></span></span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span class="hps">En</span> <span class="hps">miamor</span><span>, por ejemplo</span><span>, hay</span>&nbsp; <span class="hps">p&aacute;ginas de</span> <span class="hps">testimonios de</span> <span class="hps">v&iacute;nculos</span> <span class="hps">eficaces entre</span> <span class="hps">las mujeres</span> <span class="hps">extranjeras y nacionales</span><span>.</span> <span class="hps">Que describen</span> <span class="hps atn">la &quot;</span><span>conveniencia</span><span>&quot; de las mujeres</span> <span class="hps">colombianas</span><span>, que son m&aacute;s</span> <span class="hps">acogedor y</span> <span class="hps">menos fr&iacute;o que</span> <span class="hps">el</span> <span class="hps">anglosaj&oacute;n</span> <span class="hps">y</span> <span class="hps">los testimonios</span> <span class="hps">resumen las</span> <span class="hps">&quot;ventajas</span><span>&quot; de</span> <span class="hps">la manera americana</span><span>:</span> <span class="hps atn">&quot;</span><span>Estos hombres</span> <span class="hps">son experimentados</span><span>,</span> <span class="hps">educado, profesional</span> <span class="hps">y pueden ofrecer</span> <span class="hps">una mejor</span> <span class="hps">nivel de vida</span> <span class="hps">que la mayor&iacute;a de</span> <span class="hps">las perspectivas</span> <span class="hps">locales</span> <span class="hps">&quot;, dicen</span><span>.</span></span></span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">&ldquo;Las diferencias con los hombres de mi pa&iacute;s, al menos en mi experiencia personal, es que ellos no son personas de confiar, te mienten mucho y no les importa lo que sientes. Con &eacute;l las cosas son muy distintas, sabemos que la honestidad es una clave para que nuestra relaci&oacute;n sea s&oacute;lida y no tiene miedo en mostrarme sus sentimientos, tanto su lado fuerte como su lado d&eacute;bil. Eso en una cultura como la m&iacute;a se ve mal entre los mismos hombres&rdquo;, afirma la periodista Natalia Torres, quien es novia de un ingl&eacute;s. Natalia aclara, sin embargo, que no la enamor&oacute; el hecho de que fuese extranjero, sino sus intereses comunes, cosa que no encontr&oacute; con ning&uacute;n hombre de su naci&oacute;n.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">No todo es lo que parece&hellip;</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Pero hombres malos hay en todas partes. Prueba de eso es que algunas mujeres no encuentran su &ldquo;para&iacute;so so&ntilde;ado&rdquo; con el extranjero que les prometi&oacute; el cielo.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Tambi&eacute;n sucede que hay cuestiones culturales que muchas mujeres no tienen en cuenta a la hora de entablar una relaci&oacute;n y querer salir de su pa&iacute;s a toda costa.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Como quien dice, no todo lo que brilla es oro, y hay muchos factores que llevan a las mujeres a relacionarse con hombres de otro pa&iacute;s. Y no todos los casos son iguales pero el m&aacute;s importante es que usted construye una relaci&oacute;n s&oacute;lida y realmente conocer a su Extranjero antes de decidirse a correr y fugarse</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Miamor tiene muy estrictas directrices acoso y comunicaci&oacute;n para mantener a todas las mujeres a gusto con su introducci&oacute;n extranjero y experiencias de dating</span></p>\r\n	<p>\r\n		<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/A1912(1).jpg" style="width: 1100px; height: 730px;" /></p>\r\n</div>\r\n', 1, '2015-06-29 21:46:56'),
(8, 'Dating Colombian Girls', 'Dating Girls Colombianos', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Dating Colombian Girls<br />\r\n	<br />\r\n	When I first came to South America one of the things I relished most was the idea of dating Latin women.<br />\r\n	<br />\r\n	In every South American country in which I travelled, from the tip of Patagonia to the northernmost reaches of the continent I met wonderful women. They were friendly, kind, sweet, generous, sincere and in many cases overwhelmingly beautiful.<br />\r\n	<br />\r\n	And nowhere in South America is this truer than for Colombian girls.<br />\r\n	<br />\r\n	Having lived in Colombia now for well over a decade I have a fair amount of experience dating Colombian women. Most of my experiences have been very positive. But it has taken a while to hone my dating skills.<br />\r\n	<br />\r\n	There are some big cultural differences that exist between the typical European or North American woman and Colombian women.<br />\r\n	<br />\r\n	The following is my impression of Colombian girls that I have formed over many years of living here from numerous dates and also a couple of serious relationships.<br />\r\n	<br />\r\n	Will I generalise? Of course I will. It takes all sorts, as they say. You can find pretty much any type of woman (and man) in Colombia just as you can in other parts of the world. But being aware of some of the trends and characteristics that apply to Colombian women and the dating scene in Colombia may prove invaluable if you&rsquo;re looking for some Latin romance.<br />\r\n	Characteristics of Colombian Girls<br />\r\n	<br />\r\n	It is difficult to generalise when it comes to describing Colombian girls physically since Colombia is such a racially diverse country.<br />\r\n	<br />\r\n	In the coastal regions, for example, the majority of the people are of black descent, whereas in the area around Medellin and the coffee region the population is much whiter. In parts of central and southern Colombia many people come from an indigenous background.<br />\r\n	<br />\r\n	One thing I can guarantee is that in pretty much any Colombian town or city you will see beautiful Colombian girls. Or you will meet them online in dating sites like MiAmor.com.co which is a Colombian based dating site.<br />\r\n	<br />\r\n	But not only are Colombian women beautiful, they can also be incredibly flirtatious. Many Colombian women enjoy being the object of admiring stares in the street, although most resent the hissing and wolf-whistling which Colombian men can sometimes be prone to.<br />\r\n	<br />\r\n	That said, a female Colombian friend once remarked that she had felt oddly unattractive while living in the USA. She would walk past numerous construction sites and be met with nothing but a disinterested silence, something that would be unimaginable in Colombia.<br />\r\n	<br />\r\n	She joked that she had considered walking back past the construction workers with a couple more buttons undone in an attempt to elicit the kind of hisses and sordid comments that would have annoyed her back home!<br />\r\n	<br />\r\n	On the whole Colombia is a country which seems to be increasingly comfortable with sexuality (especially among the younger generations), despite some strong conservative traditions.<br />\r\n	<br />\r\n	In some Colombian cities, most notably Medellin and Cali, it has become quite common to see women with silicon implants.<br />\r\n	<br />\r\n	This supposedly goes back to the days of the drug cartels, of which the Medellin and Cali cartels were the largest and most powerful. The tremendous wealth which was generated from the trafficking of drugs created a culture of elaborate spending.<br />\r\n	<br />\r\n	Pablo Escobar famously imported elephants and other exotic animals to his Hacienda Napoles resort. And many Medellin girls got silicon implants, lured by the glamorous lifestyle that being the girlfriend of a &#39;Mafioso&#39; could provide.<br />\r\n	<br />\r\n	The elephants have long since gone, but the culture of silicon enhancements has remained.<br />\r\n	<br />\r\n	<br />\r\n	Dating Colombian Girls - What to Expect<br />\r\n	<br />\r\n	I&#39;ve found approaching and engaging Colombian girls to be much less daunting than in, for example, my home country of the UK.<br />\r\n	<br />\r\n	Depending on how you make your move you will very occasionally get the cold shoulder, but more often than not the girl will be happy to talk to you even if she&rsquo;s not interested in anything more than friendship or a quick chat.<br />\r\n	<br />\r\n	European girls would tend to invent an imaginary boyfriend to deter your advances, Colombian girls are much more likely to give you her number and then either dodge your calls, make excuses for not going out with you, or worse still, accept a date and then stand you up. It&rsquo;s something I learned the hard way in my first year or so in Colombia!<br />\r\n	<br />\r\n	Being able to communicate reasonably well in Spanish definitely helps when it comes to spotting if there is genuine interest there or not, if your connecting online you can use a translation service if your Spanish isn&rsquo;t up to scratch.<br />\r\n	<br />\r\n	If you hear either &ldquo;Es que esta lloviendo / esta hacienda mucho frio&rdquo; (&ldquo;The thing is it&rsquo;s raining / it&rsquo;s very cold&rdquo;) or &ldquo;Es que tengo mucha pereza&rdquo; (roughly translated as &ldquo;I can&rsquo;t really be bothered&rdquo;) then alarm bells should start ringing!<br />\r\n	<br />\r\n	At the same time, however, you have to realise that in Colombia the onus is on the man to call and to make arrangements. But if you make an effort and keep getting fobbed off then it&rsquo;s probably best to cut your losses.<br />\r\n	<br />\r\n	The Dating Ritual - Some Tips<br />\r\n	<br />\r\n	OK, so you&rsquo;ve met a girl and she&rsquo;s agreed to go out with you or you are coming to Colombia to meet someone that you have met online. There are some general rules to which you should adhere to in order to increase your chances of making a good impression.<br />\r\n	<br />\r\n	Colombia is still a very conservative country and many Colombian girls will expect you to collect them in your car or in a taxi on your way to a date (particularly the first few dates), and when going home.<br />\r\n	<br />\r\n	If the girl does agree to meet you there make sure you&rsquo;re on time. While it&rsquo;s often acceptable for the girl to arrive 20 minutes late<br />\r\n	<br />\r\n	To most Colombia women, however, being &ldquo;detallista&rdquo; (attentive to the details) i.e. opening cars doors for her etc is more important than wining and dining her in a fancy restaurant. Not that it hurts!<br />\r\n	<br />\r\n	People in Colombia dress smartly when they go out. And hygiene is viewed with an even greater importance than it is in Europe and North America.<br />\r\n	<br />\r\n	Colombian girls take huge pride in their appearance and will often spend an hour or two getting ready.<br />\r\n	<br />\r\n	If you&rsquo;re a backpacker, try to dress up as much as possible and remember that in cities like Medellin and Cali where the climate is perfect for shorts and sandals, the locals seldom wear them, especially at night.<br />\r\n	<br />\r\n	When it comes to paying the bill you should always offer to pay. There are actually many Colombian girls who insist on paying or at least making a contribution, but it&rsquo;s always polite to offer.<br />\r\n	<br />\r\n	The concept that the man should pay the bill can be so taken for granted that you sometimes don&rsquo;t even receive a simple &ldquo;Gracias&rdquo; as you leave. This is something that still annoys me, but as with many things it&rsquo;s an aspect of traditional Colombian culture which it&rsquo;s best just to try to accept.<br />\r\n	<br />\r\n	While some Colombian women will accept a kiss on the first date, traditionally the unwritten rule seems to be at least the second if not the third date. Coming on too strong on the first date could blow your chances completely. Try to read her signals as best you can. Touching her hand and then judging her reaction can be a good way to test the water.<br />\r\n	<br />\r\n	All in all, I find dating Colombian girls less stressful than dating girls in Britain. There isn&rsquo;t so much emphasis on impressing the girl with your career, studies or sophistication.<br />\r\n	<br />\r\n	Simply being a nice, gentlemanly guy can be enough for her to consider the date a success.</span></p>\r\n<p>\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Colombian_couple_beach(2).jpg" style="width: 1100px; height: 580px;" /></p>\r\n', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Dating Girls Colombianos<br />\r\n	<br />\r\n	Cuando llegu&eacute; por primera vez a Am&eacute;rica del Sur una de las cosas que m&aacute;s disfrutaba era la idea de salir con mujeres latinas.<br />\r\n	<br />\r\n	En todos los pa&iacute;ses de Am&eacute;rica del Sur en el que viajaba, desde la punta de la Patagonia hasta los confines septentrionales del continente conoc&iacute; a mujeres maravillosas. Eran amables, dulces, generosas, sinceras y en muchos casos abrumadoramente hermosas.<br />\r\n	<br />\r\n	Y en ninguna parte de Am&eacute;rica del Sur es m&aacute;s cierto que para las chicas colombianas.<br />\r\n	<br />\r\n	Habiendo vivido en Colombia ahora por m&aacute;s de una d&eacute;cada&nbsp; tengo una buena cantidad de experiencia de salir con mujeres colombianas. La mayor&iacute;a de mis experiencias han sido muy positivas. Pero he tomado un tiempo para perfeccionar mis habilidades de conquista.<br />\r\n	<br />\r\n	Hay algunas diferencias culturales grandes que existen entre la mujer t&iacute;pica europea o norteamericana y las mujeres colombianas.<br />\r\n	<br />\r\n	Lo que sigue es mi impresi&oacute;n de las chicas colombianas que he formado a lo largo de muchos a&ntilde;os de vivir aqu&iacute; desde numerosas fechas y tambi&eacute;n un par de relaciones serias.<br />\r\n	<br />\r\n	&iquest;Voy a generalizar? Por supuesto que lo har&eacute;. Hay de todo, como dicen. Usted puede encontrar casi cualquier tipo de mujer (y hombre) en Colombia del mismo modo que en otras partes del mundo. Pero ser consciente de algunas de las tendencias y caracter&iacute;sticas que se aplican a las mujeres de Colombia y el mundo de las citas en Colombia puede resultar muy &uacute;til si usted est&aacute; buscando un poco de romance latino.<br />\r\n	[7/05/15 9:34:53 am] jhoana salas: Caracter&iacute;sticas de las muchachas colombianas<br />\r\n	<br />\r\n	Es dif&iacute;cil generalizar cuando se trata de la descripci&oacute;n de las chicas colombianas f&iacute;sicamente ya que Colombia es un pa&iacute;s racialmente diverso.<br />\r\n	<br />\r\n	En las regiones costeras, por ejemplo, la mayor&iacute;a de las personas son de origen negro, mientras que en el &aacute;rea alrededor de Medell&iacute;n y la regi&oacute;n cafetera de la poblaci&oacute;n es mucho m&aacute;s blanca. En algunas partes de Colombia muchas personas del centro y sur provienen de un fondo ind&iacute;gena.<br />\r\n	<br />\r\n	Una cosa que puedo garantizar es que en casi cualquier pueblo o ciudad colombiana podr&aacute;s ver hermosas chicas colombianas. O usted podra encontrarlas en l&iacute;nea en los sitios de citas como MiAmor.com.co que es un sitio de citas basado en colombianas.<br />\r\n	<br />\r\n	Pero no s&oacute;lo son las mujeres colombianas hermosas, tambi&eacute;n pueden ser muy coquetas. Muchas mujeres colombianas gozan de ser el objeto de admiracion de miradas en la calle, aunque m&aacute;s resienten el silbido que los hombres colombianos a veces pueden ser propensos a.<br />\r\n	<br />\r\n	Dicho esto, una amiga colombiana dijo una vez que se hab&iacute;a sentido extra&ntilde;amente atractiva mientras viv&iacute;a en los EE.UU.. Ella caminaba pasando numerosas obras de construcci&oacute;n con nada m&aacute;s que un silencio desinteresado, algo que ser&iacute;a inimaginable en Colombia.<br />\r\n	<br />\r\n	Ella brome&oacute; que ella hab&iacute;a considerado volver caminando m&aacute;s all&aacute; de los trabajadores de la construcci&oacute;n con un par de botones desabrochados en un intento de obtener el tipo de silbidos y comentarios s&oacute;rdidos que tendr&iacute;a en Colombia pero no entonces se regreso a casa molesta!<br />\r\n	<br />\r\n	En todo&nbsp; Colombia es un pa&iacute;s que parece ser cada vez m&aacute;s c&oacute;modo con la sexualidad (especialmente entre las generaciones m&aacute;s j&oacute;venes), a pesar de algunas fuertes tradiciones conservadoras.<br />\r\n	<br />\r\n	En algunas ciudades de Colombia, sobre todo Medell&iacute;n y Cali, se ha vuelto muy com&uacute;n ver a las mujeres con implantes de silicona.<br />\r\n	<br />\r\n	Esto supone que se remonta a la &eacute;poca de los carteles de la droga, de los cuales los carteles de Medell&iacute;n y Cali fueron el m&aacute;s grande y poderoso. La enorme riqueza que se genera a partir del tr&aacute;fico de drogas ha creado una cultura de la elaborada gasto.<br />\r\n	<br />\r\n	Pablo Escobar famoso importador de elefantes y otros animales ex&oacute;ticos a su resort Hacienda N&aacute;poles. Y muchas chicas de Medell&iacute;n recibieron implantes de silicona, atra&iacute;dos por el estilo de vida glamoroso que ser la novia de un &#39;mafioso&#39; podr&iacute;a proporcionar.<br />\r\n	[7/05/15 9:35:21 am] jhoana salas: Los elefantes han pasado desde hace mucho tiempo, pero la cultura de mejoras de la silicona se ha mantenido.<br />\r\n	<br />\r\n	<br />\r\n	Dating Chicas colombianas - &iquest;Qu&eacute; esperar?<br />\r\n	<br />\r\n	He encontrado que las chicas Colombianas se acercan y participan, por ejemplo, mi pa&iacute;s de origen del Reino Unido.<br />\r\n	<br />\r\n	Dependiendo de c&oacute;mo usted hace su movimiento va muy de vez en cuando obtiene el hombro fr&iacute;o, pero m&aacute;s a menudo la chica estar&aacute; encantada de hablar con usted, incluso si ella no est&aacute; interesado en nada m&aacute;s que una amistad o una charla r&aacute;pida.<br />\r\n	<br />\r\n	Chicas europeas tender&iacute;an a inventar un novio imaginario para disuadir a sus avances, las chicas colombianas son mucho m&aacute;s duras que le den su n&uacute;mero y luego o bien esquivan sus llamadas, hacen excusas para no salir contigo, o peor a&uacute;n, aceptan una fecha y despu&eacute;s no van a esta. Es algo que aprend&iacute; de la manera dif&iacute;cil en mi primer a&ntilde;o m&aacute;s o menos en Colombia!<br />\r\n	<br />\r\n	Ser capaz de comunicarse razonablemente bien en espa&ntilde;ol sin duda ayuda a la hora de detectar si existe un inter&eacute;s genuino all&iacute; o no, si su conexi&oacute;n es en l&iacute;nea que usted puede utilizar un servicio de traducci&oacute;n si su espa&ntilde;ol no est&aacute; a la altura.<br />\r\n	<br />\r\n	Si escucha bien &quot;Es Que esta lloviendo / esta haciendo Mucho frio&quot; (&quot;La cosa se est&aacute; lloviendo / hace mucho fr&iacute;o&quot;) o &quot;Es que tengo mucha pereza&quot; (que podr&iacute;a traducirse como &quot;No puedo ser molestado&quot;) a continuaci&oacute;n, las campanas de alarma deber&iacute;an empezar a sonar!<br />\r\n	<br />\r\n	Al mismo tiempo, sin embargo, usted tiene que darse cuenta de que en Colombia la responsabilidad recae en el hombre para llamar y hacer arreglos. Pero si haces un esfuerzo y sigue siendo amable entonces es probable la mejor manera de reducir sus p&eacute;rdidas.<br />\r\n	<br />\r\n	El Ritual Citas - Algunos consejos<br />\r\n	<br />\r\n	OK, as&iacute; que has conocido a una chica y ella ha accedido a salir con usted o vas a venir a Colombia para conocer a alguien que has conocido en l&iacute;nea. Hay algunas reglas generales a las que usted debe adherirse a fin de aumentar sus posibilidades de hacer una buena impresi&oacute;n.<br />\r\n	<br />\r\n	Colombia sigue siendo un pa&iacute;s muy conservador, y muchas chicas colombianas esperar&aacute; que usted pueda recogerla en su coche o en un taxi en su camino a un sitio (en especial las primeras citas), y lo mismo cuando se va a casa.<br />\r\n	<br />\r\n	Si la chica est&aacute; de acuerdo para cumplir all&iacute; aseg&uacute;rate de que est&aacute;s a tiempo. Si bien a menudo es aceptable para la chica para llegar 20 minutos tarde.<br />\r\n	<br />\r\n	Para la mayor&iacute;a de las mujeres en Colombia, sin embargo, ser &quot;detallista&quot; (atentos a los detalles) es decir, la apertura de puertas de autos para ella, etc es m&aacute;s importante que agasajar a ella en un restaurante de lujo. No es que me duele!<br />\r\n	<br />\r\n	La gente en Colombia se visten elegantemente cuando salen. Y la higiene es vista con una importancia a&uacute;n mayor de lo que es en Europa y Am&eacute;rica del Norte.<br />\r\n	<br />\r\n	Las chicas colombianas toman gran orgullo en su apariencia y, a menudo pasan una o dos horas prepar&aacute;ndose.<br />\r\n	<br />\r\n	Si eres un mochilero, trate de vestirse lo m&aacute;s posible y recuerda que en ciudades como Medell&iacute;n y Cali, donde el clima es perfecto para los pantalones cortos y sandalias, los locales rara vez los usan, especialmente por la noche.<br />\r\n	<br />\r\n	Cuando se trata de pagar la factura siempre se debe ofrecer a pagar. En realidad, hay muchas chicas colombianas que insisten en pagar o por lo menos hacer una contribuci&oacute;n, pero es siempre amable para ofrecer.<br />\r\n	<br />\r\n	El concepto de que el hombre debe pagar la factura puede ser a veces duro ya que en ocaciones&nbsp; ni siquiera recibe un simple &quot;Gracias&quot; al salir. Esto es algo que todav&iacute;a me molesta, pero como con muchas cosas que es un aspecto de la cultura tradicional colombiana que lo mejor es s&oacute;lo para tratar de aceptar.<br />\r\n	<br />\r\n	Mientras que algunas mujeres colombianas aceptar&aacute;n un beso en la primera cita, tradicionalmente la regla no escrita parece ser al menos la segunda, si no la tercera cita. Viniendo en demasiado fuerte en la primera cita podr&iacute;a explotar sus posibilidades por completo. Trate de leer sus se&ntilde;ales de lo mejor que pueda. Tocar la mano y luego juzgar su reacci&oacute;n puede ser una buena manera de probar el agua.<br />\r\n	<br />\r\n	Con todo, me parece que las chicas colombianas son menos estresante que salir con chicas en Gran Breta&ntilde;a. No hay tanto &eacute;nfasis en impresionar a la chica de sus carreras, estudios o sofisticaci&oacute;n.<br />\r\n	<br />\r\n	Simplemente siendo un buen tipo, caballeroso puede ser suficiente para que ella considere a la fecha un &eacute;xito.</span></p>\r\n<p>\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/Colombian_couple_beach(4).jpg" style="width: 1100px; height: 580px;" /></p>\r\n', 1, '2015-06-29 21:47:43');
INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(9, 'Foreigners looking for a Colombian wife', 'EXTRANJERO BUSCA SU ESPOSA COLOMBIANA', '<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span lang="en"><span title="Las colombianas además de ser muy bonitas, son inteligentes, comprensivas y saben cómo llevar una relación en armonía por mucho tiempo , dice Luc Amon, un francés que llegó a Colombia hace 20 años y encontró aquí a la mujer de sus sueños.\r\n\r\n">Colombians, besides being very beautiful, are intelligent, comprehensive and know how to lead a long harmonious relationship, says Luc Amon, a Frenchman who came to Colombia 20 years ago and found the woman of his dreams here.</span><br />\r\n	<br />\r\n	<span title="Me casé en mi país, pero el matrimonio no funcionó porque a las mujeres francesas el cuento de la liberación femenina les hizo olvidar también el amor y otros valores.">I was first married in my country, but the marriage did not work because as Amon explains it French women are consumed with women&#39;s liberation which made her forget the love and other values in our relationship. </span><span title="Conocí españolas, italianas, argentinas y todas resultaron poco sentimentales.">I have known Spanish, Italian and Argentine women and all were unsentimental. </span><span title="Diez años después de viajar, conocí a una bogotana con la que llevo casi 20 años de feliz matrimonio , relata Amon.\r\n\r\n">Ten years after I first started traveling, I met a girl from Bogota with whom I have now been happily married to for almost <span lang="en"><span title="Diez años después de viajar, conocí a una bogotana con la que llevo casi 20 años de feliz matrimonio , relata Amon.\r\n\r\n">20 years</span></span>, tells Amon.</span><br />\r\n	<br />\r\n	<span title="Según este francés, Colombia es un país lleno de problemas, pero también tiene gente con mucha calidad humana que hace que de este un buen lugar para vivir y perfecto si se quiere formar una familia .\r\n\r\n">According to this French man, Colombia is a country that has some problems, but with people that have great human qualities which makes this a good place to live and perfect if you want to start a family.</span><br />\r\n	<br />\r\n	<span title="Como Luc Amon existen otros extranjeros que llegaron a Colombia buscando su media naranja y la encontraron.">Like Luc Amon there are many other foreigners who travel to Colombia or search online looking for and finding their better half. </span><span title="Según las sitas de web como MiAmor.com.co,\r\n\r\n">According to websites such as MiAmor.com.co,</span><br />\r\n	<br />\r\n	<span title="Según estadísticas de las agencias , además de los que vienen a buscar a la mujer de sus sueños, llegan semanalmente 40 cartas de extranjeros que quieren información sobre las colombianas.">According to statistics from Colombian agencies, their are 40 requests weekly from foreigners who want to travel to Colombia to find the woman of their dreams. </span><span title="Después de recibir los datos solicitados, el 50 por ciento viene personalmente.">After receiving information from the agencies, 50 percent of those that requested information fly to Colombia personally. </span><span title="Otros, en menos proporción, envían mensajes clasificados a los periódicos y revistas del corazón.\r\n\r\n">The others use sights such as www.miamor.com.co to find their perfect matches before traveling to Colombia.</span><br />\r\n	<br />\r\n	<br />\r\n	<span title="Fama internacional La fama de la mujer colombiana traspasa fronteras.">The growing fame of the Colombian woman around the world has made Colombia a very appealing destination for foreigners. </span><span title="Según Nelly Campos, hace cinco años solo venían dos o tres extranjeros al año a buscar alguna colombiana, pero ahora ese número se multiplicó .\r\n\r\n">According to Nelly Campos, only five years ago there were maybe two or three foreigners a year coming to find their Colombian match, but now that number multiplied significantly.</span><br />\r\n	<br />\r\n	<span title="Según algunos extranjeros que estuvieron de paso en Colombia para entablar amistad con su posible media naranja , se arriesgan a venir a este país porque sus amigos que están casados con paisas, caleñas, costeñas o bogotanas viven muy felices y tienen familias grandes y unidas.">According to some foreigners who were traveling through Colombia looking to potentially meet their better halves, they were coming here because they had friends who had married to girls from Medellin or from Cali, Bogota&nbsp; or from the coast and live very happy lives here in Colombia and have created large families together. </span><span title="Nosotros venimos a ver si tenemos la misma suerte , dicen.\r\n\r\n">We are coming to see if we will have the same fate, they say.</span><br />\r\n	<br />\r\n	<span title="Los extranjeros que vienen al país llegan con un objetivo claro: entablar una relación seria y duradera.">Many foreigners who come to Colombia arrive with a clear objective: establish a serious and lasting relationship. </span><span title="Michael Petterson dice que yo vengo desde Reno porque quiero encontrar a esa mujer de la que hablan mis amigos, bella y cariñosa.">Michael Petterson says I have travelled from Reno because I want to find that woman that my friends speak about, beautiful and loving. </span><span title="Yo envié mis datos y me enviaron la foto de dos candidatas, ambas me gustaron voy a ver con quien hay química .\r\n\r\n">I met several girls on MiAmor and I&#39;m coming here to see with whom there is some chemistry.</span><br />\r\n	<br />\r\n	<span title="No sólo los hombres de otros países se interesan por conseguir esposa en Colombia.">Not only men from other countries are interested in getting wife in Colombia, but Colombian women </span><span title="Ellas los prefieren de otras latitudes.">prefer men from other latitudes. </span><span title="Según Gloria María Méndez , la mayoría de las mujeres que se afilian esperan encontrar a un hombre no machista ni celoso ni posesivo como el colombiano y especialmente que sea estadounidense, canadiense o australiano .\r\n\r\n">According to Gloria Maria Mendez, most women who join www.miamor.com.co do not want to find a sexist or jealous or possessive men like Colombian men and the prefer men such as those from US, Canada or Australian men.</span><br />\r\n	<br />\r\n	<span title="Méndez agrega que las mujeres no solo buscan a un marido extranjero para mejorar el nivel de vida, sino una relación sentimental, sincera, tierna, cariñosa y con los miles de adjetivos que ellas escriben en los avisos clasificados.">Mendez says women are not only seeking a foreign husband to improve living standards, but an honest, sincere, tender, loving caring man and many more things that they put in their MiAmor profile. </span><span title="La ventaja es que ellas y los extranjeros quieren familia, calor de hogar.">The advantage is that the foreigners and the Colombian women want family and a loving home. </span><span title="Eso es lo que más les gusta de las colombianas .\r\n\r\n">That&#39;s what I like best about the Colombians.</span><br />\r\n	<br />\r\n	<span title="No en vano, durante los dos últimos meses del año pasado se celebraron 46 matrimonios de argentinos, franceses, estadounidenses e italianos con colombianas.">Not surprisingly, during the last two months of 2014,&nbsp; 46 couples were married in Colombia, they were men from Argentina, France, America, Italy and many others countries marrying with Colombian women. </span><span title="La mayoría de las nuevas parejas se fueron para el lugar de origen del esposo, las otras aún permanecen en Colombia.\r\n\r\n">Most new couples moved to the country of origin of the husband,&nbsp; and the other are still in Colombia.</span><br />\r\n	<br />\r\n	<span title="Esa es la mentalidad de un extranjero y eso es lo que les agrada a las mujeres, a las cerca de quince mil que están afiliadas a las agencias.">That is the mentality of a foreigner and that&#39;s what Colombian women like, their about fifteen thousand Colombians who are affiliated with different dating agencies. </span><span title="La culpa es de los propios colombianos que no son respetuosos, y además son unos rudos.\r\n\r\n">They blame it on the Colombian men themselves who are rude and not respectful to their women</span><br />\r\n	<br />\r\n	<span title="Más que belleza No solo la belleza, la gracia y la simpatía de la mujer colombiana han inspirado a los nacionales.">More than just beauty , grace and sympathy of Colombian women have inspired nationalities. </span><span title="También se han convertido en la musa de inspiración de algunos poetas extranjeros.\r\n\r\n">They have also become the muse of inspiration from some foreign poets as well.</span><br />\r\n	<br />\r\n	<span title="Peter Mackenzee es un canadienses de 46 años que llegó al país hace uno con el fin de conocer la belleza latina, de la que tanto había oído hablar.">Peter Mackenzee is a 46 Canadian who arrived in the country to understand the Latin beauty, which he had heard so much about. </span><span title="Cuando llegué vi que ellas eran tan parecidas a las otras, que me desilusioné.">When I arrived I saw that they were very similar to others, this made me become disillusioned. </span><span title="Sin embargo, al poco tiempo empecé a conocerlas.">But soon I began to really get to know them. </span><span title="Con los poemas que les he escrito pienso publicar un libro .\r\n\r\n">With the poems I wrote I am thinking of&nbsp; publishing a book.</span><br />\r\n	<br />\r\n	<span title="Después de que uno las conoce se da cuenta que ellas son más que fama y belleza, tienen talento, son trabajadoras y saben sortear los problemas con mucha fuerza , agrega.\r\n\r\n">Once you get to know them, you realise that they are so much more than just beautiful , they have talent, are very hard workers and they are trying to overcome the problems of the country. </span><br />\r\n	<br />\r\n	<span title="Llegué a Colombia buscando a una mujer de esas de pantalla, como Inés de Hinojosa, oa una Pocahontas, pero afortunadamente no encontré a ninguna de esas.">I came to Colombia looking for a woman that I had seen on the screen, but fortunately I did not meet any of those. </span><span title="A cambio, conocí a la mujer campesina ya aquella trabajadora que lucha por sacar adelante su familia , cuenta Mackenzee Según él, las colombianas son más aterrizadas y es la clase de mujer con la que uno se quiere casar, la venezolana es para aventuras, la">In return, I met some rural women who worked hard and struggled to bring up their families, said Mackenzee According to him, the Colombians are more grounded and are the kind of woman you want to marry, Venezuelan&#39;s are for adventure, </span><span title="mexicana es linda, pero muy reprimida.">Mexicans are cute, but very repressed. </span><span title="La mujer colombiana sabe concretar las relaciones y las lleva por muy buen camino .\r\n\r\n">Colombian woman knows how to make a good relationship and have the keys for happy journey ahead.</span><br />\r\n	<br />\r\n	<span title="Según Mackenzee, he conocido muchos solterones empedernidos que apenas llegan a Colombia quieren quedarse y casarse.">According Mackenzee, I have met many singles who have come to Colombia to stay and get married. </span><span title="Ahora estoy convencido que lo mejor que tiene este país son los seres humanos .">I am now convinced that the best thing about this country are the human beings.</span></span></span></p>\r\n<p>\r\n	<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/131510177(1).jpg" style="width: 1100px; height: 734px;" /></p>\r\n', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		<br />\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Las colombianas adem&aacute;s de ser muy bonitas, son inteligentes, comprensivas y saben c&oacute;mo llevar una relaci&oacute;n en armon&iacute;a por mucho tiempo , dice Luc Amon, un franc&eacute;s que lleg&oacute; a Colombia hace 20 a&ntilde;os y encontr&oacute; aqu&iacute; a la mujer de sus sue&ntilde;os.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Me cas&eacute; en mi pa&iacute;s, pero el matrimonio no funcion&oacute; porque a las mujeres francesas el cuento de la liberaci&oacute;n femenina les hizo olvidar tambi&eacute;n el amor y otros valores. Conoc&iacute; espa&ntilde;olas, italianas, argentinas y todas resultaron poco sentimentales. Diez a&ntilde;os despu&eacute;s de viajar, conoc&iacute; a una bogotana con la que llevo casi 20 a&ntilde;os de feliz matrimonio , relata Amon.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Seg&uacute;n este franc&eacute;s, Colombia es un pa&iacute;s lleno de problemas, pero tambi&eacute;n tiene gente con mucha calidad humana que hace que de este un buen lugar para vivir y perfecto si se quiere formar una familia .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Como Luc Amon existen otros extranjeros que llegaron a Colombia buscando su media naranja y la encontraron. Seg&uacute;n las sitas de web como MiAmor.com.co,</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;"><span id="result_box" lang="es"><span class="hps">Seg&uacute;n las estad&iacute;sticas de</span> <span class="hps">las agencias</span> <span class="hps">colombianas</span><span>,</span> <span class="hps">su son</span> <span class="hps">40</span> <span class="hps">las solicitudes</span> <span class="hps">semanales</span> <span class="hps">de los extranjeros</span> <span class="hps">que quieren viajar</span> <span class="hps">a Colombia para</span> <span class="hps">encontrar a la mujer</span> <span class="hps">de sus sue&ntilde;os.</span> <span class="hps">Despu&eacute;s de recibir</span> <span class="hps">la informaci&oacute;n de las</span> <span class="hps">agencias</span><span>, el 50</span> <span class="hps">por ciento de</span> <span class="hps">los que</span> <span class="hps">solicit&oacute; informaci&oacute;n</span> <span class="hps">vuela</span> <span class="hps">a Colombia</span> <span class="hps">personalmente</span><span>.</span> <span class="hps">Los</span> <span class="hps">otros utilizan</span> <span class="hps">puntos de inter&eacute;s como</span> <span class="hps">www.miamor.com.co</span> <span class="hps">encontrar</span> <span class="hps">sus</span> <span class="hps">partidos</span> <span class="hps">perfectos</span> <span class="hps">antes de viajar a</span> <span class="hps">Colombia</span><span>.</span></span></span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Fama internacional La fama de la mujer colombiana traspasa fronteras. Seg&uacute;n Nelly Campos, hace cinco a&ntilde;os solo ven&iacute;an dos o tres extranjeros al a&ntilde;o a buscar alguna colombiana, pero ahora ese n&uacute;mero se multiplic&oacute; .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Seg&uacute;n algunos extranjeros que estuvieron de paso en Colombia para entablar amistad con su posible media naranja , se arriesgan a venir a este pa&iacute;s porque sus amigos que est&aacute;n casados con paisas, cale&ntilde;as, coste&ntilde;as o bogotanas viven muy felices y tienen familias grandes y unidas. Nosotros venimos a ver si tenemos la misma suerte , dicen.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Los extranjeros que vienen al pa&iacute;s llegan con un objetivo claro: entablar una relaci&oacute;n seria y duradera. Michael Petterson dice que yo vengo desde Reno porque quiero encontrar a esa mujer de la que hablan mis amigos, bella y cari&ntilde;osa. Yo envi&eacute; mis datos y me enviaron la foto de dos candidatas, ambas me gustaron voy a ver con quien hay qu&iacute;mica .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">No s&oacute;lo los hombres de otros pa&iacute;ses se interesan por conseguir esposa en Colombia. Ellas los prefieren de otras latitudes. Seg&uacute;n Gloria Mar&iacute;a M&eacute;ndez , la mayor&iacute;a de las mujeres que se afilian esperan encontrar a un hombre no machista ni celoso ni posesivo como el colombiano y especialmente que sea estadounidense, canadiense o australiano .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">M&eacute;ndez agrega que las mujeres no solo buscan a un marido extranjero para mejorar el nivel de vida, sino una relaci&oacute;n sentimental, sincera, tierna, cari&ntilde;osa y con los miles de adjetivos que ellas escriben en MiAmor.com.co . La ventaja es que ellas y los extranjeros quieren familia, calor de hogar. Eso es lo que m&aacute;s les gusta de las colombianas .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">No en vano, durante los dos &uacute;ltimos meses del a&ntilde;o pasado se celebraron 46 matrimonios de argentinos, franceses, estadounidenses e italianos con colombianas. La mayor&iacute;a de las nuevas parejas se fueron para el lugar de origen del esposo, las otras a&uacute;n permanecen en Colombia.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Esa es la mentalidad de un extranjero y eso es lo que les agrada a las mujeres, a las cerca de quince mil que est&aacute;n afiliadas a las agencias. La culpa es de los propios colombianos que no son respetuosos, y adem&aacute;s son unos rudos.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">M&aacute;s que belleza No solo la belleza, la gracia y la simpat&iacute;a de la mujer colombiana han inspirado a los nacionales. Tambi&eacute;n se han convertido en la musa de inspiraci&oacute;n de algunos poetas extranjeros.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Peter Mackenzee es un canadienses de 46 a&ntilde;os que lleg&oacute; al pa&iacute;s hace uno con el fin de conocer la belleza latina, de la que tanto hab&iacute;a o&iacute;do hablar. Cuando llegu&eacute; vi que ellas eran tan parecidas a las otras, que me desilusion&eacute;. Sin embargo, al poco tiempo empec&eacute; a conocerlas. Con los poemas que les he escrito pienso publicar un libro .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Despu&eacute;s de que uno las conoce se da cuenta que ellas son m&aacute;s que fama y belleza, tienen talento, son trabajadoras y saben sortear los problemas con mucha fuerza.</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Llegu&eacute; a Colombia buscando a una mujer de esas de pantalla, como In&eacute;s de Hinojosa, o a una Pocahontas, pero afortunadamente no encontr&eacute; a ninguna de esas. A cambio, conoc&iacute; a la mujer campesina y a aquella trabajadora que lucha por sacar adelante su familia , cuenta Mackenzee Seg&uacute;n &eacute;l, las colombianas son m&aacute;s aterrizadas y es la clase de mujer con la que uno se quiere casar, la venezolana es para aventuras, la mexicana es linda, pero muy reprimida. La mujer colombiana sabe concretar las relaciones y las lleva por muy buen camino .</span></p>\r\n	<p>\r\n		<span style="font-family:tahoma,geneva,sans-serif;">Seg&uacute;n Mackenzee, he conocido muchos solterones empedernidos que apenas llegan a Colombia quieren quedarse y casarse. Ahora estoy convencido que lo mejor que tiene este pa&iacute;s son los seres humanos .</span></p>\r\n	<p>\r\n		<img alt="" src="/home/team3nits/public_html/ckeditortest_WORKING/ckfinder/userfiles/images/131510177.jpg" style="width: 1100px; height: 734px;" /></p>\r\n</div>\r\n', 1, '2015-06-29 21:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_bodytype`
--

CREATE TABLE IF NOT EXISTS `dateing_bodytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_booking`
--

CREATE TABLE IF NOT EXISTS `dateing_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `price` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `dateing_booking`
--

INSERT INTO `dateing_booking` (`id`, `title`, `description`, `price`) VALUES
(1, 'abarth', 'abarth services', '233');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_carmakers`
--

CREATE TABLE IF NOT EXISTS `dateing_carmakers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `dateing_carmakers`
--

INSERT INTO `dateing_carmakers` (`id`, `user_id`, `name`, `description`, `image`, `status`) VALUES
(1, 0, 'Toyota', 'Toyota', 'cc1.jpg', 0),
(2, 0, 'Nisan', 'Nisan', 'cc2.jpg', 0),
(3, 0, 'Honda', 'Honda', 'cc3.jpg', 0),
(4, 0, 'Mitsubisi', 'Mitsubisi', 'cc6.jpg', 0),
(5, 0, 'Volkswagen', 'Volkswagen', 'cc8.jpg', 0),
(6, 0, 'BMW', 'BMW', 'cc5.jpg', 0),
(7, 0, 'Suzuki', 'Suzuki', 'cc9.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_carmodels`
--

CREATE TABLE IF NOT EXISTS `dateing_carmodels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `dateing_carmodels`
--

INSERT INTO `dateing_carmodels` (`id`, `cat_id`, `name`, `description`, `image`, `status`) VALUES
(8, 2, 'Micra', 'Micra', '', 0),
(9, 7, 'Swift LXI', 'Swift LXI', '', 0),
(10, 7, 'Swift VXI', 'Swift VXI', '', 0),
(11, 7, 'Swift Desire', 'Swift Desire', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_car_moreimage`
--

CREATE TABLE IF NOT EXISTS `dateing_car_moreimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `pro_id` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `dateing_car_moreimage`
--

INSERT INTO `dateing_car_moreimage` (`id`, `title`, `pro_id`, `image`) VALUES
(1, 'dfgdgdfg', '1', '1407705466listing-2.jpg'),
(2, 'Best property', '1', '1407705545listing-1.jpg'),
(3, 'bmw', '2', '1407744491bmw-x6-red-car-awesome-luxury-motion-road-trees-visualrich.jpg'),
(4, 'bmw', '2', '1407744534bmw-x6-red-car-awesome-luxury-motion-road-trees-visualrich.jpg'),
(5, 'bmw', '3', '1407744754bmw-2-series-active-tourer-m-sport-angle-interior.jpg'),
(6, 'bmw', '3', '1407744840bmw-2-series-interior-2.jpg'),
(7, 'liva', '8', '1407744944toyota_etios_liva_trd-16.jpg'),
(8, 'liva', '8', '14077449773509238607_62603f302c_thumb.jpg'),
(9, 'innova', '9', '1407745055toyota-innova-interior.jpg'),
(10, 'innova', '9', '1407745096Toyota-Innova-interior1.jpg'),
(11, 'Nissan', '10', '1407745223nissan-pathfinder-concept-interior.jpg'),
(12, 'Nissan', '10', '14077453722014_nissan_rogue_interior_pictures.jpg'),
(13, 'Nissan', '11', '1407745492Nissan-Evalia-210.jpg'),
(14, 'Nissan', '11', '1407745531nissan_evalia-int.jpg'),
(15, 'Suzuki Car', '12', '1407745627Suzuki_Ionis_concept_car_interior.jpg'),
(16, 'Suzuki Car', '12', '1407745658suzuki_sxbox_interior_00.jpg'),
(17, 'Suzuki Car', '13', '1407745720suzuki-swift-interior.jpg'),
(18, 'Suzuki Car', '13', '1407745764New Swift interior.JPG'),
(19, 'Honda', '14', '1407745845Honda-City-Interior2.jpg'),
(20, 'Honda', '14', '1407745880HONDA-CITY-CABIN.jpg'),
(21, 'Honda Suv', '15', '1407745958112_0806_20z+2008_honda_crv+interior_view.jpg'),
(22, 'Honda Suv', '15', '14077459842008-Honda-CR-V-EX-L-i05.jpg'),
(23, 'Mitsubishi', '16', '14077460552007-mitsubishi-outlander12.jpg'),
(24, 'Mitsubishi', '16', '1407746107mitsubishi-outlander-3.jpg'),
(25, 'Mitsubishi', '17', '1407746221mitsubishi-prototype-i-miev-interior-photo-372902-s-1280x782.jpg'),
(26, 'Mitsubishi', '17', '1407746280Mitsubishi-Outlander-Interior.jpg'),
(27, 'The New Polo', '18', '1407746362New-Polo-Interior-1.jpg'),
(28, 'The New Polo', '18', '1407746397volkswagen-polo-mkv-2009-official-interior-img_17.jpg'),
(29, 'Passat', '19', '14077464702012-volkswagen-passat-tdi-interior-1.jpg'),
(30, 'Passat', '19', '14077465212013_volkswagen_passat-pic-4969656232301200496.png'),
(41, 'bmw', '2', '1409836421-Chrysanthemum.jpg'),
(42, 'bmw', '2', '1409836421-Desert.jpg'),
(43, 'bmw', '2', '1409836421-Koala.jpg'),
(44, 'bmw', '2', '1409836421-Lighthouse.jpg'),
(49, '', '36', '1410324786Apple_Friends.gif'),
(50, '', '36', '1410324786kindapr4.jpg'),
(51, '', '36', '1410324786KindergartenNewsHeader.jpg'),
(52, '', '36', '1410324786news23.gif'),
(53, '', '37', '1410327261Hydrangeas.jpg'),
(54, '', '37', '1410327261Jellyfish.jpg'),
(55, '', '37', '1410327261Tulips.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_category`
--

CREATE TABLE IF NOT EXISTS `dateing_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `dateing_category`
--

INSERT INTO `dateing_category` (`id`, `user_id`, `name`, `description`, `image`, `status`) VALUES
(5, 0, 'Health & Beauty', 'Health & Beauty  Description goes here', 'item_3.png', 1),
(8, 0, 'Home Services', 'Home Services Description goes here', 'item_5.png', 0),
(26, 0, 'Lorem ipsum dolor sit amet consectetur', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'img2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_cms`
--

CREATE TABLE IF NOT EXISTS `dateing_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `pagedetail` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `dateing_cms`
--

INSERT INTO `dateing_cms` (`id`, `pagename`, `title`, `title_translated`, `pagedetail`, `description_translated`, `image`) VALUES
(1, 'About Us', 'About Us', 'sobre Nosotros', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor was created to provide a service to enable foreigners to connect with Colombians. It was created by American Dean Moriarty who fell in love with a Colombian. &quot; a friend and I were venturing through Colombia, speaking little if no Spanish and I met my wife in a lotto shop in Cali, I know that&nbsp; it was a lotto shop now but at the time I thought it was a money change spot. After trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventually started talking over google translate&quot; the rest is history and 8 years on Dean and Cindy are happily married. Dean has sung the praises of Colombian women to all that will listen. &quot; I wanted to help others that may not have the opportunity to travel to Colombia yet or who wanted to make connections first before coming here to this beautiful country&quot;.</span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Dean has been working on MiAmor for a long time, he and Cindy have tried to create a service that will really help connect foreigners with Colombians, It is the only such website that is actually based in Colombia, for Colombians and those that want to meet Colombians.<br />\r\n	<br />\r\n	&quot;I wanted to create a very simple to use service, that included lots of ways to connect and show interest in the person of interest, you can flirt, befriend, message, chat, translation chat or video chat, as well as you can search singles on the go with the MiAmor App&quot;<br />\r\n	<br />\r\n	MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.<br />\r\n	<br />\r\n	MiAmor is Colombia and Colombia is MiAmor</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span title="MiAmor was created to provide a service to enable foreigners to connect with Colombians.">Miamor fue creado para proporcionar un servicio que permitira que los extranjeros se conecten con los colombianos. </span><span title="It was created by American Dean Moriarty who fell in love with a Colombian.">Fue creado por el estadounidense Dean Moriarty, que se enamor&oacute; de una colombiana. </span><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span title="Dean has sung the praises of Colombian women to all that will listen.">Dean ha cantado las alabanzas de las mujeres colombianas a todos los que quieran escuchar. </span><span .="" beautiful="" before="" colombia="" coming="" connections="" first="" have="" help="" here="" i="" make="" may="" not="" opportunity="" or="" others="" that="" the="" this="" title="" to="" travel="" wanted="" who="" yet="">&quot;Yo quer&iacute;a ayudar a otros que aun no han tenido la oportunidad de viajar a Colombia o que quieren hacer conexiones primero antes de venir aqu&iacute; a este hermoso pa&iacute;s&quot;.</span></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span title="Dean has been working on MiAmor for a long time, he and Cindy have tryed to create a service that will really help connect foreigners with Colombians, It is the only such website that is actualy based in Colombia, for Colombians and those that want to meet">Dean ha estado trabajando en miamor durante mucho tiempo, &eacute;l y Cindy estan tratando de crear un servicio que realmente ayude a conectar a los extranjeros con los colombianos, es el &uacute;nico sitio web que existe actualmente en Colombia, para las colombianas y los que quieren conocer </span><span title="Colombians.\r\n\r\n">colombianos.</span><br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<span a="" and="" as="" buscar="" can="" chat="" como="" conectar="" connect="" crear="" create="" de="" el="" en="" i="" in="" included="" interest="" la="" lots="" maneras="" mostrar="" muy="" n="" o="" of="" or="" person="" persona="" puede="" que="" s="" se="" sencillo="" show="" simple="" span="" that="" the="" title="" to="" translation="" un="" use="" usted="" utilizar="" very="" video="" wanted="" ways="" well="" y="" you=""><span app="" con="" individuales="" la="" marcha="" miamor="" sobre="" span="" title="singles on the go with the MiAmor App"> <span title="MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.\r\n\r\n">Miamor tiene una enorme base de datos de mujeres solteras colombianos que buscan conocer hombres extranjeros y una base de datos cada vez mayor de extranjeros que quieren encontrar a alguien especial en Colombia.</span></span></span></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span a="" and="" as="" buscar="" can="" chat="" como="" conectar="" connect="" crear="" create="" de="" el="" en="" i="" in="" included="" interest="" la="" lots="" maneras="" mostrar="" muy="" n="" o="" of="" or="" person="" persona="" puede="" que="" s="" se="" sencillo="" show="" simple="" span="" that="" the="" title="" to="" translation="" un="" use="" usted="" utilizar="" very="" video="" wanted="" ways="" well="" y="" you=""><span app="" con="" individuales="" la="" marcha="" miamor="" sobre="" span="" title="singles on the go with the MiAmor App"><span title="MiAmor is Colombia and Colombia is MiAmor">Miamor es Colombia y Colombia es miamor</span></span></span></span></span></span></p>\r\n', ''),
(5, 'Terms', 'Terms And Condition', 'Términos y Condiciones', '<p>\r\n	Terms And Condition</p>\r\n', '<p>\n	<span class="short_text" id="result_box" lang="es" tabindex="-1"><span class="hps">T&eacute;rminos y</span> <span class="hps">Condiciones</span></span></p>\n', ''),
(6, 'Privacy Policy', 'Privacy Policy', 'política de privacidad', '<p>\r\n	Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>\r\n', '<p>\n	<span id="result_box" lang="es" tabindex="-1"><span class="hps">Lorem</span> <span class="hps">ipsum</span> <span class="hps">dolor sit</span> <span class="hps">amet</span><span>,</span> <span class="hps">elit</span> <span class="hps">nisi nec purus</span><span>, sed</span> <span class="hps">diam</span> <span class="hps">nonummy</span> <span class="hps">euismod</span> <span class="hps">NIBH</span> <span class="hps">tincidunt</span> <span class="hps">ut</span> <span class="hps">laoreet</span> <span class="hps">dolore</span> <span class="hps">magna</span> <span class="hps">aliquam</span> <span class="hps">erat</span> <span class="hps">volutpat</span><span>.</span> <span class="hps">Ut</span> <span class="hps">wisi</span> <span class="hps">enim</span> <span class="hps">ad</span> <span class="hps">minim</span> <span class="hps">veniam</span><span>,</span> <span class="hps">quis</span> <span class="hps">ullamcorper</span> <span class="hps">taci&oacute;n</span> <span class="hps">nostrud</span> <span class="hps">exerci</span> <span class="hps">lobortis</span> <span class="hps">suscipit</span> <span class="hps">nisl</span> <span class="hps">aliquip</span> <span class="hps">ut</span> <span class="hps">commodo</span> <span class="hps">ex</span> <span class="hps">ea</span> <span class="hps">consequat</span><span>.</span> <span class="hps">DUIS</span> <span class="hps">autem</span> <span class="hps">vel</span> <span class="hps">eum</span> <span class="hps">iriure</span> <span class="hps">dolor</span> <span class="hps">en</span> <span class="hps">hendrerit</span> <span class="hps">en</span> <span class="hps">vulputate</span> <span class="hps">Velit</span> <span class="hps">esse</span> <span class="hps">molestie</span> <span class="hps">consequat</span><span>, vel</span> <span class="hps">illum</span> <span class="hps">dolore</span> <span class="hps">eu</span> <span class="hps">nulla</span> <span class="hps">feugiat</span> <span class="hps">facilisis</span> <span class="hps">a</span> <span class="hps">vero</span> <span class="hps">eros</span> <span class="hps">et</span> <span class="hps">accumsan</span> <span class="hps">et</span> <span class="hps">iusto</span> <span class="hps">odio</span> <span class="hps">dignissim</span> <span class="hps">qui</span> <span class="hps">blandit</span> <span class="hps">praesent</span> <span class="hps">luptatum</span> <span class="hps">zzril</span> <span class="hps">delenit</span> <span class="hps">augue</span> <span class="hps">DUIS</span> <span class="hps">dolore</span> <span class="hps">te</span> <span class="hps">feugait</span> <span class="hps">nulla</span> <span class="hps">facilisi</span><span>.</span> <span class="hps">Nam</span> <span class="hps">liber</span> <span class="hps">tempor</span> <span class="hps">cum</span> <span class="hps">soluta</span> <span class="hps">nobis</span> <span class="hps">eleifend</span> <span class="hps">opci&oacute;n</span> <span class="hps">nihil</span> <span class="hps">congue</span> <span class="hps">doming</span> <span class="hps">imperdiet</span> <span class="hps">id quod</span> <span class="hps">Mazim</span> <span class="hps">placerat</span> <span class="hps">ASSUM</span> <span class="hps">facer</span> <span class="hps">possim</span><span>.</span> <span class="hps">TYPI</span> <span class="hps">insitam</span> <span class="hps">claritatem</span> <span class="hps">no</span> <span class="hps">habent</span><span>;</span> <span class="hps">est</span> <span class="hps">usus</span> <span class="hps">legentis</span> <span class="hps">en IIS</span> <span class="hps">qui</span> <span class="hps">facit</span> <span class="hps">eorum</span> <span class="hps">claritatem</span><span>.</span> <span class="hps">Investigationes</span> <span class="hps">demonstraverunt</span> <span class="hps">Lectores</span> <span class="hps">legere</span> <span class="hps">me</span> <span class="hps">lius</span> <span class="hps">quod</span> <span class="hps">saepius</span> <span class="hps">legunt</span> <span class="hps">ii</span><span>.</span> <span class="hps">Claritas</span> <span class="hps">est</span> <span class="hps">etiam</span> <span class="hps">processus</span> <span class="hps">dynamicus</span><span>,</span> <span class="hps">qui</span> <span class="hps">sequitur</span> <span class="hps">mutationem</span> <span class="hps">consuetudium</span> <span class="hps">Lectorum</span><span>.</span> <span class="hps">Mirum</span> <span class="hps">est</span> <span class="hps">quam</span> <span class="hps">Notare</span> <span class="hps">littera</span> <span class="hps">gothica</span><span>,</span> <span class="hps">quam</span> <span class="hps">nunc</span> <span class="hps">putamus</span> <span class="hps">falciparum</span> <span class="hps">claram</span><span>,</span> <span class="hps">anteposuerit</span> <span class="hps">litterarum</span> <span class="hps">Formas</span> <span class="hps">humanitatis</span> <span class="hps">por</span> <span class="hps">seacula</span> <span class="hps">quarta</span> <span class="hps">d&eacute;cima</span> <span class="hps">quinta</span> <span class="hps">y</span> <span class="hps">d&eacute;cima</span><span>.</span> <span class="hps">Eodem</span> <span class="hps">Modo,</span> <span class="hps">qui</span> <span class="hps">nunc</span> <span class="hps">nobis</span> <span class="hps">videntur</span> <span class="hps">Parum</span> <span class="hps">aclaraciones</span><span>,</span> <span class="hps">sollemnes</span> <span class="hps">Fiant</span> <span class="hps">tipificaciones</span> <span class="hps">en</span> <span class="hps">futurum</span><span>.</span></span></p>\n', ''),
(7, 'How It Works', 'How It Works', '¿Cómo funciona?', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor connects singles from around the world with Colombians, it is as simple as <span style="font-size: 16px;">1,2,3</span> creating a profile, searching for your ideal match and then contacting the person that has caught your eye.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor understands that there may be a language barrier between some new matches, so we have installed a fully functional Chat translation service that is able to be used by all members. There are other ways to contact each other also from flirtatious winks and kisses, to sending messages, Chat, and also video Chat for those that really want to connect.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor is the vehicle to enable you to find that special person and connnect with them.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Good luck</p>\r\n', '<div id="gt-src-tools">\r\n	<div id="gt-src-tools-l">\r\n		<div id="gt-input-tool" style="display: inline-block;">\r\n			<div id="itamenu">\r\n				&nbsp;</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class="almost_half_cell" id="gt-res-content">\r\n	<div dir="ltr" style="zoom:1">\r\n		<span id="result_box" lang="es"><span class="hps">Miamor</span> <span class="hps">conecta</span> <span class="hps">a los solteros</span><span class="hps"> alrededor del</span> <span class="hps">mundo con</span> <span class="hps">los colombianos</span><span>, es</span> <span class="hps">tan simple como <span style="font-size:16px;">1,2,3 </span>crear</span> <span class="hps">un perfil</span><span>, en busca de</span> <span class="hps">tu partido ideal</span> <span class="hps">y luego</span> <span class="hps">ponerse en contacto con</span> <span class="hps">la persona</span> <span class="hps">que mas te ha llamado</span> <span class="hps">la atencion.</span> </span></div>\r\n	<div dir="ltr" style="zoom:1">\r\n		&nbsp;</div>\r\n	<div dir="ltr" style="zoom:1">\r\n		<span lang="es"><span class="hps">Miamor</span> <span class="hps">entiende</span> <span class="hps">de que puede haber</span> <span class="hps">una barrera</span>&nbsp; con el <span class="hps">lenguaje entre</span> <span class="hps">algunas nuevas</span> <span class="hps">partidos</span><span>, as&iacute; que nos</span> <span class="hps">han</span> <span class="hps">instalado un</span> <span class="hps">servicio de traducci&oacute;n al</span> <span class="hps">Chat en</span> <span class="hps">totalmente funcional que</span> <span class="hps">es</span> <span class="hps">capaz de ser utilizado</span> <span class="hps">por todos los miembros</span><span>, hay otras</span> <span class="hps">formas de contactar</span> <span class="hps">cada uno</span> con el <span class="hps">otro tambi&eacute;n</span> <span class="hps">a partir</span> <span class="hps">gui&ntilde;os</span> <span class="hps">silvidos</span> <span class="hps">y besos</span><span>,</span> <span class="hps">por</span> <span class="hps">env&iacute;o de mensajes</span><span>,</span> <span class="hps">Chatea</span><span>, y</span> <span class="hps">tambi&eacute;n</span> <span class="hps">v&iacute;deo Chat</span> <span class="hps">para aquellos que quieren</span>&nbsp;<span class="hps"> conectarse</span> <span class="hps">realmente</span><span>.</span></span></div>\r\n	<div dir="ltr" style="zoom:1">\r\n		<br />\r\n		<br />\r\n		<span lang="es"><span class="hps">Miamor</span> <span class="hps">es el vinculo para</span><span> activarlos a</span> <span class="hps">ustedes para encontrar</span> <span class="hps">esa persona</span> <span class="hps">especial y</span> <span class="hps">Connnect</span>aerse <span class="hps">con ellos</span><span>.</span></span></div>\r\n	<div dir="ltr" style="zoom:1">\r\n		<br />\r\n		&nbsp;</div>\r\n	<div dir="ltr" style="zoom:1">\r\n		<span lang="es"><span class="hps">Buena suerte</span></span></div>\r\n</div>\r\n', ''),
(8, 'Tell Us', 'Tell Us', 'Cuéntenos', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">If you have a question, thought, or a suggestion we&#39;d love to hear from you, we want to do everything we can to make your experiance on MiAmor a successful and pleasurable one.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Please include what you do or don&rsquo;t like about our website, thoughts about experiences you&rsquo;ve had meeting singles on MiAmor or let us know services that you would like to see. Your feedback provides us with valuable information to enable us to make your experiance here even more enjoyable.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Thanks</span></p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span class="hps">Si</span> <span class="hps">usted tiene alguna pregunta</span><span>, pensamiento,</span> <span class="hps">o</span> <span class="hps">una sugerencia</span><span>, nos encantar&iacute;a</span> <span class="hps">saber de usted</span><span>, queremos</span> <span class="hps">hacer todo lo</span> <span class="hps">posible para que su</span> <span class="hps">experiancia</span> <span class="hps">en</span> <span class="hps">miamor</span> <span class="hps">sea un &eacute;xito</span> <span class="hps">y sea placentera.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span class="hps">Por favor </span><span class="hps">incluya</span> <span class="hps">lo que le gusta o</span> <span class="hps">no le gusta de</span> <span class="hps">nuestro sitio web</span><span>, los pensamientos</span> <span class="hps">acerca de las experiencias</span> <span class="hps">que has tenido</span> <span class="hps">con las reuniones</span> <span class="hps">individuales</span> <span class="hps">en</span> <span class="hps">miamor</span> <span class="hps">o</span> <span class="hps">h&aacute;ganos saber</span> <span class="hps">los servicios</span> <span class="hps">que le gustar&iacute;a</span> <span class="hps">ver.</span> <span class="hps">Sus comentarios</span> <span class="hps">nos proporciona</span>n <span class="hps">informaci&oacute;n valiosa</span> <span class="hps">que nos permitira</span> <span class="hps">hacer su</span> <span class="hps">experiancia</span>&nbsp;<span class="hps">a&uacute;n m&aacute;s agradable</span><span> aqui.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span class="hps">gracias</span></span></p>\r\n', ''),
(9, 'Sms content', 'Sms content', 'Sms content', '<p>\r\n	MiAmor test,&nbsp;</p>\r\n', '<p>\r\n	New text</p>\r\n', ''),
(10, 'GET IN TOUCH', 'GET IN TOUCH', 'GET IN TOUCH', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Let us know what you think, we want to hear from you ?</span></p>\r\n', '<div class="almost_half_cell" id="gt-res-content">\r\n	<div dir="ltr" style="zoom:1">\r\n		<span class="short_text" id="result_box" lang="es"><span class="hps">H&aacute;ganos saber</span> <span class="hps">lo que piensa,</span> <span class="hps">queremos</span> <span class="hps">saber de ti</span><span>?</span></span></div>\r\n</div>\r\n', ''),
(11, 'Advertise', 'Advertise', 'anunciar', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">If you have a question, thought, or a suggestion we&#39;d love to hear from you, we want to do everything we can to make your experiance on MiAmor a successful and pleasurable one.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Please include what you do or don&rsquo;t like about our website, thoughts about experiences you&rsquo;ve had meeting singles on MiAmor or let us know services that you would like to see. Your feedback provides us with valuable information to enable us to make your experiance here even more enjoyable.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Thanks</span></p>\r\n', '<p>\r\n	If you have a question, thought, or a suggestion we&#39;d love to hear from you, we want to do everything we can to make your experiance on MiAmor a successful and pleasurable one.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Please include what you do or don&rsquo;t like about our website, thoughts about experiences you&rsquo;ve had meeting singles on MiAmor or let us know services that you would like to see. Your feedback provides us with valuable information to enable us to make your experiance here even more enjoyable.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Thanks</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_countries`
--

CREATE TABLE IF NOT EXISTS `dateing_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dateing_countries`
--

INSERT INTO `dateing_countries` (`id`, `name`) VALUES
(1, 'White/Caucasian'),
(2, 'Latino/Hispanic'),
(3, 'Black/African'),
(4, 'Indian'),
(5, 'Asian'),
(6, 'Middle Eastern'),
(7, 'Pacific Islander'),
(8, 'Native American'),
(9, 'Mixed/Other');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_ethnicity`
--

CREATE TABLE IF NOT EXISTS `dateing_ethnicity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ethnicity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `dateing_ethnicity`
--

INSERT INTO `dateing_ethnicity` (`id`, `ethnicity`) VALUES
(1, 'American'),
(2, 'African'),
(3, 'Asian'),
(4, 'European'),
(5, 'Latino'),
(6, 'Indian'),
(7, 'Oceania'),
(8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_favourite`
--

CREATE TABLE IF NOT EXISTS `dateing_favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `to_id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Dumping data for table `dateing_favourite`
--

INSERT INTO `dateing_favourite` (`id`, `date`, `to_id`, `from_id`, `status`) VALUES
(29, '2014-11-07 16:17:25', 5, 1, 0),
(30, '2014-11-14 01:28:08', 7, 1, 0),
(31, '2014-11-20 00:11:11', 8, 1, 1),
(33, '2014-12-05 01:48:06', 6, 1, 0),
(36, '2014-12-11 04:05:35', 4, 1, 1),
(37, '2014-12-11 16:37:40', 5, 1, 0),
(40, '2014-12-15 02:21:46', 3, 1, 0),
(41, '2015-02-05 01:53:53', 11, 1, 1),
(42, '2015-02-05 03:33:22', 5, 1, 0),
(43, '2015-02-05 03:33:49', 5, 1, 1),
(44, '2015-02-05 03:48:08', 3, 1, 1),
(45, '2015-02-05 03:52:54', 1, 5, 1),
(46, '2015-02-27 18:59:09', 6, 1, 1),
(47, '2015-05-07 10:32:25', 22, 1, 1),
(48, '2015-05-07 10:33:05', 41, 1, 1),
(49, '2015-05-07 10:33:28', 41, 22, 1),
(50, '2015-06-30 12:12:40', 26, 5, 1),
(51, '2015-06-30 12:14:03', 78, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_homecms`
--

CREATE TABLE IF NOT EXISTS `dateing_homecms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `title_translated` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pagedetail` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `dateing_homecms`
--

INSERT INTO `dateing_homecms` (`id`, `pagename`, `title`, `title_translated`, `pagedetail`, `description_translated`, `image`) VALUES
(1, 'how_it_works', 'HOW IT WORKS', 'COMO FUNCIONA', '<p>\r\n	3 simple steps to start connecting with singles</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\n<span id="result_box" lang="es"><span class="hps">3</span> <span class="hps">pasos sencillos</span> <span class="hps">para empezar a</span> <span class="hps">conectar</span> <span class="hps">con gente soltera</span></span>\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(5, 'create_profile', 'Create profile', 'crear perfil', '<p>\r\n	Describe yourself, let others no your qualities, add photos, a video if you wish, and tell us about you and who your ideal partner would be</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-medium" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\n<span id="result_box" lang="es"><span class="hps">Describa</span> <span class="hps">a ti mismo</span><span>,</span> <span class="hps">que los dem&aacute;s</span> <span class="hps">no</span> <span class="hps">sus</span> <span class="hps">cualidades</span><span>,</span> <span class="hps">a&ntilde;adir fotos</span><span>,</span> <span class="hps">un video</span><span>, si</span> <span class="hps">lo desea,</span> <span class="hps">y nos dicen</span> <span class="hps">acerca de usted</span> <span class="hps">y qui&eacute;n</span> <span class="hps">ser&iacute;a</span> <span class="hps">tu pareja ideal</span></span>\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(6, 'create_section', 'Create selections', 'crear selecciones', '<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Who and what are you looking for? &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Create selections based on the type of relationship you want and your ideal partner</p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span class="hps">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &iquest;Qui&eacute;n y qu&eacute;</span> <span class="hps">es lo que buscas</span><span>?</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; <span class="hps">Crear</span> <span class="hps">selecciones basadas en</span> <span class="hps">el tipo de</span> <span class="hps">relaci&oacute;n que desea</span> <span class="hps">y</span> <span class="hps">tu pareja ideal</span></span></p>\r\n', ''),
(7, 'contact_section', 'Contact your selections', 'contacte con sus selecciones', '<p>\r\n	Flirt&nbsp; by sending winks and kisses with those you like, befriend,Chat, Video Chat, full translation service offered</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 96px;">\r\n<span id="result_box" lang="es"><span class="hps">Liga</span> <span class="hps">enviando</span> <span class="hps">gui&ntilde;os y</span> <span class="hps">besos con</span> <span class="hps">los que</span> <span class="hps">se quiere,</span> <span class="hps">amistad</span><span>,</span> <span class="hps">chat</span><span>,</span> <span class="hps">video chat</span><span>, servicio de traducci&oacute;n</span> <span class="hps">completa</span> <span class="hps">ofreci&oacute;</span></span>\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(8, 'find_match', 'Find your perfect match with miamor apps', 'Encontrar pareja perfecta con aplicaciones miamor', '<p>\r\n	<span class="short_text" id="result_box" lang="es">Search your connections on the go </span></p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 48px;">\r\n<span class="short_text" id="result_box" lang="es"><span class="hps">Buscar</span> <span class="hps">sus conexiones</span> <span class="hps">en el camino</span></span>\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(9, 'about_miamor', 'miamor-our stories', 'miamor-nuestras historias', '<p>\r\n	Our members share their stories with you</p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span class="hps">Nuestros</span> <span class="hps">miembros comparten</span> <span class="hps">sus historias</span> <span class="hps">con usted</span></span></p>\r\n', ''),
(10, 'get_in_touch', 'GET IN TOUCH', 'PONTE EN CONTACTO', '<p>\r\n	We want to hear from you</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-medium" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\n<span class="short_text" id="result_box" lang="es"><span class="hps">Queremos</span> <span class="hps">saber de usted</span></span>\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(11, 'find_partners', 'Connect with singles for fun, dating and long term relationships', 'Conecte con sencillos para la diversión, citas y relaciones de largo plazo', '<p>\r\n	Start chatting and getting to know compatible partners now</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\n<span id="result_box" lang="es"><span class="hps">Empezar a chatear</span> <span class="hps">y conocer</span> <span class="hps">socios compatibles</span> <span class="hps">ahora</span></span>\r\n\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_homeimages`
--

CREATE TABLE IF NOT EXISTS `dateing_homeimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms' AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dateing_homeimages`
--

INSERT INTO `dateing_homeimages` (`id`, `pagename`, `image`) VALUES
(1, 'background1', '1_1423820363.jpg'),
(5, 'create_profile', '5_1423820965.png'),
(6, 'create_section', '6_1423821200.png'),
(7, 'contact_section', '7_1423821226.png'),
(8, 'google_play', '8_1423821601.png'),
(9, 'about_miamor', '9_1423821263.png'),
(11, 'miamor_app', '11_1423821643.png'),
(12, 'background2', '12_1423820636.jpg'),
(13, 'background3', '13_1423820897.jpg'),
(14, 'background4', '14_1423820945.jpg'),
(15, 'app_store', '15_1423821659.png');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_individual_adds`
--

CREATE TABLE IF NOT EXISTS `dateing_individual_adds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `s_id` varchar(30) NOT NULL,
  `startingslot` int(11) NOT NULL,
  `multiple` int(11) NOT NULL,
  `size` varchar(100) NOT NULL,
  `u_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `dateing_individual_adds`
--

INSERT INTO `dateing_individual_adds` (`id`, `p_id`, `s_id`, `startingslot`, `multiple`, `size`, `u_id`, `image`, `start_date`, `active`) VALUES
(17, 3, '1', 1, 0, '1', 4, '1419401217.jpg', '2014-12-24 13:06:57', 1),
(18, 3, '2,3', 2, 1, '2', 9, '1418910753.jpg', '2014-12-18 20:52:33', 1),
(19, 4, '1,2,3', 1, 1, '3', 6, '1418911062.jpg', '2015-06-09 14:43:17', 1),
(20, 5, '1,2,3', 1, 1, '3', 4, '1419419561.JPG', '2014-12-24 18:12:41', 1),
(21, 21, '1', 1, 0, '1', 4, '1419400971.jpg', '2014-12-24 13:02:51', 1),
(22, 22, '1', 1, 0, '1', 4, '1418914376.jpeg', '2014-12-18 21:52:56', 1),
(23, 17, '2', 2, 0, '1', 4, '1418973861.jpeg', '2014-12-19 14:24:21', 1),
(24, 18, '1', 1, 0, '1', 4, '1418973988.jpeg', '2014-12-19 14:26:28', 1),
(25, 19, '3', 3, 0, '1', 4, '1418974267.jpg', '2014-12-19 14:31:07', 1),
(26, 20, '4,5', 4, 1, '2', 4, '1418974303.jpg', '2014-12-19 14:31:43', 1),
(27, 23, '3', 3, 0, '1', 4, '1418974336.jpg', '2014-12-19 14:32:16', 1),
(28, 2, '1,2,3,4', 1, 0, '4', 4, '1428478158.png', '2015-04-08 07:29:18', 1),
(38, 14, '3', 3, 0, '1', 4, '', '2014-12-19 20:11:53', 1),
(39, 14, '1,2', 1, 1, '2', 4, '1419403544.jpg', '2014-12-24 13:45:44', 1),
(40, 13, '2,3,4', 2, 1, '3', 4, '', '2014-12-19 20:24:11', 1),
(41, 10, '1,2,3', 1, 1, '3', 4, '1419403204.jpg', '2014-12-24 19:41:16', 1),
(42, 13, '1', 1, 0, '1', 4, '', '2014-12-22 07:01:41', 1),
(43, 7, '1', 1, 0, '1', 4, '1419418903.jpg', '2014-12-24 18:01:43', 1),
(44, 10, '4', 4, 0, '1', 4, '', '2014-12-24 21:39:33', 1),
(45, 2, '7', 0, 0, '', 0, '', '2015-06-09 14:50:54', 1),
(46, 2, '8', 0, 0, '', 0, '', '2015-06-09 14:53:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_invoice`
--

CREATE TABLE IF NOT EXISTS `dateing_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `invoice` varchar(255) NOT NULL,
  `doc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_message`
--

CREATE TABLE IF NOT EXISTS `dateing_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `del_status` tinyint(1) NOT NULL DEFAULT '0',
  `del_sender` tinyint(1) NOT NULL DEFAULT '0',
  `view_status` tinyint(4) NOT NULL,
  `delete_status_permanent` tinyint(1) NOT NULL DEFAULT '0',
  `delete_sender_permanent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `dateing_message`
--

INSERT INTO `dateing_message` (`id`, `from_id`, `to_id`, `message`, `date`, `del_status`, `del_sender`, `view_status`, `delete_status_permanent`, `delete_sender_permanent`) VALUES
(10, 5, 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2014-10-15 01:48:53', 0, 0, 0, 0, 0),
(11, 1, 4, 'jhbkjnlkmlkm', '2014-10-17 14:13:49', 0, 1, 0, 0, 1),
(12, 8, 1, 'sadasd asdasdas sadasdas', '2014-10-22 11:58:51', 1, 1, 0, 1, 1),
(13, 1, 8, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2014-10-23 00:06:02', 0, 1, 0, 0, 0),
(14, 1, 8, '  \r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '2014-10-23 00:30:39', 0, 0, 0, 0, 0),
(15, 1, 8, 'hiuhiuhoikm', '2014-10-25 01:05:19', 1, 1, 0, 1, 1),
(16, 1, 8, 'gjgkjhkhn', '2014-10-25 01:56:03', 1, 1, 0, 1, 1),
(17, 1, 3, 'Test Message................', '2014-10-29 11:29:33', 0, 1, 0, 0, 1),
(18, 1, 3, 'Again another test.................', '2014-10-29 11:32:28', 0, 0, 0, 0, 0),
(20, 8, 1, 'm,mn,nm,nm,nm,', '2014-10-30 01:35:36', 1, 1, 0, 1, 1),
(22, 8, 1, 'vbnvnvnvn', '2014-10-30 05:03:22', 1, 1, 0, 1, 1),
(23, 1, 8, 'hkjlkjlk;l,', '2014-10-31 14:30:10', 1, 1, 0, 1, 1),
(24, 1, 3, 'hello', '2014-10-31 14:34:37', 0, 1, 0, 0, 0),
(25, 1, 3, 'bjgkuhilkj', '2014-10-31 15:01:42', 0, 1, 0, 0, 1),
(26, 1, 8, 'hbibiiuhiuhuih', '2014-10-31 15:12:21', 1, 1, 0, 1, 1),
(27, 1, 3, 'i love you', '2014-10-31 15:13:11', 0, 1, 0, 0, 0),
(35, 1, 5, 'ojoijoiioj', '2014-11-14 14:58:26', 0, 1, 0, 0, 1),
(34, 1, 5, 'hhiuniniun', '2014-11-14 14:58:10', 0, 1, 0, 0, 1),
(36, 4, 3, 'hey', '2015-02-12 05:38:44', 0, 0, 0, 0, 0),
(37, 1, 4, 'HELLO', '2015-04-08 08:42:39', 0, 0, 1, 0, 0),
(38, 1, 5, 'Lorem Ipsum', '2015-06-10 09:38:16', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_order`
--

CREATE TABLE IF NOT EXISTS `dateing_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `car_model` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `dateing_order`
--

INSERT INTO `dateing_order` (`id`, `user_id`, `car_id`, `location`, `date`, `email`, `phone`, `address`, `car_model`) VALUES
(1, 47, 2, 'kolkata', '09/09/2014', 'nitsindranil94@gmail.com', '1234567890', 'kolkata', '1300'),
(2, 46, 154, 'kolkata', '09/09/2014', 'nits.avik@gmail.com', '9900000000', 'kolkta', '2600');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_package`
--

CREATE TABLE IF NOT EXISTS `dateing_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `price` varchar(200) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0' COMMENT 'Duration in months',
  `for` enum('M','F') NOT NULL,
  `create_profile` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `view_profile_pages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_photos` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_video` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `search_members` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_winks` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `reply_to_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_friend_requests` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_kisses` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `video_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `translate_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `purchase_translation_tokens` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `viewed_me` enum('A','R') NOT NULL DEFAULT 'R',
  `duration_time` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `dateing_package`
--

INSERT INTO `dateing_package` (`id`, `title`, `title_translated`, `description`, `description_translated`, `price`, `duration`, `for`, `create_profile`, `view_profile_pages`, `add_photos`, `add_video`, `search_members`, `send_winks`, `reply_to_messages`, `send_friend_requests`, `send_kisses`, `send_messages`, `chat`, `video_chat`, `translate_chat`, `purchase_translation_tokens`, `viewed_me`, `duration_time`) VALUES
(1, 'Public', 'Público', 'This is the public package for men', 'Este es el paquete pública para los hombres', '0', 0, 'M', 'A', 'R', 'A', 'A', 'A', 'R', 'A', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'A', 0),
(2, 'Trial', 'Membresía de prueba', 'This is a trial membership package for men', 'Este es un paquete de membresía de prueba para los homb', '0', 0, 'M', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 60),
(3, 'Basic', 'Básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '23.99', 1, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(4, 'Premium ', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombres', '28.99', 1, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(5, 'Basic', 'Básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '50.97', 3, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(6, 'Premium ', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombres', '56.97', 3, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(7, 'Basic', 'Básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '87', 6, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(8, 'Premium', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombres', '101.94', 6, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(9, 'Basic', 'Básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '126', 12, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(10, 'Premium', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombres', '162', 12, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_package_women`
--

CREATE TABLE IF NOT EXISTS `dateing_package_women` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `price` varchar(200) NOT NULL,
  `for` enum('M','F') NOT NULL,
  `create_profile` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `view_profile_pages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_photos` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_video` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `search_members` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_winks` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `reply_to_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_friend_requests` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_kisses` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `video_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `translate_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `purchase_translation_tokens` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `viewed_me` enum('A','R') NOT NULL DEFAULT 'R',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dateing_package_women`
--

INSERT INTO `dateing_package_women` (`id`, `title`, `title_translated`, `description`, `description_translated`, `price`, `for`, `create_profile`, `view_profile_pages`, `add_photos`, `add_video`, `search_members`, `send_winks`, `reply_to_messages`, `send_friend_requests`, `send_kisses`, `send_messages`, `chat`, `video_chat`, `translate_chat`, `purchase_translation_tokens`, `viewed_me`) VALUES
(1, 'Women Package', 'mulleres Package', 'This is the public package for women', 'Este é o paquete públicos para as mulleres', '0', 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_payment`
--

CREATE TABLE IF NOT EXISTS `dateing_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `mode` varchar(100) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `cheque_no` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `dateing_payment`
--

INSERT INTO `dateing_payment` (`id`, `user_id`, `car_id`, `mode`, `bank_name`, `cheque_no`, `image`, `status`) VALUES
(1, 47, 19, 'Via Cheque', '1234567890', '', '1410162257Apple_Friends.gif', 'approved'),
(2, 47, 17, 'PayPal', 'rte', '', '1410167907KindergartenNewsHeader.jpg', 'approved'),
(3, 47, 15, 'PayPal', '12345', '', '1410168972news23.gif', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_port`
--

CREATE TABLE IF NOT EXISTS `dateing_port` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `c_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_quickpics`
--

CREATE TABLE IF NOT EXISTS `dateing_quickpics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dateing_quickpics`
--

INSERT INTO `dateing_quickpics` (`id`, `amount`) VALUES
(1, '6');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_religion`
--

CREATE TABLE IF NOT EXISTS `dateing_religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `religion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_sentnewsletter`
--

CREATE TABLE IF NOT EXISTS `dateing_sentnewsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dateing_sentnewsletter`
--

INSERT INTO `dateing_sentnewsletter` (`id`, `email`) VALUES
(1, 'nitsindranil94@gmail.com'),
(2, 'nits.avik@gmail.com'),
(3, 'damon@optimumstudio.com'),
(4, ''),
(5, 'nits.bikash@gmail.com'),
(6, 'nits.sandeeptewary@gmail.com'),
(7, 'nits.riju@gmail.com'),
(8, 'nits.tanmoy@gmail.com'),
(9, 'nits.abhishek85@gmail.com'),
(10, 'nits.bikash111@gmail.com'),
(11, 'asdbik@gmail.com'),
(12, 'tlast@gmail.com'),
(13, 'nits.ashish1@gmail.com'),
(14, 'ag@gmail.com'),
(15, 'lovelybrotherbum@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_settings`
--

CREATE TABLE IF NOT EXISTS `dateing_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_perc` int(11) NOT NULL COMMENT 'Compatiable Percentage',
  `comp_users` int(11) NOT NULL,
  `template_users1` int(11) NOT NULL,
  `not_logged` int(11) NOT NULL,
  `bank_location` text NOT NULL,
  `account_number` text NOT NULL,
  `ref_number` text NOT NULL,
  `swift_code` text NOT NULL,
  `iban` text NOT NULL,
  `branch_code` text NOT NULL,
  `beneficiary` text NOT NULL,
  `bank` text NOT NULL,
  `bank_info` text NOT NULL,
  `template_users2` int(11) NOT NULL,
  `mail_sent1` int(11) NOT NULL,
  `mail_sent2` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dateing_settings`
--

INSERT INTO `dateing_settings` (`id`, `comp_perc`, `comp_users`, `template_users1`, `not_logged`, `bank_location`, `account_number`, `ref_number`, `swift_code`, `iban`, `branch_code`, `beneficiary`, `bank`, `bank_info`, `template_users2`, `mail_sent1`, `mail_sent2`) VALUES
(1, 40, 5, 15, 2, 'jojo', '4564565', '54654', 'tet', 'retert', 'retert', 'retert', 'retert', 'It usually takes three to five business days for Miamor to receive and process your bank transfer.', 10, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_shipping`
--

CREATE TABLE IF NOT EXISTS `dateing_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `delivery_date` varchar(100) NOT NULL,
  `arrival_date` varchar(100) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_social`
--

CREATE TABLE IF NOT EXISTS `dateing_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `dateing_social`
--

INSERT INTO `dateing_social` (`id`, `name`, `link`, `image`) VALUES
(1, 'Facebook', 'https://www.facebook.com/miamor.com.co', ''),
(2, 'Twitter', 'http://www.twitter.com', ''),
(28, 'Linked In', 'https://in.linkedin.com', ''),
(29, 'Google Plus', 'https://plus.google.com', ''),
(30, '', '', ''),
(31, 'sally ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_story`
--

CREATE TABLE IF NOT EXISTS `dateing_story` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `dateing_story`
--

INSERT INTO `dateing_story` (`id`, `title`, `title_translated`, `description`, `description_translated`, `image`) VALUES
(2, 'My Story', 'Mi historia', 'test test test test test test test test test test test test test test test test test test test test miamor test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'tel_story.png'),
(3, 'My another story', 'Mi otra historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'tel_story1.png'),
(4, 'My third story', 'Mi tercera historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'Kisses-3-love-18886362-500-335.jpg'),
(5, 'My fourth story', 'Mi cuarta historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'love-couple-young-wallpaper.jpg'),
(6, 'My Story With You. ', 'My Story With You. ', 'My Story With You. was so much special to describe. \r\nI love the each time i spent with you.\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nEach time we lough.. \r\nwe talk , We hold our hands. ', 'My Story With You. was so much special to describe. \r\nI love the each time i spent with you..\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \r\nEach time we lough.. \r\nwe talk , We hold our hands. ', 'courting23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tbladmin`
--

CREATE TABLE IF NOT EXISTS `dateing_tbladmin` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `paypal_email` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dateing_tbladmin`
--

INSERT INTO `dateing_tbladmin` (`id`, `name`, `admin_username`, `admin_password`, `email`, `address`, `image`, `status`, `paypal_email`, `secret_key`, `publishable_key`) VALUES
(1, '', 'admin', 'admin', 'nits.tanmoy@gmail.com', '', '', '1', 'nits.arpita@gmail.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tblorderdetails`
--

CREATE TABLE IF NOT EXISTS `dateing_tblorderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) DEFAULT NULL,
  `productid` varchar(255) DEFAULT NULL,
  `quantity` varchar(256) NOT NULL,
  `order_status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `dateing_tblorderdetails`
--

INSERT INTO `dateing_tblorderdetails` (`id`, `orderid`, `productid`, `quantity`, `order_status`) VALUES
(8, '6', '2', '1', '0'),
(9, '6', '3', '1', '0'),
(10, '7', '1', '6', '0'),
(11, '8', '3', '1', '0'),
(12, '9', '1', '1', '0'),
(13, '10', '1', '1', '0'),
(14, '11', '1', '1', '0'),
(15, '12', '1', '1', '0'),
(16, '13', '1', '1', '0'),
(17, '14', '1', '1', '0'),
(18, '15', '1', '1', '0'),
(19, '16', '1', '1', '0'),
(20, '17', '1', '1', '0'),
(21, '18', '1', '1', '0'),
(22, '19', '1', '1', '0'),
(23, '20', '1', '1', '0'),
(24, '21', '1', '1', '0'),
(25, '22', '1', '1', '0'),
(26, '23', '2', '1', '0'),
(27, '24', '2', '1', '0'),
(28, '25', '3', '1', '0'),
(29, '26', '2', '1', '0'),
(30, '27', '1', '1', '0'),
(31, '28', '1', '1', '0'),
(32, '29', '1', '2', '0');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tblorders`
--

CREATE TABLE IF NOT EXISTS `dateing_tblorders` (
  `orderid` int(11) NOT NULL AUTO_INCREMENT,
  `orderdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_user_id` varchar(20) NOT NULL DEFAULT '',
  `billingid` int(11) NOT NULL,
  `orderamount` varchar(50) NOT NULL DEFAULT '',
  `order_status` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `dispatch` enum('1','0') NOT NULL DEFAULT '0',
  `type` enum('S','P') NOT NULL DEFAULT 'S',
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_transactions`
--

CREATE TABLE IF NOT EXISTS `dateing_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `txn_id` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dateing_transactions`
--

INSERT INTO `dateing_transactions` (`id`, `user_id`, `txn_id`, `amount`, `status`) VALUES
(1, 10, 'I-RMPRS0YJGJHA', '10.00', 'verified'),
(2, 11, 'I-F0FRRX99G7DR', '15.00', 'verified'),
(3, 0, 'I-0SAWCKDT8JJA', '15.00', 'verified'),
(4, 0, 'I-7KGHUHT9DASH', '15.00', 'verified'),
(5, 12, 'I-PNELT8YTH819', '15.00', 'verified'),
(6, 1, 'I-E2VRP7RTV1E0', '101.94', 'verified'),
(7, 26, 'I-MRPKAM06NPPH', '50.97', 'verified'),
(8, 30, 'I-M65UXPE8US82', '56.97', 'verified'),
(9, 0, 'I-DE42VF5DNL16', '28.99', 'verified'),
(10, 22, 'I-P7VRDXHY8X8C', '5.00', 'verified'),
(11, 46, 'I-FAM14PKMBPBL', '5.00', 'verified'),
(12, 49, 'I-3DE1TMPL9VG8', '5.00', 'verified'),
(13, 78, 'I-LTWJ4HDKTWM0', '28.99', 'verified'),
(14, 89, 'I-40UH6PK74BYM', '56.97', 'verified'),
(15, 1, 'I-KUGHNHYNDSGJ', '126.00', 'verified');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user`
--

CREATE TABLE IF NOT EXISTS `dateing_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_user_id` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `subscription_start` date NOT NULL,
  `subscription_end` date NOT NULL,
  `old_subcribe_details` varchar(255) NOT NULL,
  `ref_id` varchar(100) NOT NULL,
  `subscription_till` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(256) NOT NULL,
  `paypal_email` text NOT NULL,
  `password` varchar(256) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `about_me` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `add_date` date NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `is_loggedin` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `ip` varchar(30) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `dpname` varchar(200) NOT NULL,
  `height` varchar(200) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `child` varchar(100) NOT NULL,
  `ethnicity` varchar(100) NOT NULL,
  `education` varchar(200) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `smoking` varchar(100) NOT NULL,
  `drinking` varchar(200) NOT NULL,
  `bodytype` varchar(200) NOT NULL,
  `personality` varchar(200) NOT NULL,
  `lookfor` varchar(200) NOT NULL,
  `relocate` varchar(255) NOT NULL,
  `appearence` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `me_doing` text NOT NULL,
  `good_at` text NOT NULL,
  `notice_about_me` text NOT NULL,
  `perfect_match` text,
  `ideal_date` text,
  `facebook_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `dont_have_fbtwt` tinyint(1) NOT NULL,
  `creditcard_no` varchar(150) NOT NULL,
  `expire_month` int(11) NOT NULL,
  `expire_year` int(11) NOT NULL,
  `ccv` varchar(100) NOT NULL,
  `admin_verify` tinyint(1) NOT NULL DEFAULT '1',
  `translator_package_id` int(5) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0',
  `fb_varify` tinyint(1) NOT NULL DEFAULT '0',
  `timeleft` int(11) NOT NULL,
  `trial_taken` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `dateing_user`
--

INSERT INTO `dateing_user` (`id`, `fb_user_id`, `type`, `subscription_id`, `subscription_start`, `subscription_end`, `old_subcribe_details`, `ref_id`, `subscription_till`, `fname`, `lname`, `email`, `paypal_email`, `password`, `gender`, `dob`, `address`, `about_me`, `image`, `add_date`, `status`, `is_loggedin`, `last_login`, `ip`, `city`, `state`, `country`, `phone`, `secret_key`, `publishable_key`, `user_type`, `dpname`, `height`, `sex`, `relation`, `child`, `ethnicity`, `education`, `religion`, `smoking`, `drinking`, `bodytype`, `personality`, `lookfor`, `relocate`, `appearence`, `language`, `me_doing`, `good_at`, `notice_about_me`, `perfect_match`, `ideal_date`, `facebook_link`, `twitter_link`, `dont_have_fbtwt`, `creditcard_no`, `expire_month`, `expire_year`, `ccv`, `admin_verify`, `translator_package_id`, `start_date`, `end_date`, `is_phone_verified`, `fb_varify`, `timeleft`, `trial_taken`) VALUES
(1, '1517545255189394', 0, 9, '2015-07-14', '2016-07-14', '', 'miamor000001', '2015-07-16 15:06:32', 'administrator', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1999-01-01', 'kolkata,india', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '7dfdf4283d6bdacf96aa459e08927954cakephp1.png', '0000-00-00', '1', 1, '2015-07-16 15:06:32', '122.160.187.85', 'kolkata', '', '', '9002561033', '', '', 'Man Interested In Women', 'Sandy', '183', 'female', 'single', 'yes', 'Indian', 'college graduate', 'hindu', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'a friend', 'yes', 'average', 'Bengali', 'TEST', '', '', 'I love the "bad boys" most- they make you feel crazy and euphoric, they are possessive and tease you mercilessly.... they treat you like princess and can kiss the daylights out of you.... and the madness of loving them is out of the world..!!', 'It does not have to be at CCD....Barista....KFC or any of these places..... just that we should be able to talk.... and about anything and everything from Comic strips to Shakespeare... If things go good, it can be followed by a short walk around the corner and most importantly.... its gotta be with someone who does not pretend to impress.', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 1, '2015-03-13', '2015-03-20', 1, 1, 0, 'N'),
(3, '', 0, 1, '2015-03-01', '2015-04-01', 'This users Women Package package change by admin on 10-03-2015', '', '2015-06-30 14:15:44', 'payle', '', 'talk2payel1987@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-02-03', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '1413179314Beautiful-Girl-Face-Pictures_beautiful-girl.jpg', '0000-00-00', '1', 0, '2015-06-30 14:11:04', '117.194.16.43', '', '', '', '213123123123', '', '', 'Man Interested In Women', 'payel', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'Ambitious and Leader Like', 'a friend', 'within country', 'attractive', 'English', '', '', '', 'I love the "bad boys" most- they make you feel crazy and euphoric, they are possessive and tease you mercilessly.... they treat you like princess and can kiss the daylights out of you.... and the madness of loving them is out of the world..!!', 'It does not have to be at CCD....Barista....KFC or any of these places..... just that we should be able to talk.... and about anything and everything from Comic strips to Shakespeare... If things go good, it can be followed by a short walk around the corner and most importantly.... its gotta be with someone who does not pretend to impress.', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(4, '', 0, 1, '2015-03-15', '2015-03-31', '', '', '2015-07-02 08:21:04', 'julia', '', 'julia@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1953-02-03', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', '1413179574Beautiful-Girl.jpg', '0000-00-00', '1', 1, '2015-07-02 08:21:04', '117.194.16.43', '', '', '', 'khjhjk', '', '', '', 'julia', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'yes socially', 'yes socially', 'curvy', 'Analytical and Quiet', 'marriage', 'not sure', 'very attractive', 'Spanish', '', '', '', '', '', 'https://www.facebook.com/', 'https://twitter.com/', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(5, '', 0, 10, '2015-03-22', '2015-04-30', '', '', '2015-07-16 23:42:16', 'karuna', '', 'nits.karunadri1@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1964-07-07', 'Mathabhanga-736146', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', '14131800681484553.jpg', '0000-00-00', '1', 1, '2015-07-16 23:42:16', '122.160.187.85', 'Matha Bhanga', '', '', '1234567890', '', '', '', 'karuna', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'Analytical and Quiet', 'casual fun', 'no', 'average', 'Hindi', '', '', '', '', '', 'https://www.facebook.com/', 'https://twitter.com/', 0, 'sdsdsdsdsd', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 1, 0, 0, 'N'),
(6, '', 0, 1, '2015-04-01', '2015-04-30', '', '', '2015-06-30 12:22:12', 'koyel', '', 'nits.koyel@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1991-06-10', '', 'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now u', '1413180181images.jpg', '0000-00-00', '1', 0, '2015-06-30 10:43:05', '122.160.187.85', 'Kolkata', '', '', '1234567890', '', '', '', 'koyel', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'Relaxed and Peaceful', 'relationship', 'yes', 'attractive', 'English', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(7, '', 0, 1, '2015-03-01', '2015-04-29', '', '', '2015-04-07 06:31:26', 'sally', '', 'nitsindranil95@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1964-07-07', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', '', '0000-00-00', '1', 0, '2015-02-26 04:53:05', '103.224.129.40', '', '', '', '123456', '', '', '', 'sally', '157', 'female', 'single', 'no', 'Indian', 'advanced degree', 'hindu', 'no', 'yes socially', 'heavy', 'Relaxed and Peaceful', 'marriage', 'within country', 'attractive', 'English', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(8, '', 0, 1, '2015-04-05', '2015-04-15', '', '', '2015-06-30 14:22:12', 'Abhishek Kundu', '', 'nits.abhishek85@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1998-02-09', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', '07cae5548f849831574d856f1867ed6bPenguins.jpg', '0000-00-00', '1', 1, '2015-06-30 14:22:12', '122.160.187.85', '', '', '', '9876543210', '', '', 'Man Interested In Women', 'Abhishek Kundu', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'You Need to Find Out', '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(11, '', 0, 3, '2015-02-01', '2015-03-01', 'This users Premium Membership package change by admin on 10-03-2015', '', '2015-05-28 11:14:00', 'Abhishek', '', 'nit.abhishekb@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1990-03-30', '', '', 'cda6d3a28672624329b63b260b60a56312101363~2d10bb905b10cb41954131e61905b3d10d516d68-stocklarge (1).jpg', '0000-00-00', '1', 0, '2015-02-26 02:02:39', '122.160.187.85', '', '', '', '9038533045', '', '', 'Interested In Men and Women', 'Tanmoy', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'relationship', '', '', '', '', '', '', '', '', 'https://www.facebook.com/', 'https://twitter.com/', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(22, '', 0, 8, '2015-05-07', '2015-06-07', '', 'miamor000022', '2015-06-19 15:03:01', 'Karun', '', 'nits.karunadri@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1988-08-11', '', '', '', '2015-03-10', '1', 0, '2015-06-19 13:45:45', '122.160.187.85', '', '', '', '1234567890', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(26, '', 0, 6, '2015-03-01', '2015-06-13', '', 'miamor000026', '2015-07-16 05:45:22', 'Bikash Mondal', '', 'nits.bikash@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1957-05-07', '', '', '269f49c0e5768cf33dafde21724201a9design.png', '2015-03-13', '1', 1, '2015-07-16 05:45:22', '122.160.187.85', 'Kolkata', '', '', '9002561033', '', '', '', 'bikash', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(31, '', 0, 1, '2015-03-02', '2015-04-22', '', 'miamor000031', '2015-05-28 11:14:28', 'Soumen', '', 'nits.soumenbh8@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1962-05-03', '', '', '', '2015-03-16', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '9831658795', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(32, '', 0, 1, '2015-04-12', '2015-04-22', '', 'miamor000032', '2015-04-07 06:33:07', 'Saroj Kumar', '', 'nits.saroj@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1957-05-07', '', '', '', '2015-03-16', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '879875465', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(33, '', 0, 1, '2015-03-22', '2015-04-14', '', 'miamor000033', '2015-05-19 14:58:28', 'Tan Ban', '', 'nits.tanmoy@gmail.com', '', 'e61743696ee32846e578b03a666f597a', 'M', '1956-05-12', '', '', '', '2015-03-16', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '5879797987', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(41, '', 0, 8, '0000-00-00', '0000-00-00', '', 'miamor000041', '2015-06-19 15:03:06', 'Karunadri', '', 'nits.karunadri@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '0000-00-00', '', '', '', '2015-04-07', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '88', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(44, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000044', '2015-05-25 07:47:28', 'Tanmoy Banerjee', '', 'nits.tanmoy@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1988-08-21', '', '', '', '2015-05-25', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '54545454545454', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(45, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000045', '2015-05-25 07:49:21', 'Tanmoy Banerjee', '', 'nits.tanmoy@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1988-08-21', '', '', '', '2015-05-25', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '1234567891', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(46, '', 0, 2, '2015-05-25', '2015-06-25', '', 'miamor000046', '2015-05-25 15:04:55', 'Tanmoy Banerjee', '', 'nits.tanmoy@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1988-08-21', '', '', '', '2015-05-25', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '1234567891', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(47, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000047', '2015-05-29 14:32:05', 'test', '', 'nitstesting@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1988-09-05', '', '', '', '2015-05-29', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '554324564', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(49, '', 0, 9, '2015-06-01', '2015-07-01', 'This users Trial Membership package change by admin on 10-06-2015', 'miamor000049', '2015-06-30 12:23:36', 'Bikash Mondal', '', 'nits.bikash111@gmail.com', '', '880fe3cda75eaed7bf515fbfb1551f78', 'M', '1990-01-27', '', '', '', '2015-06-01', '1', 0, '2015-06-30 10:39:47', '122.160.187.85', 'Kolkata', '', '', '789798789', '', '', '', 'Bik', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(50, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000050', '2015-06-02 23:49:58', 'damon de berry', '', 'damon@optimumstudio.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '0000-00-00', '', '', '', '2015-06-02', '0', 0, '0000-00-00 00:00:00', '190.27.114.231', '', '', '', '', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(51, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000051', '2015-06-03 07:02:21', 'Abhijit Roy', '', 'nits.abhijit@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1985-08-14', '', '', '', '2015-06-03', '0', 0, '0000-00-00 00:00:00', '122.163.58.63', '', '', '', '8967796895', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(78, '', 0, 8, '2015-06-19', '2015-12-19', '', 'miamor000078', '2015-06-25 11:29:12', 'Sandeep Tewary', '', 'nits.sandeeptewary@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1985-08-14', '', '', '', '2015-06-18', '1', 1, '2015-06-25 11:29:12', '122.163.102.83', 'Kolkata', '', '', '919876543210', '', '', '', 'Sandy', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'test', 'test', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 1, 1, 0, 'Y'),
(54, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000054', '2015-06-06 13:36:30', 'damon de berry', '', 'damon@optimumstudio.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '0000-00-00', '', '', '', '2015-06-06', '0', 0, '0000-00-00 00:00:00', '190.27.114.231', '', '', '', '', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(55, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000055', '2015-06-08 11:13:04', 'Mamata Ban', '', 'mamataban@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-03-03', '', '', '', '2015-06-08', '1', 1, '2015-06-08 11:13:04', '122.160.187.85', '', '', '', '123456', '', '', '', '', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(76, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000076', '2015-06-16 14:30:04', 'Babu', '', 'nits.babu2@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-05-03', '', '', '', '2015-06-16', '1', 0, '2015-06-16 14:30:04', '122.160.187.85', '', '', '', '987777777', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 120, 'N'),
(74, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000074', '2015-06-16 14:20:47', 'Babu', '', 'nits.babu@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-05-03', '', '', '', '2015-06-16', '1', 0, '2015-06-16 14:20:22', '122.160.187.85', '', '', '', '987777777', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 100, 'N'),
(75, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000075', '2015-06-16 11:29:43', 'Babu', '', 'nits.babu1@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-05-03', '', '', '', '2015-06-16', '1', 0, '2015-06-16 10:12:16', '122.160.187.85', '', '', '', '987777777', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 70, 'N'),
(86, '1033366390025669', 0, 1, '0000-00-00', '0000-00-00', '', '', '2015-06-20 12:48:38', 'Jhoana Garcia', '', 'jhaoana2118@hotmail.com', '', '', 'F', '0000-00-00', '', '', '', '2015-06-20', '1', 1, '2015-06-20 12:48:38', '190.27.23.218', '', '', '', '', '', '', 'M', '', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, 0, 'N'),
(87, '1517545255189394', 0, 1, '0000-00-00', '0000-00-00', '', '', '2015-06-25 07:55:35', 'Rahul Roy', '', 'nits.santanu@gmail.com', '', '', 'M', '0000-00-00', '', '', '33ead1c375fc5c0ccee455a0727e8fe1logo.png', '2015-06-25', '1', 0, '2015-06-25 06:24:49', '122.163.15.219', '', '', '', '', '', '', 'F', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, 0, 'N'),
(88, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000088', '2015-06-30 10:35:17', 'Arya Stark', '', 'arya.stark@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1990-03-03', '', '', '', '2015-06-30', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '123456', '', '', '', '', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N'),
(89, '', 0, 6, '2015-07-09', '2015-10-09', '', 'miamor000089', '2015-07-09 21:32:21', 'Ashish Kumar', '', 'nits.ashis@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-04-18', '', '', '', '2015-07-09', '1', 1, '2015-07-09 14:22:37', '122.160.187.85', '', '', '', '123456', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_userquery`
--

CREATE TABLE IF NOT EXISTS `dateing_userquery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dateing_userquery`
--

INSERT INTO `dateing_userquery` (`id`, `name`, `email`, `query`, `message`, `date`) VALUES
(3, 'Indranil Gupta', 'nitsindranil94@gmail.com', 'This is my test query', 'This is my test message...', '2015-01-21 07:17:30'),
(4, 'Bikash Mondal', 'nits.bikash@gmail.com', 'Test query', 'Test message...', '2015-01-21 07:06:03');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user_1`
--

CREATE TABLE IF NOT EXISTS `dateing_user_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_user_id` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(256) NOT NULL,
  `paypal_email` text NOT NULL,
  `password` varchar(256) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `about_me` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `add_date` date NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `is_loggedin` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `ip` varchar(30) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `dpname` varchar(200) NOT NULL,
  `height` varchar(200) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `child` varchar(100) NOT NULL,
  `ethnicity` varchar(100) NOT NULL,
  `education` varchar(200) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `smoking` varchar(100) NOT NULL,
  `drinking` varchar(200) NOT NULL,
  `bodytype` varchar(200) NOT NULL,
  `personality` varchar(200) NOT NULL,
  `lookfor` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dateing_user_1`
--

INSERT INTO `dateing_user_1` (`id`, `fb_user_id`, `type`, `fname`, `lname`, `email`, `paypal_email`, `password`, `gender`, `dob`, `address`, `about_me`, `image`, `add_date`, `status`, `is_loggedin`, `last_login`, `ip`, `city`, `state`, `country`, `phone`, `secret_key`, `publishable_key`, `user_type`, `dpname`, `height`, `sex`, `relation`, `child`, `ethnicity`, `education`, `religion`, `smoking`, `drinking`, `bodytype`, `personality`, `lookfor`) VALUES
(1, '', 0, 'Indranil', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'male', '1987-1-1', 'kolkata,india', '', '1413352743profile3.jpg', '0000-00-00', '1', 0, '2014-10-15 01:24:21', '122.160.187.85', '', '', '', '1234567890', '', '', 'man instade in woman', 'Damon', '183', 'male', 'single', 'yes', 'India', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'yes', 'a friend'),
(3, '', 0, 'payle', '', 'talk2payel1987@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-2-3', '', '', '1413179314Beautiful-Girl-Face-Pictures_beautiful-girl.jpg', '0000-00-00', '1', 0, '2014-10-13 00:45:48', '117.194.16.43', '', '', '', '213123123123', '', '', '', 'payel', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'yes', 'a friend'),
(4, '', 0, 'julia', '', 'julia@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1953-2-3', '', '', '1413179574Beautiful-Girl.jpg', '0000-00-00', '1', 0, '2014-10-13 00:58:15', '117.194.16.43', '', '', '', 'khjhjk', '', '', '', 'julia', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes socially', 'yes', 'curvy', 'yes', 'marriage'),
(5, '', 0, 'karuna', '', 'nits.karunadri@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1964-7-7', '', '', '14131800681484553.jpg', '0000-00-00', '1', 1, '2014-10-15 01:35:52', '122.160.187.85', '', '', '', '1234567890', '', '', '', 'karuna', '150', 'male', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes', 'yes regularly', 'curvy', 'Select your personality', 'casual fun'),
(6, '', 0, 'koyel', '', 'nits.koyel@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1991-6-10', '', '', '1413180181images.jpg', '0000-00-00', '1', 1, '2014-10-13 01:01:50', '122.160.187.85', '', '', '', '1234567890', '', '', '', 'koyel', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes', 'yes regularly', 'curvy', 'yes', 'relationship'),
(7, '', 0, 'sally', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '--', '', '', '', '0000-00-00', '0', 0, '0000-00-00 00:00:00', '103.224.129.40', '', '', '', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user_billing`
--

CREATE TABLE IF NOT EXISTS `dateing_user_billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fname` varchar(256) NOT NULL,
  `lname` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `zip` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_viewedme`
--

CREATE TABLE IF NOT EXISTS `dateing_viewedme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `viewed_user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=139 ;

--
-- Dumping data for table `dateing_viewedme`
--

INSERT INTO `dateing_viewedme` (`id`, `user_id`, `viewed_user_id`, `date`) VALUES
(6, 49, 5, '2015-06-10 11:10:34'),
(7, 49, 1, '2015-06-10 11:10:54'),
(10, 49, 26, '2015-06-10 11:12:13'),
(29, 5, 8, '2015-06-11 07:40:18'),
(32, 5, 7, '2015-06-11 11:01:01'),
(53, 1, 44, '2015-06-15 12:55:53'),
(57, 5, 3, '2015-06-16 13:42:59'),
(81, 78, 8, '2015-06-19 15:09:37'),
(83, 1, 6, '2015-06-19 16:27:10'),
(87, 1, 1, '2015-06-19 16:29:34'),
(95, 86, 5, '2015-06-20 12:53:06'),
(96, 78, 5, '2015-06-22 10:32:34'),
(103, 1, 45, '2015-06-29 21:07:52'),
(105, 1, 87, '2015-06-29 21:08:10'),
(111, 1, 26, '2015-06-29 21:18:56'),
(114, 3, 5, '2015-06-30 14:13:50'),
(115, 8, 26, '2015-06-30 14:18:34'),
(118, 1, 41, '2015-07-02 07:48:44'),
(124, 1, 78, '2015-07-08 06:09:19'),
(125, 1, 4, '2015-07-13 20:14:08'),
(128, 1, 5, '2015-07-13 20:19:24'),
(136, 1, 3, '2015-07-13 21:06:39'),
(138, 1, 85, '2015-07-13 21:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_wishlist`
--

CREATE TABLE IF NOT EXISTS `dateing_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dating_block_reason`
--

CREATE TABLE IF NOT EXISTS `dating_block_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `dating_block_reason`
--

INSERT INTO `dating_block_reason` (`id`, `user_id`, `reason`, `date`) VALUES
(1, 7, 'This user has been blocked.', '2015-02-12 15:33:13'),
(2, 41, 'bad language', '2015-05-31 17:44:36');

-- --------------------------------------------------------

--
-- Table structure for table `dating_block_user`
--

CREATE TABLE IF NOT EXISTS `dating_block_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `block_to` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dating_block_user`
--

INSERT INTO `dating_block_user` (`id`, `user_id`, `block_to`, `date`) VALUES
(2, 8, 1, '2015-01-21 07:49:06'),
(4, 1, 3, '2015-02-27 18:55:32');

-- --------------------------------------------------------

--
-- Table structure for table `dating_chat`
--

CREATE TABLE IF NOT EXISTS `dating_chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=137 ;

--
-- Dumping data for table `dating_chat`
--

INSERT INTO `dating_chat` (`id`, `from`, `to`, `message`, `sent`, `recd`) VALUES
(1, '1', '8', 'Hi', '2014-11-13 07:03:02', 1),
(2, '8', '1', 'Hello', '2014-11-13 07:04:41', 1),
(3, '1', '8', 'How are you?', '2014-11-13 07:04:55', 1),
(4, '1', '8', '??', '2014-11-13 07:05:27', 1),
(5, '1', '8', 'hi', '2014-11-13 07:06:43', 1),
(6, '8', '1', 'hello', '2014-11-13 07:13:09', 1),
(7, '8', '1', 'Hello...', '2014-11-13 07:21:02', 1),
(8, '1', '8', 'hi', '2014-11-13 07:21:11', 1),
(9, '1', '8', 'hello', '2014-11-13 07:23:10', 1),
(10, '8', '1', 'hi', '2014-11-13 07:24:02', 1),
(11, '8', '1', 'hi', '2014-11-13 07:40:46', 1),
(12, '1', '8', 'hello', '2014-11-13 07:46:34', 1),
(13, '8', '1', 'hi', '2014-11-13 07:47:01', 1),
(14, '8', '1', 'hello', '2014-11-13 07:49:05', 1),
(15, '1', '8', 'hi', '2014-11-13 07:49:12', 1),
(16, '8', '1', 'hi', '2014-11-13 07:52:43', 1),
(17, '1', '4', 'hello', '2014-11-13 07:52:53', 1),
(18, '1', '8', 'hello.............', '2014-11-13 07:53:25', 1),
(19, '8', '1', 'hi', '2014-11-13 07:53:45', 1),
(20, '1', '8', 'hi', '2014-11-13 07:55:00', 1),
(21, '1', '8', 'hola', '2014-11-13 23:26:56', 1),
(22, '1', '8', 'how are you', '2014-11-13 23:27:03', 1),
(23, '1', '3', 'I don''t know', '2014-11-14 01:06:52', 1),
(24, '1', '8', 'hello..this is a test for language change', '2014-11-14 01:18:09', 1),
(25, '1', '8', 'hi', '2014-11-19 02:29:18', 1),
(26, '1', '8', 'hi', '2014-11-19 05:12:01', 1),
(27, '1', '8', 'hi', '2014-11-28 23:40:41', 1),
(28, '1', '8', 'hola baby girl', '2014-11-29 21:48:33', 1),
(29, '8', '1', ':)', '2014-12-06 06:40:36', 1),
(30, '1', '3', 'hello xx', '2014-12-11 14:03:46', 1),
(31, '1', '3', 'hello', '2014-12-11 14:03:59', 1),
(32, '4', '1', 'hi', '2015-02-12 05:37:32', 1),
(33, '4', '1', 'hi', '2015-02-12 05:41:53', 1),
(34, '1', '4', 'dfdfdf', '2015-02-12 05:57:22', 1),
(35, '1', '4', 'hrllo', '2015-02-12 05:57:26', 1),
(36, '1', '4', 'name', '2015-02-12 05:57:35', 1),
(37, '1', '4', 'hello', '2015-02-19 22:24:39', 1),
(38, '1', '4', 'Hello', '2015-02-19 22:24:50', 1),
(39, '12', '11', 'ki re', '2015-02-25 06:05:05', 1),
(40, '11', '12', 'bol bhai', '2015-02-25 06:05:17', 1),
(41, '11', '12', 'kamon achis?', '2015-02-25 06:05:24', 1),
(42, '12', '11', 'ha ha ha', '2015-02-25 06:05:42', 1),
(43, '11', '12', 'ohhh', '2015-02-25 06:05:55', 1),
(44, '12', '11', 'bujhlam', '2015-02-25 06:06:03', 1),
(45, '11', '12', 'hey', '2015-02-25 06:11:06', 1),
(46, '1', '5', 'hello', '2015-03-11 10:15:51', 1),
(47, '1', '5', 'hello', '2015-03-11 10:16:02', 1),
(48, '1', '4', 'hi', '2015-03-13 06:55:28', 1),
(49, '1', '4', ':)', '2015-03-13 06:55:59', 1),
(50, '1', '4', 'dgdfdfdsf', '2015-03-13 06:56:41', 1),
(51, '1', '4', 'ki', '2015-04-06 12:29:12', 1),
(52, '1', '5', 'hello', '2015-04-07 03:03:04', 1),
(53, '1', '4', 'hello', '2015-04-09 11:36:02', 1),
(54, '1', '4', 'kl', '2015-04-13 08:03:10', 1),
(55, '1', '4', 'hello', '2015-04-13 23:05:16', 1),
(56, '1', '4', 'hello', '2015-04-13 23:17:05', 1),
(57, '1', '5', 'hello', '2015-05-19 00:57:37', 1),
(58, '1', '5', 'jguggh', '2015-05-19 15:06:52', 1),
(59, '1', '5', 'hey buddy', '2015-05-25 01:17:20', 1),
(60, '5', '1', 'how are you?', '2015-05-25 01:17:33', 1),
(61, '1', '5', 'I am good', '2015-05-25 01:17:55', 1),
(62, '4', '1', 'hi damon', '2015-05-29 08:11:08', 1),
(63, '4', '1', 'how are you ?', '2015-05-29 08:11:16', 1),
(64, '1', '4', 'i''m good thanks and you?', '2015-05-29 09:36:32', 1),
(65, '1', '5', 'hii', '2015-06-03 00:03:56', 1),
(66, '8', '1', 'hii', '2015-06-03 05:27:51', 1),
(67, '1', '8', 'hello', '2015-06-03 05:28:05', 1),
(68, '1', '8', 'how are you?', '2015-06-03 05:28:42', 1),
(69, '8', '1', 'I am fine how are you?', '2015-06-03 05:28:53', 1),
(70, '8', '1', 'hii', '2015-06-03 05:32:15', 1),
(71, '8', '1', 'hello', '2015-06-03 05:32:46', 1),
(72, '1', '8', 'how are you?', '2015-06-03 05:32:56', 1),
(73, '1', '8', 'hiiiiiiii', '2015-06-03 05:33:36', 1),
(74, '8', '1', 'hii', '2015-06-03 05:34:38', 1),
(75, '1', '8', 'hello', '2015-06-03 05:34:44', 1),
(76, '8', '1', 'how are you?', '2015-06-03 05:35:18', 1),
(77, '1', '8', 'I am fine', '2015-06-03 05:35:25', 1),
(78, '8', '1', 'hii', '2015-06-03 05:38:53', 1),
(79, '1', '8', 'hello', '2015-06-03 05:38:59', 1),
(80, '5', '1', 'hi', '2015-06-07 00:07:39', 1),
(81, '1', '4', 'hi', '2015-06-07 04:00:14', 1),
(82, '5', '1', 'hi', '2015-06-08 03:22:51', 1),
(83, '5', '1', 'hello', '2015-06-08 03:23:16', 1),
(84, '1', '5', 'hi', '2015-06-08 03:54:03', 1),
(85, '1', '4', 'helo', '2015-06-08 03:54:14', 1),
(86, '3', '5', 'hii', '2015-06-11 04:15:04', 1),
(87, '5', '3', 'hello', '2015-06-11 04:15:15', 1),
(88, '3', '5', 'how are you?', '2015-06-11 04:15:41', 1),
(89, '5', '3', 'I am fine', '2015-06-11 04:15:51', 1),
(90, '3', '5', 'cool', '2015-06-11 04:15:56', 1),
(91, '3', '5', 'hiiiii', '2015-06-11 04:16:48', 1),
(92, '5', '3', 'hello', '2015-06-11 04:16:57', 1),
(93, '3', '5', 'hello', '2015-06-11 04:20:11', 1),
(94, '3', '5', 'hiiii', '2015-06-11 04:20:38', 1),
(95, '5', '3', 'how are you?', '2015-06-11 04:20:46', 1),
(96, '5', '3', 'jghjghjkhgkj', '2015-06-11 04:53:24', 1),
(97, '3', '5', 'hi', '2015-06-11 05:03:24', 1),
(98, '3', '5', 'hi', '2015-06-11 05:05:53', 1),
(99, '3', '5', 'hi', '2015-06-11 05:06:37', 1),
(100, '3', '5', 'fggfg', '2015-06-11 05:06:44', 1),
(101, '3', '5', 'hgj', '2015-06-11 05:07:10', 1),
(102, '3', '5', 'gfhgh', '2015-06-11 05:09:11', 1),
(103, '3', '5', 'gh', '2015-06-11 05:09:32', 1),
(104, '3', '5', 'fchf', '2015-06-11 05:12:25', 1),
(105, '3', '5', 'dgdfgfdgfh', '2015-06-11 05:19:53', 1),
(106, '3', '5', 'hiiii', '2015-06-11 05:41:58', 1),
(107, '1', '5', 'hi', '2015-06-15 06:03:50', 1),
(108, '1', '5', 'hi', '2015-06-15 06:05:22', 1),
(109, '3', '5', 'hi', '2015-06-15 06:10:12', 1),
(110, '3', '5', 'vxsdfdsfs', '2015-06-15 06:10:39', 1),
(111, '3', '5', 'dsdsadsad', '2015-06-15 06:10:42', 1),
(112, '3', '5', 'gfhgfhgfh', '2015-06-15 06:11:33', 1),
(113, '3', '5', 'hi', '2015-06-15 06:18:50', 1),
(114, '3', '5', 'hi', '2015-06-15 06:22:21', 1),
(115, '3', '5', 'sdsfsaf', '2015-06-15 06:28:29', 1),
(116, '3', '5', 'sdsd', '2015-06-15 06:28:40', 1),
(117, '3', '5', 'vbfxcbfvbfd', '2015-06-15 06:40:19', 1),
(118, '3', '5', 'gfdgfdg'']', '2015-06-15 06:40:24', 1),
(119, '3', '5', 'dfvvdfsg', '2015-06-15 06:44:34', 1),
(120, '3', '5', 'i[io[io', '2015-06-15 06:44:37', 1),
(121, '3', '5', 'wewqewqe', '2015-06-15 06:55:56', 1),
(122, '3', '5', 'ewrewrewrew', '2015-06-15 06:55:58', 1),
(123, '3', '5', 'hi', '2015-06-15 08:19:29', 1),
(124, '3', '5', 'hi', '2015-06-15 08:23:30', 1),
(125, '8', '1', 'hi', '2015-06-30 07:22:35', 1),
(126, '8', '1', 'hello', '2015-06-30 07:23:42', 1),
(127, '8', '1', 'hii', '2015-06-30 07:27:17', 1),
(128, '8', '1', 'hello', '2015-06-30 07:29:24', 1),
(129, '1', '4', 'hi', '2015-07-02 01:20:14', 1),
(130, '1', '4', 'hi', '2015-07-02 01:27:21', 1),
(131, '1', '4', 'hi', '2015-07-02 02:30:43', 1),
(132, '1', '4', 'Hel', '2015-07-02 02:32:50', 1),
(133, '1', '4', 'sadsfsdf', '2015-07-02 02:38:18', 1),
(134, '1', '4', 'again', '2015-07-02 02:41:11', 1),
(135, '1', '4', 'hi', '2015-07-02 02:42:59', 1),
(136, '1', '4', 'hello', '2015-07-13 13:28:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_compatibility_setting`
--

CREATE TABLE IF NOT EXISTS `dating_compatibility_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `looking_for` varchar(255) NOT NULL,
  `partner_looking_for` varchar(255) NOT NULL,
  `looking_for_important` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `partner_marital_status` varchar(255) NOT NULL,
  `marital_status_important` varchar(255) NOT NULL,
  `believe_love` varchar(255) NOT NULL,
  `partner_believe_love` varchar(255) NOT NULL,
  `believe_love_important` varchar(255) NOT NULL,
  `have_kids` varchar(255) NOT NULL,
  `partner_have_kids` varchar(255) NOT NULL,
  `have_kids_important` varchar(255) NOT NULL,
  `family_importance` varchar(255) NOT NULL,
  `partner_family_importance` varchar(255) NOT NULL,
  `family_importance_important` varchar(255) NOT NULL,
  `loyal_in_relation` varchar(255) NOT NULL,
  `partner_loyal_in_relation` varchar(255) NOT NULL,
  `loyal_in_relation_important` varchar(255) NOT NULL,
  `romantic_person` varchar(255) NOT NULL,
  `partner_romantic_person` varchar(255) NOT NULL,
  `romantic_person_important` varchar(255) NOT NULL,
  `longest_relation` varchar(255) NOT NULL,
  `partner_longest_relation` varchar(255) NOT NULL,
  `longest_relation_important` varchar(255) NOT NULL,
  `religion_importance` varchar(255) NOT NULL,
  `partner_religion_importance` varchar(255) NOT NULL,
  `religion_importance_important` varchar(255) NOT NULL,
  `willing_relocate` varchar(255) NOT NULL,
  `partner_willing_relocate` varchar(255) NOT NULL,
  `willing_relocate_important` varchar(255) NOT NULL,
  `new_experience` varchar(255) NOT NULL,
  `partner_new_experience` varchar(255) NOT NULL,
  `new_experience_important` varchar(255) NOT NULL,
  `show_affection` varchar(255) NOT NULL,
  `partner_show_affection` varchar(255) NOT NULL,
  `show_affection_important` varchar(255) NOT NULL,
  `friend_with_ex` varchar(255) NOT NULL,
  `partner_friend_with_ex` varchar(255) NOT NULL,
  `friend_with_ex_important` varchar(255) NOT NULL,
  `financially_secure` varchar(255) NOT NULL,
  `partner_financially_secure` varchar(255) NOT NULL,
  `financially_secure_important` varchar(255) NOT NULL,
  `most_enjoy` varchar(255) NOT NULL,
  `partner_most_enjoy` varchar(255) NOT NULL,
  `most_enjoy_important` varchar(255) NOT NULL,
  `smoke` varchar(255) NOT NULL,
  `partner_smoke` varchar(255) NOT NULL,
  `smoke_important` varchar(255) NOT NULL,
  `drink` varchar(255) NOT NULL,
  `partner_drink` varchar(255) NOT NULL,
  `drink_important` varchar(255) NOT NULL,
  `exercise` varchar(255) NOT NULL,
  `partner_exercise` varchar(255) NOT NULL,
  `exercise_important` varchar(255) NOT NULL,
  `bill_pay` varchar(255) NOT NULL,
  `partner_bill_pay` varchar(255) NOT NULL,
  `bill_pay_important` varchar(255) NOT NULL,
  `want_from_relationship` varchar(255) NOT NULL,
  `partner_want_from_relationship` varchar(255) NOT NULL,
  `want_from_relationship_important` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dating_compatibility_setting`
--

INSERT INTO `dating_compatibility_setting` (`id`, `user_id`, `looking_for`, `partner_looking_for`, `looking_for_important`, `marital_status`, `partner_marital_status`, `marital_status_important`, `believe_love`, `partner_believe_love`, `believe_love_important`, `have_kids`, `partner_have_kids`, `have_kids_important`, `family_importance`, `partner_family_importance`, `family_importance_important`, `loyal_in_relation`, `partner_loyal_in_relation`, `loyal_in_relation_important`, `romantic_person`, `partner_romantic_person`, `romantic_person_important`, `longest_relation`, `partner_longest_relation`, `longest_relation_important`, `religion_importance`, `partner_religion_importance`, `religion_importance_important`, `willing_relocate`, `partner_willing_relocate`, `willing_relocate_important`, `new_experience`, `partner_new_experience`, `new_experience_important`, `show_affection`, `partner_show_affection`, `show_affection_important`, `friend_with_ex`, `partner_friend_with_ex`, `friend_with_ex_important`, `financially_secure`, `partner_financially_secure`, `financially_secure_important`, `most_enjoy`, `partner_most_enjoy`, `most_enjoy_important`, `smoke`, `partner_smoke`, `smoke_important`, `drink`, `partner_drink`, `drink_important`, `exercise`, `partner_exercise`, `exercise_important`, `bill_pay`, `partner_bill_pay`, `bill_pay_important`, `want_from_relationship`, `partner_want_from_relationship`, `want_from_relationship_important`) VALUES
(3, 1, '2', '4', '1', '2', '2', '1', '1', '1', '1', '1', '1', '1', '1', '3', '1', '4', '1', '1', '1', '3', '1', '4', '4', '1', '1', '2', '1', '1', '1', '1', '1', '1', '1', '4', '1', '1', '3', '2', '1', '1', '2', '1', '2', '1', '1', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '2', '1', '1', '2', '1'),
(9, 26, '3', '4', '2', '1', '1', '', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 4, '2', '1,2,3', '2', '2', '1,3,4', '1', '2', '1,2', '3', '1', '1,2', '3', '2', '1,2,3', '2', '1', '1,2,3', '1', '2', '1,2,3', '1', '1', '4,5', '1', '2', '1,2', '3', '2', '1,2,4', '2', '2', '1,2', '3', '2', '1,2,3,4', '2', '2', '2,3', '2', '2', '2', '1', '2', '1,2', '3', '1', '1', '3', '1', '1', '3', '1', '1,2', '1', '1', '3,4', '2', '4', '1,4,5', '3'),
(5, 5, '3', '1,2,3', '2', '3', '1,3,4', '1', '2', '1,2', '3', '1', '1,2', '3', '1', '1,2,3', '2', '2', '1,2,3', '1', '3', '1,2,3', '1', '2', '4,5', '1', '3', '1,2', '3', '3', '1,2,4', '2', '3', '1,2', '3', '3', '1,2,3,4', '2', '1', '2,3', '2', '3', '2', '1', '1', '1,2', '3', '2', '1', '3', '2', '1', '3', '4', '1,2', '1', '3', '3,4', '2', '5', '1,4,5', '3'),
(6, 3, '5', '1,2,3', '2', '4', '1,3,4', '1', '1', '1,2', '3', '1', '1,2', '3', '2', '1,2,3', '2', '3', '1,2,3', '1', '1', '1,2,3', '1', '3', '4,5', '1', '4', '1,2', '3', '4', '1,2,4', '2', '1', '1,2', '3', '1', '1,2,3,4', '2', '3', '2,3', '2', '1', '2', '1', '1', '1,2', '3', '2', '1', '3', '1', '1', '3', '2', '1,2', '1', '2', '3,4', '2', '2', '1,4,5', '3'),
(7, 6, '4', '1,2,3', '2', '5', '1,3,4', '1', '2', '1,2', '3', '2', '1,2', '3', '3', '1,2,3', '2', '4', '1,2,3', '1', '3', '1,2,3', '1', '5', '4,5', '1', '1', '1,2', '3', '1', '1,2,4', '2', '3', '1,2', '3', '4', '1,2,3,4', '2', '2', '2,3', '2', '2', '2', '1', '3', '1,2', '3', '3', '1', '3', '3', '1', '3', '1', '1,2', '1', '4', '3,4', '2', '1', '1,4,5', '3'),
(8, 8, '6', '1,2,3', '1', '1', '1,3', '3', '1', '1,2', '3', '2', '1,2', '3', '3', '2,3', '1', '2', '2', '1', '1', '1,3', '1', '2', '2,3', '1', '4', '4', '1', '', '', '1', '', '', '1', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1,2', '3');

-- --------------------------------------------------------

--
-- Table structure for table `dating_draft`
--

CREATE TABLE IF NOT EXISTS `dating_draft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=83 ;

--
-- Dumping data for table `dating_draft`
--

INSERT INTO `dating_draft` (`id`, `user_id`, `to_id`, `message`, `date`) VALUES
(71, 1, 3, 'Hi,\r\nAbhishek. How are you?\r\n\r\nAbhi', '2014-11-29 06:57:35'),
(74, 1, 0, 'Test message', '2014-11-29 07:04:32'),
(82, 1, 3, 'Test Message...', '2014-12-06 06:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `dating_friend_requests`
--

CREATE TABLE IF NOT EXISTS `dating_friend_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `new_request` int(11) NOT NULL,
  `accept` tinyint(1) NOT NULL DEFAULT '0',
  `deny` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `dating_friend_requests`
--

INSERT INTO `dating_friend_requests` (`id`, `from_id`, `to_id`, `new_request`, `accept`, `deny`, `date`) VALUES
(20, 11, 12, 0, 1, 0, '2015-02-25 06:04:24'),
(21, 1, 3, 1, 0, 0, '2015-03-14 23:59:52'),
(18, 1, 8, 0, 1, 0, '2014-11-19 01:39:00'),
(17, 1, 4, 0, 1, 0, '2014-11-13 23:25:39'),
(19, 1, 5, 0, 1, 0, '2014-11-28 15:18:49'),
(22, 3, 8, 1, 0, 0, '2015-05-29 14:56:29'),
(23, 1, 6, 1, 0, 0, '2015-05-31 18:48:12'),
(27, 3, 5, 0, 1, 0, '2015-06-30 14:13:55'),
(26, 78, 5, 0, 1, 0, '2015-06-19 14:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `dating_interest`
--

CREATE TABLE IF NOT EXISTS `dating_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `dating_interest`
--

INSERT INTO `dating_interest` (`id`, `user_id`, `type`, `title`, `link`) VALUES
(3, 1, 1, 'My Music 2', 'www.youtube.com/watch?v=eytE4BU9Atk'),
(7, 1, 3, 'Test TV show 1', 'http://www.google.com'),
(10, 1, 4, 'Fear and Loathing in Las Vegas', 'http://www.amazon.com/Fear-Loathing-Las-Vegas-American/dp/0679785892'),
(11, 1, 1, 'I LOVE MUSIC', ''),
(12, 7, 1, 'You Tube', 'https://www.youtube.com/'),
(13, 7, 1, 'Find partners for fun, dating and long term relationships', 'https://www.youtube.com/'),
(14, 1, 2, 'Scarface', '');

-- --------------------------------------------------------

--
-- Table structure for table `dating_kiss`
--

CREATE TABLE IF NOT EXISTS `dating_kiss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `form_status` tinyint(1) NOT NULL DEFAULT '1',
  `to_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `dating_kiss`
--

INSERT INTO `dating_kiss` (`id`, `from_id`, `to_id`, `message`, `date`, `form_status`, `to_status`) VALUES
(1, 3, 1, '', '2014-10-14 12:09:09', 1, 1),
(15, 1, 5, '', '2015-02-06 18:36:14', 1, 1),
(14, 1, 3, '', '2014-12-10 22:02:50', 1, 1),
(13, 5, 1, '', '2014-11-07 15:50:04', 1, 1),
(12, 1, 3, '', '2014-11-07 00:18:23', 1, 1),
(11, 1, 4, '', '2014-11-07 00:10:52', 1, 1),
(10, 1, 3, '', '2014-11-07 00:10:48', 1, 1),
(16, 1, 4, '', '2015-06-07 11:00:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dating_languages`
--

CREATE TABLE IF NOT EXISTS `dating_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `dating_languages`
--

INSERT INTO `dating_languages` (`id`, `name`, `name_translated`) VALUES
(3, 'English', 'Inglés'),
(4, 'Spanish', 'español'),
(5, 'French', 'french'),
(6, 'Arabic', 'arabic'),
(7, 'Italian', 'italian'),
(8, 'German', 'german'),
(9, 'Indian', 'indian'),
(10, 'Chinese', 'chinese'),
(11, 'Other', 'other');

-- --------------------------------------------------------

--
-- Table structure for table `dating_moreimages`
--

CREATE TABLE IF NOT EXISTS `dating_moreimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `dating_moreimages`
--

INSERT INTO `dating_moreimages` (`id`, `user_id`, `title`, `image`) VALUES
(4, 1, '', 'eb4c13caba8fc0295dc0a43139e83d05How-to-have-healthy-skin_.jpg'),
(7, 1, 'Title1', '703e67a097e2b98c330035efed0c4462images12.jpg'),
(6, 1, 'Title2', 'f9f9f5286b6f4e1e4b5fd021b6a36377images9.jpg'),
(8, 8, 'Title1', 'cafe51eb07b1224ad07ad951f8dd7704goodone of kelsandmom.jpg'),
(9, 8, 'Title2', '2a830e2dd3ce61286cca05f98a068e5eHydrangeas.jpg'),
(14, 8, 'Title3', '167deb95c71c9cba9a94c6a3b908a3a0up-im.png'),
(11, 8, 'Title4', '0e00ca92905d78197d3921a3d7453a2fPenguins.jpg'),
(13, 1, 'Title3', '054b4fc61248a40603f3baf6e894d8aeup-im.png'),
(15, 8, 'Title4', '9aa232dcc8d88423e450642d8d802985sample.png'),
(16, 8, '', '2dff0b2a4cf83a4c5c790ff21d0a7cb5happy-face.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dating_newsletter`
--

CREATE TABLE IF NOT EXISTS `dating_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `user` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dating_reference`
--

CREATE TABLE IF NOT EXISTS `dating_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `ref_user_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `dating_reference`
--

INSERT INTO `dating_reference` (`id`, `ref_id`, `ref_user_id`, `user_id`) VALUES
(2, 'miamor000001', 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `dating_report`
--

CREATE TABLE IF NOT EXISTS `dating_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `report_for` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `dating_report`
--

INSERT INTO `dating_report` (`id`, `user_id`, `report_for`, `message`, `date`) VALUES
(2, 1, 5, 'This is a test report...', '2015-01-21 05:30:58'),
(4, 1, 8, 'Test report...', '2015-01-21 05:54:33'),
(6, 1, 3, 'test report for payle', '2015-01-21 06:06:06'),
(8, 1, 5, 'test report...k', '2015-01-21 06:15:47'),
(9, 1, 4, 'test report...', '2015-01-21 06:21:01');

-- --------------------------------------------------------

--
-- Table structure for table `dating_sendnewsletter`
--

CREATE TABLE IF NOT EXISTS `dating_sendnewsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dating_sendnewsletter`
--

INSERT INTO `dating_sendnewsletter` (`id`, `title`, `description`, `user`, `date`) VALUES
(1, 'This is the tile', 'desc', 0, '2015-03-11 06:31:22'),
(2, 'Title', '<p>\r\n	attchement with mail</p>\r\n', 0, '2015-03-11 07:07:38'),
(3, 'Find partners for fun, dating and long term relationships', '<p>\r\n	Find partners for fun, dating and long term relationships</p>\r\n', 0, '2015-03-11 07:09:40'),
(4, 'Find partners for fun, dating and long term relationships', '<p>\r\n	Find partners for fun, dating and long term relationships</p>\r\n', 0, '2015-03-11 07:13:08'),
(5, 'Find perfect match from miamor apps', '<p>\r\n	Find perfect match from miamor apps.&nbsp;<br />\r\n	with the attachement .</p>\r\n', 0, '2015-03-11 07:16:17'),
(6, 'Lorem test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		Lorem Ipsum Doller sit amet</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-06-09 12:36:27'),
(7, 'test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		Test</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-14 04:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `dating_sms`
--

CREATE TABLE IF NOT EXISTS `dating_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mobilenumber` varchar(255) NOT NULL,
  `verifycode` varchar(255) NOT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `dating_sms`
--

INSERT INTO `dating_sms` (`id`, `user_id`, `mobilenumber`, `verifycode`, `is_verified`) VALUES
(10, 3, '+917044352633', '1827282914', 0),
(6, 1, '+573142928688', '2142475828', 1),
(9, 5, '+919002561033', '550626946', 1),
(11, 49, '+919331861603', '2079533980', 0),
(12, 0, '+573142928688', '1015192544', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_template`
--

CREATE TABLE IF NOT EXISTS `dating_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dating_template`
--

INSERT INTO `dating_template` (`id`, `subject`, `content`) VALUES
(1, 'Welcome to Miamor', '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e8e8e8">\r\n  <tr>\r\n    <td>\r\n    <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">\r\n  <tr>\r\n    <td height="70" bgcolor="#718ea0">\r\n    <table width="600" border="0" cellspacing="0" cellpadding="0">\r\n  <tr>\r\n    <td width="5%">&nbsp;</td>\r\n    <td width="90%" style="text-align:left;">[LOGO]</td>\r\n    <td width="5%">&nbsp;</td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td>\r\n    <table width="600" border="0" cellspacing="0" cellpadding="0">\r\n  <tr>\r\n    <td width="5%">&nbsp;</td>\r\n    <td width="90%">\r\n    	<table width="100%" border="0" cellspacing="0" cellpadding="0">\r\n  <tr>\r\n    <td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td><font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [FIRST NAME]</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td><font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, \r\n    lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. \r\n    Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. \r\n    Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor \r\n    eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</font> </td>\r\n  </tr>\r\n  <tr>\r\n  	<td><font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Please Click on this link below </font></td>\r\n  </tr>\r\n  <tr>\r\n  	<td><font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">[LINK]</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td><font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br>\r\n    <font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[FIRST NAME]</font>\r\n</td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n    <td width="5%;">&nbsp;</td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n  	<td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n  	<td>&nbsp;</td>\r\n  </tr>\r\n  <tr>\r\n    <td height="70" bgcolor="#0099ff">\r\n    <table width="600" border="0" cellspacing="0" cellpadding="0">\r\n  <tr>\r\n    <td width="5%">&nbsp;</td>\r\n    <td width="90%">\r\n    <table width="100%" border="0" cellspacing="0" cellpadding="0">\r\n  <tr>\r\n    <td width="50%" height="70"><font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright © 2015 Miamor</font></td>\r\n    <td align="right" width="50%"><font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;">\r\n    <a style="text-decoration:none; color:#fff;" href="">Terms and Conditions</a>  |  \r\n    <a style="text-decoration:none; color:#fff;" href="">Privacy Policy</a></font></td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n    <td width="5%">&nbsp;</td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n  </tr>\r\n</table>\r\n\r\n    </td>\r\n  </tr>\r\n</table>'),
(2, 'Compability Matching ', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												[LIST]\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<ul style="padding:0;list-style: none;margin:0">\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																</ul>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(3, 'You have received a message from', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																You have recieved a message from [SENDERNAME]. Please login to see the message.</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(4, 'You Have Been Viewed By -', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																You have been viewed by these people....</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<ul style="padding:0;list-style: none;margin:0">\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																</ul>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(5, 'These are matches we think you will like -', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																These are the matches we think you will like..</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<ul style="padding:0;list-style: none;margin:0">\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																</ul>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n');
INSERT INTO `dating_template` (`id`, `subject`, `content`) VALUES
(6, 'Your Compatibility matches -', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Here are your compatiable matches..</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<ul style="padding:0;list-style: none;margin:0">\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																</ul>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(7, 'We haven’t seen you for awhile? we think you would like these singles…', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												[LIST]\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<ul style="padding:0;list-style: none;margin:0">\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																	<li style="float:left;width:30%;margin:0 1%;text-align: center">\r\n																		<div style="background:url(http://i.imgur.com/u51zj19.png) no-repeat;width:118px;height:111px;margin:0 auto;">\r\n																			<img alt="" src="http://i.imgur.com/6dKx5pq.jpg" style="width:94px;margin:0 auto;margin-top:10px;max-height:100px" /></div>\r\n																		<h2 style="color:#0099FF;margin:2px;font-family:arial;font-size:18px;">\r\n																			Teneile</h2>\r\n																		<p style="color:##8b8989;margin:2px;font-family:arial;font-size:12px;">\r\n																			28 years old</p>\r\n																		<button style="background:#0099FF;border:0;padding:2px 5px;color:#fff;text-transform: uppercase;font-size:12px;border-radius:3px;margin-bottom:10px;cursor: pointer;margin-top:4px">View Profile</button></li>\r\n																</ul>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] Miamor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `dating_translation_package`
--

CREATE TABLE IF NOT EXISTS `dating_translation_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `days` int(5) NOT NULL,
  `price` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dating_translation_package`
--

INSERT INTO `dating_translation_package` (`id`, `name`, `days`, `price`) VALUES
(1, 'Package 1', 7, 15),
(2, 'Package 2', 14, 30),
(3, 'Package 3', 21, 45),
(4, 'package 4', 30, 55);

-- --------------------------------------------------------

--
-- Table structure for table `dating_userimages`
--

CREATE TABLE IF NOT EXISTS `dating_userimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_img` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `dating_userimages`
--

INSERT INTO `dating_userimages` (`id`, `user_id`, `title`, `image`, `main_img`) VALUES
(1, 8, '', '07cae5548f849831574d856f1867ed6bPenguins.jpg', 1),
(2, 8, '', 'a99af7d5ce1b1de9362e86b66c0434c8Lighthouse.jpg', 0),
(5, 8, '', 'be2b06fa5253a1963dec66de6327db56Tulips.jpg', 0),
(9, 11, '', 'cda6d3a28672624329b63b260b60a56312101363~2d10bb905b10cb41954131e61905b3d10d516d68-stocklarge (1).jpg', 1),
(11, 1, '', '7dfdf4283d6bdacf96aa459e08927954cakephp1.png', 1),
(12, 26, '', '269f49c0e5768cf33dafde21724201a9design.png', 1),
(13, 87, '', '33ead1c375fc5c0ccee455a0727e8fe1logo.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dating_user_video`
--

CREATE TABLE IF NOT EXISTS `dating_user_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `dating_user_video`
--

INSERT INTO `dating_user_video` (`id`, `user_id`, `video`) VALUES
(2, 1, 'www.youtube.com/watch?v=eytE4BU9Atk'),
(3, 1, 'https://www.youtube.com/watch?v=WH9ZJu4wRUE');

-- --------------------------------------------------------

--
-- Table structure for table `dating_videosessionchat`
--

CREATE TABLE IF NOT EXISTS `dating_videosessionchat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `sessionid` text,
  `tok` text,
  `is_session` tinyint(1) NOT NULL DEFAULT '0',
  `quit` tinyint(1) DEFAULT '0',
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `dating_videosessionchat`
--

INSERT INTO `dating_videosessionchat` (`id`, `to_id`, `from_id`, `sessionid`, `tok`, `is_session`, `quit`, `is_finished`) VALUES
(60, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2NjczODk0NH5nZXc0OTdaTTA3WWN2L2tzODhFVnFud0t-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NWQzZDA1OTkxNTBjOWFhMGY3MjY4YjFiMDkxMjU4NTE2MzliNTdiNjpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTmpjek9EazBOSDVuWlhjME9UZGFUVEEzV1dOMkwydHpPRGhGVm5GdWQwdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NjczOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY2NzM5LjAxMTIyMjY5MDM0NSZleHBpcmVfdGltZT0xNDMxMDc3NTM5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(61, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzA0NTMxNn4yTEVEOWhxNjRiRWlreUNrMHpUZmZkMFJ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZDAwNDg0YWNiODViZGFhNmE2MGFhYjczMDY3ODgyZTgxMWZjM2Y4MjpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpBME5UTXhObjR5VEVWRU9XaHhOalJpUldscmVVTnJNSHBVWm1aa01GSi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzA0NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MDQ1LjM3NzkzNzQ5Mjk4JmV4cGlyZV90aW1lPTE0MzEwNzc4NDUmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(62, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2NzI1NTkzNX5MQTNISUpCVnJHZDdzK0RYS0lhdzFWUWN-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MTM1Nzk5ZGZlMGYyMTcyYjBmZGZmYTA0OGY4ODkwYWI2OWRmYjBhNTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpJMU5Ua3pOWDVNUVROSVNVcENWbkpIWkRkekswUllTMGxoZHpGV1VXTi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzI1NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MjU2MTg0MTAzNjcyOCZleHBpcmVfdGltZT0xNDMxMDc4MDU1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(63, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzM0MjAxOH4vYU15blFMMEluTzhqK1dQUzlXc3VhVGV-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9Nzk3OGMzNTcyM2Q3NTNiMzkzYWYyODQ1MTNlMjA0MzExMzk0NWM0YTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpNME1qQXhPSDR2WVUxNWJsRk1NRWx1VHpocUsxZFFVemxYYzNWaFZHVi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzM0MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MzQyLjA4NzA3ODk1MDkmZXhwaXJlX3RpbWU9MTQzMTA3ODE0MiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(64, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzQxOTAyM35rUHZtZ1BVQzNRb1I3Y1k3Y3c3V1dwK0x-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZGZkM2JmNjhjNjY4ZTY1OGU2ZTA1NjA1MWIwNTY5YTRmM2IxYzczMDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpReE9UQXlNMzVyVUhadFoxQlZRek5SYjFJM1kxazNZM2MzVjFkd0sweC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzQxOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3NDE5LjA4MjE0NTU1Mzk3NyZleHBpcmVfdGltZT0xNDMxMDc4MjE5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(65, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzUzNDkzNX5HS0VxVGd4VnFXSmFNUXNNMmRrSlZFYkx-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NzY4ZGVjYmY1YjM3MGRhNDI4NTc4Y2E1ODdiMmVmMmI4MzhmOTBiZDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpVek5Ea3pOWDVIUzBWeFZHZDRWbkZYU21GTlVYTk5NbVJyU2xaRllreC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzUzNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3NTM1MTMwNDI0MjQ1MyZleHBpcmVfdGltZT0xNDMxMDc4MzM0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(66, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzkwNDE4N35LZ2hGUGVEL0k2RDluVUJ5bklTdTVadGx-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MzIyMTU4ODM0YzIzNGU5NDI1NzU3MzQzZWE4MTEyMDczNmRjMWQyODpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprd05ERTROMzVMWjJoR1VHVkVMMGsyUkRsdVZVSjVia2xUZFRWYWRHeC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzkwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTA0LjI0MTg5MzkwMTM0MCZleHBpcmVfdGltZT0xNDMxMDc4NzA0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(67, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2Nzk2NjI4M354RGhnT09ybUN0Ym8xV1FPd1o4T21qRVB-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9M2I2ZWMyNWJiNDM3ODc2ODg0MzYxNGY0NzUxMTc4NzIxMjcyNTI0NzpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprMk5qSTRNMzU0UkdoblQwOXliVU4wWW04eFYxRlBkMW80VDIxcVJWQi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2Nzk2NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTY2LjM0MTk0NTQxNzYzJmV4cGlyZV90aW1lPTE0MzEwNzg3NjYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(68, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2Nzk5NzE5NH5GN2xla25sc2JYeWEybmFHV1U0c2EydkR-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NWNkYjViYjhjZTM3MGYyZWUzNTczMTA1MzEyZDA4ZDExYTQ3NDU3MDpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprNU56RTVOSDVHTjJ4bGEyNXNjMkpZZVdFeWJtRkhWMVUwYzJFeWRrUi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2Nzk5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTk3LjI2MTMxMjA5OTc5MiZleHBpcmVfdGltZT0xNDMxMDc4Nzk3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(69, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2ODA3ODkyNX52RG1qZ25HTURubE1xWnAydHkxdFBBSE1-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NmE3ODA0MjE1Yzk4YTRlZDk3YWI2NDc5MzhmMTgxZDcwY2JiMmRmMTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RBM09Ea3lOWDUyUkcxcVoyNUhUVVJ1YkUxeFduQXlkSGt4ZEZCQlNFMS1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODA3OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MDc4Ljk5NTM2NTE0ODk2JmV4cGlyZV90aW1lPTE0MzEwNzg4NzgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(70, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2ODEwNTk5N34wNzdtUmlCWSt1bnhpM0xUWWZoSm9Tbit-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZTNkOWQxOTY0M2VkNjZiOWUyZGI5YTczMTFkZGI2ZDk2OTA0MTYxMTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RFd05UazVOMzR3TnpkdFVtbENXU3QxYm5ocE0weFVXV1pvU205VGJpdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODEwNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MTA2LjA2MTM1MDQzMjQxOCZleHBpcmVfdGltZT0xNDMxMDc4OTA2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(71, 1, 5, '1_MX40NTIzMTAxMn5-MTQzMTA2ODE4MTE0Nn5oYXp5TTBLSzlZY2dlYmpLTlY0b0VNTUd-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9OTdhYjkyNjZjNGMwZTlkOTE1MTE0Njc2Zjg0NzJiZWJhNjVkZjdmYzpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RFNE1URTBObjVvWVhwNVRUQkxTemxaWTJkbFltcExUbFkwYjBWTlRVZC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODE4MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MTgxLjIxMTM3MDkwMzcyOCZleHBpcmVfdGltZT0xNDMxMDc4OTgxJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(72, 1, 5, '2_MX40NTIzMTAxMn5-MTQzMTA5MTYzMzc1OX54UlRWaXRJeDdqRU9BWVJRcktGYUV0N1B-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NDBjM2I0NzY0ZmFjOGM3NTA3NGQ4Yjc1M2RjNmRmMmJiODJlOWQ3MDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEE1TVRZek16YzFPWDU0VWxSV2FYUkplRGRxUlU5QldWSlJja3RHWVVWME4xQi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA5MTYzMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDkxNjMzLjgyOTE4ODEwMjY5JmV4cGlyZV90aW1lPTE0MzExMDI0MzMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(73, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA5MjAzNjQyMH5ncGtqejJjc0NsY2ZSTHBWcXdPR3hZa0t-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZjJiNjdiOTY4MGM2ZTU4YzExMjM3NDA3NWM4ZjY1OGQzZmI2NjA2NTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEE1TWpBek5qUXlNSDVuY0d0cWVqSmpjME5zWTJaU1RIQldjWGRQUjNoWmEwdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA5MjAzNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDkyMDM2LjQ4MTAxMzUxNTYwNCZleHBpcmVfdGltZT0xNDMxMTAyODM2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(74, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTMyMzYyNzc2MX45eWlIWUdDVEpOSzJIVE8yWldVYWJiVHF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9Y2RkNTk0NWE4YzA4NmFjOGEzMzcyNDU5OGY0OGJjYzM4MDY2ODE4MzpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVE15TXpZeU56YzJNWDQ1ZVdsSVdVZERWRXBPU3pKSVZFOHlXbGRWWVdKaVZIRi1VSDQmY3JlYXRlX3RpbWU9MTQzMTMyMzYyNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzIzNjI3LjgyMTgxNjcxNzkyNSZleHBpcmVfdGltZT0xNDMxMzM0NDI3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(75, 1, 5, '2_MX40NTIzMTAxMn5-MTQzMTMyMzY5NDgzNn5EVG5qc2dOeEJKcWJpbDUxRm85cTRaL2x-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9OGE5ZjUxMjUwNGNjN2MxM2Y2OTk2ZTI5MGU1Nzc1YjIzNTM1OGY5NDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVE15TXpZNU5EZ3pObjVFVkc1cWMyZE9lRUpLY1dKcGJEVXhSbTg1Y1RSYUwyeC1VSDQmY3JlYXRlX3RpbWU9MTQzMTMyMzY5NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzIzNjk0LjkyMDI2Mjk0OTI3JmV4cGlyZV90aW1lPTE0MzEzMzQ0OTQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(76, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTM3ODkwNDM0MH41QTU3VVNOQ0t4Qkh4akpmazd5SjRocGR-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZDdiNTE1MDUyN2I3YTM4NDc3OTdiZGI0MmUxMGYyZjhlYTBjNjQ1NTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVE0zT0Rrd05ETTBNSDQxUVRVM1ZWTk9RMHQ0UWtoNGFrcG1hemQ1U2pSb2NHUi1VSDQmY3JlYXRlX3RpbWU9MTQzMTM3ODkwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzc4OTA0LjQxNzc2OTUwMDMwJmV4cGlyZV90aW1lPTE0MzEzODk3MDQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(77, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMjA0NDYwNDAzOX5ObGxLTkZIVWRGaDRzdnVIU2RRWnJoWEF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9N2RjMzg5N2U2MzI2YzlhODJhNmQyYjYwZDk5ZTVlNGU1YWZlMjY0YTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNakEwTkRZd05EQXpPWDVPYkd4TFRrWklWV1JHYURSemRuVklVMlJSV25Kb1dFRi1VSDQmY3JlYXRlX3RpbWU9MTQzMjA0NDYwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyMDQ0NjA0LjE3NzczODYzMjAmZXhwaXJlX3RpbWU9MTQzMjA1NTQwNCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(78, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMjA3MzEzMzM5MH4vbzRHeUJDUDY0SEZJYzNQbGNSUUxqSnF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MzhiYzFiNzNhYjIwNjlmNGQwYjgxYmY5MmVhZjU0NDMwNmRkMmM4YjpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNakEzTXpFek16TTVNSDR2YnpSSGVVSkRVRFkwU0VaSll6TlFiR05TVVV4cVNuRi1VSDQmY3JlYXRlX3RpbWU9MTQzMjA3MzEzMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyMDczMTMzLjQ1MTU5OTg0MzQwMSZleHBpcmVfdGltZT0xNDMyMDgzOTMzJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(79, 1, 5, '1_MX40NTIzMTAxMn5-MTQzMjU0MTg5NzMxM341VDV0S2pFSitVbXhpQ2RaRExEY3dIREZ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZWE3N2QxYWQzN2U4YjA4Nzk0NTExODYwMGFkYzY5MjM0N2MyMTlkYjpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNalUwTVRnNU56TXhNMzQxVkRWMFMycEZTaXRWYlhocFEyUmFSRXhFWTNkSVJFWi1VSDQmY3JlYXRlX3RpbWU9MTQzMjU0MTg5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyNTQxODk3LjM3MTY3MDYwMjQ3MCZleHBpcmVfdGltZT0xNDMyNTUyNjk3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(80, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMzEwMDE0ODM5MH5mM3pCZjVFb0UvWXpoWHpQUmI2TUpuVUZ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MWE2ODA4MzQ3Yzk4MzZhYjAxMzljYWE3MmYwYmE2MDcyYWNkOGY2ODpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNekV3TURFME9ETTVNSDVtTTNwQ1pqVkZiMFV2V1hwb1dIcFFVbUkyVFVwdVZVWi1VSDQmY3JlYXRlX3RpbWU9MTQzMzEwMDE0OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMTAwMTQ4LjQ1NTgyMzg0MTcyJmV4cGlyZV90aW1lPTE0MzMxMTA5NDgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(81, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMzE2MzYzNzA4N345Zyt4ZldJSmxiUW53eHp1dXpWcGNhQnp-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZTYzYTkzYzRiNzU4ZDJhYmRkMWZhMTlkZTlhYjNmOTA2ZmU0MTFjMTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNekUyTXpZek56QTROMzQ1Wnl0NFpsZEpTbXhpVVc1M2VIcDFkWHBXY0dOaFFucC1VSDQmY3JlYXRlX3RpbWU9MTQzMzE2MzYzNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMTYzNjM3LjE1MjAwNDE0NTgxJmV4cGlyZV90aW1lPTE0MzMxNzQ0MzcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(82, 1, 8, '2_MX40NTIzMTAxMn5-MTQzMzMzNDc4ODQ2NH5IZEVDOU1rNi9Ubk44bExDbHUzdFowQVp-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZGYyMDJkOTRlNjM4YjNjNzM1YjJlYTMyZmQ0YzU4NjBhNDllZTZhMzpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNek16TkRjNE9EUTJOSDVJWkVWRE9VMXJOaTlVYms0NGJFeERiSFV6ZEZvd1FWcC1VSDQmY3JlYXRlX3RpbWU9MTQzMzMzNDc4OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMzM0Nzg4LjUyOTczNTQwODc4JmV4cGlyZV90aW1lPTE0MzMzNDU1ODgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_wink`
--

CREATE TABLE IF NOT EXISTS `dating_wink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `form_status` tinyint(1) NOT NULL DEFAULT '1',
  `to_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `dating_wink`
--

INSERT INTO `dating_wink` (`id`, `from_id`, `to_id`, `message`, `date`, `form_status`, `to_status`) VALUES
(2, 3, 1, '', '2014-10-14 12:11:56', 1, 1),
(23, 1, 5, '', '2015-02-06 18:36:09', 1, 1),
(22, 1, 8, '', '2014-12-06 06:09:19', 1, 1),
(21, 1, 5, '', '2014-11-20 01:35:16', 1, 1),
(20, 1, 8, '', '2014-11-20 00:14:58', 1, 1),
(19, 1, 4, '', '2014-11-07 15:49:58', 1, 1),
(18, 1, 3, '', '2014-11-07 15:39:10', 1, 1),
(17, 1, 3, '', '2014-11-07 00:10:54', 1, 1),
(16, 1, 8, '', '2014-11-06 03:30:36', 1, 1),
(24, 1, 3, '', '2015-05-29 13:02:33', 1, 1),
(25, 1, 11, '', '2015-05-29 13:26:23', 1, 1),
(26, 3, 8, '', '2015-05-29 14:55:36', 1, 1),
(27, 1, 4, '', '2015-06-07 11:00:07', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
