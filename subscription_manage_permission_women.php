<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
if(isset($_REQUEST['submit']))
{
	$create_profile = isset($_POST['create_profile']) ? $_POST['create_profile'] : 'M';
	$add_photos = isset($_POST['add_photos']) ? $_POST['add_photos'] : 'R';
	$add_video = isset($_POST['add_video']) ? $_POST['add_video'] : 'R';
	$search_members = isset($_POST['search_members']) ? $_POST['search_members'] : 'R';
	$send_winks = isset($_POST['send_winks']) ? $_POST['send_winks'] : 'R';
	$reply_to_messages = isset($_POST['reply_to_messages']) ? $_POST['reply_to_messages'] : 'R';
	$view_profile_pages = isset($_POST['view_profile_pages']) ? $_POST['view_profile_pages'] : 'R';
	$send_friend_requests = isset($_POST['send_friend_requests']) ? $_POST['send_friend_requests'] : 'R';
	$send_kisses = isset($_POST['send_kisses']) ? $_POST['send_kisses'] : 'R';
	$send_messages = isset($_POST['send_messages']) ? $_POST['send_messages'] : 'R';
	$chat = isset($_POST['chat']) ? $_POST['chat'] : 'R';
	$video_chat = isset($_POST['video_chat']) ? $_POST['video_chat'] : 'R';
	$translate_chat = isset($_POST['translate_chat']) ? $_POST['translate_chat'] : 'R';
	$purchase_translation_tokens = isset($_POST['purchase_translation_tokens']) ? $_POST['purchase_translation_tokens'] : 'R';
        $viewed_me = isset($_POST['viewed_me']) ? $_POST['viewed_me'] : 'R';
	$fields = array(
		'create_profile' => mysql_real_escape_string($create_profile),
		'add_photos' => mysql_real_escape_string($add_photos),
		'add_video' => mysql_real_escape_string($add_video),
		'search_members' => mysql_real_escape_string($search_members),
		'send_winks' => mysql_real_escape_string($send_winks),
		'reply_to_messages' => mysql_real_escape_string($reply_to_messages),
		'view_profile_pages' => mysql_real_escape_string($view_profile_pages),
		'send_friend_requests' => mysql_real_escape_string($send_friend_requests),
		'send_kisses' => mysql_real_escape_string($send_kisses),
		'send_messages' => mysql_real_escape_string($send_messages),
		'chat' => mysql_real_escape_string($chat),
		'video_chat' => mysql_real_escape_string($video_chat),
		'translate_chat' => mysql_real_escape_string($translate_chat),
		'purchase_translation_tokens' => mysql_real_escape_string($purchase_translation_tokens),
                'viewed_me' => mysql_real_escape_string($viewed_me)
		);
		$fieldsList = array();
		foreach ($fields as $field => $value)
		{
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
		$editQuery = "UPDATE `dateing_package_women` SET " . implode(', ', $fieldsList). " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
		//echo $editQuery;
		if (mysql_query($editQuery))
		{
			$_SESSION['msg'] = "Subscription Package Updated Successfully";
		}
		else
		{
			$_SESSION['msg'] = "Error occuried while updating Subscription Package";
		}
		//header('Location:social.php');
		//exit();
}
	$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `dateing_package_women` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

?>

<!DOCTYPE html>

<html>

    

    <head>

        <title>Edit Subscription Package</title>

        <!-- Bootstrap -->

        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">

        <link href="assets/styles.css" rel="stylesheet" media="screen">

       

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    </head>

    

    <body>

         <?php include_once('includes/header.php');?>

        <div class="container-fluid">

            <div class="row-fluid">

                 <?php include_once('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Manage Permission</div>
                            </div>
<div class="block-content collapse in">
	<div class="span12">
		<!--subscription_package_edit.php-->
		<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
			<fieldset>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Create Profile</label>
					<div class="controls">
						<label><input name="create_profile" type="radio" value="A" <?php if($categoryRowset['create_profile'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="create_profile" type="radio" value="R" <?php if($categoryRowset['create_profile'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Add Photos</label>
					<div class="controls">
						<label><input name="add_photos" type="radio" value="A" <?php if($categoryRowset['add_photos'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="add_photos" type="radio" value="R" <?php if($categoryRowset['add_photos'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Add Video</label>
					<div class="controls">
						<label><input name="add_video" type="radio" value="A" <?php if($categoryRowset['add_video'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="add_video" type="radio" value="R" <?php if($categoryRowset['add_video'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Search Members</label>
					<div class="controls">
						<label><input name="search_members" type="radio" value="A" <?php if($categoryRowset['search_members'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="search_members" type="radio" value="R" <?php if($categoryRowset['search_members'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Send Winks ( More then 5 )</label>
					<div class="controls">
						<label><input name="send_winks" type="radio" value="A" <?php if($categoryRowset['send_winks'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="send_winks" type="radio" value="R" <?php if($categoryRowset['send_winks'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Reply To Messages</label>
					<div class="controls">
						<label><input name="reply_to_messages" type="radio" value="A" <?php if($categoryRowset['reply_to_messages'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="reply_to_messages" type="radio" value="R" <?php if($categoryRowset['reply_to_messages'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<!----------------------------------------------------------------------------------------------->
				<!----------------------------------------------------------------------------------------------->
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">View Profile Pages</label>
					<div class="controls">
						<label><input name="view_profile_pages" type="radio" value="A" <?php if($categoryRowset['view_profile_pages'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="view_profile_pages" type="radio" value="R" <?php if($categoryRowset['view_profile_pages'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Send Friend Requests</label>
					<div class="controls">
						<label><input name="send_friend_requests" type="radio" value="A" <?php if($categoryRowset['send_friend_requests'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="send_friend_requests" type="radio" value="R" <?php if($categoryRowset['send_friend_requests'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Send Kisses</label>
					<div class="controls">
						<label><input name="send_kisses" type="radio" value="A" <?php if($categoryRowset['send_kisses'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="send_kisses" type="radio" value="R" <?php if($categoryRowset['send_kisses'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Send Messages</label>
					<div class="controls">
						<label><input name="send_messages" type="radio" value="A" <?php if($categoryRowset['send_messages'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="send_messages" type="radio" value="R" <?php if($categoryRowset['send_messages'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Chat</label>
					<div class="controls">
						<label><input name="chat" type="radio" value="A" <?php if($categoryRowset['chat'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="chat" type="radio" value="R" <?php if($categoryRowset['chat'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="focusedInput">Video Chat</label>
					<div class="controls">
						<label><input name="video_chat" type="radio" value="A" <?php if($categoryRowset['video_chat'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="video_chat" type="radio" value="R" <?php if($categoryRowset['video_chat'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Translate Chat</label>
					<div class="controls">
						<label><input name="translate_chat" type="radio" value="A" <?php if($categoryRowset['translate_chat'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="translate_chat" type="radio" value="R" <?php if($categoryRowset['translate_chat'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="focusedInput">Purchase Translation Tokens</label>
					<div class="controls">
						<label><input name="purchase_translation_tokens" type="radio" value="A" <?php if($categoryRowset['purchase_translation_tokens'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="purchase_translation_tokens" type="radio" value="R" <?php if($categoryRowset['purchase_translation_tokens'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
                                <div class="control-group">
					<label class="control-label" for="focusedInput">View-Viewed Me</label>
					<div class="controls">
						<label><input name="viewed_me" type="radio" value="A" <?php if($categoryRowset['viewed_me'] == 'A')echo 'checked="checked"';?>><span>Approved</span></label>
						<label><input name="viewed_me" type="radio" value="R" <?php if($categoryRowset['viewed_me'] == 'R')echo 'checked="checked"';?>><span>Restricted</span></label>
					</div>
				</div>
				
				<div class="form-actions">
					<button type="submit" class="btn btn-primary"  name="submit">Save changes</button>
					<button type="reset" class="btn" onClick="location.href='subscription_package_management_women.php'">Back</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>

            <hr>

             <?php include_once('includes/footer.php');?>

        </div>
<style>
.form-horizontal .controls label
{
	float: left;
	height: 22px;
	margin-bottom: 5px;
	margin-top: 5px;
}
.form-horizontal .controls input[type="radio"]
{
	float: left;
}
.form-horizontal .controls span
{
	float: left;
	margin: 0px 7px;
}
.form-horizontal .controls label:first-child
{
	margin-right: 30px;
}
.form-horizontal .controls label:second-child
{
	margin-left: 30px;
}
</style>
        <!--/.fluid-container-->

        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">

        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">

        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">



        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">



        <script src="vendors/jquery-1.9.1.js"></script>

        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="vendors/jquery.uniform.min.js"></script>

        <script src="vendors/chosen.jquery.min.js"></script>

        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>

        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />

        

        <script type="text/javascript" src="js/colorpicker.js"></script>

        <script type="text/javascript" src="js/eye.js"></script>

        <script type="text/javascript" src="js/utils.js"></script>

        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>





        <script src="assets/scripts.js"></script>

        <script>

        $(function() {

            $(".datepicker").datepicker();

            $(".uniform_on").uniform();

            $(".chzn-select").chosen();

            $('.textarea').wysihtml5();



            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {

                var $total = navigation.find('li').length;

                var $current = index+1;

                var $percent = ($current/$total) * 100;

                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead

                if($current >= $total) {

                    $('#rootwizard').find('.pager .next').hide();

                    $('#rootwizard').find('.pager .finish').show();

                    $('#rootwizard').find('.pager .finish').removeClass('disabled');

                } else {

                    $('#rootwizard').find('.pager .next').show();

                    $('#rootwizard').find('.pager .finish').hide();

                }

            }});

            $('#rootwizard .finish').click(function() {

                alert('Finished!, Starting over!');

                $('#rootwizard').find("a[href*='tab1']").trigger('click');

            });

        });

        </script>

        <script type="text/javascript" src="js/jquery.js"></script>

                <script type="text/javascript" src="js/chat.js"></script>

    </body>



</html>
