-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2016 at 07:13 AM
-- Server version: 5.5.47-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miamor`
--

-- --------------------------------------------------------

--
-- Table structure for table `dateing_add`
--

CREATE TABLE IF NOT EXISTS `dateing_add` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_add`
--

INSERT INTO `dateing_add` (`id`, `title`, `description`, `url`, `image`) VALUES
(6, 'add2', 'add2', NULL, 'printer70.png'),
(7, 'By Bik', 'Lorem Ipsum Doller Sit Amet', 'http://team3.nationalitsolution.co.in', 'wings_rightt.png'),
(8, 'Add With URL', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'adds1.jpg'),
(9, 'For Top', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'wpid-REAL-ESTATE-PHOTOGRAPHY.jpg'),
(10, 'For Bottom', 'Lorem Ipsum Doller Sit Amet', 'http://nationalitsolution.com/', 'Real-Estate1.jpg'),
(11, 'jane image', 'Test', 'http://team3.nationalitsolution.co.in/journal', '20150410_145647.jpg'),
(12, 'Test 1', 'Laura', '', 'Laura Archbold.jpg'),
(14, 'test 3', '', '', 'i_love_my_colombian_girlfriend_sweatshirt_dark.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_add_pages`
--

CREATE TABLE IF NOT EXISTS `dateing_add_pages` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `pagename` varchar(255) DEFAULT NULL,
  `no_add` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_add_pages`
--

INSERT INTO `dateing_add_pages` (`id`, `description`, `pagename`, `no_add`, `amount`) VALUES
(3, 'Profile Page', 'profile.php', 3, 1.00),
(4, 'Search Page', 'search_results.php', 3, 1.00),
(5, 'Edit Profile Page', 'edit_profile.php', 3, 1.00),
(7, 'Interest Page', 'interest.php', 6, 2.00),
(8, 'Gallery Page', 'my_gallery.php', 6, 5.00),
(9, 'Whose online Page', 'whose_online.php', 6, 2.00),
(10, 'Message Page', 'message.php', 6, 3.00),
(11, 'Friend Page', 'my_friends.php', 6, 1.00),
(12, 'Wink Page', 'wink.php', 6, 10.00),
(13, 'Kiss Page', 'kiss.php', 6, 20.00),
(14, 'Compatibility Page', 'compatibility_user.php', 6, 10.00),
(15, 'Favourite Page', 'favourite_member.php', 6, 10.00);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_advertise`
--

CREATE TABLE IF NOT EXISTS `dateing_advertise` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `company` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_advertise`
--

INSERT INTO `dateing_advertise` (`id`, `name`, `email`, `telephone`, `company`, `message`, `date`) VALUES
(2, 'damon de berry', 'damon@optimumstudio.com', '123456', 'miamor', 'test', '2015-06-20 12:42:21'),
(10, 'jojo', 'jhaoana2118@hotmail.com', '31322650006', 'fitlink', 'hola realmente me gusta Miamor ', '2016-08-30 14:03:04'),
(4, 'Abhijit Roy', 'nits.santanu@gmail.com', '919876543210', 'NITS', 'sfdsfdsf', '2015-06-22 09:58:12'),
(5, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', '919876543210', 'NITS', 'fsdfsdfsdfdsfdsfdsfsdfd', '2015-06-22 10:21:57'),
(6, 'damon de berry', 'damon@optimumstudio.com', '01234567', 'me', 'test', '2015-06-22 15:50:09'),
(8, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', '919876543210', 'sdfsdf', 'sddfgfdgfdgdf', '2015-06-24 12:25:50'),
(11, 'jorge', 'jhaoana2118@hotmail.com', '0224114132', 'Cindy Johanna', 'akjhfabcmnbACBHJDb vmnSD', '2016-09-02 21:03:11'),
(12, 'Damon de Berry', 'damon@fitlink.co.nz', '2', 'd', 'rgsg', '2016-09-03 14:11:19'),
(13, 'hfhyjf', 'vhjf@fhyfjy.com', '12', 'vjyfj', 'chgf', '2016-09-08 20:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_assign_ads`
--

CREATE TABLE IF NOT EXISTS `dateing_assign_ads` (
  `id` int(11) NOT NULL,
  `add_id` int(11) NOT NULL,
  `addpage_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0' COMMENT '0:Right, 1:Left, 2:Bottom, 3:Top '
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_bank`
--

CREATE TABLE IF NOT EXISTS `dateing_bank` (
  `id` int(11) NOT NULL,
  `ib_name` varchar(255) NOT NULL,
  `aba_code` varchar(255) NOT NULL,
  `iban` varchar(255) NOT NULL,
  `swift` varchar(255) NOT NULL,
  `rb_name` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''1'' - Default account'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_bank`
--

INSERT INTO `dateing_bank` (`id`, `ib_name`, `aba_code`, `iban`, `swift`, `rb_name`, `account`, `account_name`, `account_number`, `city`, `country`, `currency`, `status`) VALUES
(1, 'CITIBANK of New York', '021000089', '', 'CITIUS33', 'BANCOLOMBIA', '36006658', 'MiAmor Ltda', '11343105438', 'Bogota', 'Colombia', '$US', 0),
(2, 'DEUTSCHE BANK of Germany', 'DE89500700100951331800', '', 'DEUTDEFF', 'BANCOLOMBIA', '100951331800', 'MiAmor Ltda', '11343105438', 'Bogota', 'Colombia', 'â‚¬', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_banner`
--

CREATE TABLE IF NOT EXISTS `dateing_banner` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(200) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_banner`
--

INSERT INTO `dateing_banner` (`id`, `user_id`, `name`, `description`, `image`, `link`, `status`) VALUES
(1, 0, 'Lorem ipsum dolor sit amet consectetur', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider1.jpg', 'http://www.google.com', 0),
(2, 0, 'Image 2', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider2.jpg', 'http://www.google.com', 0),
(3, 0, 'Image 3', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'slider3.jpg', 'http://www.google.com', 0),
(8, 0, '', '', '1059388609_banner.jpg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_billing`
--

CREATE TABLE IF NOT EXISTS `dateing_billing` (
  `billing_id` int(8) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `billing_fname` varchar(100) NOT NULL,
  `billing_lname` varchar(100) NOT NULL,
  `billing_add` varchar(100) NOT NULL,
  `billing_city` varchar(100) NOT NULL,
  `billing_zip` int(8) NOT NULL,
  `billing_state` varchar(256) NOT NULL,
  `billing_ephone` bigint(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `shiping_fname` varchar(100) NOT NULL,
  `shiping_lname` varchar(100) NOT NULL,
  `shiping_add` varchar(100) NOT NULL,
  `shiping_city` varchar(50) NOT NULL,
  `shiping_zip` int(8) NOT NULL,
  `shiping_state` varchar(256) NOT NULL,
  `shiping_ephone` bigint(20) NOT NULL,
  `email1` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dateing_billing`
--

INSERT INTO `dateing_billing` (`billing_id`, `orderid`, `billing_fname`, `billing_lname`, `billing_add`, `billing_city`, `billing_zip`, `billing_state`, `billing_ephone`, `email`, `shiping_fname`, `shiping_lname`, `shiping_add`, `shiping_city`, `shiping_zip`, `shiping_state`, `shiping_ephone`, `email1`) VALUES
(11, '6', 'Avik', 'Ghosh', 'kolkata', 'kolkata', 123123, 'HI', 12312321, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'kolkata', 'kolkata', 123123, 'HI', 0, ''),
(12, '', 'Avik', 'Ghosh', 'Kolkata', 'kolkata', 700017, 'AE', 1234567890, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'Kolkata', 'kolkata', 700017, 'AE', 0, ''),
(13, '', 'aaa', 'aaa', 'aaa', 'aaaa', 700111, 'AA', 1111111111, 'aa@gmail.com', 'aaaa', 'aaaa', 'aaaa', 'aaaaa', 7001111, 'ID', 0, ''),
(14, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1231231231, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(15, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(16, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(17, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(18, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(19, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(20, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(21, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(22, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(23, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(24, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(25, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(26, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(27, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(28, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(29, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(30, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(31, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(32, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(33, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(34, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, ''),
(35, '', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 1234567, 'nits.avik@gmail.com', 'Avik', 'Ghosh', 'India', 'Kolkata', 700106, 'WB', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_blogs`
--

CREATE TABLE IF NOT EXISTS `dateing_blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `titletranslated` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text NOT NULL,
  `description_translated` text CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_blogs`
--

INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(1, 'The Sexiest Nationalities', 'Las nacionalidades mÃ¡s sexys', '<p>\r\n	While traditional courting rituals and dating customs have been evolving over the years, re-entering the dating arena still seems somewhat bland, and disheartening. It&rsquo;s not fun to embark on a journey to couple-hood when the pickings are slim, especially if you are truly limited to a small population of single people in your corner of the world. MiAmor.com.co presents a variety of new options to expand your, er, dating palette.</p>\r\n<p>\r\n	When you limit yourse<span class="text_exposed_show">lf to dating domestically, depending on where you live, that could mean you are only dated like-minded, similar looking people. But when you realize that there are a whole world of single people out there, the possibilities become endless and exciting. Recently we decided to conduct a poll amongst our American members to discover which nationalities top your list of the sexiest nationalities on the planet. </span></p>\r\n<div class="text_exposed_show">\r\n	<p>\r\n		The Top Ten Sexiest Nationalities for Women:</p>\r\n	<p>\r\n		Colombian Famous Face: Shakira<br />\r\n		Brazilian Famous Face: Camila Alves<br />\r\n		American Famous Face: Jennifer Lawrence<br />\r\n		Spanish Famous Face: Penelope Cruz<br />\r\n		Russian Famous Face: Anne Vyalitsyna<br />\r\n		Dutch Famous Face: Famke Janssen<br />\r\n		French Famous Face: Eva Green<br />\r\n		Bulgarian Famous Face: Nina Dobrev<br />\r\n		Swedish Famous Face: Malin Akerman<br />\r\n		Italian Famous Face: Carla Bruni</p>\r\n	<p>\r\n		<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/bg-main.jpg" style="width: 400px; height: 267px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span title="While traditional courting rituals and dating customs have been evolving over the years, re-entering the dating arena still seems somewhat bland, and disheartening.">Mientras rituales de cortejo y costumbres tradicionales de citas han ido evolucionando con los a&ntilde;os, volver a entrar en el escenario de citas todav&iacute;a parece un poco soso, y desalentador. </span><span title="It''s not fun to embark on a journey to couple-hood when the pickings are slim, especially if you are truly limited to a small population of single people in your corner of the world.">No es divertido para embarcarse en un viaje para pareja de campana cuando las ganancias son escasas, especialmente si usted est&aacute; realmente limitado a una peque&ntilde;a poblaci&oacute;n de personas solteras en su rinc&oacute;n del mundo. </span>MiAmor.com.co <span title="Dating abroad presents a variety of new options to expand your, er, dating palette.\r\n\r\n">presenta una variedad de nuevas opciones para ampliar su paleta&nbsp; y&nbsp;&nbsp; data.</span><br />\r\n	<br />\r\n	<span title="When you limit yourself to dating domestically, depending on where you live, that could mean you are only dated like-minded, similar looking people.">Cuando usted se limita a salir con el pa&iacute;s, dependiendo de donde usted vive, eso podr&iacute;a significar que s&oacute;lo son anticuadas ideas afines, personas de aspecto similar. </span><span title="But when you realize that there are a whole world of single people out there, the possibilities become endless and exciting.">Pero cuando te das cuenta de que hay todo un mundo de personas solteras por ah&iacute;, las posibilidades se vuelven infinitas y emocionante. </span><span title="Recently we decided to conduct a poll amongst our American members to discover which nationalities top your list of the sexiest nationalities on the planet.\r\n\r\n">Recientemente decidimos realizar una encuesta entre nuestros miembros americanos para descubrir qu&eacute; nacionalidades arriba su lista de las nacionalidades m&aacute;s sexy del planeta.</span><br />\r\n	<br />\r\n	&nbsp; <span title="The Top Ten Sexiest Nationalities for Women:\r\n\r\n">las m&aacute;s sexy Nacionalidades Top 10&nbsp;&nbsp; Mujer:</span><br />\r\n	<br />\r\n	<span title="Colombian Famous Face: Shakira\r\n">Cara famoso colombiano: Shakira</span><br />\r\n	<span title="Brazilian Famous Face: Camila Alves\r\n">Cara famoso brasile&ntilde;o: Camila Alves</span><br />\r\n	<span title="American Famous Face: Jennifer Lawrence\r\n">Americana cara famosa: Jennifer Lawrence</span><br />\r\n	<span title="Spanish Famous Face: Penelope Cruz\r\n">Espa&ntilde;ol Rostro Famoso: Pen&eacute;lope Cruz</span><br />\r\n	<span title="Russian Famous Face: Anne Vyalitsyna\r\n">Rusia cara famosa: Anne Vyalitsyna</span><br />\r\n	<span title="Dutch Famous Face: Famke Janssen\r\n">Holand&eacute;s cara famosa: Famke Janssen</span><br />\r\n	<span title="French Famous Face: Eva Green\r\n">Franc&eacute;s Cara famoso: Eva Green</span><br />\r\n	<span title="Bulgarian Famous Face: Nina Dobrev\r\n">B&uacute;lgaro cara famosa: Nina Dobrev</span><br />\r\n	<span title="Swedish Famous Face: Malin Akerman\r\n">Sueco cara famosa: Malin Akerman</span><br />\r\n	<span title="Italian Famous Face: Carla Bruni">Italiano cara famosa: Carla Bruni</span></span></p>\r\n<p>\r\n	<br />\r\n	<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/bg-main.jpg" style="width: 400px; height: 267px;" /></p>\r\n', 1, '2016-09-27 07:50:14'),
(2, 'Discover Colombia', 'Descubra Colombia', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		Let&#39;s face it, you&#39;ve done the Caribbean, you&#39;ve been through Europe, the states are boring and you need something new. Flying to the far East sounds great but is way too expensive. You&#39;re looking for that sense of adventure that you can&#39;t find on a cruise ship or another boring mega-resort and you don&#39;t want to spend thousands of dollars and fly halfway across the world.</p>\r\n	<p>\r\n		You want a place close by with the culture of Europe, the adventure of Costa Rica, the party atmosphere of Greece, the beauty of Alaska, the beaches of the Caribbean, lots of activities, and the friendliest, happiest, best looking people in South America.</p>\r\n	<p>\r\n		The answer is Colombia. This rediscovered gem is right in our backyard and is quickly becoming one of the fastest growing destinations for young tourists, eco-lovers, and foreign culture enthusiasts.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>Here are ten reasons why you need to get down to Colombia now.</strong></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>1.</strong> <strong>It&#39;s not as dangerous as you think.</strong></p>\r\n	<p>\r\n		<br />\r\n		It&#39;s amazing at how much the negative/dangerous stereotype about colombia exists throughout the world. When I first went to Colombia is 2002, my friends and family couldn&#39;t believe it. They thought I was crazy for going into a militarized danger zone and I was definitely going to get kidnapped, shot, robbed, or sold into the drug trade.</p>\r\n	<p>\r\n		Fortunately for me, that wasn&#39;t the case. Although, Colombia has had a shaky past, things are different now. Kidnappings are down by half, murder and crime rates have dropped significantly, and there has been a huge decrease in violence. The dramatic change can be credited to former President Alvaro Uribe and very motivated officials who have gone through great measures to fight crime and make Colombia a safe place for tourists.</p>\r\n	<p>\r\n		Now American and foreign tourists can be seen everywhere in Colombia and locals are happy to finally have a blossoming tourism industry.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>2. The People &amp; Hospitality</strong></p>\r\n	<p>\r\n		<br />\r\n		Colombia has a unique mix of ethnic cultures, ranging from indigenous African roots to Spanish settlers. This combination creates some of the nicest and best looking people on the planet. Locals are friendly and usually very accepting of foreign tourists. Just a short time ago, Colombia was shunned by the tourism world for being too dangerous. Nowadays, the people of Colombia compensate for the shaky past by creating a positive, friendly, and safe environment for tourists. Colombians foster their new and growing tourism industry and you can be sure to meet some very interesting and friendly people. The Women will be quick to take you out on the dancefloor for a salsa session and the Men will be quick to quench your thirst with a shot of aguardiente.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>3. The Beaches</strong></p>\r\n	<p>\r\n		<br />\r\n		Tayrona National Park BeachVirgin Beach in Tayrona National Park<br />\r\n		Colombia has over 300 beaches along its caribbean and pacific coasts. There are beaches for all types of beach-goers. You have your relaxed resort-style beaches on the caribbean coast in and around Cartagena. You also have untouched, tropical paradise on the island of San Andres and Providencia.</p>\r\n	<p>\r\n		Are you more of the adventure beacher? Try Tayrona National Park. This eco-lovers dream has untouched beaches surrounded by rainforests, hundreds of species of rare animals, and lush tropical landscapes. Be sure to stay in an Eco-Cabana (ecohab), a small cabin high up, with spectacular views of the ocean. Like surfing? Try Nuqui on the Western pacific side.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>4.</strong> <strong>The Scenery</strong></p>\r\n	<p>\r\n		<br />\r\n		Mountain ChurchLas Lajas Sanctuary Nestled in the Mountains<br />\r\n		Having Pacific and Caribbean coastlines, lush rainforests, and rolling mountains really create a tremendous backdrop for a vacation. If you are into sightseeing, hiking, eco-tourism, rare species of wild animals, and untouched paradises, Colombia has it all. The beaches on the Northern Caribbean coast boast white sandy beaches and island paradises. The beaches on the Western Pacific side are a little more geared towards the adventure traveller with lush green forests descending into the virgin beaches with exciting campgrounds and surf spots.</p>\r\n	<p>\r\n		The Andes mountains bring about some of the highest points in Colombia and some of the nicest scenery as well. Snow capped mountains line the horizons, while active volcano&#39;s create some breathtaking views and pictures.</p>\r\n	<p>\r\n		The cities have their own distinct architecture which is not to be missed. The Walled City of Cartagena is some of the most original and colorful architecture on the planet and a walking tour is a must. Each city has it&#39;s own unique flair and the country as a whole is a great place of scenery watching, and people watching.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>5.</strong> <strong>The Coffee</strong></p>\r\n	<p>\r\n		<br />\r\n		Colombian CoffeeColombian Coffee Beans<br />\r\n		Visiting a coffee plantation in Colombia is like visiting a winery in Napa Valley, only without the hangover.</p>\r\n	<p>\r\n		Colombia has perfect weather conditions that allow it to grow the best coffee in the world. The coffee region or &quot;Eje Cafetero&quot; is south of medellin, has a pleasant climate and here you will experience lush green landscapes, and rolling mountains and the best coffee in the world. Take a few days and experience life as a coffee farmer in Colombia. Learn how this very popular product is produced, take tours of the facilities, and mingle with some local farmers.</p>\r\n	<p>\r\n		Coffee tourism has become a very popular attraction in Colombia. Don&#39;t miss the Parque Nacional del Cafe, which is an amusement park based on coffee. The kids will love it. Just don&#39;t give them too much coffee.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>6.</strong> <strong>The Festivals</strong></p>\r\n	<p>\r\n		<br />\r\n		Carnaval de BarranquillaCarnaval in Barranquilla<br />\r\n		Attending a festival in Colombia is an absolute necessity if you want to experience the people and the culture. The Carnaval de Barranquilla leads the pack as the best one in the country and is only rivaled by the festival in Rio de Janeiro as the best festival in the world. Also experience the Carnaval de Bogota and the Carnival of Blacks and Whites in Cali.</p>\r\n	<p>\r\n		During these festivals, the entire city shuts down for a few days and you see all races, colors, classes, and ages come together in an extravagant celebration with drinking, dancing, flamboyant and colorful parades, music, energy, and great times. If you are lucky enough to be in Colombia during one of these festivals, I highly recommend being a part of one of these cultural events.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>7. The Food</strong></p>\r\n	<p>\r\n		<br />\r\n		Bandeja PaisaBandeja Paisa - Delicious<br />\r\n		Colombia has some rare and delicious cuisine that you can&#39;t find anywehere else in the world. The arepas, corn patties filled with meats and chesses are a must everywhere you go and are as common as french fries.</p>\r\n	<p>\r\n		Ajiaco and sancocho are traditional soup dishes made from meats and vegetables which change depending on the region you are in. Being a meat lover, my personal favorite is the Bandeja Paisa. This hearty dish is not for vegatarians as it contains different meats such as beef, chicken, pork, sausage, fried eggs, rice, beans, and plantains. This dish varies depending on where you are in Colombia depending on the local specialty. If you are adventurous and find yourself in the Santander region, try a plate of Hormigas Culonas, &quot;Large-Bottomed Ants&quot; which are salty and crunchy just like a peanut.</p>\r\n	<p>\r\n		Colombians take pride in their food and you will find that dishes are always fresh with limited preservatives or over-the-top dressings or seasonings typically found in American foods. Rare, exotic fruits are in abundance all over the country and typically complement well-cooked meals and desserts. You will find that meals do not bloat you or make you tired, and always leave you wanting more.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>8.</strong> <strong>The Nightlife</strong></p>\r\n	<p>\r\n		<br />\r\n		Colombia is known for its extravagant nightlife, bars, clubs, and restaurants. The entertainment is top-notch, the people are friendly, and the Women are amazingly beautiful. The Colombian people have a natural flair for dancing and partying, and they do it well. Cali is known as the Nightlife capital of Colombia and it is easy to see why. Beautiful people are accompanied by a wide range of reataurants, live music, drinking, dancing, and good vibes overall.</p>\r\n	<p>\r\n		If you find yourself in Bogot&aacute;, be sure to check out Andr&eacute;s Carne de Res. One of my personal favorite restaurants/nightclubs.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>9. The Women</strong></p>\r\n	<p>\r\n		<br />\r\n		A Colombian Woman, A Group of Pretty Colombian Girls<br />\r\n		If you are looking to experience true exotic beauty, derived from the eccentric mix of African and Spanish cultures, the Colombian people are the ones. On the other hand, If you are a single guy, looking to mingle with some of the most beautiful women in the world, Colombia is definitely where you want to be.</p>\r\n	<p>\r\n		Colombian people, Women especially take great pride in their appearance. Women are very well kept and are never shy when flaunting their beauty. They learn early on how to dance and mingle and what it takes to be stunningly gorgeous. It is no coincidence that there are beautiful women all over the place.</p>\r\n	<p>\r\n		Colombian women tend to be very polite and approachable. They enjoy drinking, dancing, talking, meeting new people, and are typically great companions on a night out. A Colombian woman, although always polite, will let you know if she is not interested in your advances, so be polite yourself, and make every effort to be genuine and friendly. Oh, and learn some Spanish Gringo! It is easy to meet and discover lots of beautiful Colombian women on MiAmor, so before you head to Colombia make the connections here.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>10. Location</strong></p>\r\n	<p>\r\n		<br />\r\n		People fly all over the world to see and experience new things. You can get history and culture in Italy, beaches and resorts in Hawaii, Eco tourism and outdoor activities in Costa Rica, and friendly outgoing cultures in Greece.</p>\r\n	<p>\r\n		Why not go to a place that has it all, AND is right in your backyard. Colombia is very close to the United States and Canada and is a new and exciting opportunity for any traveler. Flights to Colombia are generally inexpensive and are only a couple hours from most North American International airports. Prices and accommodations are usually cheaper than their foreign counterparts, and there are always friendly people to show you around.</p>\r\n	<p>\r\n		<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/colonial-buildings-in-old-cartagena-colombia-david-smith.jpg" style="width: 1000px; height: 666px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span title="Let''s face it, you''ve done the Caribbean, you''ve been through Europe, the states are boring and you need something new.">Seamos realistas, que ha hecho el Caribe, que ha pasado por Europa, los estados son aburridos y&nbsp; necesitan algo nuevo. </span><span title="Flying to the far East sounds great but is way too expensive.">Volando hacia el lejano Oriente suena muy bien, pero es demasiado caro. </span><span title="You''re looking for that sense of adventure that you can''t find on a cruise ship or another boring mega-resort and you don''t want to spend thousands of dollars and fly halfway across the world.\r\n\r\n">Usted est&aacute; en busca de ese sentido de la aventura que no se puede encontrar en un crucero o en otro aburrido mega-resort y no quieren gastar miles de d&oacute;lares y volar al otro lado del mundo.</span><br />\r\n	<br />\r\n	<span title="You want a place close by with the culture of Europe, the adventure of Costa Rica, the party atmosphere of Greece, the beauty of Alaska, the beaches of the Caribbean, lots of activities, and the friendliest, happiest, best looking people in South">&iquest;Quieres un lugar cercano con la cultura de Europa, la aventura de Costa Rica, el ambiente de fiesta de Grecia, la belleza de Alaska, las playas del Caribe, un mont&oacute;n de actividades, y los m&aacute;s felices, mejor, buscan gente m&aacute;s amable del Sur </span><span title="America.\r\n\r\n">Am&eacute;rica.</span><br />\r\n	<br />\r\n	<span title="The answer is Colombia.">La respuesta es Colombia. </span><span title="This rediscovered gem is right in our backyard and is quickly becoming one of the fastest growing destinations for young tourists, eco-lovers, and foreign culture enthusiasts.\r\n\r\n">Esta joya redescubierta es justo en nuestro patio trasero y se est&aacute; convirtiendo r&aacute;pidamente en uno de los destinos de m&aacute;s r&aacute;pido crecimiento para los turistas j&oacute;venes, eco-amantes y entusiastas de la cultura extranjera.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><strong><span title="Here are ten reasons why you need to get down to Colombia now.\r\n\r\n">Aqu&iacute; hay diez razones por las que necesitas ir a Colombia ahora.</span></strong></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="1. It''s not as dangerous as you think.\r\n"><strong>1.</strong> <strong>No es tan peligroso como parece.</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="It''s amazing at how much the negative/dangerous stereotype about colombia exists throughout the world.">Es incre&iacute;ble lo mucho que existe el estereotipo negativo / peligroso de Colombia en todo el mundo. </span><span title="When I first went to Colombia is 2002, my friends and family couldn''t believe it.">Cuando fui por primera vez a Colombia es 2002, mis amigos y mi familia no se lo pod&iacute;an creer. </span><span title="They thought I was crazy for going into a militarized danger zone and I was definitely going to get kidnapped, shot, robbed, or sold into the drug trade.\r\n\r\n">Pensaron que estaba loco por entrar en una zona de peligro militarizada y yo&nbsp; sin duda iba ser secuestrado, robado o vendido en el comercio de la droga.</span><br />\r\n	<br />\r\n	<span title="Fortunately for me, that wasn''t the case.">Afortunadamente para m&iacute;, no fue el caso. </span><span title="Although, Colombia has had a shaky past, things are different now.">Aunque, Colombia ha tenido un pasado inestable, las cosas son diferentes ahora. </span><span title="Kidnappings are down by half, murder and crime rates have dropped significantly, and there has been a huge decrease in violence.">Los secuestros han disminuido en las tasas de medio, de asesinato y el crimen han bajado considerablemente, y ha habido una gran disminuci&oacute;n de la violencia. </span><span title="The dramatic change can be credited to former President Alvaro Uribe and very motivated officials who have gone through great measures to fight crime and make Colombia a safe place for tourists.\r\n\r\n">El cambio dram&aacute;tico puede ser acreditado con el ex presidente &Aacute;lvaro Uribe y funcionarios que han pasado por grandes medidas para combatir la delincuencia y hacer de Colombia un lugar seguro para los turistas muy motivada.</span><br />\r\n	<br />\r\n	<span title="Now American and foreign tourists can be seen everywhere in Colombia and locals are happy to finally have a blossoming tourism industry.\r\n\r\n">Ahora los turistas estadounidenses y extranjeros pueden verse por todas partes en Colombia y los lugare&ntilde;os est&aacute;n contentos de finalmente tener una industria tur&iacute;stica floreciente.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="2. The People &amp; Hospitality\r\n"><strong>2. La Gente &amp; Hospitalidad</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Colombia has a unique mix of ethnic cultures, ranging from indigenous African roots to Spanish settlers.">Colombia tiene una mezcla &uacute;nica de culturas &eacute;tnicas, que van desde las ra&iacute;ces africanas ind&iacute;genas a los colonos espa&ntilde;oles. </span><span title="This combination creates some of the nicest and best looking people on the planet.">Esta combinaci&oacute;n crea algunas de las personas m&aacute;s bonitas y mejor aspecto en el planeta. </span><span title="Locals are friendly and usually very accepting of foreign tourists.">La gente es amable y normalmente muy bien acogidas por los turistas extranjeros. </span><span title="Just a short time ago, Colombia was shunned by the tourism world for being too dangerous.">Hace poco tiempo, Colombia fue rechazado por el mundo del turismo por ser demasiado peligroso. </span><span title="Nowadays, the people of Colombia compensate for the shaky past by creating a positive, friendly, and safe environment for tourists.">Hoy en d&iacute;a, la gente de Colombia compensan el pasado inestable mediante la creaci&oacute;n de un ambiente positivo, amable y seguro para los turistas. </span><span title="Colombians foster their new and growing tourism industry and you can be sure to meet some very interesting and friendly people.">Colombianos fomentan su nueva y creciente industria del turismo y usted puede estar seguro de conocer a algunas personas muy interesantes y amigables. </span><span title="The Women will be quick to take you out on the dancefloor for a salsa session and the Men will be quick to quench your thirst with a shot of aguardiente.\r\n\r\n">Las mujeres se apresuran para llevarlo a cabo en la pista para una sesi&oacute;n de salsa y los hombres ser&aacute;n r&aacute;pida para saciar su sed con un trago de aguardiente.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="3. The Beaches\r\n"><strong>3.</strong> <strong>Las Playas</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Tayrona National Park BeachVirgin Beach in Tayrona National Park\r\n">Parque Tayrona Playa Virgen en el Parque Nacional Tayrona</span><br />\r\n	<span title="Colombia has over 300 beaches along its caribbean and pacific coasts.">Colombia cuenta con m&aacute;s de 300 playas a lo largo de sus costas Caribe y el Pac&iacute;fico. </span><span title="There are beaches for all types of beach-goers.">Hay playas para todos los tipos de visitantes. </span><span title="You have your relaxed resort-style beaches on the caribbean coast in and around Cartagena.">Usted tiene sus playas estilo relajado, resort en la costa caribe en los alrededores de Cartagena. </span><span title="You also have untouched, tropical paradise on the island of San Andres and Providencia.\r\n\r\n">Usted tambi&eacute;n tiene intacto para&iacute;so tropical en la isla de San Andr&eacute;s y Providencia.</span><br />\r\n	<br />\r\n	<span title="Are you more of the adventure beacher?">&iquest;Es usted m&aacute;s de la beacher aventura? </span><span title="Try Tayrona National Park.">Pruebe Parque Nacional Tayrona. </span><span title="This eco-lovers dream has untouched beaches surrounded by rainforests, hundreds of species of rare animals, and lush tropical landscapes.">Este sue&ntilde;o eco-amantes tiene playas v&iacute;rgenes rodeadas de selvas tropicales, cientos de especies de animales raros, y exuberantes paisajes tropicales. </span><span title="Be sure to stay in an Eco-Cabana (ecohab), a small cabin high up, with spectacular views of the ocean.">Aseg&uacute;rese de mantenerse en un Eco-Cabana (ecohab), una peque&ntilde;a caba&ntilde;a en lo alto, con espectaculares vistas del oc&eacute;ano. </span><span title="Like surfing?">Al igual que el surf? </span><span title="Try Nuqui on the Western pacific side.\r\n\r\n">Trate Nuqui en el lado del Pac&iacute;fico Occidental.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="4. The Scenery\r\n"><strong>4.</strong> <strong>El Paisaje</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Mountain ChurchLas Lajas Sanctuary Nestled in the Mountains\r\n">Las Lajas Santuario enclavado en las monta&ntilde;as</span><br />\r\n	<span title="Having Pacific and Caribbean coastlines, lush rainforests, and rolling mountains really create a tremendous backdrop for a vacation.">Tener las costas del Pac&iacute;fico y del Caribe, exuberantes selvas y monta&ntilde;as ondulantes realmente crean un enorme tel&oacute;n de fondo para unas vacaciones. </span><span title="If you are into sightseeing, hiking, eco-tourism, rare species of wild animals, and untouched paradises, Colombia has it all.">Si usted est&aacute; en hacer turismo, senderismo, ecoturismo, especies raras de animales salvajes, y para&iacute;sos v&iacute;rgenes, Colombia tiene todo. </span><span title="The beaches on the Northern Caribbean coast boast white sandy beaches and island paradises.">Las playas de la costa norte del Caribe cuentan con playas de arena blanca y para&iacute;sos de la isla. </span><span title="The beaches on the Western Pacific side are a little more geared towards the adventure traveller with lush green forests descending into the virgin beaches with exciting campgrounds and surf spots.\r\n\r\n">Las playas en el lado del Pac&iacute;fico Occidental son un poco m&aacute;s orientado hacia el viajero de aventura con exuberantes bosques verdes que descienden hacia las playas v&iacute;rgenes con campamentos emocionantes y lugares para practicar surf.</span><br />\r\n	<br />\r\n	<span title="The Andes mountains bring about some of the highest points in Colombia and some of the nicest scenery as well.">Las monta&ntilde;as de los Andes lograr algunos de los puntos m&aacute;s altos de Colombia y algunos de los paisajes m&aacute;s bonitos tambi&eacute;n. </span><span title="Snow capped mountains line the horizons, while active volcano''s create some breathtaking views and pictures.\r\n\r\n">Cumbres nevadas alinean los horizontes, mientras que el volc&aacute;n&nbsp; activos Crear unas vistas impresionantes y fotos.</span><br />\r\n	<br />\r\n	<span title="The cities have their own distinct architecture which is not to be missed.">Las ciudades tienen su propia arquitectura distinta que no se puede perder. </span><span title="The Walled City of Cartagena is some of the most original and colorful architecture on the planet and a walking tour is a must.">La ciudad amurallada de Cartagena es algo de la arquitectura m&aacute;s original y colorido en el planeta y un recorrido a pie es una necesidad. </span><span title="Each city has it''s own unique flair and the country as a whole is a great place of scenery watching, and people watching.\r\n\r\n">Cada ciudad tiene su propio estilo &uacute;nico y el pa&iacute;s en su conjunto es un gran lugar de observaci&oacute;n de paisajes, y observar a la gente.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="5. The Coffee\r\n"><strong>5.</strong> <strong>El Caf&eacute;</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Colombian CoffeeColombian Coffee Beans\r\n">Caf&eacute; de Colombia</span><br />\r\n	<span title="Visiting a coffee plantation in Colombia is like visiting a winery in Napa Valley, only without the hangover.\r\n\r\n">Visitar una plantaci&oacute;n de caf&eacute; en Colombia es como visitar una bodega en Napa Valley, s&oacute;lo que sin la resaca.</span><br />\r\n	<br />\r\n	<span title="Colombia has perfect weather conditions that allow it to grow the best coffee in the world.">Colombia tiene condiciones clim&aacute;ticas perfectas que permiten que crezca el mejor caf&eacute; del mundo. </span><span a="" and="" best="" climate="" coffee="" eje="" experience="" green="" has="" here="" in="" is="" lush="" mountains="" of="" pleasant="" rolling="" south="" the="" title="The coffee region or " will="" you="">La regi&oacute;n cafetera o &quot;Eje Cafetero&quot; es al sur de Medell&iacute;n, cuenta con un clima agradable y aqu&iacute; usted experimentar&aacute; exuberantes paisajes verdes y monta&ntilde;as y el mejor caf&eacute; del mundo rodando. </span><span title="Take a few days and experience life as a coffee farmer in Colombia.">T&oacute;mese unos d&iacute;as y la experiencia de la vida como un productor de caf&eacute; en Colombia. </span><span title="Learn how this very popular product is produced, take tours of the facilities, and mingle with some local farmers.\r\n\r\n">Aprenda c&oacute;mo se produce este producto muy popular, tomar tours de las instalaciones, y se mezclan con algunos agricultores locales.</span><br />\r\n	<br />\r\n	<span title="Coffee tourism has become a very popular attraction in Colombia.">Turismo caf&eacute; se ha convertido en una atracci&oacute;n muy popular en Colombia. </span><span title="Don''t miss the Parque Nacional del Cafe, which is an amusement park based on coffee.">No te pierdas el Parque Nacional del Caf&eacute;, que es un parque de atracciones a base de caf&eacute;. </span><span title="The kids will love it.">A los ni&ntilde;os les encantar&aacute;. </span><span title="Just don''t give them too much coffee.\r\n\r\n">Simplemente no les damos demasiado caf&eacute;.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="6. The Festivals\r\n"><strong>6. Las Fiestas</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Carnaval de BarranquillaCarnaval in Barranquilla\r\n">Carnaval de Barranquilla</span><br />\r\n	<span title="Attending a festival in Colombia is an absolute necessity if you want to experience the people and the culture.">Asistir a un festival en Colombia es una necesidad absoluta si quieres experimentar la gente y la cultura. </span><span title="The Carnaval de Barranquilla leads the pack as the best one in the country and is only rivaled by the festival in Rio de Janeiro as the best festival in the world.">El Carnaval de Barranquilla lidera el grupo como uno de los mejores en el pa&iacute;s y s&oacute;lo se ve igualado por el festival en R&iacute;o de Janeiro como el mejor festival del mundo. </span><span title="Also experience the Carnaval de Bogota and the Carnival of Blacks and Whites in Cali.\r\n\r\n">Tambi&eacute;n experimentar el Carnaval de Bogot&aacute; y el Carnaval de Negros y Blancos en Pasto.</span><br />\r\n	<br />\r\n	<span title="During these festivals, the entire city shuts down for a few days and you see all races, colors, classes, and ages come together in an extravagant celebration with drinking, dancing, flamboyant and colorful parades, music, energy, and great times.">Durante estas fiestas, la ciudad entera se apaga durante unos d&iacute;as y ver todas las razas, colores, clases y edades se dan cita en una celebraci&oacute;n extravagante con la bebida, el baile, extravagante y coloridos desfiles, m&uacute;sica, energ&iacute;a y grandes momentos. </span><span title="If you are lucky enough to be in Colombia during one of these festivals, I highly recommend being a part of one of these cultural events.\r\n\r\n">Si tienes la suerte de estar en Colombia durante una de estas fiestas, le recomiendo ser parte de uno de estos eventos culturales.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="7. The Food\r\n"><strong>7</strong>.<strong> La Comida</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Bandeja PaisaBandeja Paisa - Delicious\r\n">La Bandeja Paisa - Delicioso</span><br />\r\n	<span title="Colombia has some rare and delicious cuisine that you can''t find anywehere else in the world.">Colombia tiene alguna rara y deliciosa cocina que no se puede encontrar en ninguna otra parte del mundo. </span><span title="The arepas, corn patties filled with meats and chesses are a must everywhere you go and are as common as french fries.\r\n\r\n">Las arepas, empanadas de ma&iacute;z rellenas de carnes y pandebono o almojabana con quesos son una necesidad donde quiera que vaya y son tan comunes como las papas fritas.</span><br />\r\n	<br />\r\n	<span title="Ajiaco and sancocho are traditional soup dishes made from meats and vegetables which change depending on the region you are in. Being a meat lover, my personal favorite is the Bandeja Paisa.">Ajiaco y sancocho son platos tradicionales sopas hechas con carnes y vegetales que cambian dependiendo de la regi&oacute;n donde se encuentra. Siendo un amante de la carne, mi favorita es la bandeja paisa. </span><span title="This hearty dish is not for vegatarians as it contains different meats such as beef, chicken, pork, sausage, fried eggs, rice, beans, and plantains.">Este suculento plato no es para vegatarians ya que contiene diferentes carnes como carne de res, pollo, cerdo, salchichas, huevos fritos, arroz, frijoles y pl&aacute;tanos. </span><span title="This dish varies depending on where you are in Colombia depending on the local specialty.">Este plato var&iacute;a dependiendo de d&oacute;nde se encuentre en Colombia seg&uacute;n la especialidad local. </span><span a="" and="" are="" crunchy="" just="" large-bottomed="" like="" peanut.="" salty="" title="If you are adventurous and find yourself in the Santander region, try a plate of Hormigas Culonas, " which="">Si le gusta la aventura y se encuentra en la regi&oacute;n de Santander, pruebe un plato de Hormigas Culonas, &quot;-Large Bottomed Hormigas&quot;, que son salado y crujiente como un cacahuete.</span><br />\r\n	<br />\r\n	<span title="Colombians take pride in their food and you will find that dishes are always fresh with limited preservatives or over-the-top dressings or seasonings typically found in American foods.">Los colombianos se enorgullecen de su comida y usted encontrar&aacute; que los platos son siempre frescos con conservantes limitados o over-the-top aderezos o condimentos encuentran t&iacute;picamente en los alimentos estadounidenses. </span><span title="Rare, exotic fruits are in abundance all over the country and typically complement well-cooked meals and desserts.">Raras, frutas ex&oacute;ticas se encuentran en abundancia en todo el pa&iacute;s y por lo general se complementan las comidas y postres bien cocidos. </span><span title="You will find that meals do not bloat you or make you tired, and always leave you wanting more.\r\n\r\n">Usted encontrar&aacute; que las comidas no te hinchan o te hacen cansado, y siempre te dejan con ganas de m&aacute;s.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="8. The Nightlife\r\n"><strong>8.</strong> <strong>La Noche</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="Colombia is known for its extravagant nightlife, bars, clubs, and restaurants.">Colombia es conocida por su vida nocturna extravagante, bares, clubes y restaurantes. </span><span title="The entertainment is top-notch, the people are friendly, and the Women are amazingly beautiful.">El entretenimiento es de primera categor&iacute;a, la gente es amable, y las mujeres son incre&iacute;blemente hermoso. </span><span title="The Colombian people have a natural flair for dancing and partying, and they do it well.">El pueblo colombiano tienen un don natural para el baile y la fiesta, y lo hacen bien. </span><span title="Cali is known as the Nightlife capital of Colombia and it is easy to see why.">Cali es conocida como la capital de la vida nocturna de Colombia y es f&aacute;cil ver por qu&eacute;. </span><span title="Beautiful people are accompanied by a wide range of reataurants, live music, drinking, dancing, and good vibes overall.\r\n\r\n">Gente guapa se acompa&ntilde;an de una amplia gama de reataurants, m&uacute;sica en vivo, beber, bailar y buen rollo general.</span><br />\r\n	<br />\r\n	<span title="If you find yourself in Bogotï¿½, be sure to check out Andrï¿½s Carne de Res.">Si usted se encuentra en Bogot&aacute;, aseg&uacute;rate de revisar Andr&eacute;s Carne de Res. </span><span title="One of my personal favorite restaurants/nightclubs.\r\n\r\n">Uno de mis restaurantes favoritos / discotecas personales.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="9. The Women\r\n"><strong>9.</strong> <strong>Las Mujeres</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="A Colombian Woman, A Group of Pretty Colombian Girls\r\n">Una mujer colombiana, un grupo de chicas guapas colombianas</span><br />\r\n	<span title="If you are looking to experience true exotic beauty, derived from the eccentric mix of African and Spanish cultures, the Colombian people are the ones.">Si usted est&aacute; buscando para experimentar la verdadera belleza ex&oacute;tica, derivado de la mezcla exc&eacute;ntrica de las culturas africanas y espa&ntilde;olas, los colombianos son los m&aacute;s. </span><span title="On the other hand, If you are a single guy, looking to mingle with some of the most beautiful women in the world, Colombia is definitely where you want to be.\r\n\r\n">Por otro lado, si usted es un hombre soltero, mirando a mezclarse con algunas de las mujeres m&aacute;s bellas del mundo, Colombia es sin duda donde quieres estar.</span><br />\r\n	<br />\r\n	<span title="Colombian people, Women especially take great pride in their appearance.">Pueblo colombiano, especialmente mujeres se enorgullecen de su apariencia. </span><span title="Women are very well kept and are never shy when flaunting their beauty.">Las mujeres est&aacute;n muy bien cuidados y nunca son t&iacute;midas cuando haciendo alarde de su belleza. </span><span title="They learn early on how to dance and mingle and what it takes to be stunningly gorgeous.">Ellos aprenden pronto a bailar y se mezclan y lo que se necesita para ser incre&iacute;blemente hermosa. </span><span title="It is no coincidence that there are beautiful women all over the place.\r\n\r\n">No es casualidad que hay mujeres hermosas por todo el lugar.</span><br />\r\n	<br />\r\n	<span title="Colombian women tend to be very polite and approachable.">Mujeres colombianas tienden a ser muy educadas y atentas. </span><span title="They enjoy drinking, dancing, talking, meeting new people, and are typically great companions on a night out.">Ellos disfrutan de beber, bailar, hablar, conocer gente nueva, y suelen ser grandes compa&ntilde;eros en una noche de fiesta. </span><span title="A Colombian woman, although always polite, will let you know if she is not interested in your advances, so be polite yourself, and make every effort to be genuine and friendly.">Una mujer colombiana, aunque siempre educada, le permitir&aacute; saber si ella no est&aacute; interesada en sus avances, as&iacute; que sea educado a s&iacute; mismo, y hacer todo lo posible para ser aut&eacute;ntico y amable. </span><span title="Oh, and learn some Spanish Gringo!">Ah, y aprender un poco de espa&ntilde;ol Gringo! </span><span title="It is easy to meet and discover lots of beautiful Colombian women on MiAmor.com.co, so before you head to Colombia make the connections here.\r\n\r\n">Es f&aacute;cil conocer y descubrir un mont&oacute;n de hermosas mujeres colombianas en MiAmor, as&iacute; que antes de ir a Colombia a haga las conexiones aqu&iacute;.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="10. Location\r\n"><strong>10.</strong> <strong>Ubicaci&oacute;n</strong></span></span></p>\r\n<p>\r\n	<br />\r\n	<span lang="es"><span title="People fly all over the world to see and experience new things.">La gente vuela por todo el mundo para ver y experimentar cosas nuevas. </span><span title="You can get history and culture in Italy, beaches and resorts in Hawaii, Eco tourism and outdoor activities in Costa Rica, and friendly outgoing cultures in Greece.\r\n\r\n">Usted puede obtener la historia y la cultura de Italia, playas y centros tur&iacute;sticos en Hawai, el turismo ecol&oacute;gico y actividades al aire libre en Costa Rica, y culturas salientes amistosos en Grecia.</span><br />\r\n	<br />\r\n	<span title="Why not go to a place that has it all, AND is right in your backyard.">&iquest;Por qu&eacute; no ir a un lugar que lo tiene todo, y est&aacute; justo en su patio trasero. </span><span title="Colombia is very close to the United States and Canada and is a new and exciting opportunity for any traveler.">Colombia est&aacute; muy cerca de los Estados Unidos y Canad&aacute;, y es una nueva y emocionante oportunidad para cualquier viajero. </span><span title="Flights to Colombia are generally inexpensive and are only a couple hours from most North American International airports.">Vuelos a Colombia son generalmente de bajo costo y son s&oacute;lo un par de horas de la mayor&iacute;a de los aeropuertos internacionales de Am&eacute;rica del Norte. </span><span title="Prices and accommodations are usually cheaper than their foreign counterparts, and there are always friendly people to show you around.">Los precios y las habitaciones son generalmente m&aacute;s baratos que sus hom&oacute;logos extranjeros, y siempre hay gente amable que le muestre todo.</span></span></p>\r\n<p>\r\n	<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/colonial-buildings-in-old-cartagena-colombia-david-smith(1).jpg" style="width: 1000px; height: 666px;" /></p>\r\n', 1, '2016-09-27 07:54:25');
INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(6, 'Colombian Women', 'Mujeres Colombianas', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		Colombian women are known to be loyal and infidelity is not really part of their vocabulary. This idea is reflected by the fact that Colombia has one of the lowest divorce rates in the world. When dating a Colombian girl, it is vital to win the approval of her family, especially if you want the relationship for long term. In fact, most Colombian women stay with their immediate family until they get married. Then, they stay with their spouse until the end. Such practice is a huge part of their culture, as I said, they are highly traditional and conventional. They really value the quality of family relationship.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Colombian women are the type of ladies that men are deeply after to, their qualities are indeed mesmerizing. It is important for Colombian women to satisfy the needs of her husband. They see to it that their family is intact. Colombian girls are independent and very determine. Colombian girls are known for their open-minded outlook, heartwarming laughter, and caring attitude. They always want to exhibit their womanhood. Most men find them to be very physically attractive, and they are often substantially thinner than their American counterparts.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		A lot of foreign men are seeing and hearing all the good qualities of Colombian women. However, there is no end to a list of positive things about these women. They don&#39;t value luxurious things or a glamorous lifestyle. They simply want to be loved. They do not expect gifts, and they are even excited when a man pays for their cab ride or buys them dinner. Being a man of honor is a quality that Colombian girls are deepy attracted to.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Thus, many men want the chance to meet Colombian women. Some men tend to prefer other girls such as Asian or Latinas. With effort and confidence, a man will not have a hard time to date a Colombian girl.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		The great thing about Colombian women is that they have the inclination for foreign men. Most Colombian women feel like Colombian men act like Casanovas. They are tired of these attitudes, and they want something different. Colombian girls are addicted to foreign movies and shows. Watching these movies, they have learned about romance. They want a man who can give them this kind of romance. MiAmor.com.co has a large database of Colombian women who want to find a foreign partner.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		They regard American and other foreign men as the ideal partners for life.. Many Colombian girls leave their country to seek a man in the United States. By following these tips, you may be able to date one of these beautiful women. If you want a Colombian girl to consider dating you, simply be romantic and affectionate. Colombian women are still traditional when it comes to dating routines. By being sweet and romantic, it will not be hard for you to make her fall in love. Once a Colombian woman believes this, she will stay devoted to you forever.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		make sure to present yourself in a neat way, this will surely attract Colombian women into you. However, you should have a few conversation topics ready to impress these women. Colombian women love music. If possible, you should be prepared to talk about a few of your favorite albums. You may even wish to listen to a little Colombian music before your first date. Attracting Colombian women can be fairly easy. You need to make sure that you look reasonably clean and attractive. Then you will definitely make a Colombian girl fall for you.</p>\r\n	<p>\r\n		<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/COLOMBIAN WOMEN IMAGE.JPG" style="width: 1000px; height: 748px;" /></p>\r\n</div>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span title="Colombian women are known to be loyal and infidelity is not really part of their vocabulary.">Mujeres colombianas son conocidos por ser leal y la infidelidad no es realmente parte de su vocabulario. </span><span title="This idea is reflected by the fact that Colombia has one of the lowest divorce rates in the world.">Esta idea se refleja en el hecho de que Colombia tiene una de las tasas de divorcio m&aacute;s bajas del mundo. </span><span title="When dating a Colombian girl, it is vital to win the approval of her family, especially if you want the relationship for long term.">Al salir con una chica colombiana, es vital para ganar la aprobaci&oacute;n de su familia, especialmente si desea que la relaci&oacute;n de largo plazo. </span><span title="In fact, most\r\n">De hecho, la mayor parte de </span><span title="Colombian women stay with their immediate family until they get married.">Mujeres colombianas permanecen con su familia inmediatamente hasta que se casan. </span><span title="Then, they stay with their spouse until the end.">A continuaci&oacute;n, se quedan con su c&oacute;nyuge hasta el final. </span><span title="Such practice is a huge part of their culture, as I said, they are highly traditional and conventional.">Esta pr&aacute;ctica es una gran parte de su cultura, como he dicho, son muy tradicional y convencional. </span><span title="They really value the quality of family relationship.\r\n\r\n">Ellos valoran mucho la calidad de la relaci&oacute;n familiar.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="Colombian women are the type of ladies that men are deeply after to, their qualities are indeed mesmerizing.">Mujeres colombianas son el tipo de mujeres que hombres est&aacute;n profundamente despu&eacute;s de sus cualidades son realmente fascinante. </span><span title="It is important for Colombian women to satisfy the needs of her husband.">Es importante que las mujeres colombianas para satisfacer las necesidades de su marido. </span><span title="They see to it that their family is intact.">Se preocupan de que su familia est&aacute; intacto. </span><span title="Colombian girls are independent and very determine.">Ni&ntilde;as colombianas son independientes y muy determinadas. </span><span title="Colombian girls are known for their open-minded outlook, heartwarming laughter, and caring attitude.">Ni&ntilde;as colombianas son conocidos por su actitud de mente abierta, la risa tierna, y la actitud solidaria. </span><span title="They always want to exhibit their womanhood.">Siempre quieren exhibir su condici&oacute;n de mujer. </span><span title="Most men find them to be very physically attractive, and they are often substantially thinner than their American counterparts.\r\n\r\n">La mayor&iacute;a de los hombres encuentran que sean muy atractivas f&iacute;sicamente, y que a menudo son considerablemente m&aacute;s delgado que sus hom&oacute;logos estadounidenses.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="A lot of foreign men are seeing and hearing all the good qualities of Colombian women.">Una gran cantidad de hombres extranjeros est&aacute;n viendo y oyendo todas las buenas cualidades de la mujer colombiana. </span><span title="However, there is no end to a list of positive things about these women.">Sin embargo, no hay fin a una lista de cosas positivas sobre estas mujeres. </span><span title="They don''t value luxurious things or a glamorous lifestyle.">No valoran las cosas de lujo o un estilo de vida glamoroso. </span><span title="They simply want to be loved.">Ellos simplemente quieren ser amados. </span><span title="They do not expect gifts, and they are even excited when a man pays for their cab ride or buys them dinner.">No esperan regalos, y&nbsp; son a&uacute;n emocionales cuando un hombre paga por su viaje en taxi o les compra la cena. </span><span title="Being a man of honor is a quality that Colombian girls are deepy attracted to.\r\n\r\n">Siendo un hombre de honor es una cualidad que las ni&ntilde;as colombianas son Deepy atra&iacute;do.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="Thus, many men want the chance to meet Colombian women.">Por lo tanto, muchos hombres quieren la oportunidad de conocer a la mujer colombiana. </span><span title="Some men tend to prefer other girls such as Asian or Latinas.">Algunos hombres tienden a preferir otras chicas tales como Asia o latinas. </span><span title="With effort and confidence, a man will not have a hard time to date a Colombian girl.\r\n\r\n">Con esfuerzo y confianza, un hombre no va a tener un tiempo dif&iacute;cil salir con una chica colombiana.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="The great thing about Colombian women is that they have the inclination for foreign men.">La gran cosa sobre las mujeres colombianas es que tienen la inclinaci&oacute;n de los hombres extranjeros. </span><span title="Most Colombian women feel like Colombian men act like Casanovas.">La mayor&iacute;a de las mujeres colombianas sienten como hombres colombianos act&uacute;an como Casanovas. </span><span title="They are tired of these attitudes, and they want something different.">Est&aacute;n cansados â€‹â€‹de estas actitudes, y quieren algo diferente. </span><span title="Colombian girls are addicted to foreign movies and shows.">Ni&ntilde;as colombianas son adictas a las pel&iacute;culas y programas extranjeros. </span><span title="Watching these movies, they have learned about romance.">Viendo estas pel&iacute;culas, que han aprendido sobre el romance. </span><span title="They want a man who can give them this kind of romance.">Ellas quieren un hombre que les puede dar este tipo de romance. </span><span title="MiAmor.com.co has a large database of Colombian women who want to find a foreign partner.\r\n\r\n">MiAmor.com.co tiene una gran base de datos de las mujeres colombianas que quieren encontrar una pareja extranjero.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="They regard American and other foreign men as the ideal partners for life.. Many Colombian girls leave their country to seek a man in the United States.">Consideran que los hombres estadounidenses y de otros como los socios ideales para la vida .. Muchas mujeres colombianas abandonan su pa&iacute;s en busca de un hombre en los Estados Unidos. </span><span title="By following these tips, you may be able to date one of these beautiful women.">Siguiendo estos consejos, usted puede ser capaz de salir con una de estas hermosas mujeres. </span><span title="If you want a Colombian girl to consider dating you, simply be romantic and affectionate.">Si quieres una chica colombiana a considerar salir contigo, simplemente ser rom&aacute;ntico y cari&ntilde;oso. </span><span title="Colombian women are still traditional when it comes to dating routines.">Mujeres colombianas siguen siendo tradicional cuando se trata de rutinas de citas. </span><span title="By being sweet and romantic, it will not be hard for you to make her fall in love.">Al ser dulce y rom&aacute;ntico, no va a ser dif&iacute;cil para usted para hacer que se enamore. </span><span title="Once a Colombian woman believes this, she will stay devoted to you forever.\r\n\r\n">Una vez que una mujer colombiana cree esto, ella se quedar&aacute; enamorada de ti para siempre.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span title="make sure to present yourself in a neat way, this will surely attract Colombian women into you.">aseg&uacute;rese de presentarse de una manera ordenada, esto seguramente atraer&aacute; a las mujeres colombianas en usted. </span><span title="However, you should have a few conversation topics ready to impress these women.">Sin embargo, usted debe tener un par de temas de conversaci&oacute;n listos para impresionar a estas mujeres. </span><span title="Colombian women love music.">Mujeres colombianas les encanta la m&uacute;sica. </span><span title="If possible, you should be prepared to talk about a few of your favorite albums.">Si es posible, usted debe estar preparado para hablar de algunos de sus discos favoritos. </span><span title="You may even wish to listen to a little Colombian music before your first date.">Incluso puedes escuchar un poco de m&uacute;sica colombiana antes de su primera cita. </span><span title="Attracting Colombian women can be fairly easy.">Atraer a las mujeres colombianas puede ser bastante f&aacute;cil. </span><span title="You need to make sure that you look reasonably clean and attractive.">Usted necesita asegurarse de que usted parece bastante limpio y atractivo. </span><span title="Then you will definitely make a Colombian girl fall for you.">Entonces definitivamente va a hacer que una chica colombiana cae para usted.</span></span></p>\r\n<p>\r\n	<span lang="es"><span title="Then you will definitely make a Colombian girl fall for you."><img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/COLOMBIAN WOMEN IMAGE(1).JPG" style="width: 1000px; height: 748px;" /></span></span></p>\r\n', 1, '2016-09-27 08:04:54'),
(7, 'Why do Colombian women prefer foreigners?', 'Por quÃ© las mujeres los prefieren extranjeros?', '<p>\r\n	<span id="result_box" lang="en"><span title="Ojos azules, acento distinto.">Blue eyes, different accents. </span><span title="Mujeres enloquecidas.">Crazed women. </span><span title="ï¿½Quï¿½ tan cierto es el imaginario de que las mujeres, a la hora de escoger entre locales y este tipo de especï¿½menes, se decantan sin pensar por los segundos?">How true is the imagination of the Colombian women, when choosing between local and this type of specimens, without thinking opt for the latter? </span><span title="ï¿½Mejor posiciï¿½n econï¿½mica?">Better off? </span><span title="ï¿½Trato igualitario?">Equal treatment? </span><span title="ï¿½Estatus social, o amor?\r\n\r\n">Social status, or love?</span><br />\r\n	<br />\r\n	<span title="Quizï¿½s todas y cada una de las anteriores.">Maybe each and every one of the above. </span><span title="Es decir, la tara cultural ha estado muy arraigada desde el comienzo de nuestra historia.">That is, the cultural shortcoming that has been well established since the beginning. </span><span title="Segï¿½n el libro de referencia del genetista colombiano Emilio Yunis, ''ï¿½Por quï¿½ somos asï¿½?'', se refleja en la enseï¿½anza repentina y continuada de que todo aquel con facciones europeas resultaba ser superior por antonomasia.">According to the reference book of the Colombian geneticist Emilio Yunis, &#39;Why are we like this?&#39;, Reflected in the sudden and continued teaching that everyone with European features proved to be superior par excellence. </span><span title="Y por ende, estar con ellos era recibir favores e incluso atenciones.\r\n\r\n">And therefore, being with them your going to be receiving favors and even attention.</span><br />\r\n	<br />\r\n	<span title="Esclavas afroamericanas e indï¿½genas, fuese por relaciones forzadas y abusivas o por voluntad propia, buscaban un ï¿½mejor futuroï¿½ con los extranjeros.">African American slaves and Indians, through forced and abusive relationships or by choice, seeked a &quot;better future&quot; with foreigners. </span><span title="Claro, no sucede en todos los casos.\r\n\r\n">Of course, not true in all cases.</span><br />\r\n	<br />\r\n	<span title="De igual modo, tiene mucho que ver en el sentimiento de inferioridad racial y complejos que han tenido muchas etnias no blancas en Latinoamï¿½rica.">Similarly, it has much to do with the feeling of racial inferiority complexes that have had many non-white ethnic groups in Latin America. </span><span title="Sus estadistas procuraron favorecer la inmigraciï¿½n de extranjeros, preferiblemente cristianos y blancos, para ï¿½mejorar la razaï¿½.">Seekt to encourage the immigration of foreigners, preferably Christians and white, to &quot;improve the race.&quot; </span><span title="Y en muchos casos, mejorar la posiciï¿½n econï¿½mica y ver que, en comparaciï¿½n con los hombres de otros lados, los locales tenï¿½an mucho que perder.\r\n\r\n">And in many cases, improve the economic position and wait and see, compared with men from other places, locals had a lot to lose.</span><br />\r\n	<br />\r\n	<span title="Esto se puede ver en columnas como la de Hï¿½ctor Abad Faciolince llamada ''El dimorfismo sexual colombianoï¿½, donde los hombres de aquel paï¿½s no salï¿½an bien parados en comparaciï¿½n con la popular belleza de sus compatriotas femeninas.">This can be seen in columns like H&eacute;ctor Abad Faciolince called &#39;The Colombian sexual dimorphism, &quot;where the men of the country do not stand out well compared to the popular beauty of their female compatriots. </span><span title="Tambiï¿½n se puede ver en la cantidad de mujeres que acuden a agencias matrimoniales por un hombre extranjero, ya que se quejan del machismo e infidelidad de los hombres locales.">It can also be seen in the number of women seeking to date online to meet a foreign man, Damon de Berry of MiAmor.com.co says Colombian women complain of machismo and infidelity of local men. </span><span title="Y no pasa solo en Latinoamï¿½rica.\r\n\r\n">This doesn&#39;t only happens in Latin America though</span><br />\r\n	<br />\r\n	<span title="En Colombia, por ejemplo, existe una agencia que muestra en su pï¿½gina testimonios de enlaces exitosos entre extranjeros y mujeres nacionales.">In MiAmor, for example, there is are page testimonies of successful links between foreign and domestic women. </span><span title="Explican el por quï¿½ de la ''deseabilidad'' de las colombianas, que son mï¿½s hogareï¿½as y menos frï¿½as que las anglosajonas, y resumen las ''ventajas'' de los norteamericanos asï¿½: ï¿½Estos hombres tienen experiencia, son educados, profesionales y pueden ofrecerte un mejor">Which describe the &#39;desirability&#39; of Colombian women, who are more homey and less cold than the Anglo-Saxon&#39;s, and the testimonies summarize the &#39;advantages&#39; of the American way: &quot;These men are experienced, polite, professional and can offer a better </span><span title="nivel de vida que la mayorï¿½a de los prospectos a nivel localï¿½, aseguran.\r\n\r\n">standard of living than most local prospects, &quot;they say.</span><br />\r\n	<br />\r\n	<span title="ï¿½Las diferencias con los hombres de mi paï¿½s, al menos en mi experiencia personal, es que ellos no son personas de confiar, te mienten mucho y no les importa lo que sientes.">&quot;The differences with the men of my country, at least in my personal experience is that they are not people to trust, they lie a lot and do not care how you feel. </span><span title="Con ï¿½l las cosas son muy distintas, sabemos que la honestidad es una clave para que nuestra relaciï¿½n sea sï¿½lida y no tiene miedo en mostrarme sus sentimientos, tanto su lado fuerte como su lado dï¿½bil.">With my current boyfriend things are very different, we know that honesty is the key to our relationship, we are not afraid to show our feelings both strong and the weak side. </span><span title="Eso en una cultura como la mï¿½a se ve mal entre los mismos hombresï¿½, afirma la periodista Natalia Torres, quien es novia de un inglï¿½s.">That in my culture,makes men look bad, &quot;says journalist Natalia Torres, who is the girlfriend of an Englishman. </span><span title="Natalia aclara, sin embargo, que no la enamorï¿½ el hecho de que fuese extranjero, sino sus intereses comunes, cosa que no encontrï¿½ con ningï¿½n hombre de su naciï¿½n.\r\n\r\n">Natalia is clear though that she did not fall for him because he was a foreigner, but their common interests, which isn&#39;t found with any men of her nation.</span><br />\r\n	<br />\r\n	<span title="No todo es lo que pareceï¿½\r\n\r\n">Not everything is what it seems ...</span><br />\r\n	<br />\r\n	<span title="Pero hombres malos hay en todas partes.">But evil men are everywhere. </span><span title="Prueba de eso es que algunas mujeres no encuentran su ï¿½paraï¿½so soï¿½adoï¿½ con el extranjero que les prometiï¿½ el cielo.\r\n\r\n">Proof of this is that some women do not find their &quot;dream paradise&quot; with the foreigner who promised heaven.</span><br />\r\n	<br />\r\n	<span title="Tambiï¿½n sucede que hay cuestiones culturales que muchas mujeres no tienen en cuenta a la hora de entablar una relaciï¿½n y querer salir de su paï¿½s a toda costa.\r\n\r\n">It also happens that there are cultural issues that many women do not consider when building a relationship and wanting to leave their country at all costs.</span><br />\r\n	<br />\r\n	<span title="Como quien dice, no todo lo que brilla es oro, y hay muchos factores que llevan a las mujeres a relacionarse con hombres de otro paï¿½s.">As they say, all that glitters is not gold, and there are many factors that arrive for women who try and relate to people from another country. </span><span title="Y no todos los casos son iguales pero el mï¿½s importante es que usted construye una relaciï¿½n sï¿½lida y realmente conocer a su Extranjero antes de decidirse a correr y fugarse\r\n\r\n">And not all the cases are the same but the most important is that you build a solid relationship and really know your foreigner before deciding to run off and escape</span><br />\r\n	<br />\r\n	<span title="Miamor tiene muy estrictas directrices acoso y comunicaciï¿½n para mantener a todas las mujeres a gusto con su introducciï¿½n extranjero y experiencias de dating">Miamor harassment has very strict guidelines with communication to keep all women at ease with foreign introductions and dating experiences</span></span></p>\r\n<p>\r\n	<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/soc_4.jpg" style="width: 1000px; height: 750px;" /></p>\r\n', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		Ojos azules, acento distinto. Mujeres enloquecidas. &iquest;Qu&eacute; tan cierto es el imaginario de que las mujeres, a la hora de escoger entre locales y este tipo de espec&iacute;menes, se decantan sin pensar por los segundos? &iquest;Mejor posici&oacute;n econ&oacute;mica? &iquest;Trato igualitario? &iquest;Estatus social, o amor?</p>\r\n	<p>\r\n		Quiz&aacute;s todas y cada una de las anteriores. Es decir, la tara cultural ha estado muy arraigada desde el comienzo de nuestra historia. Seg&uacute;n el libro de referencia del genetista colombiano Emilio Yunis, &lsquo;&iquest;Por qu&eacute; somos as&iacute;?&rsquo;, se refleja en la ense&ntilde;anza repentina y continuada de que todo aquel con facciones europeas resultaba ser superior por antonomasia. Y por ende, estar con ellos era recibir favores e incluso atenciones.</p>\r\n	<p>\r\n		Esclavas afroamericanas e ind&iacute;genas, fuese por relaciones forzadas y abusivas o por voluntad propia, buscaban un &ldquo;mejor futuro&rdquo; con los extranjeros. Claro, no sucede en todos los casos.</p>\r\n	<p>\r\n		De igual modo, tiene mucho que ver en el sentimiento de inferioridad racial y complejos que han tenido muchas etnias no blancas en Latinoam&eacute;rica. Sus estadistas procuraron favorecer la inmigraci&oacute;n de extranjeros, preferiblemente cristianos y blancos, para &ldquo;mejorar la raza&rdquo;. Y en muchos casos, mejorar la posici&oacute;n econ&oacute;mica y ver que, en comparaci&oacute;n con los hombres de otros lados, los locales ten&iacute;an mucho que perder.</p>\r\n	<p>\r\n		<span id="result_box" lang="es"><span class="hps">Esto se puede ver</span> <span class="hps">en las columnas</span> <span class="hps">como</span> <span class="hps">H&eacute;ctor</span> <span class="hps">Abad</span> <span class="hps">Faciolince</span> <span class="hps atn">llama &#39;</span><span>El</span> <span class="hps">dimorfismo</span> <span class="hps">sexual</span> <span class="hps">en Colombia</span><span>,</span> <span class="hps atn">&quot;</span><span>donde los hombres</span> <span class="hps">del pa&iacute;s</span> <span class="hps">no se destacan</span> <span class="hps">bien en comparaci&oacute;n con</span> <span class="hps">la belleza</span> <span class="hps">popular de</span> <span class="hps">sus</span> <span class="hps">compatriotas femeninas</span><span>.</span> <span class="hps">Tambi&eacute;n</span> <span class="hps">se puede ver en</span> <span class="hps">el n&uacute;mero de mujeres</span> <span class="hps">que buscan</span> <span class="hps">hasta la fecha</span> <span class="hps">en l&iacute;nea para</span> <span class="hps">conocer a un</span> <span class="hps">hombre extranjero</span><span>,</span> <span class="hps">Damon</span> <span class="hps">de Berry</span> <span class="hps">de</span> <span class="hps">MiAmor.com.co</span> <span class="hps">dice que las mujeres</span> <span class="hps">colombianas</span> <span class="hps">se quejan</span> <span class="hps">del machismo</span> <span class="hps">y la infidelidad</span> <span class="hps">de los hombres</span> <span class="hps">locales.</span> <span class="hps">Esto no</span> <span class="hps">s&oacute;lo ocurre</span> <span class="hps">en Am&eacute;rica Latina</span><span>, aunque</span></span></p>\r\n	<p>\r\n		<span id="result_box" lang="es"><span class="hps">En</span> <span class="hps">miamor</span><span>, por ejemplo</span><span>, hay</span>&nbsp; <span class="hps">p&aacute;ginas de</span> <span class="hps">testimonios de</span> <span class="hps">v&iacute;nculos</span> <span class="hps">eficaces entre</span> <span class="hps">las mujeres</span> <span class="hps">extranjeras y nacionales</span><span>.</span> <span class="hps">Que describen</span> <span class="hps atn">la &quot;</span><span>conveniencia</span><span>&quot; de las mujeres</span> <span class="hps">colombianas</span><span>, que son m&aacute;s</span> <span class="hps">acogedor y</span> <span class="hps">menos fr&iacute;o que</span> <span class="hps">el</span> <span class="hps">anglosaj&oacute;n</span> <span class="hps">y</span> <span class="hps">los testimonios</span> <span class="hps">resumen las</span> <span class="hps">&quot;ventajas</span><span>&quot; de</span> <span class="hps">la manera americana</span><span>:</span> <span class="hps atn">&quot;</span><span>Estos hombres</span> <span class="hps">son experimentados</span><span>,</span> <span class="hps">educado, profesional</span> <span class="hps">y pueden ofrecer</span> <span class="hps">una mejor</span> <span class="hps">nivel de vida</span> <span class="hps">que la mayor&iacute;a de</span> <span class="hps">las perspectivas</span> <span class="hps">locales</span> <span class="hps">&quot;, dicen</span><span>.</span></span></p>\r\n	<p>\r\n		&ldquo;Las diferencias con los hombres de mi pa&iacute;s, al menos en mi experiencia personal, es que ellos no son personas de confiar, te mienten mucho y no les importa lo que sientes. Con &eacute;l las cosas son muy distintas, sabemos que la honestidad es una clave para que nuestra relaci&oacute;n sea s&oacute;lida y no tiene miedo en mostrarme sus sentimientos, tanto su lado fuerte como su lado d&eacute;bil. Eso en una cultura como la m&iacute;a se ve mal entre los mismos hombres&rdquo;, afirma la periodista Natalia Torres, quien es novia de un ingl&eacute;s. Natalia aclara, sin embargo, que no la enamor&oacute; el hecho de que fuese extranjero, sino sus intereses comunes, cosa que no encontr&oacute; con ning&uacute;n hombre de su naci&oacute;n.</p>\r\n	<p>\r\n		No todo es lo que parece&hellip;</p>\r\n	<p>\r\n		Pero hombres malos hay en todas partes. Prueba de eso es que algunas mujeres no encuentran su &ldquo;para&iacute;so so&ntilde;ado&rdquo; con el extranjero que les prometi&oacute; el cielo.</p>\r\n	<p>\r\n		Tambi&eacute;n sucede que hay cuestiones culturales que muchas mujeres no tienen en cuenta a la hora de entablar una relaci&oacute;n y querer salir de su pa&iacute;s a toda costa.</p>\r\n	<p>\r\n		Como quien dice, no todo lo que brilla es oro, y hay muchos factores que llevan a las mujeres a relacionarse con hombres de otro pa&iacute;s. Y no todos los casos son iguales pero el m&aacute;s importante es que usted construye una relaci&oacute;n s&oacute;lida y realmente conocer a su Extranjero antes de decidirse a correr y fugarse</p>\r\n	<p>\r\n		Miamor tiene muy estrictas directrices acoso y comunicaci&oacute;n para mantener a todas las mujeres a gusto con su introducci&oacute;n extranjero y experiencias de dating</p>\r\n	<p>\r\n		<img alt="" src="/home/damon789/public_html/www.miamor.com.co/ckeditortest_WORKING/ckfinder/userfiles/images/soc_4(2).jpg" style="width: 1000px; height: 750px;" /></p>\r\n</div>\r\n', 1, '2016-09-27 08:15:00'),
(8, 'Dating Colombian Girls', 'Dating Girls Colombianos', '<p>\r\n	Dating Colombian Girls<br />\r\n	<br />\r\n	When I first came to South America one of the things I relished most was the idea of dating Latin women.<br />\r\n	<br />\r\n	In every South American country in which I travelled, from the tip of Patagonia to the northernmost reaches of the continent I met wonderful women. They were friendly, kind, sweet, generous, sincere and in many cases overwhelmingly beautiful.<br />\r\n	<br />\r\n	And nowhere in South America is this truer than for Colombian girls.<br />\r\n	<br />\r\n	Having lived in Colombia now for well over a decade I have a fair amount of experience dating Colombian women. Most of my experiences have been very positive. But it has taken a while to hone my dating skills.<br />\r\n	<br />\r\n	There are some big cultural differences that exist between the typical European or North American woman and Colombian women.<br />\r\n	<br />\r\n	The following is my impression of Colombian girls that I have formed over many years of living here from numerous dates and also a couple of serious relationships.<br />\r\n	<br />\r\n	Will I generalise? Of course I will. It takes all sorts, as they say. You can find pretty much any type of woman (and man) in Colombia just as you can in other parts of the world. But being aware of some of the trends and characteristics that apply to Colombian women and the dating scene in Colombia may prove invaluable if you&rsquo;re looking for some Latin romance.<br />\r\n	Characteristics of Colombian Girls<br />\r\n	<br />\r\n	It is difficult to generalise when it comes to describing Colombian girls physically since Colombia is such a racially diverse country.<br />\r\n	<br />\r\n	In the coastal regions, for example, the majority of the people are of black descent, whereas in the area around Medellin and the coffee region the population is much whiter. In parts of central and southern Colombia many people come from an indigenous background.<br />\r\n	<br />\r\n	One thing I can guarantee is that in pretty much any Colombian town or city you will see beautiful Colombian girls. Or you will meet them online in dating sites like MiAmor.com.co which is a Colombian based dating site.<br />\r\n	<br />\r\n	But not only are Colombian women beautiful, they can also be incredibly flirtatious. Many Colombian women enjoy being the object of admiring stares in the street, although most resent the hissing and wolf-whistling which Colombian men can sometimes be prone to.<br />\r\n	<br />\r\n	That said, a female Colombian friend once remarked that she had felt oddly unattractive while living in the USA. She would walk past numerous construction sites and be met with nothing but a disinterested silence, something that would be unimaginable in Colombia.<br />\r\n	<br />\r\n	She joked that she had considered walking back past the construction workers with a couple more buttons undone in an attempt to elicit the kind of hisses and sordid comments that would have annoyed her back home!<br />\r\n	<br />\r\n	On the whole Colombia is a country which seems to be increasingly comfortable with sexuality (especially among the younger generations), despite some strong conservative traditions.<br />\r\n	<br />\r\n	In some Colombian cities, most notably Medellin and Cali, it has become quite common to see women with silicon implants.<br />\r\n	<br />\r\n	This supposedly goes back to the days of the drug cartels, of which the Medellin and Cali cartels were the largest and most powerful. The tremendous wealth which was generated from the trafficking of drugs created a culture of elaborate spending.<br />\r\n	<br />\r\n	Pablo Escobar famously imported elephants and other exotic animals to his Hacienda Napoles resort. And many Medellin girls got silicon implants, lured by the glamorous lifestyle that being the girlfriend of a &#39;Mafioso&#39; could provide.<br />\r\n	<br />\r\n	The elephants have long since gone, but the culture of silicon enhancements has remained.<br />\r\n	<br />\r\n	<br />\r\n	Dating Colombian Girls - What to Expect<br />\r\n	<br />\r\n	I&#39;ve found approaching and engaging Colombian girls to be much less daunting than in, for example, my home country of the UK.<br />\r\n	<br />\r\n	Depending on how you make your move you will very occasionally get the cold shoulder, but more often than not the girl will be happy to talk to you even if she&rsquo;s not interested in anything more than friendship or a quick chat.<br />\r\n	<br />\r\n	European girls would tend to invent an imaginary boyfriend to deter your advances, Colombian girls are much more likely to give you her number and then either dodge your calls, make excuses for not going out with you, or worse still, accept a date and then stand you up. It&rsquo;s something I learned the hard way in my first year or so in Colombia!<br />\r\n	<br />\r\n	Being able to communicate reasonably well in Spanish definitely helps when it comes to spotting if there is genuine interest there or not, if your connecting online you can use a translation service if your Spanish isn&rsquo;t up to scratch.<br />\r\n	<br />\r\n	If you hear either &ldquo;Es que esta lloviendo / esta hacienda mucho frio&rdquo; (&ldquo;The thing is it&rsquo;s raining / it&rsquo;s very cold&rdquo;) or &ldquo;Es que tengo mucha pereza&rdquo; (roughly translated as &ldquo;I can&rsquo;t really be bothered&rdquo;) then alarm bells should start ringing!<br />\r\n	<br />\r\n	At the same time, however, you have to realise that in Colombia the onus is on the man to call and to make arrangements. But if you make an effort and keep getting fobbed off then it&rsquo;s probably best to cut your losses.<br />\r\n	<br />\r\n	The Dating Ritual - Some Tips<br />\r\n	<br />\r\n	OK, so you&rsquo;ve met a girl and she&rsquo;s agreed to go out with you or you are coming to Colombia to meet someone that you have met online. There are some general rules to which you should adhere to in order to increase your chances of making a good impression.<br />\r\n	<br />\r\n	Colombia is still a very conservative country and many Colombian girls will expect you to collect them in your car or in a taxi on your way to a date (particularly the first few dates), and when going home.<br />\r\n	<br />\r\n	If the girl does agree to meet you there make sure you&rsquo;re on time. While it&rsquo;s often acceptable for the girl to arrive 20 minutes late<br />\r\n	<br />\r\n	To most Colombia women, however, being &ldquo;detallista&rdquo; (attentive to the details) i.e. opening cars doors for her etc is more important than wining and dining her in a fancy restaurant. Not that it hurts!<br />\r\n	<br />\r\n	People in Colombia dress smartly when they go out. And hygiene is viewed with an even greater importance than it is in Europe and North America.<br />\r\n	<br />\r\n	Colombian girls take huge pride in their appearance and will often spend an hour or two getting ready.<br />\r\n	<br />\r\n	If you&rsquo;re a backpacker, try to dress up as much as possible and remember that in cities like Medellin and Cali where the climate is perfect for shorts and sandals, the locals seldom wear them, especially at night.<br />\r\n	<br />\r\n	When it comes to paying the bill you should always offer to pay. There are actually many Colombian girls who insist on paying or at least making a contribution, but it&rsquo;s always polite to offer.<br />\r\n	<br />\r\n	The concept that the man should pay the bill can be so taken for granted that you sometimes don&rsquo;t even receive a simple &ldquo;Gracias&rdquo; as you leave. This is something that still annoys me, but as with many things it&rsquo;s an aspect of traditional Colombian culture which it&rsquo;s best just to try to accept.<br />\r\n	<br />\r\n	While some Colombian women will accept a kiss on the first date, traditionally the unwritten rule seems to be at least the second if not the third date. Coming on too strong on the first date could blow your chances completely. Try to read her signals as best you can. Touching her hand and then judging her reaction can be a good way to test the water.<br />\r\n	<br />\r\n	All in all, I find dating Colombian girls less stressful than dating girls in Britain. There isn&rsquo;t so much emphasis on impressing the girl with your career, studies or sophistication.<br />\r\n	<br />\r\n	Simply being a nice, gentlemanly guy can be enough for her to consider the date a success.</p>\r\n', '<p>\r\n	Dating Girls Colombianos<br />\r\n	<br />\r\n	Cuando llegu&eacute; por primera vez a Am&eacute;rica del Sur una de las cosas que m&aacute;s disfrutaba era la idea de salir con mujeres latinas.<br />\r\n	<br />\r\n	En todos los pa&iacute;ses de Am&eacute;rica del Sur en el que viajaba, desde la punta de la Patagonia hasta los confines septentrionales del continente conoc&iacute; a mujeres maravillosas. Eran amables, dulces, generosas, sinceras y en muchos casos abrumadoramente hermosas.<br />\r\n	<br />\r\n	Y en ninguna parte de Am&eacute;rica del Sur es m&aacute;s cierto que para las chicas colombianas.<br />\r\n	<br />\r\n	Habiendo vivido en Colombia ahora por m&aacute;s de una d&eacute;cada&nbsp; tengo una buena cantidad de experiencia de salir con mujeres colombianas. La mayor&iacute;a de mis experiencias han sido muy positivas. Pero he tomado un tiempo para perfeccionar mis habilidades de conquista.<br />\r\n	<br />\r\n	Hay algunas diferencias culturales grandes que existen entre la mujer t&iacute;pica europea o norteamericana y las mujeres colombianas.<br />\r\n	<br />\r\n	Lo que sigue es mi impresi&oacute;n de las chicas colombianas que he formado a lo largo de muchos a&ntilde;os de vivir aqu&iacute; desde numerosas fechas y tambi&eacute;n un par de relaciones serias.<br />\r\n	<br />\r\n	&iquest;Voy a generalizar? Por supuesto que lo har&eacute;. Hay de todo, como dicen. Usted puede encontrar casi cualquier tipo de mujer (y hombre) en Colombia del mismo modo que en otras partes del mundo. Pero ser consciente de algunas de las tendencias y caracter&iacute;sticas que se aplican a las mujeres de Colombia y el mundo de las citas en Colombia puede resultar muy &uacute;til si usted est&aacute; buscando un poco de romance latino.<br />\r\n	[7/05/15 9:34:53 am] jhoana salas: Caracter&iacute;sticas de las muchachas colombianas<br />\r\n	<br />\r\n	Es dif&iacute;cil generalizar cuando se trata de la descripci&oacute;n de las chicas colombianas f&iacute;sicamente ya que Colombia es un pa&iacute;s racialmente diverso.<br />\r\n	<br />\r\n	En las regiones costeras, por ejemplo, la mayor&iacute;a de las personas son de origen negro, mientras que en el &aacute;rea alrededor de Medell&iacute;n y la regi&oacute;n cafetera de la poblaci&oacute;n es mucho m&aacute;s blanca. En algunas partes de Colombia muchas personas del centro y sur provienen de un fondo ind&iacute;gena.<br />\r\n	<br />\r\n	Una cosa que puedo garantizar es que en casi cualquier pueblo o ciudad colombiana podr&aacute;s ver hermosas chicas colombianas. O usted podra encontrarlas en l&iacute;nea en los sitios de citas como MiAmor.com.co que es un sitio de citas basado en colombianas.<br />\r\n	<br />\r\n	Pero no s&oacute;lo son las mujeres colombianas hermosas, tambi&eacute;n pueden ser muy coquetas. Muchas mujeres colombianas gozan de ser el objeto de admiracion de miradas en la calle, aunque m&aacute;s resienten el silbido que los hombres colombianos a veces pueden ser propensos a.<br />\r\n	<br />\r\n	Dicho esto, una amiga colombiana dijo una vez que se hab&iacute;a sentido extra&ntilde;amente atractiva mientras viv&iacute;a en los EE.UU.. Ella caminaba pasando numerosas obras de construcci&oacute;n con nada m&aacute;s que un silencio desinteresado, algo que ser&iacute;a inimaginable en Colombia.<br />\r\n	<br />\r\n	Ella brome&oacute; que ella hab&iacute;a considerado volver caminando m&aacute;s all&aacute; de los trabajadores de la construcci&oacute;n con un par de botones desabrochados en un intento de obtener el tipo de silbidos y comentarios s&oacute;rdidos que tendr&iacute;a en Colombia pero no entonces se regreso a casa molesta!<br />\r\n	<br />\r\n	En todo&nbsp; Colombia es un pa&iacute;s que parece ser cada vez m&aacute;s c&oacute;modo con la sexualidad (especialmente entre las generaciones m&aacute;s j&oacute;venes), a pesar de algunas fuertes tradiciones conservadoras.<br />\r\n	<br />\r\n	En algunas ciudades de Colombia, sobre todo Medell&iacute;n y Cali, se ha vuelto muy com&uacute;n ver a las mujeres con implantes de silicona.<br />\r\n	<br />\r\n	Esto supone que se remonta a la &eacute;poca de los carteles de la droga, de los cuales los carteles de Medell&iacute;n y Cali fueron el m&aacute;s grande y poderoso. La enorme riqueza que se genera a partir del tr&aacute;fico de drogas ha creado una cultura de la elaborada gasto.<br />\r\n	<br />\r\n	Pablo Escobar famoso importador de elefantes y otros animales ex&oacute;ticos a su resort Hacienda N&aacute;poles. Y muchas chicas de Medell&iacute;n recibieron implantes de silicona, atra&iacute;dos por el estilo de vida glamoroso que ser la novia de un &#39;mafioso&#39; podr&iacute;a proporcionar.<br />\r\n	[7/05/15 9:35:21 am] jhoana salas: Los elefantes han pasado desde hace mucho tiempo, pero la cultura de mejoras de la silicona se ha mantenido.<br />\r\n	<br />\r\n	<br />\r\n	Dating Chicas colombianas - &iquest;Qu&eacute; esperar?<br />\r\n	<br />\r\n	He encontrado que las chicas Colombianas se acercan y participan, por ejemplo, mi pa&iacute;s de origen del Reino Unido.<br />\r\n	<br />\r\n	Dependiendo de c&oacute;mo usted hace su movimiento va muy de vez en cuando obtiene el hombro fr&iacute;o, pero m&aacute;s a menudo la chica estar&aacute; encantada de hablar con usted, incluso si ella no est&aacute; interesado en nada m&aacute;s que una amistad o una charla r&aacute;pida.<br />\r\n	<br />\r\n	Chicas europeas tender&iacute;an a inventar un novio imaginario para disuadir a sus avances, las chicas colombianas son mucho m&aacute;s duras que le den su n&uacute;mero y luego o bien esquivan sus llamadas, hacen excusas para no salir contigo, o peor a&uacute;n, aceptan una fecha y despu&eacute;s no van a esta. Es algo que aprend&iacute; de la manera dif&iacute;cil en mi primer a&ntilde;o m&aacute;s o menos en Colombia!<br />\r\n	<br />\r\n	Ser capaz de comunicarse razonablemente bien en espa&ntilde;ol sin duda ayuda a la hora de detectar si existe un inter&eacute;s genuino all&iacute; o no, si su conexi&oacute;n es en l&iacute;nea que usted puede utilizar un servicio de traducci&oacute;n si su espa&ntilde;ol no est&aacute; a la altura.<br />\r\n	<br />\r\n	Si escucha bien &quot;Es Que esta lloviendo / esta haciendo Mucho frio&quot; (&quot;La cosa se est&aacute; lloviendo / hace mucho fr&iacute;o&quot;) o &quot;Es que tengo mucha pereza&quot; (que podr&iacute;a traducirse como &quot;No puedo ser molestado&quot;) a continuaci&oacute;n, las campanas de alarma deber&iacute;an empezar a sonar!<br />\r\n	<br />\r\n	Al mismo tiempo, sin embargo, usted tiene que darse cuenta de que en Colombia la responsabilidad recae en el hombre para llamar y hacer arreglos. Pero si haces un esfuerzo y sigue siendo amable entonces es probable la mejor manera de reducir sus p&eacute;rdidas.<br />\r\n	<br />\r\n	El Ritual Citas - Algunos consejos<br />\r\n	<br />\r\n	OK, as&iacute; que has conocido a una chica y ella ha accedido a salir con usted o vas a venir a Colombia para conocer a alguien que has conocido en l&iacute;nea. Hay algunas reglas generales a las que usted debe adherirse a fin de aumentar sus posibilidades de hacer una buena impresi&oacute;n.<br />\r\n	<br />\r\n	Colombia sigue siendo un pa&iacute;s muy conservador, y muchas chicas colombianas esperar&aacute; que usted pueda recogerla en su coche o en un taxi en su camino a un sitio (en especial las primeras citas), y lo mismo cuando se va a casa.<br />\r\n	<br />\r\n	Si la chica est&aacute; de acuerdo para cumplir all&iacute; aseg&uacute;rate de que est&aacute;s a tiempo. Si bien a menudo es aceptable para la chica para llegar 20 minutos tarde.<br />\r\n	<br />\r\n	Para la mayor&iacute;a de las mujeres en Colombia, sin embargo, ser &quot;detallista&quot; (atentos a los detalles) es decir, la apertura de puertas de autos para ella, etc es m&aacute;s importante que agasajar a ella en un restaurante de lujo. No es que me duele!<br />\r\n	<br />\r\n	La gente en Colombia se visten elegantemente cuando salen. Y la higiene es vista con una importancia a&uacute;n mayor de lo que es en Europa y Am&eacute;rica del Norte.<br />\r\n	<br />\r\n	Las chicas colombianas toman gran orgullo en su apariencia y, a menudo pasan una o dos horas prepar&aacute;ndose.<br />\r\n	<br />\r\n	Si eres un mochilero, trate de vestirse lo m&aacute;s posible y recuerda que en ciudades como Medell&iacute;n y Cali, donde el clima es perfecto para los pantalones cortos y sandalias, los locales rara vez los usan, especialmente por la noche.<br />\r\n	<br />\r\n	Cuando se trata de pagar la factura siempre se debe ofrecer a pagar. En realidad, hay muchas chicas colombianas que insisten en pagar o por lo menos hacer una contribuci&oacute;n, pero es siempre amable para ofrecer.<br />\r\n	<br />\r\n	El concepto de que el hombre debe pagar la factura puede ser a veces duro ya que en ocaciones&nbsp; ni siquiera recibe un simple &quot;Gracias&quot; al salir. Esto es algo que todav&iacute;a me molesta, pero como con muchas cosas que es un aspecto de la cultura tradicional colombiana que lo mejor es s&oacute;lo para tratar de aceptar.<br />\r\n	<br />\r\n	Mientras que algunas mujeres colombianas aceptar&aacute;n un beso en la primera cita, tradicionalmente la regla no escrita parece ser al menos la segunda, si no la tercera cita. Viniendo en demasiado fuerte en la primera cita podr&iacute;a explotar sus posibilidades por completo. Trate de leer sus se&ntilde;ales de lo mejor que pueda. Tocar la mano y luego juzgar su reacci&oacute;n puede ser una buena manera de probar el agua.<br />\r\n	<br />\r\n	Con todo, me parece que las chicas colombianas son menos estresante que salir con chicas en Gran Breta&ntilde;a. No hay tanto &eacute;nfasis en impresionar a la chica de sus carreras, estudios o sofisticaci&oacute;n.<br />\r\n	<br />\r\n	Simplemente siendo un buen tipo, caballeroso puede ser suficiente para que ella considere a la fecha un &eacute;xito.</p>\r\n', 1, '2016-09-06 23:50:46');
INSERT INTO `dateing_blogs` (`id`, `title`, `titletranslated`, `description`, `description_translated`, `user_id`, `post_date_time`) VALUES
(9, 'Foreigners looking for a Colombian wife', 'EXTRANJERO BUSCA SU ESPOSA COLOMBIAN', '<p>\r\n	<br />\r\n	<span lang="en"><span title="Las colombianas ademï¿½s de ser muy bonitas, son inteligentes, comprensivas y saben cï¿½mo llevar una relaciï¿½n en armonï¿½a por mucho tiempo , dice Luc Amon, un francï¿½s que llegï¿½ a Colombia hace 20 aï¿½os y encontrï¿½ aquï¿½ a la mujer de sus sueï¿½os.\r\n\r\n">Colombians, besides being very beautiful, are intelligent, comprehensive and know how to lead a long harmonious relationship, says Luc Amon, a Frenchman who came to Colombia 20 years ago and found the woman of his dreams here.</span><br />\r\n	<br />\r\n	<span title="Me casï¿½ en mi paï¿½s, pero el matrimonio no funcionï¿½ porque a las mujeres francesas el cuento de la liberaciï¿½n femenina les hizo olvidar tambiï¿½n el amor y otros valores.">I was first married in my country, but the marriage did not work because as Amon explains it French women are consumed with women&#39;s liberation which made her forget the love and other values in our relationship. </span><span title="Conocï¿½ espaï¿½olas, italianas, argentinas y todas resultaron poco sentimentales.">I have known Spanish, Italian and Argentine women and all were unsentimental. </span><span title="Diez aï¿½os despuï¿½s de viajar, conocï¿½ a una bogotana con la que llevo casi 20 aï¿½os de feliz matrimonio , relata Amon.\r\n\r\n">Ten years after I first started traveling, I met a girl from Bogota with whom I have now been happily married to for almost <span lang="en"><span title="Diez aï¿½os despuï¿½s de viajar, conocï¿½ a una bogotana con la que llevo casi 20 aï¿½os de feliz matrimonio , relata Amon.\r\n\r\n">20 years</span></span>, tells Amon.</span><br />\r\n	<br />\r\n	<span title="Segï¿½n este francï¿½s, Colombia es un paï¿½s lleno de problemas, pero tambiï¿½n tiene gente con mucha calidad humana que hace que de este un buen lugar para vivir y perfecto si se quiere formar una familia .\r\n\r\n">According to this French man, Colombia is a country that has some problems, but with people that have great human qualities which makes this a good place to live and perfect if you want to start a family.</span><br />\r\n	<br />\r\n	<span title="Como Luc Amon existen otros extranjeros que llegaron a Colombia buscando su media naranja y la encontraron.">Like Luc Amon there are many other foreigners who travel to Colombia or search online looking for and finding their better half. </span><span title="Segï¿½n las sitas de web como MiAmor.com.co,\r\n\r\n">According to websites such as MiAmor.com.co,</span><br />\r\n	<br />\r\n	<span title="Segï¿½n estadï¿½sticas de las agencias , ademï¿½s de los que vienen a buscar a la mujer de sus sueï¿½os, llegan semanalmente 40 cartas de extranjeros que quieren informaciï¿½n sobre las colombianas.">According to statistics from Colombian agencies, their are 40 requests weekly from foreigners who want to travel to Colombia to find the woman of their dreams. </span><span title="Despuï¿½s de recibir los datos solicitados, el 50 por ciento viene personalmente.">After receiving information from the agencies, 50 percent of those that requested information fly to Colombia personally. </span><span title="Otros, en menos proporciï¿½n, envï¿½an mensajes clasificados a los periï¿½dicos y revistas del corazï¿½n.\r\n\r\n">The others use sights such as www.miamor.com.co to find their perfect matches before traveling to Colombia.</span><br />\r\n	<br />\r\n	<br />\r\n	<span title="Fama internacional La fama de la mujer colombiana traspasa fronteras.">The growing fame of the Colombian woman around the world has made Colombia a very appealing destination for foreigners. </span><span title="Segï¿½n Nelly Campos, hace cinco aï¿½os solo venï¿½an dos o tres extranjeros al aï¿½o a buscar alguna colombiana, pero ahora ese nï¿½mero se multiplicï¿½ .\r\n\r\n">According to Nelly Campos, only five years ago there were maybe two or three foreigners a year coming to find their Colombian match, but now that number multiplied significantly.</span><br />\r\n	<br />\r\n	<span title="Segï¿½n algunos extranjeros que estuvieron de paso en Colombia para entablar amistad con su posible media naranja , se arriesgan a venir a este paï¿½s porque sus amigos que estï¿½n casados con paisas, caleï¿½as, costeï¿½as o bogotanas viven muy felices y tienen familias grandes y unidas.">According to some foreigners who were traveling through Colombia looking to potentially meet their better halves, they were coming here because they had friends who had married to girls from Medellin or from Cali, Bogota&nbsp; or from the coast and live very happy lives here in Colombia and have created large families together. </span><span title="Nosotros venimos a ver si tenemos la misma suerte , dicen.\r\n\r\n">We are coming to see if we will have the same fate, they say.</span><br />\r\n	<br />\r\n	<span title="Los extranjeros que vienen al paï¿½s llegan con un objetivo claro: entablar una relaciï¿½n seria y duradera.">Many foreigners who come to Colombia arrive with a clear objective: establish a serious and lasting relationship. </span><span title="Michael Petterson dice que yo vengo desde Reno porque quiero encontrar a esa mujer de la que hablan mis amigos, bella y cariï¿½osa.">Michael Petterson says I have travelled from Reno because I want to find that woman that my friends speak about, beautiful and loving. </span><span title="Yo enviï¿½ mis datos y me enviaron la foto de dos candidatas, ambas me gustaron voy a ver con quien hay quï¿½mica .\r\n\r\n">I met several girls on MiAmor and I&#39;m coming here to see with whom there is some chemistry.</span><br />\r\n	<br />\r\n	<span title="No sï¿½lo los hombres de otros paï¿½ses se interesan por conseguir esposa en Colombia.">Not only men from other countries are interested in getting wife in Colombia, but Colombian women </span><span title="Ellas los prefieren de otras latitudes.">prefer men from other latitudes. </span><span title="Segï¿½n Gloria Marï¿½a Mï¿½ndez , la mayorï¿½a de las mujeres que se afilian esperan encontrar a un hombre no machista ni celoso ni posesivo como el colombiano y especialmente que sea estadounidense, canadiense o australiano .\r\n\r\n">According to Gloria Maria Mendez, most women who join www.miamor.com.co do not want to find a sexist or jealous or possessive men like Colombian men and the prefer men such as those from US, Canada or Australian men.</span><br />\r\n	<br />\r\n	<span title="Mï¿½ndez agrega que las mujeres no solo buscan a un marido extranjero para mejorar el nivel de vida, sino una relaciï¿½n sentimental, sincera, tierna, cariï¿½osa y con los miles de adjetivos que ellas escriben en los avisos clasificados.">Mendez says women are not only seeking a foreign husband to improve living standards, but an honest, sincere, tender, loving caring man and many more things that they put in their MiAmor profile. </span><span title="La ventaja es que ellas y los extranjeros quieren familia, calor de hogar.">The advantage is that the foreigners and the Colombian women want family and a loving home. </span><span title="Eso es lo que mï¿½s les gusta de las colombianas .\r\n\r\n">That&#39;s what I like best about the Colombians.</span><br />\r\n	<br />\r\n	<span title="No en vano, durante los dos ï¿½ltimos meses del aï¿½o pasado se celebraron 46 matrimonios de argentinos, franceses, estadounidenses e italianos con colombianas.">Not surprisingly, during the last two months of 2014,&nbsp; 46 couples were married in Colombia, they were men from Argentina, France, America, Italy and many others countries marrying with Colombian women. </span><span title="La mayorï¿½a de las nuevas parejas se fueron para el lugar de origen del esposo, las otras aï¿½n permanecen en Colombia.\r\n\r\n">Most new couples moved to the country of origin of the husband,&nbsp; and the other are still in Colombia.</span><br />\r\n	<br />\r\n	<span title="Esa es la mentalidad de un extranjero y eso es lo que les agrada a las mujeres, a las cerca de quince mil que estï¿½n afiliadas a las agencias.">That is the mentality of a foreigner and that&#39;s what Colombian women like, their about fifteen thousand Colombians who are affiliated with different dating agencies. </span><span title="La culpa es de los propios colombianos que no son respetuosos, y ademï¿½s son unos rudos.\r\n\r\n">They blame it on the Colombian men themselves who are rude and not respectful to their women</span><br />\r\n	<br />\r\n	<span title="Mï¿½s que belleza No solo la belleza, la gracia y la simpatï¿½a de la mujer colombiana han inspirado a los nacionales.">More than just beauty , grace and sympathy of Colombian women have inspired nationalities. </span><span title="Tambiï¿½n se han convertido en la musa de inspiraciï¿½n de algunos poetas extranjeros.\r\n\r\n">They have also become the muse of inspiration from some foreign poets as well.</span><br />\r\n	<br />\r\n	<span title="Peter Mackenzee es un canadienses de 46 aï¿½os que llegï¿½ al paï¿½s hace uno con el fin de conocer la belleza latina, de la que tanto habï¿½a oï¿½do hablar.">Peter Mackenzee is a 46 Canadian who arrived in the country to understand the Latin beauty, which he had heard so much about. </span><span title="Cuando lleguï¿½ vi que ellas eran tan parecidas a las otras, que me desilusionï¿½.">When I arrived I saw that they were very similar to others, this made me become disillusioned. </span><span title="Sin embargo, al poco tiempo empecï¿½ a conocerlas.">But soon I began to really get to know them. </span><span title="Con los poemas que les he escrito pienso publicar un libro .\r\n\r\n">With the poems I wrote I am thinking of&nbsp; publishing a book.</span><br />\r\n	<br />\r\n	<span title="Despuï¿½s de que uno las conoce se da cuenta que ellas son mï¿½s que fama y belleza, tienen talento, son trabajadoras y saben sortear los problemas con mucha fuerza , agrega.\r\n\r\n">Once you get to know them, you realise that they are so much more than just beautiful , they have talent, are very hard workers and they are trying to overcome the problems of the country. </span><br />\r\n	<br />\r\n	<span title="Lleguï¿½ a Colombia buscando a una mujer de esas de pantalla, como Inï¿½s de Hinojosa, oa una Pocahontas, pero afortunadamente no encontrï¿½ a ninguna de esas.">I came to Colombia looking for a woman that I had seen on the screen, but fortunately I did not meet any of those. </span><span title="A cambio, conocï¿½ a la mujer campesina ya aquella trabajadora que lucha por sacar adelante su familia , cuenta Mackenzee Segï¿½n ï¿½l, las colombianas son mï¿½s aterrizadas y es la clase de mujer con la que uno se quiere casar, la venezolana es para aventuras, la">In return, I met some rural women who worked hard and struggled to bring up their families, said Mackenzee According to him, the Colombians are more grounded and are the kind of woman you want to marry, Venezuelan&#39;s are for adventure, </span><span title="mexicana es linda, pero muy reprimida.">Mexicans are cute, but very repressed. </span><span title="La mujer colombiana sabe concretar las relaciones y las lleva por muy buen camino .\r\n\r\n">Colombian woman knows how to make a good relationship and have the keys for happy journey ahead.</span><br />\r\n	<br />\r\n	<span title="Segï¿½n Mackenzee, he conocido muchos solterones empedernidos que apenas llegan a Colombia quieren quedarse y casarse.">According Mackenzee, I have met many singles who have come to Colombia to stay and get married. </span><span title="Ahora estoy convencido que lo mejor que tiene este paï¿½s son los seres humanos .">I am now convinced that the best thing about this country are the human beings.</span></span></p>\r\n', '<div class="_5pbx userContent" data-ft="{">\r\n	<p>\r\n		<br />\r\n		Las colombianas adem&aacute;s de ser muy bonitas, son inteligentes, comprensivas y saben c&oacute;mo llevar una relaci&oacute;n en armon&iacute;a por mucho tiempo , dice Luc Amon, un franc&eacute;s que lleg&oacute; a Colombia hace 20 a&ntilde;os y encontr&oacute; aqu&iacute; a la mujer de sus sue&ntilde;os.</p>\r\n	<p>\r\n		Me cas&eacute; en mi pa&iacute;s, pero el matrimonio no funcion&oacute; porque a las mujeres francesas el cuento de la liberaci&oacute;n femenina les hizo olvidar tambi&eacute;n el amor y otros valores. Conoc&iacute; espa&ntilde;olas, italianas, argentinas y todas resultaron poco sentimentales. Diez a&ntilde;os despu&eacute;s de viajar, conoc&iacute; a una bogotana con la que llevo casi 20 a&ntilde;os de feliz matrimonio , relata Amon.</p>\r\n	<p>\r\n		Seg&uacute;n este franc&eacute;s, Colombia es un pa&iacute;s lleno de problemas, pero tambi&eacute;n tiene gente con mucha calidad humana que hace que de este un buen lugar para vivir y perfecto si se quiere formar una familia .</p>\r\n	<p>\r\n		Como Luc Amon existen otros extranjeros que llegaron a Colombia buscando su media naranja y la encontraron. Seg&uacute;n las sitas de web como MiAmor.com.co,</p>\r\n	<p>\r\n		<span id="result_box" lang="es"><span class="hps">Seg&uacute;n las estad&iacute;sticas de</span> <span class="hps">las agencias</span> <span class="hps">colombianas</span><span>,</span> <span class="hps">su son</span> <span class="hps">40</span> <span class="hps">las solicitudes</span> <span class="hps">semanales</span> <span class="hps">de los extranjeros</span> <span class="hps">que quieren viajar</span> <span class="hps">a Colombia para</span> <span class="hps">encontrar a la mujer</span> <span class="hps">de sus sue&ntilde;os.</span> <span class="hps">Despu&eacute;s de recibir</span> <span class="hps">la informaci&oacute;n de las</span> <span class="hps">agencias</span><span>, el 50</span> <span class="hps">por ciento de</span> <span class="hps">los que</span> <span class="hps">solicit&oacute; informaci&oacute;n</span> <span class="hps">vuela</span> <span class="hps">a Colombia</span> <span class="hps">personalmente</span><span>.</span> <span class="hps">Los</span> <span class="hps">otros utilizan</span> <span class="hps">puntos de inter&eacute;s como</span> <span class="hps">www.miamor.com.co</span> <span class="hps">encontrar</span> <span class="hps">sus</span> <span class="hps">partidos</span> <span class="hps">perfectos</span> <span class="hps">antes de viajar a</span> <span class="hps">Colombia</span><span>.</span></span></p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Fama internacional La fama de la mujer colombiana traspasa fronteras. Seg&uacute;n Nelly Campos, hace cinco a&ntilde;os solo ven&iacute;an dos o tres extranjeros al a&ntilde;o a buscar alguna colombiana, pero ahora ese n&uacute;mero se multiplic&oacute; .</p>\r\n	<p>\r\n		Seg&uacute;n algunos extranjeros que estuvieron de paso en Colombia para entablar amistad con su posible media naranja , se arriesgan a venir a este pa&iacute;s porque sus amigos que est&aacute;n casados con paisas, cale&ntilde;as, coste&ntilde;as o bogotanas viven muy felices y tienen familias grandes y unidas. Nosotros venimos a ver si tenemos la misma suerte , dicen.</p>\r\n	<p>\r\n		Los extranjeros que vienen al pa&iacute;s llegan con un objetivo claro: entablar una relaci&oacute;n seria y duradera. Michael Petterson dice que yo vengo desde Reno porque quiero encontrar a esa mujer de la que hablan mis amigos, bella y cari&ntilde;osa. Yo envi&eacute; mis datos y me enviaron la foto de dos candidatas, ambas me gustaron voy a ver con quien hay qu&iacute;mica .</p>\r\n	<p>\r\n		No s&oacute;lo los hombres de otros pa&iacute;ses se interesan por conseguir esposa en Colombia. Ellas los prefieren de otras latitudes. Seg&uacute;n Gloria Mar&iacute;a M&eacute;ndez , la mayor&iacute;a de las mujeres que se afilian esperan encontrar a un hombre no machista ni celoso ni posesivo como el colombiano y especialmente que sea estadounidense, canadiense o australiano .</p>\r\n	<p>\r\n		M&eacute;ndez agrega que las mujeres no solo buscan a un marido extranjero para mejorar el nivel de vida, sino una relaci&oacute;n sentimental, sincera, tierna, cari&ntilde;osa y con los miles de adjetivos que ellas escriben en MiAmor.com.co . La ventaja es que ellas y los extranjeros quieren familia, calor de hogar. Eso es lo que m&aacute;s les gusta de las colombianas .</p>\r\n	<p>\r\n		No en vano, durante los dos &uacute;ltimos meses del a&ntilde;o pasado se celebraron 46 matrimonios de argentinos, franceses, estadounidenses e italianos con colombianas. La mayor&iacute;a de las nuevas parejas se fueron para el lugar de origen del esposo, las otras a&uacute;n permanecen en Colombia.</p>\r\n	<p>\r\n		Esa es la mentalidad de un extranjero y eso es lo que les agrada a las mujeres, a las cerca de quince mil que est&aacute;n afiliadas a las agencias. La culpa es de los propios colombianos que no son respetuosos, y adem&aacute;s son unos rudos.</p>\r\n	<p>\r\n		M&aacute;s que belleza No solo la belleza, la gracia y la simpat&iacute;a de la mujer colombiana han inspirado a los nacionales. Tambi&eacute;n se han convertido en la musa de inspiraci&oacute;n de algunos poetas extranjeros.</p>\r\n	<p>\r\n		Peter Mackenzee es un canadienses de 46 a&ntilde;os que lleg&oacute; al pa&iacute;s hace uno con el fin de conocer la belleza latina, de la que tanto hab&iacute;a o&iacute;do hablar. Cuando llegu&eacute; vi que ellas eran tan parecidas a las otras, que me desilusion&eacute;. Sin embargo, al poco tiempo empec&eacute; a conocerlas. Con los poemas que les he escrito pienso publicar un libro .</p>\r\n	<p>\r\n		Despu&eacute;s de que uno las conoce se da cuenta que ellas son m&aacute;s que fama y belleza, tienen talento, son trabajadoras y saben sortear los problemas con mucha fuerza.</p>\r\n	<p>\r\n		Llegu&eacute; a Colombia buscando a una mujer de esas de pantalla, como In&eacute;s de Hinojosa, o a una Pocahontas, pero afortunadamente no encontr&eacute; a ninguna de esas. A cambio, conoc&iacute; a la mujer campesina y a aquella trabajadora que lucha por sacar adelante su familia , cuenta Mackenzee Seg&uacute;n &eacute;l, las colombianas son m&aacute;s aterrizadas y es la clase de mujer con la que uno se quiere casar, la venezolana es para aventuras, la mexicana es linda, pero muy reprimida. La mujer colombiana sabe concretar las relaciones y las lleva por muy buen camino .</p>\r\n	<p>\r\n		Seg&uacute;n Mackenzee, he conocido muchos solterones empedernidos que apenas llegan a Colombia quieren quedarse y casarse. Ahora estoy convencido que lo mejor que tiene este pa&iacute;s son los seres humanos .</p>\r\n</div>\r\n', 1, '2016-09-16 22:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_bodytype`
--

CREATE TABLE IF NOT EXISTS `dateing_bodytype` (
  `id` int(11) NOT NULL,
  `body_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_booking`
--

CREATE TABLE IF NOT EXISTS `dateing_booking` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `price` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_booking`
--

INSERT INTO `dateing_booking` (`id`, `title`, `description`, `price`) VALUES
(1, 'abarth', 'abarth services', '233');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_carmakers`
--

CREATE TABLE IF NOT EXISTS `dateing_carmakers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_carmakers`
--

INSERT INTO `dateing_carmakers` (`id`, `user_id`, `name`, `description`, `image`, `status`) VALUES
(1, 0, 'Toyota', 'Toyota', 'cc1.jpg', 0),
(2, 0, 'Nisan', 'Nisan', 'cc2.jpg', 0),
(3, 0, 'Honda', 'Honda', 'cc3.jpg', 0),
(4, 0, 'Mitsubisi', 'Mitsubisi', 'cc6.jpg', 0),
(5, 0, 'Volkswagen', 'Volkswagen', 'cc8.jpg', 0),
(6, 0, 'BMW', 'BMW', 'cc5.jpg', 0),
(7, 0, 'Suzuki', 'Suzuki', 'cc9.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_carmodels`
--

CREATE TABLE IF NOT EXISTS `dateing_carmodels` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_carmodels`
--

INSERT INTO `dateing_carmodels` (`id`, `cat_id`, `name`, `description`, `image`, `status`) VALUES
(8, 2, 'Micra', 'Micra', '', 0),
(9, 7, 'Swift LXI', 'Swift LXI', '', 0),
(10, 7, 'Swift VXI', 'Swift VXI', '', 0),
(11, 7, 'Swift Desire', 'Swift Desire', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_car_moreimage`
--

CREATE TABLE IF NOT EXISTS `dateing_car_moreimage` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `pro_id` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_car_moreimage`
--

INSERT INTO `dateing_car_moreimage` (`id`, `title`, `pro_id`, `image`) VALUES
(1, 'dfgdgdfg', '1', '1407705466listing-2.jpg'),
(2, 'Best property', '1', '1407705545listing-1.jpg'),
(3, 'bmw', '2', '1407744491bmw-x6-red-car-awesome-luxury-motion-road-trees-visualrich.jpg'),
(4, 'bmw', '2', '1407744534bmw-x6-red-car-awesome-luxury-motion-road-trees-visualrich.jpg'),
(5, 'bmw', '3', '1407744754bmw-2-series-active-tourer-m-sport-angle-interior.jpg'),
(6, 'bmw', '3', '1407744840bmw-2-series-interior-2.jpg'),
(7, 'liva', '8', '1407744944toyota_etios_liva_trd-16.jpg'),
(8, 'liva', '8', '14077449773509238607_62603f302c_thumb.jpg'),
(9, 'innova', '9', '1407745055toyota-innova-interior.jpg'),
(10, 'innova', '9', '1407745096Toyota-Innova-interior1.jpg'),
(11, 'Nissan', '10', '1407745223nissan-pathfinder-concept-interior.jpg'),
(12, 'Nissan', '10', '14077453722014_nissan_rogue_interior_pictures.jpg'),
(13, 'Nissan', '11', '1407745492Nissan-Evalia-210.jpg'),
(14, 'Nissan', '11', '1407745531nissan_evalia-int.jpg'),
(15, 'Suzuki Car', '12', '1407745627Suzuki_Ionis_concept_car_interior.jpg'),
(16, 'Suzuki Car', '12', '1407745658suzuki_sxbox_interior_00.jpg'),
(17, 'Suzuki Car', '13', '1407745720suzuki-swift-interior.jpg'),
(18, 'Suzuki Car', '13', '1407745764New Swift interior.JPG'),
(19, 'Honda', '14', '1407745845Honda-City-Interior2.jpg'),
(20, 'Honda', '14', '1407745880HONDA-CITY-CABIN.jpg'),
(21, 'Honda Suv', '15', '1407745958112_0806_20z+2008_honda_crv+interior_view.jpg'),
(22, 'Honda Suv', '15', '14077459842008-Honda-CR-V-EX-L-i05.jpg'),
(23, 'Mitsubishi', '16', '14077460552007-mitsubishi-outlander12.jpg'),
(24, 'Mitsubishi', '16', '1407746107mitsubishi-outlander-3.jpg'),
(25, 'Mitsubishi', '17', '1407746221mitsubishi-prototype-i-miev-interior-photo-372902-s-1280x782.jpg'),
(26, 'Mitsubishi', '17', '1407746280Mitsubishi-Outlander-Interior.jpg'),
(27, 'The New Polo', '18', '1407746362New-Polo-Interior-1.jpg'),
(28, 'The New Polo', '18', '1407746397volkswagen-polo-mkv-2009-official-interior-img_17.jpg'),
(29, 'Passat', '19', '14077464702012-volkswagen-passat-tdi-interior-1.jpg'),
(30, 'Passat', '19', '14077465212013_volkswagen_passat-pic-4969656232301200496.png'),
(41, 'bmw', '2', '1409836421-Chrysanthemum.jpg'),
(42, 'bmw', '2', '1409836421-Desert.jpg'),
(43, 'bmw', '2', '1409836421-Koala.jpg'),
(44, 'bmw', '2', '1409836421-Lighthouse.jpg'),
(49, '', '36', '1410324786Apple_Friends.gif'),
(50, '', '36', '1410324786kindapr4.jpg'),
(51, '', '36', '1410324786KindergartenNewsHeader.jpg'),
(52, '', '36', '1410324786news23.gif'),
(53, '', '37', '1410327261Hydrangeas.jpg'),
(54, '', '37', '1410327261Jellyfish.jpg'),
(55, '', '37', '1410327261Tulips.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_category`
--

CREATE TABLE IF NOT EXISTS `dateing_category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_category`
--

INSERT INTO `dateing_category` (`id`, `user_id`, `name`, `description`, `image`, `status`) VALUES
(5, 0, 'Health & Beauty', 'Health & Beauty  Description goes here', 'item_3.png', 1),
(8, 0, 'Home Services', 'Home Services Description goes here', 'item_5.png', 0),
(26, 0, 'Lorem ipsum dolor sit amet consectetur', 'Lorem ipsum dolor sit amet dolore magna aliqua', 'img2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_cms`
--

CREATE TABLE IF NOT EXISTS `dateing_cms` (
  `id` int(11) NOT NULL,
  `pagename` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `pagedetail` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms';

--
-- Dumping data for table `dateing_cms`
--

INSERT INTO `dateing_cms` (`id`, `pagename`, `title`, `title_translated`, `pagedetail`, `description_translated`, `image`) VALUES
(1, 'About Us', 'About Us', 'sobre Nosotros', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor was created to provide a service to enable foreigners to connect with Colombians. It was created by American Dean Moriaty who fell in love with a Colombian. &quot; a friend and I were venturing through Colombia, speaking little if any Spanish and I met my wife in a lotto shop in Cali, I know that&nbsp; it was a lotto shop now but at the time I thought it was a money exchange spot. After trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventually started talking through google translate&quot; the rest is history and 8 years on Dean and Cindy are happily married. Dean has sung the praises of Colombian women to all that will listen. &quot; I wanted to help others that may not have the opportunity to travel to Colombia yet or who wanted to make connections first before coming here to this beautiful country&quot;.</span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Dean has been working on MiAmor for a long time, he and Cindy have tried to create a service that will really help connect foreigners with Colombians, It is the only such website that is actually based in Colombia, for Colombians and those that want to meet Colombians.<br />\r\n	<br />\r\n	&quot;I wanted to create a very simple to use service, that included lots of ways to connect and show interest in the person of interest, you can flirt, befriend, message, chat, translation chat or video chat, as well as you can search singles on the go with the MiAmor App&quot;<br />\r\n	<br />\r\n	MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.<br />\r\n	<br />\r\n	MiAmor is Colombia and Colombia is MiAmor</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span title="MiAmor was created to provide a service to enable foreigners to connect with Colombians.">MiAmor fue creado para proporcionar un servicio que permitira que los extranjeros se conecten con los colombianos. </span><span title="It was created by American Dean Moriarty who fell in love with a Colombian.">Fue creado por el estadounidense Dean Moriaty, que se enamor&oacute; de una colombiana. </span><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span title="Dean has sung the praises of Colombian women to all that will listen.">Dean ha cantado las alabanzas de las mujeres colombianas a todos los que quieran escuchar. </span><span .="" beautiful="" before="" colombia="" coming="" connections="" first="" have="" help="" here="" i="" make="" may="" not="" opportunity="" or="" others="" that="" the="" this="" title="" to="" travel="" wanted="" who="" yet="">&quot;Yo quer&iacute;a ayudar a otros que aun no han tenido la oportunidad de viajar a Colombia o que quieren hacer conexiones primero antes de venir aqu&iacute; a este hermoso pa&iacute;s&quot;.</span></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span title="Dean has been working on MiAmor for a long time, he and Cindy have tryed to create a service that will really help connect foreigners with Colombians, It is the only such website that is actualy based in Colombia, for Colombians and those that want to meet">Dean ha estado trabajando en miAmor durante mucho tiempo, &eacute;l y Cindy estan tratando de crear un servicio que realmente ayude a conectar a los extranjeros con los colombianos, es el &uacute;nico sitio web que existe actualmente en Colombia, para las colombianas y los que quieren conocer </span><span title="Colombians.\r\n\r\n">colombianos.</span><br />\r\n	<br />\r\n	<br />\r\n	<br />\r\n	<span a="" and="" as="" buscar="" can="" chat="" como="" conectar="" connect="" crear="" create="" de="" el="" en="" i="" in="" included="" interest="" la="" lots="" maneras="" mostrar="" muy="" n="" o="" of="" or="" person="" persona="" puede="" que="" s="" se="" sencillo="" show="" simple="" span="" that="" the="" title="" to="" translation="" un="" use="" usted="" utilizar="" very="" video="" wanted="" ways="" well="" y="" you=""><span app="" con="" individuales="" la="" marcha="" miamor="" sobre="" span="" title="singles on the go with the MiAmor App"> <span title="MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.\r\n\r\n">MiAmor tiene una enorme base de datos de mujeres solteras colombianos que buscan conocer hombres extranjeros y una base de datos cada vez mayor de extranjeros que quieren encontrar a alguien especial en Colombia.</span></span></span></span></span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span a="" amigo="" and="" at="" aventurarse="" bamos="" but="" cambio="" change="" de="" dinero.="" en="" era="" ese="" esposa="" friend="" hablando="" hay="" i="" if="" in="" it="" know="" little="" lotto="" lugar="" met="" mi="" momento="" money="" my="" no="" now="" oles="" pero="" poco="" que="" s="" shop="" si="" span="" spanish="" speaking="" spot.="" that="" the="" thought="" through="" tienda="" time="" title="" un="" una="" venturing="" was="" were="" wife="" y="" yo=""><span 8="" a="" americanos="" and="" are="" cambiar="" casados.="" chica="" cindy="" de="" dean="" diagramas="" dibujando="" el="" empezamos="" en="" es="" esta="" eventualy="" felizmente="" google="" hablar="" happily="" historia="" history="" is="" jugando="" lares="" llegar="" mica="" mis="" n="" nos="" on="" os="" pasos="" pobre="" que="" rest="" resto="" sobre="" span="" the="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate" traductor="" tratando="" una="" y="" years=""><span a="" and="" as="" buscar="" can="" chat="" como="" conectar="" connect="" crear="" create="" de="" el="" en="" i="" in="" included="" interest="" la="" lots="" maneras="" mostrar="" muy="" n="" o="" of="" or="" person="" persona="" puede="" que="" s="" se="" sencillo="" show="" simple="" span="" that="" the="" title="" to="" translation="" un="" use="" usted="" utilizar="" very="" video="" wanted="" ways="" well="" y="" you=""><span app="" con="" individuales="" la="" marcha="" miamor="" sobre="" span="" title="singles on the go with the MiAmor App"><span title="MiAmor is Colombia and Colombia is MiAmor">MiAmor es Colombia y Colombia es MiAmor</span></span></span></span></span></span></p>\r\n', ''),
(5, 'Terms', 'Terms And Condition', 'Términos y Condiciones', '<p>\r\n	Terms And Condition</p>\r\n', '<p>\r\n	<span class="short_text" id="result_box" lang="es" tabindex="-1"><span class="hps">T&eacute;rminos y</span> <span class="hps">Condiciones</span></span></p>\r\n', ''),
(6, 'Privacy Policy', 'Privacy Policy', 'política de privacidad', '<p>\r\n	This Privacy Policy (&quot;Policy&quot;) explains how information about you is collected, used and disclosed by MiAmor, Inc. and its subsidiaries and affiliates (&quot;MiAmor,&quot; &quot;we&quot; or &quot;us&quot;). This Policy applies to all of the websites (including www.MiAmor.com.co), products, applications (including mobile applications and applications and pages operated by MiAmor and available on social networking sites and other platforms and our downloadable products) and services offered by MiAmor wherever this Policy is displayed (collectively, the &quot;Services&quot;).</p>\r\n<p>\r\n	Please note that we are based in the Republic of Colombia. As such, any personal information you provide us with or is collected by us as detailed below is transferred to and processed by us in Colombia (or other countries). By registering or subscribing to the Services, you agree to this transfer and processing.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>1.&nbsp;&nbsp;&nbsp; </strong><strong>INFORMATION WE COLLECT</strong></p>\r\n<p style="margin-left:72.0pt;">\r\n	1.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Information You Provide.</strong>When you register or subscribe to our Services, we may collect a variety of information, including your name, birth date, photographs, email address, password, phone number, including mobile phone number, billing information, credit card information and other contact or demographic information you provide. We also collect other profile data such as your personal interests and background, gender, age, geographical location and physical characteristics and any content or other communications you provide or make in connection with your use of the Services. We may also obtain information from other sources and combine that with information we collect through our Services.</p>\r\n<p style="margin-left:72.0pt;">\r\n	2.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Information Automatically Collected.</strong>When you access our Services, our servers automatically record information that your browser sends whenever you visit a website, such as your IP address, browser type, Internet service provider, platform type, the site from which you came and the site to which you are going when you leave our website, date and time stamp and one or more cookies that may uniquely identify your browser or your account. When you access our Services using a mobile device, we may receive or collect your mobile device ID (often referred to as UDID), mobile carrier, device manufacturer and phone number. We also collect information about your usage of, and activity on, our Services. We may associate information we automatically collect with your personal information. We and service providers acting on our behalf, such as Google Analytics, use this information to understand and analyze trends, to administer our Services, to gather demographic information about our user base and to deliver you a more personalized experience.</p>\r\n<p style="margin-left:72.0pt;">\r\n	3.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Cookies and Other Technologies.</strong>Our Services, email messages and advertisements may use &quot;cookies&quot; and other technologies such as pixel tags and Web beacons. &quot;Cookies&quot; are small data files stored on your hard drive by a website. Among other things, cookies help us improve our Services and your experience. We use cookies to see which areas and features are popular and to count visits to our websites. We may also use cookies and other technologies to serve advertisements to you on our Services or on sites other than our websites. Web beacons and pixel tags are electronic images that may be used on our Services or in our emails. We use Web beacons and pixel tags to deliver cookies, count visits, understand usage and campaign effectiveness, prevent fraud and to tell if an email or advertisement has been opened and acted upon.</p>\r\n<p style="margin-left:72.0pt;">\r\n	4.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Third Party Advertisers.</strong>Any advertisers and advertising networks (which include third party ad servers, ad agencies, ad technology vendors and research firms) that serve ads or provide offers to our users, on our Services or on sites other than our websites, may also use their own cookies, web beacons, pixels and other tracking technologies. Such tracking technologies are governed by the privacy policies of the entities placing the ads, and are not subject to this Policy. You can visit networkadvertising.org [hyperlink] to see a list of advertising networks that typically serve these kinds of ads and to exercise your choice for those advertising networks. We do not control these advertisers or advertising networks or any other third parties whose websites may be linked from our Services or their practices. We recommend that your review the privacy statements of all third-party websites you visit.</p>\r\n<p style="margin-left:72.0pt;">\r\n	5.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Social Networking Sites.</strong>When you use our Services through a social networking site (a &quot;Social Networking Site&quot;), such as Facebook, where our Services are also referred to as an &quot;application&quot;, you permit MiAmor to access certain information from your profile for that site. The information you permit MiAmor to access varies by Social Networking Site and is affected by the privacy settings you establish at such site. By using our Services through a Social Networking Site, you are authorizing MiAmor to collect, store, retain and use indefinitely, in accordance with this Privacy Policy, any and all information that you agreed the Social Networking Site could provide to MiAmor through the Social Networking Site application programming interface. Your agreement takes place when you &quot;accept&quot; or &quot;allow&quot; (or other similar terms) our application on a Social Networking Site.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>2.&nbsp;&nbsp;&nbsp; </strong><strong>USE OF PERSONAL INFORMATION WE COLLECT</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	We use the personal information we collect for purposes described in this Policy or disclosed to you on or in connection with our Services. Ways we may use information about you include:</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to operate, provide, maintain, develop, deliver, protect and improve our Services, products, applications, content and advertising and develop new products and services;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to maintain and administer your MiAmor account;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to maintain and display your MiAmor profile;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to respond to your comments and questions and provide customer service;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to detect and prevent abusive, fraudulent, malicious or potentially illegal activities, and to protect the rights, safety or property of MiAmor or our users;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to enforce our Terms of Use Agreement;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to keep you posted on our latest product or service announcements, software updates, security alerts and any technical notices;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to communicate with you about new contests, promotions and rewards, upcoming events and other news about products and services offered by MiAmor and our selected partners;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to provide you with confirmation of purchases, invoices, support and administrative messages or notice about changes to our terms, conditions or policies;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to administer any contests or promotions;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to perform other functions as otherwise described to you at the time of collection; and</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to link or combine information about you with other information we get from third parties to help understand your needs and provide you with better service.</p>\r\n<p style="margin-left:36.0pt;">\r\n	MiAmor may store and process personal information in the United States and other countries.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>3.&nbsp;&nbsp;&nbsp; </strong><strong>SHARING PERSONAL INFORMATION WITH THIRD PARTIES</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	Our Services allow you to share information about yourself with other individuals and other companies. Consider your own privacy and personal safety when sharing your information with anyone. When sharing information about others, please consider their safety and privacy and get their consent for that sharing.</p>\r\n<p style="margin-left:36.0pt;">\r\n	We do not share your personal information with third parties other than as follows:</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; with your consent; for example, when you agree to our sharing your information with other third parties for their own marketing purposes subject to their separate privacy policies;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to (i) satisfy any applicable law, regulation, subpoena, legal process or other government request, (ii) enforce our Terms of Use Agreement, including the investigation of potential violations thereof, (iii)&nbsp;investigate and defend ourselves against any third party claims or allegations, (iv) protect against harm to the rights, property or safety of MiAmor, its users or the public as required or permitted by law and (v) detect, prevent or otherwise address fraud, security or technical issues;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; in connection with any merger, sale of company assets, reorganization, financing, change of control or acquisition of all or a portion of our business by another company or third party or in the event of bankruptcy; and</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; with third party vendors, consultants and other service providers that perform services on our behalf, in order to carry out their work for us.</p>\r\n<p style="margin-left:36.0pt;">\r\n	We may also share information with others in an aggregated form that does not directly identify you.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>4.&nbsp;&nbsp;&nbsp; </strong><strong>CHOICES</strong></p>\r\n<p style="margin-left:72.0pt;">\r\n	0.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Communications from MiAmor.</strong>You can opt out of receiving promotional messages from us such as emails, text messages and telephone calls by visiting the &quot;settings&quot; page of your account. If you opt out of our promotional communications, we may still send you non-promotional messages about your account or our ongoing business relations.</p>\r\n<p style="margin-left:72.0pt;">\r\n	1.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Cookies.</strong>You can generally remove or block most cookies using the settings in your browser. In some cases, however, our third party vendors may employ &quot;Flash&quot; cookies to help us detect fraudulent or abusive uses of the Services and we or our third party advertisers may also use these cookies. Flash cookies are managed through a different interface than your web browser and you can access your Flash cookie settings <a href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html" target="_blank">here</a>. If you choose to disable cookies, however, it may impact your ability to use our Services.</p>\r\n<p style="margin-left:72.0pt;">\r\n	2.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Advertising.</strong>You may receive selected advertising on some of the MiAmor pages, all advertisers must be prior approved and all advertising is inline with MiAmor&rsquo;s policies and advertising criteria.</p>\r\n<p style="margin-left:72.0pt;">\r\n	3.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Access to Your Information.</strong>You may access and change your contact preferences and your information by visiting the &quot;Profile&quot; and &quot;settings&quot; sections of your account or by writing to us at the address provided below.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>5.&nbsp;&nbsp;&nbsp; </strong><strong>SECURITY</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	MiAmor takes reasonable measures to help protect your personal information in an effort to prevent loss, misuse and unauthorized access, disclosure, alteration and destruction.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>6.&nbsp;&nbsp;&nbsp; </strong><strong>UPDATES TO THIS POLICY</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	We reserve the right to modify this Policy from time to time. If we make any changes to this Policy, we will change the &quot;Last Revision&quot; date below and will post the updated Policy on this page.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>7.&nbsp;&nbsp;&nbsp; </strong><strong>CONTACTING MIAMOR</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	If you have questions about this Policy, please contact us at admin@MiAmor.com.co or by writing to us at: MiAmor, Inc., MiAmor Privacy, Carrera 77 #43-43 Int 203, Medellin, Colombia</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>8.&nbsp;&nbsp;&nbsp; </strong><strong>LAST REVISION DATE</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	This Policy was last revised on September 21 2015</p>\r\n', '<p>\r\n	This Privacy Policy (&quot;Policy&quot;) explains how information about you is collected, used and disclosed by MiAmor, Inc. and its subsidiaries and affiliates (&quot;MiAmor,&quot; &quot;we&quot; or &quot;us&quot;). This Policy applies to all of the websites (including www.MiAmor.com.co), products, applications (including mobile applications and applications and pages operated by MiAmor and available on social networking sites and other platforms and our downloadable products) and services offered by MiAmor wherever this Policy is displayed (collectively, the &quot;Services&quot;).</p>\r\n<p>\r\n	Please note that we are based in the Republic of Colombia. As such, any personal information you provide us with or is collected by us as detailed below is transferred to and processed by us in Colombia (or other countries). By registering or subscribing to the Services, you agree to this transfer and processing.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>1.&nbsp;&nbsp;&nbsp; </strong><strong>INFORMATION WE COLLECT</strong></p>\r\n<p style="margin-left:72.0pt;">\r\n	1.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Information You Provide.</strong>When you register or subscribe to our Services, we may collect a variety of information, including your name, birth date, photographs, email address, password, phone number, including mobile phone number, billing information, credit card information and other contact or demographic information you provide. We also collect other profile data such as your personal interests and background, gender, age, geographical location and physical characteristics and any content or other communications you provide or make in connection with your use of the Services. We may also obtain information from other sources and combine that with information we collect through our Services.</p>\r\n<p style="margin-left:72.0pt;">\r\n	2.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Information Automatically Collected.</strong>When you access our Services, our servers automatically record information that your browser sends whenever you visit a website, such as your IP address, browser type, Internet service provider, platform type, the site from which you came and the site to which you are going when you leave our website, date and time stamp and one or more cookies that may uniquely identify your browser or your account. When you access our Services using a mobile device, we may receive or collect your mobile device ID (often referred to as UDID), mobile carrier, device manufacturer and phone number. We also collect information about your usage of, and activity on, our Services. We may associate information we automatically collect with your personal information. We and service providers acting on our behalf, such as Google Analytics, use this information to understand and analyze trends, to administer our Services, to gather demographic information about our user base and to deliver you a more personalized experience.</p>\r\n<p style="margin-left:72.0pt;">\r\n	3.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Cookies and Other Technologies.</strong>Our Services, email messages and advertisements may use &quot;cookies&quot; and other technologies such as pixel tags and Web beacons. &quot;Cookies&quot; are small data files stored on your hard drive by a website. Among other things, cookies help us improve our Services and your experience. We use cookies to see which areas and features are popular and to count visits to our websites. We may also use cookies and other technologies to serve advertisements to you on our Services or on sites other than our websites. Web beacons and pixel tags are electronic images that may be used on our Services or in our emails. We use Web beacons and pixel tags to deliver cookies, count visits, understand usage and campaign effectiveness, prevent fraud and to tell if an email or advertisement has been opened and acted upon.</p>\r\n<p style="margin-left:72.0pt;">\r\n	4.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Third Party Advertisers.</strong>Any advertisers and advertising networks (which include third party ad servers, ad agencies, ad technology vendors and research firms) that serve ads or provide offers to our users, on our Services or on sites other than our websites, may also use their own cookies, web beacons, pixels and other tracking technologies. Such tracking technologies are governed by the privacy policies of the entities placing the ads, and are not subject to this Policy. You can visit networkadvertising.org [hyperlink] to see a list of advertising networks that typically serve these kinds of ads and to exercise your choice for those advertising networks. We do not control these advertisers or advertising networks or any other third parties whose websites may be linked from our Services or their practices. We recommend that your review the privacy statements of all third-party websites you visit.</p>\r\n<p style="margin-left:72.0pt;">\r\n	5.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Social Networking Sites.</strong>When you use our Services through a social networking site (a &quot;Social Networking Site&quot;), such as Facebook, where our Services are also referred to as an &quot;application&quot;, you permit MiAmor to access certain information from your profile for that site. The information you permit MiAmor to access varies by Social Networking Site and is affected by the privacy settings you establish at such site. By using our Services through a Social Networking Site, you are authorizing MiAmor to collect, store, retain and use indefinitely, in accordance with this Privacy Policy, any and all information that you agreed the Social Networking Site could provide to MiAmor through the Social Networking Site application programming interface. Your agreement takes place when you &quot;accept&quot; or &quot;allow&quot; (or other similar terms) our application on a Social Networking Site.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>2.&nbsp;&nbsp;&nbsp; </strong><strong>USE OF PERSONAL INFORMATION WE COLLECT</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	We use the personal information we collect for purposes described in this Policy or disclosed to you on or in connection with our Services. Ways we may use information about you include:</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to operate, provide, maintain, develop, deliver, protect and improve our Services, products, applications, content and advertising and develop new products and services;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to maintain and administer your MiAmor account;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to maintain and display your MiAmor profile;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to respond to your comments and questions and provide customer service;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to detect and prevent abusive, fraudulent, malicious or potentially illegal activities, and to protect the rights, safety or property of MiAmor or our users;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to enforce our Terms of Use Agreement;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to keep you posted on our latest product or service announcements, software updates, security alerts and any technical notices;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to communicate with you about new contests, promotions and rewards, upcoming events and other news about products and services offered by MiAmor and our selected partners;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to provide you with confirmation of purchases, invoices, support and administrative messages or notice about changes to our terms, conditions or policies;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to administer any contests or promotions;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to perform other functions as otherwise described to you at the time of collection; and</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to link or combine information about you with other information we get from third parties to help understand your needs and provide you with better service.</p>\r\n<p style="margin-left:36.0pt;">\r\n	MiAmor may store and process personal information in the United States and other countries.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>3.&nbsp;&nbsp;&nbsp; </strong><strong>SHARING PERSONAL INFORMATION WITH THIRD PARTIES</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	Our Services allow you to share information about yourself with other individuals and other companies. Consider your own privacy and personal safety when sharing your information with anyone. When sharing information about others, please consider their safety and privacy and get their consent for that sharing.</p>\r\n<p style="margin-left:36.0pt;">\r\n	We do not share your personal information with third parties other than as follows:</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; with your consent; for example, when you agree to our sharing your information with other third parties for their own marketing purposes subject to their separate privacy policies;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; to (i) satisfy any applicable law, regulation, subpoena, legal process or other government request, (ii) enforce our Terms of Use Agreement, including the investigation of potential violations thereof, (iii)&nbsp;investigate and defend ourselves against any third party claims or allegations, (iv) protect against harm to the rights, property or safety of MiAmor, its users or the public as required or permitted by law and (v) detect, prevent or otherwise address fraud, security or technical issues;</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; in connection with any merger, sale of company assets, reorganization, financing, change of control or acquisition of all or a portion of our business by another company or third party or in the event of bankruptcy; and</p>\r\n<p style="margin-left:72.0pt;">\r\n	o&nbsp;&nbsp; with third party vendors, consultants and other service providers that perform services on our behalf, in order to carry out their work for us.</p>\r\n<p style="margin-left:36.0pt;">\r\n	We may also share information with others in an aggregated form that does not directly identify you.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>4.&nbsp;&nbsp;&nbsp; </strong><strong>CHOICES</strong></p>\r\n<p style="margin-left:72.0pt;">\r\n	0.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Communications from MiAmor.</strong>You can opt out of receiving promotional messages from us such as emails, text messages and telephone calls by visiting the &quot;settings&quot; page of your account. If you opt out of our promotional communications, we may still send you non-promotional messages about your account or our ongoing business relations.</p>\r\n<p style="margin-left:72.0pt;">\r\n	1.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Cookies.</strong>You can generally remove or block most cookies using the settings in your browser. In some cases, however, our third party vendors may employ &quot;Flash&quot; cookies to help us detect fraudulent or abusive uses of the Services and we or our third party advertisers may also use these cookies. Flash cookies are managed through a different interface than your web browser and you can access your Flash cookie settings <a href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html" target="_blank">here</a>. If you choose to disable cookies, however, it may impact your ability to use our Services.</p>\r\n<p style="margin-left:72.0pt;">\r\n	2.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Advertising.</strong>You may receive selected advertising on some of the MiAmor pages, all advertisers must be prior approved and all advertising is inline with MiAmor&rsquo;s policies and advertising criteria.</p>\r\n<p style="margin-left:72.0pt;">\r\n	3.&nbsp;&nbsp;&nbsp;&nbsp; <strong>Access to Your Information.</strong>You may access and change your contact preferences and your information by visiting the &quot;Profile&quot; and &quot;settings&quot; sections of your account or by writing to us at the address provided below.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>5.&nbsp;&nbsp;&nbsp; </strong><strong>SECURITY</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	MiAmor takes reasonable measures to help protect your personal information in an effort to prevent loss, misuse and unauthorized access, disclosure, alteration and destruction.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>6.&nbsp;&nbsp;&nbsp; </strong><strong>UPDATES TO THIS POLICY</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	We reserve the right to modify this Policy from time to time. If we make any changes to this Policy, we will change the &quot;Last Revision&quot; date below and will post the updated Policy on this page.</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>7.&nbsp;&nbsp;&nbsp; </strong><strong>CONTACTING MIAMOR</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	If you have questions about this Policy, please contact us at admin@MiAmor.com.co or by writing to us at: MiAmor, Inc., MiAmor Privacy, Carrera 77 #43-43 Int 203, Medellin, Colombia</p>\r\n<p style="margin-left:36.0pt;">\r\n	<strong>8.&nbsp;&nbsp;&nbsp; </strong><strong>LAST REVISION DATE</strong></p>\r\n<p style="margin-left:36.0pt;">\r\n	This Policy was last revised on September 21 2015</p>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(7, 'How It Works', 'How It Works', '¿Cómo funciona?', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor connects singles from around the world with Colombians, it is as simple as <span style="font-size: 16px;">1,2,3</span> creating a profile, searching for your ideal match and then contacting the person that has caught your eye.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor understands that there may be a language barrier between some new matches, so we have installed a fully functional Chat translation service that is able to be used by all members. There are other ways to contact each other also from flirtatious winks and kisses, to sending messages, Chat, and also video Chat for those that really want to connect.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor is the vehicle to enable you to find that special person and connnect with them.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Good luck</p>\r\n', '<p>\r\n	MiAmor conecta a los solteros alrededor del mundo con las colombianos, es tan simple como 1,2,3 crear un perfil en busca de tu partido ideal y luego ponte en contacto con la persona que mas te ha llamado la atencion.<br />\r\n	<br />\r\n	MiAmor entiende que puede haber una barrera con el lenguaje entre algunos nuevos partidos, as&iacute; que hemos instalado un servicio de traducci&oacute;n al Chat totalmente funcional que podr&aacute; ser utilizado por todos los miembros, hay otras formas de contactarse cada uno con el otro tambi&eacute;n a partir gui&ntilde;os silvidos y besos, por env&iacute;o de mensajes, Chat, y tambi&eacute;n por v&iacute;deo Chat para aquellos que quieren conectarse realmente.<br />\r\n	&#8232;&#8232;MiAmor es el vinculo para activarlos&nbsp; a ustedes para encontrar a esa persona especial y Conectarse con ellos.<br />\r\n	&#8232;<br />\r\n	Buena suerte</p>\r\n', ''),
(8, 'Tell Us', 'Tell Us', 'Cuéntenos', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">If you have a question, thought, or a suggestion we&#39;d love to hear from you, we want to do everything we can to make your experiance on MiAmor a successful and pleasurable one.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Please include what you do or don&rsquo;t like about our website, thoughts about experiences you&rsquo;ve had meeting singles on MiAmor or let us know services that you would like to see. Your feedback provides us with valuable information to enable us to make your experiance here even more enjoyable.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Thanks</span></p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span class="hps">Si</span> <span class="hps">usted tiene alguna pregunta</span><span>, pensamiento,</span> <span class="hps">o</span> <span class="hps">una sugerencia</span><span>, nos encantar&iacute;a</span> <span class="hps">saber de usted</span><span>, queremos</span> <span class="hps">hacer todo lo</span> <span class="hps">posible para que su</span> <span class="hps">experiancia</span> <span class="hps">en</span> <span class="hps">miamor</span> <span class="hps">sea un &eacute;xito</span> <span class="hps">y sea placentera.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span class="hps">Por favor </span><span class="hps">incluya</span> <span class="hps">lo que le gusta o</span> <span class="hps">no le gusta de</span> <span class="hps">nuestro sitio web</span><span>, los pensamientos</span> <span class="hps">acerca de las experiencias</span> <span class="hps">que has tenido</span> <span class="hps">con las reuniones</span> <span class="hps">individuales</span> <span class="hps">en</span> <span class="hps">miamor</span> <span class="hps">o</span> <span class="hps">h&aacute;ganos saber</span> <span class="hps">los servicios</span> <span class="hps">que le gustar&iacute;a</span> <span class="hps">ver.</span> <span class="hps">Sus comentarios</span> <span class="hps">nos proporciona</span>n <span class="hps">informaci&oacute;n valiosa</span> <span class="hps">que nos permitira</span> <span class="hps">hacer su</span> <span class="hps">experiancia</span>&nbsp;<span class="hps">a&uacute;n m&aacute;s agradable</span><span> aqui.</span></span></p>\r\n<p>\r\n	<br />\r\n	<br />\r\n	<span lang="es"><span class="hps">gracias</span></span></p>\r\n', ''),
(9, 'Sms content', 'Sms content', 'Sms content', '<p>\r\n	You have lots of beautiful women waiting for you... some naked and wet too</p>\r\n<p>\r\n	MiAmor</p>\r\n', '', ''),
(10, 'GET IN TOUCH', 'GET IN CONTACT', 'GET IN CONTACT', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">Let us know what you think, we want to hear from you ?</span></p>\r\n', '<div class="almost_half_cell" id="gt-res-content" style="text-align:center;color:#FFF">\r\n	<div dir="ltr" style="zoom:1">\r\n		<span class="short_text" id="result_box" lang="es"><span class="hps">H&aacute;ganos saber</span> <span class="hps">lo que piensa,</span> <span class="hps">queremos</span> <span class="hps">saber de ti</span><span>?</span></span></div>\r\n</div>\r\n', ''),
(11, 'Advertise', 'Advertise', 'Anunciar', '<p>\r\n	MiAmor is committed to our advertisers and we believe that strategic advertising placement is complementary to MiAmor. MiAmor reaches and attracts users from around the globe, hoping to meet Colombians, many advertising opportunities exist and we would like to help you grow your business by creating an advertising campaign with you that is honest, exciting and successful.</p>\r\n<p>\r\n	<br />\r\n	For more information regarding advertising on MiAmor please complete the form below and we will get back to you at our earliest convenience.<br />\r\n	<br />\r\n	Thank you</p>\r\n', '<p>\r\n	Miamor est&aacute; comprometida con nuestros anunciantes y creemos que la colocaci&oacute;n de publicidad estrat&eacute;gica es complementaria a miamor. Miamor alcanza y atrae a usuarios de todo el mundo, con la esperanza de conocer a los colombianos, existen muchas oportunidades de publicidad y nos gustar&iacute;a ayudar a hacer crecer su negocio mediante la creaci&oacute;n de una campa&ntilde;a de publicidad con usted en la que sea honesto, emocionante y exitoso.<br />\r\n	<br />\r\n	<br />\r\n	Para obtener m&aacute;s informaci&oacute;n con respecto a la publicidad en miamor por favor complete el siguiente formulario y nos pondremos en contacto con usted en la brevedad posible.<br />\r\n	<br />\r\n	Gracias</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_countries`
--

CREATE TABLE IF NOT EXISTS `dateing_countries` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `translated_name` varchar(25) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_countries`
--

INSERT INTO `dateing_countries` (`id`, `name`, `translated_name`) VALUES
(1, 'White/Caucasian', 'Blanco/caucasico'),
(2, 'Latino/Hispanic', 'Latino/Hispano'),
(3, 'Black/African', 'Negro/Africano'),
(4, 'Indian', 'Indio'),
(5, 'Asian', 'Asiático'),
(6, 'Middle Eastern', 'Medio Este'),
(7, 'Pacific Islander', 'Islas del pacifico'),
(8, 'Native American', 'Nativo Americano'),
(9, 'Mixed/Other', 'Mixta/Otros');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_ethnicity`
--

CREATE TABLE IF NOT EXISTS `dateing_ethnicity` (
  `id` int(11) NOT NULL,
  `ethnicity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `translated_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_ethnicity`
--

INSERT INTO `dateing_ethnicity` (`id`, `ethnicity`, `translated_name`) VALUES
(1, 'American', 'Americana'),
(2, 'African', 'Africana'),
(3, 'Asian', 'Asiatica'),
(4, 'European', 'Europea'),
(5, 'Latino', 'Latina'),
(6, 'Indian', 'india'),
(7, 'Oceania', 'Oceania'),
(8, 'Other', 'otra');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_favourite`
--

CREATE TABLE IF NOT EXISTS `dateing_favourite` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `to_id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_favourite`
--

INSERT INTO `dateing_favourite` (`id`, `date`, `to_id`, `from_id`, `status`) VALUES
(29, '2014-11-07 16:17:25', 5, 1, 0),
(30, '2014-11-14 01:28:08', 7, 1, 0),
(31, '2014-11-20 00:11:11', 8, 1, 1),
(33, '2014-12-05 01:48:06', 6, 1, 0),
(36, '2014-12-11 04:05:35', 4, 1, 1),
(37, '2014-12-11 16:37:40', 5, 1, 0),
(40, '2014-12-15 02:21:46', 3, 1, 0),
(41, '2015-02-05 01:53:53', 11, 1, 1),
(42, '2015-02-05 03:33:22', 5, 1, 0),
(43, '2015-02-05 03:33:49', 5, 1, 1),
(44, '2015-02-05 03:48:08', 3, 1, 1),
(45, '2015-02-05 03:52:54', 1, 5, 0),
(46, '2015-02-27 18:59:09', 6, 1, 1),
(47, '2015-05-07 10:32:25', 22, 1, 1),
(48, '2015-05-07 10:33:05', 41, 1, 0),
(49, '2015-05-07 10:33:28', 41, 22, 1),
(50, '2015-06-30 12:12:40', 26, 5, 1),
(51, '2015-06-30 12:14:03', 78, 5, 1),
(52, '2015-07-17 10:14:07', 1, 26, 1),
(53, '2015-07-17 11:06:22', 26, 1, 1),
(54, '2015-08-09 20:37:26', 105, 5, 1),
(55, '2015-08-09 20:38:11', 5, 105, 1),
(56, '2015-08-13 22:54:37', 86, 50, 1),
(57, '2015-08-28 19:13:15', 106, 5, 0),
(58, '2015-08-28 19:13:26', 86, 5, 1),
(59, '2015-08-29 18:19:01', 105, 1, 1),
(60, '2015-09-09 18:01:38', 5, 26, 1),
(61, '2016-06-01 16:29:24', 86, 1, 1),
(62, '2016-06-01 22:32:25', 159, 1, 1),
(63, '2016-06-05 22:07:05', 86, 26, 1),
(64, '2016-06-05 22:09:20', 159, 26, 1),
(65, '2016-08-29 21:48:14', 4, 26, 1),
(66, '2016-09-02 11:50:34', 5, 4, 1),
(67, '2016-09-02 21:03:57', 1, 86, 1),
(68, '2016-09-03 13:48:52', 46, 5, 1),
(69, '2016-09-03 13:49:24', 49, 5, 1),
(70, '2016-09-03 14:02:32', 87, 5, 1),
(71, '2016-09-05 21:41:11', 22, 26, 1),
(72, '2016-09-06 09:00:16', 163, 5, 1),
(73, '2016-09-08 15:19:17', 173, 5, 1),
(74, '2016-09-08 15:19:23', 173, 5, 1),
(75, '2016-09-08 15:19:35', 173, 5, 1),
(76, '2016-09-08 19:55:48', 5, 86, 1),
(77, '2016-09-08 20:04:40', 173, 86, 1),
(78, '2016-09-08 20:04:44', 173, 86, 1),
(79, '2016-09-08 20:39:51', 4, 86, 1),
(80, '2016-09-08 20:55:31', 161, 86, 1),
(81, '2016-09-08 20:55:53', 26, 86, 1),
(82, '2016-09-08 20:58:03', 78, 86, 1),
(83, '2016-09-28 19:53:59', 0, 26, 1),
(84, '2016-09-28 20:08:39', 174, 26, 0),
(85, '1970-01-01 05:00:00', 1, 5, 0),
(86, '1970-01-01 05:00:00', 3, 5, 1),
(87, '1970-01-01 05:00:00', 118, 5, 1),
(88, '1970-01-01 05:00:00', 120, 5, 1),
(89, '1970-01-01 05:00:00', 174, 5, 1),
(90, '1970-01-01 05:00:00', 4, 5, 1),
(91, '1969-12-31 18:30:00', 3, 26, 1),
(92, '1969-12-31 18:30:00', 168, 26, 1),
(93, '1969-12-31 18:30:00', 173, 26, 1),
(94, '1970-01-01 05:00:00', 8, 26, 1),
(95, '1970-01-01 05:00:00', 168, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_homecms`
--

CREATE TABLE IF NOT EXISTS `dateing_homecms` (
  `id` int(11) NOT NULL,
  `pagename` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `title_translated` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pagedetail` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms';

--
-- Dumping data for table `dateing_homecms`
--

INSERT INTO `dateing_homecms` (`id`, `pagename`, `title`, `title_translated`, `pagedetail`, `description_translated`, `image`) VALUES
(1, 'how_it_works', 'HOW IT WORKS', 'COMO FUNCIONA', '<p>\r\n	3 simple steps to start connecting with singles</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\n3 pasos sencillos para empezar a conectarte con gente soltera\r\n\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(5, 'create_profile', 'Create profile', 'crear perfil', '<p>\r\n	Describe yourself, let others no your qualities, add photos, a video if you wish, and tell us about you and who your ideal partner would be</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-medium" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\nDescribete a ti mismo, tus cualidades, a&ntilde;ade fotos, videos si lo deseas y nos hablas acerca de ti y acerca de la persona que buscas.\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(6, 'create_section', 'Create selections', 'crear selecciones', '<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Who and what are you looking for? &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Create selections based on the type of relationship you want and your ideal partner</p>\r\n', '<p>\r\n	<span id="result_box" lang="es"><span class="hps">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &iquest;Qui&eacute;n y qu&eacute;</span> <span class="hps">es lo que buscas</span><span>?</span> <span class="hps">Crear</span> <span class="hps">selecciones basadas en</span> <span class="hps">el tipo de</span> <span class="hps">relaci&oacute;n que desea</span> <span class="hps">y</span> <span class="hps">tu pareja ideal</span></span></p>\r\n', ''),
(7, 'contact_section', 'Contact your selections', 'Contactese con sus selecciones', '<p>\r\n	Flirt&nbsp; by sending winks and kisses with those you like, befriend,Chat, Video Chat, full translation service offered</p>\r\n', '<p>\r\n	Conquista enviando gui&ntilde;os y besos a los que quieras, amistad, chat, video chat, servicio de traducci&oacute;n completa.</p>\r\n', ''),
(8, 'find_match', 'Find your perfect match with MiAmor apps', 'Encuentra tu pareja perfecta con la aplicación de MiAmor', '<p>\r\n	<span class="short_text" id="result_box" lang="es">Search your connections on the go </span></p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 48px;">\r\nBusca tus conexiones en el camino\r\n\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(9, 'about_miamor', 'MiAmor - The story', 'MiAmor - La historia', '<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;">MiAmor was created to provide a service to enable foreigners to connect with Colombians.&nbsp; &quot; I wanted to help others that may not have had the opportunity to travel to Colombia yet or who have wanted to make connections first before coming here to this beautiful country&quot;. </span></p>\r\n<p>\r\n	<br />\r\n	<span style="font-family:tahoma,geneva,sans-serif;">We have created a service that will really help connect foreigners with Colombians, It is the only such website that is actually based in Colombia, for Colombians and those that want to meet Colombians.<br />\r\n	<br />\r\n	&quot;I wanted to create a very simple to use service, that included lots of ways to connect and show your interest in those that you are attracted to, you can flirt, befriend, message, chat, translation chat or video chat, as well as searching singles on the go with the MiAmor App&quot;<br />\r\n	<br />\r\n	MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.<br />\r\n	<br />\r\n	MiAmor is Colombia and Colombia is MiAmor</span></p>\r\n<p>\r\n	<span style="font-family:tahoma,geneva,sans-serif;"><span style="font-family:tahoma,geneva,sans-serif;">- Dean Moriaty (MiAmor CEO)</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '<div style="float:left;padding:22px;width:96%">\r\n	<p>\r\n		<span id="result_box" lang="es"><span title="MiAmor was created to provide a service to enable foreigners to connect with Colombians.">MiAmor fue creado para proporcionar un servicio que permitira que los extranjeros se conecten con los colombianos.&nbsp;</span><span span="" title=""><span span="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate"><span title="Dean has sung the praises of Colombian women to all that will listen."> </span><span title="">&quot;Yo quer&iacute;a ayudar a otros que aun no han tenido la oportunidad de viajar a Colombia o que quieren hacer conexiones primero antes de venir aqu&iacute; a este hermoso pa&iacute;s&quot;. </span></span></span></span></p>\r\n	<p>\r\n		<span lang="es"><span span="" title=""><span span="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate"><span title="Dean has been working on MiAmor for a long time, he and Cindy have tryed to create a service that will really help connect foreigners with Colombians, It is the only such website that is actualy based in Colombia, for Colombians and those that want to meet">Hemos de crear un servicio que realmente ayude a conectar a los extranjeros con los colombianos, es el &uacute;nico sitio web que existe actualmente en Colombia, para las colombianas y los que quieren conocer </span><span title="Colombians.\r\n\r\n">colombianos.</span><br />\r\n		<br />\r\n		<br />\r\n		<br />\r\n		<span span="" title=""><span span="" title="singles on the go with the MiAmor App"> <span title="MiAmor has a huge database of single Colombian women who are looking to meet foreign men and an ever growing database of foreigners that would like to find someone special in Colombia.\r\n\r\n">MiAmor tiene una enorme base de datos de mujeres solteras colombianos que buscan conocer hombres extranjeros y una base de datos cada vez mayor de extranjeros que quieren encontrar a alguien especial en Colombia.</span></span></span></span></span></span></p>\r\n	<p>\r\n		<br />\r\n		<br />\r\n		<span lang="es"><span span="" title=""><span span="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate"><span span="" title=""><span span="" title="singles on the go with the MiAmor App"><span title="MiAmor is Colombia and Colombia is MiAmor">MiAmor es Colombia y Colombia es MiAmor</span></span></span></span></span></span></p>\r\n	<p>\r\n		<span lang="es"><span span="" title=""><span span="" title="trying to get this poor girl to change my american dollars into pasos by drawing diagrams and playing a comical version of charades, we eventualy started talking over google translate"><span span="" title=""><span span="" title="singles on the go with the MiAmor App"><span title="MiAmor is Colombia and Colombia is MiAmor"><span style="font-family:tahoma,geneva,sans-serif;"><span style="font-family:tahoma,geneva,sans-serif;">- Dean Moriaty (MiAmor CEO)</span></span></span></span></span></span></span></span></p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(10, 'get_in_touch', 'GET IN CONTACT', 'PONTE EN CONTACTO', '<p>\r\n	We want to hear from you</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-medium" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\nQueremos saber de ti&nbsp;\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', ''),
(11, 'find_partners', 'Connect with singles for fun, dating and long term relationships', 'Conectate con solteros para diversión, citas y relaciones de largo plazo', '<p>\r\n	Start chatting and getting to know compatible partners now</p>\r\n', '<pre class="tw-data-text vk_txt tw-ta tw-text-small" data-fulltext="" data-placeholder="Translation" dir="ltr" id="tw-target-text" style="text-align: left; height: 72px;">\r\nEmpieza a chatear y conoce gente compatible ahora\r\n\r\n\r\n\r\n\r\n\r\n</pre>\r\n<p>\r\n	&nbsp;</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_homeimages`
--

CREATE TABLE IF NOT EXISTS `dateing_homeimages` (
  `id` int(11) NOT NULL,
  `pagename` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='adventure_circle_cms';

--
-- Dumping data for table `dateing_homeimages`
--

INSERT INTO `dateing_homeimages` (`id`, `pagename`, `image`) VALUES
(1, 'background1', '1_1474441574.jpg'),
(5, 'create_profile', '5_1423820965.png'),
(6, 'create_section', '6_1423821200.png'),
(7, 'contact_section', '7_1423821226.png'),
(8, 'google_play', '8_1423821601.png'),
(9, 'about_miamor', '9_1423821263.png'),
(11, 'miamor_app', '11_1423821643.png'),
(12, 'background2', '12_1423820636.jpg'),
(13, 'background3', '13_1474444053.png'),
(14, 'background4', '14_1474447042.jpg'),
(15, 'app_store', '15_1423821659.png');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_individual_adds`
--

CREATE TABLE IF NOT EXISTS `dateing_individual_adds` (
  `id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `s_id` varchar(30) NOT NULL,
  `startingslot` int(11) NOT NULL,
  `multiple` int(11) NOT NULL,
  `size` varchar(100) NOT NULL,
  `u_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_individual_adds`
--

INSERT INTO `dateing_individual_adds` (`id`, `p_id`, `s_id`, `startingslot`, `multiple`, `size`, `u_id`, `image`, `start_date`, `active`) VALUES
(17, 3, '1', 1, 0, '1', 4, '1419401217.jpg', '2014-12-25 01:36:57', 1),
(18, 3, '2,3', 2, 1, '2', 9, '1418910753.jpg', '2014-12-19 09:22:33', 1),
(19, 4, '1,2,3', 1, 1, '3', 6, '1418911062.jpg', '2015-06-10 03:13:17', 1),
(20, 5, '1,2,3', 1, 1, '3', 4, '1419419561.JPG', '2014-12-25 06:42:41', 1),
(21, 21, '1', 1, 0, '1', 4, '1419400971.jpg', '2014-12-25 01:32:51', 1),
(22, 22, '1', 1, 0, '1', 4, '1418914376.jpeg', '2014-12-19 10:22:56', 1),
(23, 17, '2', 2, 0, '1', 4, '1418973861.jpeg', '2014-12-20 02:54:21', 1),
(24, 18, '1', 1, 0, '1', 4, '1418973988.jpeg', '2014-12-20 02:56:28', 1),
(25, 19, '3', 3, 0, '1', 4, '1418974267.jpg', '2014-12-20 03:01:07', 1),
(26, 20, '4,5', 4, 1, '2', 4, '1418974303.jpg', '2014-12-20 03:01:43', 1),
(27, 23, '3', 3, 0, '1', 4, '1418974336.jpg', '2014-12-20 03:02:16', 1),
(28, 2, '1,2,3,4', 1, 0, '4', 4, '1428478158.png', '2015-04-08 19:59:18', 1),
(38, 14, '3', 3, 0, '1', 4, '', '2014-12-20 08:41:53', 1),
(39, 14, '1,2', 1, 1, '2', 4, '1419403544.jpg', '2014-12-25 02:15:44', 1),
(40, 13, '2,3,4', 2, 1, '3', 4, '', '2014-12-20 08:54:11', 1),
(41, 10, '1,2,3', 1, 1, '3', 4, '1419403204.jpg', '2014-12-25 08:11:16', 1),
(42, 13, '1', 1, 0, '1', 4, '', '2014-12-22 19:31:41', 1),
(43, 7, '1', 1, 0, '1', 4, '1419418903.jpg', '2014-12-25 06:31:43', 1),
(44, 10, '4', 4, 0, '1', 4, '', '2014-12-25 10:09:33', 1),
(45, 2, '7', 0, 0, '', 0, '', '2015-06-10 03:20:54', 1),
(46, 2, '8', 0, 0, '', 0, '', '2015-06-10 03:23:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_invoice`
--

CREATE TABLE IF NOT EXISTS `dateing_invoice` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `invoice` varchar(255) NOT NULL,
  `doc` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_message`
--

CREATE TABLE IF NOT EXISTS `dateing_message` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `del_status` tinyint(1) NOT NULL DEFAULT '0',
  `del_sender` tinyint(1) NOT NULL DEFAULT '0',
  `view_status` tinyint(4) NOT NULL,
  `delete_status_permanent` tinyint(1) NOT NULL DEFAULT '0',
  `delete_sender_permanent` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_message`
--

INSERT INTO `dateing_message` (`id`, `from_id`, `to_id`, `message`, `date`, `del_status`, `del_sender`, `view_status`, `delete_status_permanent`, `delete_sender_permanent`) VALUES
(10, 5, 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2014-10-15 01:48:53', 1, 1, 0, 0, 0),
(11, 1, 4, 'jhbkjnlkmlkm', '2014-10-17 14:13:49', 0, 1, 0, 0, 1),
(12, 8, 1, 'sadasd asdasdas sadasdas', '2014-10-22 11:58:51', 1, 1, 0, 1, 1),
(13, 1, 8, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2014-10-23 00:06:02', 0, 1, 0, 0, 1),
(14, 1, 8, '  \r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '2014-10-23 00:30:39', 0, 0, 0, 0, 0),
(15, 1, 8, 'hiuhiuhoikm', '2014-10-25 01:05:19', 1, 1, 0, 1, 1),
(16, 1, 8, 'gjgkjhkhn', '2014-10-25 01:56:03', 1, 1, 0, 1, 1),
(17, 1, 3, 'Test Message................', '2014-10-29 11:29:33', 0, 1, 0, 0, 1),
(18, 1, 3, 'Again another test.................', '2014-10-29 11:32:28', 0, 0, 0, 0, 0),
(20, 8, 1, 'm,mn,nm,nm,nm,', '2014-10-30 01:35:36', 1, 1, 0, 1, 1),
(22, 8, 1, 'vbnvnvnvn', '2014-10-30 05:03:22', 1, 1, 0, 1, 1),
(23, 1, 8, 'hkjlkjlk;l,', '2014-10-31 14:30:10', 1, 1, 0, 1, 1),
(24, 1, 3, 'hello', '2014-10-31 14:34:37', 0, 1, 0, 0, 1),
(25, 1, 3, 'bjgkuhilkj', '2014-10-31 15:01:42', 0, 1, 0, 0, 1),
(26, 1, 8, 'hbibiiuhiuhuih', '2014-10-31 15:12:21', 1, 1, 0, 1, 1),
(27, 1, 3, 'i love you', '2014-10-31 15:13:11', 0, 1, 0, 0, 1),
(35, 1, 5, 'ojoijoiioj', '2014-11-14 14:58:26', 1, 1, 0, 0, 1),
(34, 1, 5, 'hhiuniniun', '2014-11-14 14:58:10', 1, 1, 0, 0, 1),
(36, 4, 3, 'hey', '2015-02-12 05:38:44', 0, 0, 0, 0, 0),
(37, 1, 4, 'HELLO', '2015-04-08 08:42:39', 0, 0, 0, 0, 0),
(38, 1, 5, 'Lorem Ipsum', '2015-06-10 09:38:16', 1, 0, 0, 0, 0),
(39, 1, 8, 'dssdsdsdsd', '2015-07-17 14:56:13', 0, 0, 0, 0, 0),
(40, 1, 8, 'dsdsdsdsdsdsd', '2015-07-17 15:18:24', 0, 0, 0, 0, 0),
(41, 1, 8, 'asasasasasas', '2015-07-17 16:06:54', 0, 0, 0, 0, 0),
(42, 1, 5, 'Test', '2015-07-20 22:57:30', 1, 0, 0, 0, 0),
(43, 1, 8, 'Hi Kundu,\r\n\r\nHow are you.', '2015-07-21 12:57:26', 0, 0, 0, 0, 0),
(44, 105, 5, 'hola como estas', '2015-08-09 20:39:49', 0, 0, 0, 0, 0),
(45, 105, 5, 'khjhghhjgjhgjh', '2015-08-09 20:47:20', 0, 0, 0, 0, 0),
(46, 1, 51, 'Hi', '2015-08-12 13:06:12', 0, 0, 0, 0, 0),
(47, 1, 51, 'hi...this is a test message..', '2015-08-12 14:13:41', 0, 0, 1, 0, 0),
(48, 1, 51, 'Hi this is another test.', '2015-08-12 14:18:35', 0, 0, 1, 0, 0),
(49, 1, 5, 'Hello Karuna,\r\n\r\nLorem Ipsum Doller Sit amet.\r\nThis is a test message from admin. \r\n\r\nThanks,\r\nAdmin', '2015-08-24 10:52:40', 0, 0, 0, 0, 0),
(50, 105, 1, 'hello amor como estas', '2015-08-29 18:14:32', 1, 0, 0, 0, 0),
(51, 1, 105, 'muy bien gracias, te amo', '2015-08-29 18:15:28', 0, 0, 0, 0, 0),
(52, 1, 105, 'hi ..test msg', '2016-06-01 16:32:23', 0, 0, 1, 0, 0),
(53, 1, 86, 'hi jojo', '2016-06-01 16:48:11', 1, 0, 0, 1, 0),
(54, 1, 6, 'hi\n', '2016-06-01 21:10:41', 0, 0, 0, 0, 0),
(55, 1, 159, 'hi mate', '2016-06-01 22:32:19', 0, 1, 0, 0, 0),
(56, 1, 86, 'hi', '2016-06-01 22:34:14', 0, 0, 0, 0, 0),
(57, 1, 159, 'hi', '2016-06-01 22:39:48', 0, 1, 0, 0, 0),
(58, 26, 159, 'Hello, are you looking for love', '2016-06-05 22:08:55', 0, 0, 0, 0, 0),
(59, 26, 1, 'I love you', '2016-06-05 22:12:44', 0, 0, 0, 0, 0),
(60, 26, 86, 'y el arroz con habichuela puerto rico me lo regalo.....', '2016-08-29 19:40:27', 1, 0, 0, 1, 0),
(61, 26, 86, 'hola jojo como estas', '2016-08-30 14:14:29', 1, 0, 0, 1, 0),
(62, 5, 26, 'Hola ', '2016-08-30 16:31:56', 0, 0, 0, 0, 0),
(63, 26, 5, 'hola', '2016-08-30 16:39:10', 0, 0, 0, 0, 0),
(64, 26, 5, 'hkjhjb', '2016-08-30 16:39:46', 0, 0, 0, 0, 0),
(65, 5, 26, 'hello', '2016-08-30 16:40:38', 0, 0, 0, 0, 0),
(66, 26, 8, 'hgmbghkk,m', '2016-08-30 21:17:00', 0, 0, 0, 0, 0),
(67, 1, 8, 'ffsdsdffd', '2016-09-01 09:17:30', 0, 1, 0, 0, 0),
(68, 1, 8, 'abcd', '2016-09-01 09:29:28', 0, 1, 0, 0, 0),
(69, 1, 8, 'jkjhkhkjh', '2016-09-01 09:30:39', 0, 1, 0, 0, 0),
(88, 5, 78, 'test msg 1', '2016-09-16 07:06:49', 0, 0, 0, 0, 0),
(70, 1, 8, 'ggfdgfd', '2016-09-01 09:32:50', 0, 1, 0, 0, 0),
(71, 1, 8, 'fsgdgfdgfd', '2016-09-01 09:33:57', 0, 1, 0, 0, 0),
(72, 1, 8, 'cxcxvcxc', '2016-09-01 09:39:46', 0, 1, 0, 0, 0),
(73, 1, 8, 'fdgdgf', '2016-09-01 09:40:05', 0, 1, 0, 0, 0),
(74, 1, 8, 'uytuytuytuutuy', '2016-09-01 09:46:55', 0, 1, 0, 0, 0),
(75, 1, 8, 'ryrtryryr', '2016-09-01 09:47:49', 0, 1, 0, 0, 0),
(76, 26, 5, 'onoonon', '2016-09-02 19:37:40', 0, 0, 0, 0, 0),
(77, 26, 86, 'Hola Jojo', '2016-09-02 19:57:00', 0, 0, 0, 0, 0),
(78, 26, 4, 'BFSD JHCVB Kkdhfksbdjc KHKJDHbshdfg,s kkasdhfkjhd', '2016-09-05 21:08:38', 0, 0, 0, 0, 0),
(79, 26, 4, 'amo amor', '2016-09-05 21:30:48', 0, 0, 0, 0, 0),
(80, 26, 86, 'hghgfhfdhf jhgjhkfgh  mnbbhjjh', '2016-09-06 17:20:17', 1, 1, 0, 1, 0),
(81, 26, 1, 'hello....good job..', '2016-09-07 05:46:55', 0, 0, 0, 0, 0),
(82, 26, 1, 'abcd', '2016-09-07 13:02:41', 0, 0, 0, 0, 0),
(83, 26, 1, 'kljljlkjlk', '2016-09-07 13:06:03', 1, 1, 0, 0, 0),
(84, 86, 5, 'jhahsdjabsnm c aslaskjkÃ±JSKJDHsiuc kSDNCIUJSJD', '2016-09-08 19:48:12', 0, 1, 0, 0, 1),
(85, 86, 5, 'jhahsdjabsnm c aslaskjkÃ±JSKJDHsiuc kSDNCIUJSJD', '2016-09-08 19:48:17', 0, 1, 0, 0, 1),
(86, 86, 5, 'jhahsdjabsnm c aslaskjkÃ±JSKJDHsiuc kSDNCIUJSJD', '2016-09-08 19:48:22', 1, 1, 0, 0, 1),
(87, 86, 5, 'hahahah', '2016-09-08 19:55:06', 1, 1, 0, 1, 1),
(91, 5, 26, 'hola Bikash', '2016-09-27 18:58:21', 0, 1, 0, 0, 0),
(89, 5, 115, 'test', '2016-09-16 07:20:00', 0, 0, 0, 0, 0),
(90, 5, 26, 'test data', '2016-09-16 07:20:59', 0, 0, 0, 0, 0),
(92, 26, 5, 'ajhsbdhaBSKbsakcbsd KJDKksdkjsd', '2016-09-28 17:49:14', 1, 0, 0, 0, 0),
(93, 26, 86, 'kjsdfkjsdfjsd', '2016-09-28 18:14:19', 0, 0, 0, 0, 0),
(94, 5, 1, 'jhxbcs<bc kshcksdc asdckjsdc LDHCKJSD C', '1970-01-01 05:00:00', 0, 0, 0, 0, 0),
(95, 5, 78, 'jhgkjaaÃ±dkfjkghdfj ', '1970-01-01 05:00:00', 0, 0, 0, 0, 0),
(96, 1, 5, 'kjjbjsdb dw DMASNDKJ AD,a jhoana te quiero mucho', '2016-10-22 01:03:06', 0, 0, 0, 0, 0),
(97, 26, 5, 'GHHHG JHHJUF HGJHGHJ', '2016-10-28 06:09:28', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_order`
--

CREATE TABLE IF NOT EXISTS `dateing_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `car_model` varchar(200) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_order`
--

INSERT INTO `dateing_order` (`id`, `user_id`, `car_id`, `location`, `date`, `email`, `phone`, `address`, `car_model`) VALUES
(1, 47, 2, 'kolkata', '09/09/2014', 'nitsindranil94@gmail.com', '1234567890', 'kolkata', '1300'),
(2, 46, 154, 'kolkata', '09/09/2014', 'nits.avik@gmail.com', '9900000000', 'kolkta', '2600');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_package`
--

CREATE TABLE IF NOT EXISTS `dateing_package` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` text CHARACTER SET utf8 NOT NULL,
  `price` varchar(200) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0' COMMENT 'Duration in months',
  `for` enum('M','F') NOT NULL,
  `create_profile` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `view_profile_pages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_photos` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_video` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `search_members` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_winks` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `reply_to_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_friend_requests` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_kisses` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `video_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `translate_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `purchase_translation_tokens` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `viewed_me` enum('A','R') NOT NULL DEFAULT 'R',
  `duration_time` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_package`
--

INSERT INTO `dateing_package` (`id`, `title`, `title_translated`, `description`, `description_translated`, `price`, `duration`, `for`, `create_profile`, `view_profile_pages`, `add_photos`, `add_video`, `search_members`, `send_winks`, `reply_to_messages`, `send_friend_requests`, `send_kisses`, `send_messages`, `chat`, `video_chat`, `translate_chat`, `purchase_translation_tokens`, `viewed_me`, `duration_time`) VALUES
(1, 'Public', 'PÃºblico', 'This is the public package for men', 'Este es el paquete pÃºblica para los hombre', '0', 0, 'M', 'A', 'R', 'A', 'A', 'A', 'R', 'A', 'R', 'R', 'R', 'A', 'A', 'A', 'R', 'A', 0),
(2, 'Trial', 'Prueba', 'This is a trial membership package for men', 'Este es un paquete de membresia de prueba para los hombre', '0', 0, 'M', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 60),
(3, 'Basic', 'básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '23.99', 1, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(4, 'Premium ', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombre', '28.99', 1, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 0),
(5, 'Basic', 'básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '50.97', 3, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(6, 'Premium ', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombre', '56.97', 3, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 0),
(7, 'Basic', 'básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '87.00', 6, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(8, 'Premium', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombre', '101.94', 6, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(9, 'Basic', 'básico', 'This is a basic package for men', 'Se trata de un paquete básico de prueba para los hombre', '126.00', 12, 'M', 'A', 'A', 'R', 'A', 'R', 'A', 'R', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0),
(10, 'Premium', 'Premium', 'This is a Premium package for men', 'Este es un paquete Premium para hombre', '162.00', 12, 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'R', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_package_women`
--

CREATE TABLE IF NOT EXISTS `dateing_package_women` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `price` varchar(200) NOT NULL,
  `for` enum('M','F') NOT NULL,
  `create_profile` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `view_profile_pages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_photos` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `add_video` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `search_members` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_winks` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `reply_to_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_friend_requests` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_kisses` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `send_messages` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `video_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `translate_chat` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `purchase_translation_tokens` enum('A','R') NOT NULL COMMENT '''A'' = ''Approved'',''R''=''Restricted''',
  `viewed_me` enum('A','R') NOT NULL DEFAULT 'R'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_package_women`
--

INSERT INTO `dateing_package_women` (`id`, `title`, `title_translated`, `description`, `description_translated`, `price`, `for`, `create_profile`, `view_profile_pages`, `add_photos`, `add_video`, `search_members`, `send_winks`, `reply_to_messages`, `send_friend_requests`, `send_kisses`, `send_messages`, `chat`, `video_chat`, `translate_chat`, `purchase_translation_tokens`, `viewed_me`) VALUES
(1, 'Women Package', 'Mujeres Package', 'This is the public package for women', 'Este es el paquete pÃºblicos para las mujeres', '0', 'M', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_payment`
--

CREATE TABLE IF NOT EXISTS `dateing_payment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `mode` varchar(100) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `cheque_no` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_payment`
--

INSERT INTO `dateing_payment` (`id`, `user_id`, `car_id`, `mode`, `bank_name`, `cheque_no`, `image`, `status`) VALUES
(1, 47, 19, 'Via Cheque', '1234567890', '', '1410162257Apple_Friends.gif', 'approved'),
(2, 47, 17, 'PayPal', 'rte', '', '1410167907KindergartenNewsHeader.jpg', 'approved'),
(3, 47, 15, 'PayPal', '12345', '', '1410168972news23.gif', 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_port`
--

CREATE TABLE IF NOT EXISTS `dateing_port` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `c_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_quickpics`
--

CREATE TABLE IF NOT EXISTS `dateing_quickpics` (
  `id` int(11) NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_quickpics`
--

INSERT INTO `dateing_quickpics` (`id`, `amount`) VALUES
(1, '3');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_religion`
--

CREATE TABLE IF NOT EXISTS `dateing_religion` (
  `id` int(11) NOT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_sentnewsletter`
--

CREATE TABLE IF NOT EXISTS `dateing_sentnewsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_sentnewsletter`
--

INSERT INTO `dateing_sentnewsletter` (`id`, `email`) VALUES
(1, 'nitsindranil94@gmail.com'),
(2, 'nits.avik@gmail.com'),
(3, 'damon@optimumstudio.com'),
(6, 'nits.sandeeptewary@gmail.com'),
(7, 'nits.riju@gmail.com'),
(8, 'nits.tanmoy@gmail.com'),
(9, 'nits.abhishek85@gmail.com'),
(10, 'nits.bikash111@gmail.com'),
(11, 'asdbik@gmail.com'),
(12, 'tlast@gmail.com'),
(13, 'nits.ashish1@gmail.com'),
(14, 'ag@gmail.com'),
(15, 'damon@fitlink.co.nz'),
(17, 'de_berry@yahoo.com'),
(18, ''),
(24, 'nits.bikash@yahoo.com'),
(25, 'eyals1974@gmail.com'),
(26, 'nits.sayan@gmail.com'),
(27, 'ziffell454@hotmail.com'),
(28, 'tw5335@gmail.com'),
(29, 'damon@fitlink.com.co'),
(30, 'dillkumar13@yahoo.com'),
(31, 'mchetan8413@gmail.com'),
(32, 'ctenorio2075@hotmail.com'),
(33, 'markshepparduk@gmail.com'),
(34, 'yasserlasso@hotmail.es'),
(35, 'trebands@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_settings`
--

CREATE TABLE IF NOT EXISTS `dateing_settings` (
  `id` int(11) NOT NULL,
  `comp_perc` int(11) NOT NULL COMMENT 'Compatiable Percentage',
  `comp_users` int(11) NOT NULL,
  `template_users1` int(11) NOT NULL,
  `not_logged` int(11) NOT NULL,
  `bank_location` text NOT NULL,
  `account_number` text NOT NULL,
  `ref_number` text NOT NULL,
  `swift_code` text NOT NULL,
  `iban` text NOT NULL,
  `branch_code` text NOT NULL,
  `beneficiary` text NOT NULL,
  `bank` text NOT NULL,
  `bank_info` text NOT NULL,
  `template_users2` int(11) NOT NULL,
  `mail_sent1` int(11) NOT NULL,
  `mail_sent2` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_settings`
--

INSERT INTO `dateing_settings` (`id`, `comp_perc`, `comp_users`, `template_users1`, `not_logged`, `bank_location`, `account_number`, `ref_number`, `swift_code`, `iban`, `branch_code`, `beneficiary`, `bank`, `bank_info`, `template_users2`, `mail_sent1`, `mail_sent2`) VALUES
(1, 10, 6, 15, 14, 'jojo', '4564565', '54654', 'tet', 'retert', 'retert', 'retert', 'retert', 'It usually takes five to ten business days for MiAmor to receive and process your bank transfer, dependent on the country of origin.', 10, 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `dateing_shipping`
--

CREATE TABLE IF NOT EXISTS `dateing_shipping` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `delivery_date` varchar(100) NOT NULL,
  `arrival_date` varchar(100) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_social`
--

CREATE TABLE IF NOT EXISTS `dateing_social` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_social`
--

INSERT INTO `dateing_social` (`id`, `name`, `link`, `image`) VALUES
(1, 'Facebook', 'https://www.facebook.com/miamor.com.co', ''),
(2, 'Twitter', 'http://www.twitter.com', ''),
(28, 'Linked In', 'https://in.linkedin.com', ''),
(29, 'Google Plus', 'https://plus.google.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_story`
--

CREATE TABLE IF NOT EXISTS `dateing_story` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_translated` varchar(55) CHARACTER SET utf8 NOT NULL,
  `description` longtext NOT NULL,
  `description_translated` longtext CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_story`
--

INSERT INTO `dateing_story` (`id`, `title`, `title_translated`, `description`, `description_translated`, `image`) VALUES
(2, 'My Story', 'Mi historia', 'test test test test test test test test test test test test test test test test test test test test miamor test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'tel_story.png'),
(3, 'My another story', 'Mi otra historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'tel_story1.png'),
(4, 'My third story', 'Mi tercera historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'Kisses-3-love-18886362-500-335.jpg'),
(5, 'My fourth story', 'Mi cuarta historia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do tempor eiusmod ut labore et dolore incididunt magna aliqua. Ut enim ad minim veniam, nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat. Duis Aute Irure dolor en reprehenderit en voluptate Velit esse cillum dolore eu nulla fugiat pariatur. Sint Excepteur occaecat cupidatat no proident, en sunt qui culpa deserunt officia mollit Identificación del anim est Laborum.', 'love-couple-young-wallpaper.jpg'),
(6, 'My Story With You. ', 'My Story With You. ', 'My Story With You. was so much special to describe. \r\nI love the each time i spent with you.\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nEach time we lough.. \r\nwe talk , We hold our hands. ', 'My Story With You. was so much special to describe. \r\nI love the each time i spent with you..\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \r\nEach time we lough.. \r\nwe talk , We hold our hands. ', 'courting23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tbladmin`
--

CREATE TABLE IF NOT EXISTS `dateing_tbladmin` (
  `id` int(8) NOT NULL,
  `name` varchar(256) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `paypal_email` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_tbladmin`
--

INSERT INTO `dateing_tbladmin` (`id`, `name`, `admin_username`, `admin_password`, `email`, `address`, `image`, `status`, `paypal_email`, `secret_key`, `publishable_key`) VALUES
(1, '', 'levityler', 'admin', 'damon@optimumstudio.com', '', '', '1', 'nits.arpita@gmail.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tblorderdetails`
--

CREATE TABLE IF NOT EXISTS `dateing_tblorderdetails` (
  `id` int(11) NOT NULL,
  `orderid` varchar(255) DEFAULT NULL,
  `productid` varchar(255) DEFAULT NULL,
  `quantity` varchar(256) NOT NULL,
  `order_status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_tblorderdetails`
--

INSERT INTO `dateing_tblorderdetails` (`id`, `orderid`, `productid`, `quantity`, `order_status`) VALUES
(8, '6', '2', '1', '0'),
(9, '6', '3', '1', '0'),
(10, '7', '1', '6', '0'),
(11, '8', '3', '1', '0'),
(12, '9', '1', '1', '0'),
(13, '10', '1', '1', '0'),
(14, '11', '1', '1', '0'),
(15, '12', '1', '1', '0'),
(16, '13', '1', '1', '0'),
(17, '14', '1', '1', '0'),
(18, '15', '1', '1', '0'),
(19, '16', '1', '1', '0'),
(20, '17', '1', '1', '0'),
(21, '18', '1', '1', '0'),
(22, '19', '1', '1', '0'),
(23, '20', '1', '1', '0'),
(24, '21', '1', '1', '0'),
(25, '22', '1', '1', '0'),
(26, '23', '2', '1', '0'),
(27, '24', '2', '1', '0'),
(28, '25', '3', '1', '0'),
(29, '26', '2', '1', '0'),
(30, '27', '1', '1', '0'),
(31, '28', '1', '1', '0'),
(32, '29', '1', '2', '0');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_tblorders`
--

CREATE TABLE IF NOT EXISTS `dateing_tblorders` (
  `orderid` int(11) NOT NULL,
  `orderdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_user_id` varchar(20) NOT NULL DEFAULT '',
  `billingid` int(11) NOT NULL,
  `orderamount` varchar(50) NOT NULL DEFAULT '',
  `order_status` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `dispatch` enum('1','0') NOT NULL DEFAULT '0',
  `type` enum('S','P') NOT NULL DEFAULT 'S'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_transactions`
--

CREATE TABLE IF NOT EXISTS `dateing_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `txn_id` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_transactions`
--

INSERT INTO `dateing_transactions` (`id`, `user_id`, `txn_id`, `amount`, `status`) VALUES
(1, 10, 'I-RMPRS0YJGJHA', 10.00, 'verified'),
(2, 11, 'I-F0FRRX99G7DR', 15.00, 'verified'),
(3, 0, 'I-0SAWCKDT8JJA', 15.00, 'verified'),
(4, 0, 'I-7KGHUHT9DASH', 15.00, 'verified'),
(5, 12, 'I-PNELT8YTH819', 15.00, 'verified'),
(6, 1, 'I-E2VRP7RTV1E0', 101.94, 'verified'),
(7, 26, 'I-MRPKAM06NPPH', 50.97, 'verified'),
(8, 30, 'I-M65UXPE8US82', 56.97, 'verified'),
(9, 0, 'I-DE42VF5DNL16', 28.99, 'verified'),
(10, 22, 'I-P7VRDXHY8X8C', 5.00, 'verified'),
(11, 46, 'I-FAM14PKMBPBL', 5.00, 'verified'),
(12, 49, 'I-3DE1TMPL9VG8', 5.00, 'verified'),
(13, 78, 'I-LTWJ4HDKTWM0', 28.99, 'verified'),
(14, 89, 'I-40UH6PK74BYM', 56.97, 'verified'),
(15, 1, 'I-KUGHNHYNDSGJ', 126.00, 'verified'),
(16, 104, 'I-G2HSRSH561AR', 162.00, 'verified'),
(17, 1, 'I-E3VJEHP38VRR', 162.00, 'verified'),
(18, 1, 'I-YKUUEV1VPXGM', 162.00, 'verified');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user`
--

CREATE TABLE IF NOT EXISTS `dateing_user` (
  `id` int(11) NOT NULL,
  `fb_user_id` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `subscription_start` date NOT NULL,
  `subscription_end` date NOT NULL,
  `old_subcribe_details` varchar(255) NOT NULL,
  `ref_id` varchar(100) NOT NULL,
  `subscription_till` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(256) NOT NULL,
  `paypal_email` text NOT NULL,
  `password` varchar(256) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `about_me` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `add_date` date NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `is_loggedin` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `ip` varchar(30) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `dpname` varchar(200) NOT NULL,
  `height` varchar(200) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `child` varchar(100) NOT NULL,
  `ethnicity` varchar(100) NOT NULL,
  `education` varchar(200) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `smoking` varchar(100) NOT NULL,
  `drinking` varchar(200) NOT NULL,
  `bodytype` varchar(200) NOT NULL,
  `personality` varchar(200) NOT NULL,
  `lookfor` varchar(200) NOT NULL,
  `relocate` varchar(255) NOT NULL,
  `appearence` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `me_doing` text NOT NULL,
  `good_at` text NOT NULL,
  `notice_about_me` text NOT NULL,
  `perfect_match` text,
  `ideal_date` text,
  `facebook_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `dont_have_fbtwt` tinyint(1) NOT NULL,
  `creditcard_no` varchar(150) NOT NULL,
  `expire_month` int(11) NOT NULL,
  `expire_year` int(11) NOT NULL,
  `ccv` varchar(100) NOT NULL,
  `admin_verify` tinyint(1) NOT NULL DEFAULT '1',
  `translator_package_id` int(5) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0',
  `fb_varify` tinyint(1) NOT NULL DEFAULT '0',
  `timeleft` int(11) NOT NULL,
  `trial_taken` enum('Y','N') NOT NULL DEFAULT 'N',
  `interest` enum('W','M','MM','WW','MW') NOT NULL COMMENT '''M'' - Man in woman, ''W'' - Women in man,''MM'' - Man in Man, ''WW'' - Women in women,''MW'' - Both Man and Women',
  `last_login_time` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_user`
--

INSERT INTO `dateing_user` (`id`, `fb_user_id`, `type`, `subscription_id`, `subscription_start`, `subscription_end`, `old_subcribe_details`, `ref_id`, `subscription_till`, `fname`, `lname`, `email`, `paypal_email`, `password`, `gender`, `dob`, `address`, `about_me`, `image`, `add_date`, `status`, `is_loggedin`, `last_login`, `ip`, `city`, `state`, `country`, `phone`, `secret_key`, `publishable_key`, `user_type`, `dpname`, `height`, `sex`, `relation`, `child`, `ethnicity`, `education`, `religion`, `smoking`, `drinking`, `bodytype`, `personality`, `lookfor`, `relocate`, `appearence`, `language`, `me_doing`, `good_at`, `notice_about_me`, `perfect_match`, `ideal_date`, `facebook_link`, `twitter_link`, `dont_have_fbtwt`, `creditcard_no`, `expire_month`, `expire_year`, `ccv`, `admin_verify`, `translator_package_id`, `start_date`, `end_date`, `is_phone_verified`, `fb_varify`, `timeleft`, `trial_taken`, `interest`, `last_login_time`) VALUES
(1, '1517545255189394', 0, 10, '2016-10-19', '2017-10-19', '', 'miamor000001', '2016-11-09 10:25:05', 'Indranil ', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1987-01-01', 'kolkata,india', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '1c1f13fca12205c56d443c71ef2a48eb230_OL_product.jpg', '2016-09-18', '1', 1, '2016-11-02 16:00:04', '122.160.187.85', 'kolkataa', '', '', '9002561033', '', '', 'Interested In Men and Women', 'Indranil ', '183', 'male', 'single', 'yes', '1', 'college graduate', 'hindu', 'no', 'no', 'heavy', 'Ambitious and Leader Like', 'a friend', 'yes', 'average', 'Bengali', 'TEST', 'Test', 'Test', 'I love the "bad boys" most- they make you feel crazy and euphoric, they are possessive and tease you mercilessly.... they treat you like princess and can kiss the daylights out of you.... and the madness of loving them is out of the world..!!', 'It does not have to be at CCD....Barista....KFC or any of these places..... just that we should be able to talk.... and about anything and everything from Comic strips to Shakespeare... If things go good, it can be followed by a short walk around the corner and most importantly.... its gotta be with someone who does not pretend to impress.', 'http://www.google.com', 'http://www.google.com', 0, '', 0, 0, '', 1, 1, '2016-11-09', '2016-11-16', 1, 1, 0, 'N', 'W', '2016-11-02 15:38:37'),
(3, '', 0, 1, '2015-03-01', '2015-04-01', 'This users Women Package package change by admin on 10-03-2015', '', '2016-10-06 21:15:15', 'payle', '', 'talk2payel1987@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-02-03', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '1413179314Beautiful-Girl-Face-Pictures_beautiful-girl.jpg', '2016-09-12', '1', 1, '2016-09-16 07:17:43', '117.194.16.43', '', '', '', '213123123123', '', '', 'Man Interested In Women', 'payel', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'Ambitious and Leader Like', 'a friend', 'within country', 'attractive', 'English', '', '', '', 'I love the "bad boys" most- they make you feel crazy and euphoric, they are possessive and tease you mercilessly.... they treat you like princess and can kiss the daylights out of you.... and the madness of loving them is out of the world..!!', 'It does not have to be at CCD....Barista....KFC or any of these places..... just that we should be able to talk.... and about anything and everything from Comic strips to Shakespeare... If things go good, it can be followed by a short walk around the corner and most importantly.... its gotta be with someone who does not pretend to impress.', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(4, '1517545255189394', 0, 1, '2016-01-04', '2017-08-26', 'This users Trial package change by admin on 26-09-2016', '', '2016-11-02 15:21:23', 'julia', '', 'julia@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1953-02-04', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', '1413179574Beautiful-Girl.jpg', '0000-00-00', '1', 1, '2016-10-19 12:06:52', '117.194.16.43', 'Maheshtala', '', '', 'khjhji', '', '', 'Interested In Men and Women', 'julia', '150', 'female', 'single', 'no', '1', 'college graduate', 'hindu', 'yes socially', 'yes socially', 'curvy', 'Analytical and Quiet', 'marriage', 'not sure', 'very attractive', 'Spanish', 'dfdsf', 'dfdsf', 'fdfsdf', 'fsdfsdf', 'gdfgdfg', '', '', 0, '', 0, 0, '', 1, 2, '0000-00-00', '0000-00-00', 0, 1, 60, 'Y', 'W', '2016-10-19 12:00:10'),
(5, '', 0, 1, '2016-01-04', '2017-08-26', '', '', '2016-11-11 06:50:04', 'karuna', '', 'nits.karunadri1@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1956-02-05', 'Mathabhanga-736146', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', 'cb7a8692b1f68218a4b34182d20a4d351_-cartagena.jpg', '2016-09-07', '1', 1, '2016-11-11 06:50:04', '122.160.187.85', 'Matha Bhanga', '', '', '1234567890', '', '', 'Woman Interested In Men', 'karuna', '150', 'female', 'single', 'no', '1', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'Analytical and Quiet', 'marriage', 'no', 'average', 'Hindi', '', '', '', '', '', '', '', 0, '', 0, 0, '', 1, 3, '2016-11-08', '2016-11-29', 1, 0, 0, 'N', 'W', '2016-11-11 06:40:56'),
(6, '', 0, 1, '2015-04-01', '2015-04-30', '', '', '2016-09-22 01:21:47', 'koyel', '', 'nits.koyel@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1991-06-10', '', 'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now u', '1413180181images.jpg', '2016-09-12', '1', 0, '2016-09-16 12:06:06', '122.160.187.85', 'Kolkata', '', '', '1234567890', '', '', '', 'koyel', '150', 'female', 'single', 'no', 'Indian', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'Relaxed and Peaceful', 'relationship', 'yes', 'attractive', 'English', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(7, '', 0, 1, '2015-03-01', '2015-04-29', '', '', '2016-10-27 13:10:52', 'sally', '', 'nitsindranil95@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1964-07-07', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).1', '', '2016-09-09', '1', 0, '2016-10-27 13:10:45', '103.224.129.40', '', '', '', '123456', '', '', '', 'sally', '157', 'female', 'single', 'no', 'Indian', 'advanced degree', 'hindu', 'no', 'yes socially', 'heavy', 'Relaxed and Peaceful', 'marriage', 'within country', 'attractive', 'English', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '2016-10-27 12:41:23'),
(8, '', 0, 1, '2015-04-05', '2015-04-15', '', '', '2016-10-28 01:10:19', 'Abhishek Kundu', '', 'nits.abhishek85@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1998-02-09', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', '07cae5548f849831574d856f1867ed6bPenguins.jpg', '2016-09-11', '1', 0, '2016-09-16 05:59:57', '122.160.187.85', 'holy', '', '', '9876543210', '', '', 'Woman Interested In Men', 'Abhishek Kundu1', '150', 'female', 'single', 'no', '1', 'college graduate', 'hindu', 'yes regularly', 'yes regularly', 'curvy', 'You Need to Find Out', '', '', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.1', '', '', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(11, '', 0, 1, '0000-00-00', '0000-00-00', 'This users Premium Membership package change by admin on 10-03-2015', '', '2016-11-01 19:45:51', 'Abhishek2', '', 'nit.abhishekb@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1990-03-30', '', '', '32cdeded708ceddc5a61b8e71288c7f8test111.jpg', '2016-09-13', '1', 0, '2016-09-26 06:39:33', '122.160.187.85', 'wer', '', '', '9038533045', '', '', 'Interested In Men and Women', 'Tanmoy', '150', 'female', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'relationship', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(26, '', 0, 6, '2016-10-01', '2017-01-01', 'This users Trial package change by admin on 27-09-2016', 'miamor000026', '2016-11-11 07:09:59', 'bikash--', '', 'nits.bikash@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1957-05-07', '', 'jhasbahhabfchbc zskjjaackkbBKBh  al menos dame un call', 'c81241b1b04e33beaac544934c2157d0youth-570881_960_720.jpg', '2015-03-13', '1', 1, '2016-11-11 07:09:59', '122.160.187.85', 'Kolkata', '', '', '9002561033', '', '', 'Man Interested In Men', 'bikash--', '150', '', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'casual fun', '', '', '', 'trabajando', 'todo', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 4, '2016-10-17', '2016-11-17', 0, 0, 0, 'N', 'W', '2016-11-11 06:29:13'),
(31, '', 0, 1, '2015-03-02', '2015-04-22', '', 'miamor000031', '2016-10-06 23:16:18', 'Soumen', '', 'nits.soumenbh8@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1962-05-03', '', '', '', '2015-03-16', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '9831658795', '', '', '', 'Soumen', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(32, '', 0, 1, '2015-04-12', '2015-04-22', '', 'miamor000032', '2016-10-27 12:16:12', 'Saroj Kumar', '', 'nits.saroj@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1957-05-07', '', '', '', '2015-03-16', '1', 0, '2016-10-27 12:16:08', '122.160.187.85', '', '', '', '879875465', '', '', '', 'Saroj', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '2016-10-27 09:35:21'),
(33, '', 0, 1, '2015-03-22', '2015-04-14', '', 'miamor000033', '2016-10-06 23:16:18', 'Tan Ban', '', 'nits.tanmoy@gmail.com', '', '926a26dc284898d2c346154ebe593652', 'M', '1956-05-12', '', '', '', '2015-03-16', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '5879797987', '', '', '', 'Tan', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(46, '1517545255189394', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000046', '2016-10-06 23:17:50', 'Tanmoy Banerjee', '', 'nits.tanmoy@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1988-08-21', '', '', '', '2015-05-25', '1', 0, '2015-09-17 12:19:14', '122.160.187.85', '', '', '', '1234567891', '', '', '', 'Tanmoy', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, 0, 'N', 'W', '0000-00-00 00:00:00'),
(49, '', 0, 1, '0000-00-00', '0000-00-00', 'This users Trial Membership package change by admin on 10-06-2015', 'miamor000049', '2016-10-28 07:27:20', 'Bikash Mondal', '', 'nits.bikash111@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1990-01-27', '', '', '', '2015-06-01', '1', 0, '2015-06-30 10:39:47', '122.160.187.85', 'Kolkata', '', '', '789798789', '', '', '', 'Bik', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(51, '', 0, 2, '0000-00-00', '0000-00-00', '', 'miamor000051', '2016-10-06 23:17:16', 'Abhijit Roy', '', 'nits.abhijit@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1985-08-14', '', '', '', '2015-06-03', '1', 0, '2016-09-21 12:00:54', '122.163.58.63', '', '', '', '8967796895', '', '', '', 'Abhijit', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 60, 'Y', 'W', '0000-00-00 00:00:00'),
(78, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000078', '2016-09-22 00:30:45', 'Sandeep Tewary', '', 'nits.sandeeptewary@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1985-08-14', '', '', '', '2015-06-18', '1', 0, '2016-09-21 12:00:40', '122.163.102.83', 'Kolkata', '', '', '919876543210', '', '', '', 'Sandy', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'test', 'test', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 1, 1, 0, 'Y', 'W', '0000-00-00 00:00:00'),
(55, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000055', '2016-10-06 23:18:39', 'Mamata Ban', '', 'mamataban@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-03-03', '', '', '', '2015-06-08', '0', 0, '2015-06-08 11:13:04', '122.160.187.85', '', '', '', '123456', '', '', '', 'Mamata', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(74, '', 0, 2, '0000-00-00', '0000-00-00', '', 'miamor000074', '2016-10-06 23:18:39', 'Babu', '', 'nits.babu@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-05-03', '', '', '', '2015-06-16', '1', 0, '2016-09-21 11:59:49', '122.160.187.85', '', '', '', '987777777', '', '', '', 'Babu', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 60, 'Y', 'W', '0000-00-00 00:00:00'),
(86, '1033366390025669', 0, 4, '0000-00-00', '0000-00-00', '', '', '2016-10-01 00:52:31', 'Jojo', '', 'jhaoana2118@hotmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1987-07-17', '', 'soy kkasbfb csjckjlfbsddhhfkj  DFKJSCHsKJDNKJSDHVKJSDV csjdhfkskdhczcjhSknckjh v lsjddkjhkvjh kdhvuuhvkjnxckjv jxc', 'dd26471a44a71f1c1966f145a18830f61385710016_kak-virtualnaya-lyubov-mozhet-stat-realnoy.jpg', '2015-06-20', '0', 0, '2016-09-30 12:18:53', '190.27.23.218', 'MedellÃ­n', '', '', '3132265006', '', '', 'Interested In Men and Women', 'Jojo', '170', '', 'single', 'no', '1', 'college', 'catholic', 'no', 'no', 'curvy', 'Relaxed and Peaceful', 'relationship', '', '', '', 'trabajar', 'la cocina', 'mi sonrisa', '', 'en una casa de chance', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, -80, 'Y', 'W', '0000-00-00 00:00:00'),
(87, '1517545255189394', 0, 1, '0000-00-00', '0000-00-00', '', '', '2016-10-06 23:21:41', 'Rahul Roy', '', 'nits.santanu123@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1987-07-17', '', '', '33ead1c375fc5c0ccee455a0727e8fe1logo.png', '2015-06-25', '1', 0, '2016-10-05 10:51:06', '122.163.15.219', '', '', '', '', '', '', 'F', 'Rahul', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 1, '2016-09-01', '2016-09-08', 0, 1, 0, 'N', 'W', '0000-00-00 00:00:00'),
(88, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000088', '2016-10-06 23:21:41', 'Arya Stark', '', 'arya.stark@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1990-03-03', '', '', '', '2015-06-30', '1', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '123456', '', '', '', 'Arya', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(89, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000089', '2016-10-06 23:21:41', 'Ashish Kumar', '', 'nits.ashis@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1986-04-18', '', '', '', '2015-07-09', '0', 0, '2015-09-01 12:13:32', '122.160.187.85', '', '', '', '123456', '', '', '', 'Ashish', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, -5, 'N', 'W', '0000-00-00 00:00:00'),
(92, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000092', '2016-10-06 23:21:41', 'Bik Mon', '', 'nits.bikash111@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1990-01-27', '', '', '', '2015-07-21', '1', 0, '2015-07-21 12:53:25', '182.73.184.114', '', '', '', '845236212', '', '', '', 'Bik', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(94, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000094', '2016-10-06 23:21:41', 'Bik Mon1', '', 'nits.bikash111@yahoo.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1990-01-27', '', '', '', '2015-07-21', '0', 0, '0000-00-00 00:00:00', '182.73.184.114', '', '', '', '845236212', '', '', '', 'Bik', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(101, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000101', '2016-10-06 23:21:41', 'Nits Test', '', 'test@nationalitsolution.net', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1958-06-06', '', '', '', '2015-07-31', '1', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '78454245452', '', '', '', 'Nits', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(104, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000104', '2016-10-06 23:21:41', 'Bikash Mondal', '', 'nits.bikas156h@gmail.com', '', '202cb962ac59075b964b07152d234b70', 'M', '1981-06-10', '', '', '', '2015-08-03', '1', 0, '2015-08-20 15:12:01', '122.160.187.85', '', '', '', '78454245452', '', '', '', 'Bikash', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(174, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000174', '2016-11-02 11:26:02', 'Soumen Deb', '', 'nits.debsoumen@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1985-05-10', '', '', '', '2016-09-26', '1', 1, '2016-09-26 08:42:51', '111.93.169.90', 'Kolkata Updated', '', '', '98563258741', '', '', 'Man Interested In Women', 'Soumen', '150', 'male', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(108, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000108', '2016-10-06 23:21:41', 'Krish Bhattacharya', '', 'nits.krishnendu@gmail.com', '', '202cb962ac59075b964b07152d234b70', 'M', '1989-04-03', '', '', '', '2015-08-12', '1', 0, '2015-08-12 12:50:32', '122.163.97.31', '', '', '', '6777898987', '', '', '', 'Krish', '', 'male', '', '', '', '', '', 'no', 'no', '', 'Pleasure Seeking and Sociable', 'relationship', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(172, '', 0, 4, '0000-00-00', '0000-00-00', '', 'miamor000172', '2016-09-09 02:11:14', 'Sayani M', '', 'nits.sayaninmaiti@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1991-08-22', '', '', '', '2016-09-07', '1', 0, '2016-09-08 13:41:03', '111.93.169.90', 'kolkata', '', '', '1234567890', '', '', 'Woman Interested In Women', 'Sayani M', '150', 'female', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'relationship', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(109, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000109', '2016-10-06 23:21:41', 'BikAsh Mon', '', 'nits.bikash147@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1990-01-27', '', '', '', '2015-08-24', '1', 0, '2015-08-24 06:05:55', '182.73.184.114', '', '', '', '9002561033', '', '', '', 'BikAsh', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(119, '1517545255189394', 0, 1, '0000-00-00', '0000-00-00', '', '', '2016-08-26 22:02:29', 'Rahul', '', 'nits.santanu@gmail.com', '', '', 'M', '1976-05-04', '', '', '', '2015-09-01', '1', 0, '2015-09-01 10:56:02', '122.160.187.85', 'Kolkata', '', '', '845236212', '', '', 'Man Interested In Women', 'Rahul121', '150', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, 0, 'N', 'W', '0000-00-00 00:00:00'),
(115, '1517545255189394', 0, 7, '0000-00-00', '0000-00-00', '', '', '2016-08-26 22:02:32', 'Bikash Mondal', '', 'nits.bikash123@gmail.com', '', '', 'M', '1990-01-04', '', '', '', '2015-08-26', '1', 0, '2015-08-26 10:24:59', '182.73.184.114', 'Kolkata', '', '', '9879854798', '', '', 'Man Interested In Women', 'Biash', '162', 'male', 'single', 'no', 'White/Caucasian', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', 'casual fun', '', '', '', '', '', '', '', '', 'facebook.com', 'twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 1, 0, 'N', 'W', '0000-00-00 00:00:00'),
(116, '', 0, 1, '2015-08-26', '2015-08-26', '', 'miamor000116', '2016-11-03 12:32:16', 'Soumen Porel', '', 'nits.soumen@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1980-03-06', '', '', '', '2015-08-26', '1', 0, '2016-11-03 12:32:11', '182.73.184.114', '', '', '', '78454245452', '', '', '', 'Soumen', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '2016-11-03 12:31:33'),
(118, '', 0, 2, '0000-00-00', '0000-00-00', '', 'miamor000118', '2016-10-06 23:24:11', 'BMondal', '', 'nits.bikash2@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1993-05-26', '', '', '', '2015-09-01', '1', 0, '2015-09-06 12:11:54', '122.160.187.85', '', '', '', '9002561033', '', '', '', 'BMondal', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 60, 'Y', 'W', '0000-00-00 00:00:00'),
(120, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000120', '2016-09-08 23:00:24', 'Ananya', '', 'nits.ananya15@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1951-05-01', '', '', '48608e4682a4bca687e431641354ef06profile-pics-18.jpg', '2015-09-02', '0', 0, '2016-08-05 10:34:46', '122.160.187.85', 'Kolkata', '', '', '9874589689', '', '', 'Woman Interested In Men', 'Atana', '150', 'male', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', '', '', 1, '', 0, 0, '', 0, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(121, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000121', '2016-10-06 23:24:11', 'kallol', '', 'nits.kollok@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1999-02-01', '', '', '', '2015-09-02', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '9874789589', '', '', '', 'kallol', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(122, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000122', '2016-10-06 23:24:11', 'Bikm', '', 'nits.bikash123456@gmail.com', '', '1c3c4d090d570ef2ced26d15be0a2ba7', 'M', '1990-01-27', '', '', '', '2015-09-06', '1', 0, '2015-09-06 15:17:53', '45.124.7.24', '', '', '', '87541452145', '', '', '', 'Bikm', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(124, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000124', '2016-10-06 23:24:11', 'Sansa', '', 'sansa@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1990-05-03', '', '', '', '2015-09-07', '1', 0, '2015-09-07 15:09:57', '122.160.187.85', '', '', '', '845236212', '', '', '', 'Sansa', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(168, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000168', '2016-09-22 00:47:44', 'Karun', '', 'nits.karunadri@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1991-08-13', '', '', '', '2016-09-06', '1', 1, '2016-09-21 12:17:44', '111.93.169.90', 'kolkata', '', '', '1234567890', '', '', 'Interested In Men and Women', 'Karun', '150', 'male', 'single', 'no', '6', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', 'www.facebook.com', 'www.twitter.com', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(151, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000151', '2016-10-06 23:24:11', 'Sujata Das Gupta', '', 'nits.sujata@gmail.com', '', '2b539be26f2684b64d105fb947b522c3', 'F', '1989-10-13', '', '', '', '2016-03-02', '0', 0, '0000-00-00 00:00:00', '122.160.187.85', '', '', '', '', '', '', '', 'Sujata', '', 'female', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(173, '', 0, 4, '0000-00-00', '0000-00-00', '', 'miamor000173', '2016-09-21 21:54:21', 'Bikash11', '', 'nits.bikash11@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1984-06-12', '', '', '', '2016-09-07', '1', 1, '2016-09-08 08:01:22', '111.93.169.90', 'kolkata', '', '', '1234567890', '', '', 'Man Interested In Women', 'Bikash11', '150', 'male', 'single', 'no', '1', 'high school', 'catholic', 'no', 'no', 'slim', 'Pleasure Seeking and Sociable', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00'),
(175, '', 0, 1, '0000-00-00', '0000-00-00', '', 'miamor000175', '2016-10-19 11:49:30', 'Fullcourt', '', 'nits.purbita@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'M', '1952-04-02', '', '', '', '2016-10-19', '1', 0, '0000-00-00 00:00:00', '111.93.169.90', '', '', '', '123456', '', '', '', '', '', 'male', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, '', '', 0, '', 0, 0, '', 1, 0, '0000-00-00', '0000-00-00', 0, 0, 0, 'N', 'W', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_userquery`
--

CREATE TABLE IF NOT EXISTS `dateing_userquery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_userquery`
--

INSERT INTO `dateing_userquery` (`id`, `name`, `email`, `query`, `message`, `date`) VALUES
(3, 'Indranil Gupta', 'nitsindranil94@gmail.com', 'This is my test query', 'This is my test message...', '2015-01-21 07:17:30'),
(4, 'Bikash Mondal', 'nits.bikash@gmail.com', 'Test query', 'Test message...', '2015-01-21 07:06:03'),
(5, 'Bikash Mondal', 'nits.bikash@gmail.com', 'Only for testing purpose.', 'Hi, I am Bikash. This is a demo contact for testing the MiAmor.', '2015-08-24 08:14:34'),
(6, 'BikashMondal', 'nits.bikash@gmail.com', 'Only for testing purpose.', 'gfh fgh fgh fgh fgh  fghfgh', '2015-08-24 11:08:54'),
(7, 'damon', 'damon@optimumstudio.com', 'hola', 'test', '2015-09-11 18:55:57'),
(8, 'Mark', 'markshepparduk@gmail.com', 'Launch', 'Hi there,\r\nI am based in the UK and was wanting to know when they website will be fully functional?\r\n\r\nAs no one can seem to register just yet.\r\n\r\nThanks\r\n\r\nMark ', '2016-09-27 13:33:40'),
(9, 'fffff', 'jhaoana2118@hotmail.com', 'fffdffffff', 'fff', '2016-09-28 18:24:32'),
(10, 'Indranil', 'nitsindranil94@gmail.com', 'Test', 'Test', '2016-10-19 11:00:29'),
(11, 'jojo', 'nitsindranil94@gmail.com', 'Test', 'Test', '2016-10-19 11:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user_1`
--

CREATE TABLE IF NOT EXISTS `dateing_user_1` (
  `id` int(11) NOT NULL,
  `fb_user_id` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `email` varchar(256) NOT NULL,
  `paypal_email` text NOT NULL,
  `password` varchar(256) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `about_me` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `add_date` date NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `is_loggedin` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `ip` varchar(30) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `secret_key` varchar(255) NOT NULL,
  `publishable_key` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `dpname` varchar(200) NOT NULL,
  `height` varchar(200) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `child` varchar(100) NOT NULL,
  `ethnicity` varchar(100) NOT NULL,
  `education` varchar(200) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `smoking` varchar(100) NOT NULL,
  `drinking` varchar(200) NOT NULL,
  `bodytype` varchar(200) NOT NULL,
  `personality` varchar(200) NOT NULL,
  `lookfor` varchar(200) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dateing_user_1`
--

INSERT INTO `dateing_user_1` (`id`, `fb_user_id`, `type`, `fname`, `lname`, `email`, `paypal_email`, `password`, `gender`, `dob`, `address`, `about_me`, `image`, `add_date`, `status`, `is_loggedin`, `last_login`, `ip`, `city`, `state`, `country`, `phone`, `secret_key`, `publishable_key`, `user_type`, `dpname`, `height`, `sex`, `relation`, `child`, `ethnicity`, `education`, `religion`, `smoking`, `drinking`, `bodytype`, `personality`, `lookfor`) VALUES
(1, '', 0, 'Indranil', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'male', '1987-1-1', 'kolkata,india', '', '1413352743profile3.jpg', '0000-00-00', '1', 0, '2014-10-15 01:24:21', '122.160.187.85', '', '', '', '1234567890', '', '', 'man instade in woman', 'Damon', '183', 'male', 'single', 'yes', 'India', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'yes', 'a friend'),
(3, '', 0, 'payle', '', 'talk2payel1987@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1954-2-3', '', '', '1413179314Beautiful-Girl-Face-Pictures_beautiful-girl.jpg', '0000-00-00', '1', 0, '2014-10-13 00:45:48', '117.194.16.43', '', '', '', '213123123123', '', '', '', 'payel', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'no', 'no', 'curvy', 'yes', 'a friend'),
(4, '', 0, 'julia', '', 'julia@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1953-2-3', '', '', '1413179574Beautiful-Girl.jpg', '0000-00-00', '1', 0, '2014-10-13 00:58:15', '117.194.16.43', '', '', '', 'khjhjk', '', '', '', 'julia', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes socially', 'yes', 'curvy', 'yes', 'marriage'),
(5, '', 0, 'karuna', '', 'nits.karunadri@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1964-7-7', '', '', '14131800681484553.jpg', '0000-00-00', '1', 1, '2014-10-15 01:35:52', '122.160.187.85', '', '', '', '1234567890', '', '', '', 'karuna', '150', 'male', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes', 'yes regularly', 'curvy', 'Select your personality', 'casual fun'),
(6, '', 0, 'koyel', '', 'nits.koyel@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '1991-6-10', '', '', '1413180181images.jpg', '0000-00-00', '1', 1, '2014-10-13 01:01:50', '122.160.187.85', '', '', '', '1234567890', '', '', '', 'koyel', '150', 'female', 'single', 'no', 'India', 'college graduate', 'hindu', 'yes', 'yes regularly', 'curvy', 'yes', 'relationship'),
(7, '', 0, 'sally', '', 'nitsindranil94@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'F', '--', '', '', '', '0000-00-00', '0', 0, '0000-00-00 00:00:00', '103.224.129.40', '', '', '', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_user_billing`
--

CREATE TABLE IF NOT EXISTS `dateing_user_billing` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fname` varchar(256) NOT NULL,
  `lname` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `zip` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dateing_viewedme`
--

CREATE TABLE IF NOT EXISTS `dateing_viewedme` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `viewed_user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dateing_viewedme`
--

INSERT INTO `dateing_viewedme` (`id`, `user_id`, `viewed_user_id`, `date`) VALUES
(6, 49, 5, '2015-06-10 23:40:34'),
(7, 49, 1, '2015-06-10 23:40:54'),
(10, 49, 26, '2015-06-10 23:42:13'),
(32, 5, 7, '2015-06-11 23:31:01'),
(53, 1, 44, '2015-06-16 01:25:53'),
(81, 78, 8, '2015-06-20 03:39:37'),
(96, 78, 5, '2015-06-22 23:02:34'),
(103, 1, 45, '2015-06-30 09:37:52'),
(114, 3, 5, '2015-07-01 02:43:50'),
(115, 8, 26, '2015-07-01 02:48:34'),
(118, 1, 41, '2015-07-02 20:18:44'),
(138, 1, 85, '2015-07-14 09:37:55'),
(142, 4, 22, '2015-07-18 00:02:40'),
(164, 5, 75, '2015-07-21 10:35:48'),
(165, 5, 41, '2015-07-21 10:36:04'),
(167, 5, 46, '2015-07-21 10:36:52'),
(209, 104, 3, '2015-08-04 20:56:08'),
(210, 104, 5, '2015-08-05 00:27:14'),
(211, 104, 1, '2015-08-05 01:23:43'),
(212, 5, 55, '2015-08-10 08:37:10'),
(235, 104, 104, '2015-08-11 19:59:02'),
(242, 1, 104, '2015-08-11 23:41:12'),
(255, 1, 51, '2015-08-13 02:41:58'),
(257, 5, 50, '2015-08-15 03:48:28'),
(258, 1, 50, '2015-08-17 19:22:28'),
(259, 1, 22, '2015-08-17 19:24:25'),
(261, 104, 86, '2015-08-21 03:41:54'),
(294, 1, 260, '2015-08-30 05:13:54'),
(302, 1, 116, '2015-08-30 06:30:28'),
(312, 105, 8, '2015-09-01 05:51:05'),
(314, 1, 89, '2015-09-01 06:11:46'),
(330, 1, 87, '2015-09-02 00:11:29'),
(332, 1, 106, '2015-09-02 00:11:42'),
(334, 1, 119, '2015-09-02 00:13:24'),
(336, 1, 115, '2015-09-02 00:13:35'),
(341, 105, 6, '2015-09-04 08:05:21'),
(359, 1, 143, '2015-09-04 20:05:48'),
(363, 1, 157, '2015-09-04 20:46:09'),
(376, 124, 375, '2015-09-08 03:24:26'),
(377, 124, 5, '2015-09-08 03:24:26'),
(379, 1, 124, '2015-09-08 03:50:51'),
(382, 105, 5, '2015-09-08 10:43:53'),
(384, 105, 1, '2015-09-08 10:48:22'),
(385, 1, 7, '2015-09-08 19:11:29'),
(389, 5, 105, '2015-09-09 01:59:14'),
(402, 26, 120, '2015-09-10 06:26:50'),
(438, 26, 369, '2015-09-10 06:38:49'),
(444, 26, 31, '2015-09-10 06:39:29'),
(463, 26, 115, '2015-09-10 06:53:02'),
(467, 105, 3, '2015-09-10 08:20:56'),
(476, 1, 27, '2015-09-10 23:07:05'),
(504, 1, 384, '2015-11-27 18:36:09'),
(509, 4, 105, '2016-01-08 20:35:55'),
(524, 1, 105, '2016-06-02 05:18:32'),
(529, 1, 1, '2016-06-02 09:37:13'),
(532, 1, 11, '2016-06-02 09:48:58'),
(548, 1, 128, '2016-06-02 11:06:33'),
(549, 1, 134, '2016-06-02 11:06:45'),
(550, 1, 120, '2016-06-02 11:06:59'),
(559, 1, 159, '2016-06-02 11:17:49'),
(562, 26, 105, '2016-06-06 10:44:23'),
(571, 26, 542, '2016-06-06 13:47:11'),
(573, 26, 272, '2016-06-06 13:48:33'),
(577, 26, 49, '2016-06-06 13:49:07'),
(579, 26, 128, '2016-06-06 13:53:40'),
(582, 26, 149, '2016-06-06 14:10:41'),
(587, 26, 46, '2016-06-06 14:24:59'),
(602, 86, 3, '2016-06-07 05:01:23'),
(609, 26, 159, '2016-07-29 15:22:44'),
(610, 26, 591, '2016-07-29 15:24:03'),
(616, 161, 78, '2016-08-05 23:22:25'),
(627, 163, 161, '2016-08-06 00:28:11'),
(628, 161, 86, '2016-08-06 01:12:33'),
(629, 161, 159, '2016-08-06 01:12:45'),
(641, 1, 163, '2016-08-26 22:13:15'),
(659, 5, 157, '2016-08-26 23:07:39'),
(670, 5, 161, '2016-08-31 02:23:22'),
(674, 26, 161, '2016-08-31 04:41:15'),
(675, 26, 6, '2016-08-31 05:31:25'),
(704, 163, 1, '2016-09-01 00:00:22'),
(721, 1, 46, '2016-09-01 22:45:33'),
(723, 1, 161, '2016-09-01 22:47:17'),
(725, 161, 1, '2016-09-01 23:53:25'),
(728, 161, 3, '2016-09-01 23:55:51'),
(729, 161, 163, '2016-09-01 23:56:10'),
(744, 22, 86, '2016-09-02 21:53:18'),
(747, 22, 468, '2016-09-02 21:56:25'),
(749, 22, 142, '2016-09-02 21:56:52'),
(750, 22, 4, '2016-09-02 21:56:53'),
(751, 22, 259, '2016-09-02 21:57:08'),
(754, 22, 1, '2016-09-02 22:05:07'),
(755, 22, 26, '2016-09-02 22:11:58'),
(775, 5, 116, '2016-09-02 22:28:56'),
(777, 5, 89, '2016-09-02 22:29:06'),
(801, 4, 86, '2016-09-03 00:21:16'),
(804, 4, 5, '2016-09-03 00:23:51'),
(824, 5, 104, '2016-09-04 02:30:11'),
(831, 1, 26, '2016-09-06 01:58:27'),
(833, 1, 8, '2016-09-06 02:07:56'),
(840, 26, 22, '2016-09-06 10:11:11'),
(844, 5, 6, '2016-09-06 22:21:43'),
(845, 5, 49, '2016-09-06 22:21:43'),
(849, 5, 163, '2016-09-07 00:40:18'),
(856, 26, 78, '2016-09-07 17:52:02'),
(865, 8, 4, '2016-09-07 19:53:51'),
(866, 172, 4, '2016-09-07 20:11:12'),
(874, 173, 7, '2016-09-08 01:14:34'),
(897, 173, 172, '2016-09-08 20:18:10'),
(898, 172, 173, '2016-09-08 20:18:38'),
(903, 5, 95, '2016-09-09 04:22:00'),
(906, 5, 377, '2016-09-09 04:23:35'),
(912, 5, 11, '2016-09-09 04:33:27'),
(913, 5, 8, '2016-09-09 04:33:38'),
(916, 86, 109, '2016-09-09 06:31:59'),
(919, 86, 122, '2016-09-09 06:52:34'),
(922, 86, 120, '2016-09-09 07:23:01'),
(929, 86, 173, '2016-09-09 08:34:40'),
(931, 86, 4, '2016-09-09 09:09:25'),
(933, 86, 161, '2016-09-09 09:25:32'),
(938, 8, 1, '2016-09-16 18:29:06'),
(941, 5, 173, '2016-09-16 19:37:28'),
(961, 86, 26, '2016-09-19 21:55:33'),
(1010, 5, 114, '2016-09-28 03:46:48'),
(1021, 5, 86, '2016-09-28 07:47:14'),
(1027, 5, 124, '2016-09-28 08:12:53'),
(1030, 26, 86, '2016-09-29 06:45:37'),
(1036, 86, 1, '2016-09-30 23:04:20'),
(1037, 86, 172, '2016-10-01 00:09:11'),
(1039, 86, 5, '2016-10-01 00:48:39'),
(1055, 4, 63, '2016-10-06 17:15:11'),
(1067, 1, 4, '2016-10-06 20:38:27'),
(1068, 1, 61, '2016-10-06 22:14:21'),
(1075, 4, 3, '2016-10-06 23:12:37'),
(1076, 1, 78, '2016-10-06 23:51:53'),
(1078, 1, 172, '2016-10-07 00:26:51'),
(1087, 1, 174, '2016-10-14 05:23:16'),
(1100, 1, 5, '2016-10-18 09:00:17'),
(1101, 4, 1, '2016-10-19 10:30:31'),
(1102, 5, 1, '2016-10-22 00:16:48'),
(1104, 5, 78, '2016-10-22 04:19:57'),
(1105, 5, 74, '2016-10-22 04:28:53'),
(1106, 5, 120, '2016-10-22 04:30:21'),
(1108, 5, 118, '2016-10-22 04:31:51'),
(1109, 5, 174, '2016-10-22 04:32:20'),
(1111, 5, 3, '2016-10-22 04:33:50'),
(1114, 1, 86, '2016-10-27 04:03:43'),
(1117, 1, 6, '2016-10-27 04:29:33'),
(1118, 1, 151, '2016-10-27 05:59:28'),
(1121, 1, 74, '2016-10-27 06:00:47'),
(1123, 1, 49, '2016-10-27 06:04:41'),
(1124, 1, 3, '2016-10-27 06:05:26'),
(1129, 26, 168, '2016-10-27 07:52:25'),
(1130, 26, 174, '2016-10-27 07:52:36'),
(1145, 26, 4, '2016-10-27 08:34:54'),
(1159, 5, 4, '2016-10-27 12:32:15'),
(1165, 26, 173, '2016-10-28 10:06:42'),
(1168, 26, 104, '2016-10-28 07:18:54'),
(1171, 26, 961, '2016-10-28 08:20:04'),
(1175, 26, 3, '2016-10-29 00:42:48'),
(1179, 26, 10, '2016-11-02 05:08:38'),
(1180, 26, 831, '2016-11-02 05:08:57'),
(1184, 26, 1178, '2016-11-02 05:12:36'),
(1186, 26, 8, '2016-11-02 06:13:56'),
(1191, 26, 5, '2016-11-03 00:52:45'),
(1193, 26, 1, '2016-11-03 01:00:42'),
(1195, 5, 168, '2016-11-09 05:37:26'),
(1197, 5, 115, '2016-11-09 06:08:25'),
(1199, 5, 33, '2016-11-09 06:10:26'),
(1200, 5, 26, '2016-11-11 06:11:31');

-- --------------------------------------------------------

--
-- Table structure for table `dateing_wishlist`
--

CREATE TABLE IF NOT EXISTS `dateing_wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dating_block_reason`
--

CREATE TABLE IF NOT EXISTS `dating_block_reason` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_block_reason`
--

INSERT INTO `dating_block_reason` (`id`, `user_id`, `reason`, `date`) VALUES
(5, 31, '', '2015-07-24 02:51:17'),
(6, 33, '', '2015-07-24 02:51:28'),
(8, 55, '', '2015-07-24 02:52:16'),
(9, 75, 'test', '2015-07-24 02:52:36'),
(10, 76, 'meri marji', '2015-07-24 03:11:08'),
(11, 22, '', '2015-07-29 11:38:31'),
(12, 1, '', '2016-08-26 20:18:56'),
(13, 157, '', '2016-08-26 20:20:09'),
(14, 5, '', '2016-09-01 19:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `dating_block_user`
--

CREATE TABLE IF NOT EXISTS `dating_block_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `block_to` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dating_chat`
--

CREATE TABLE IF NOT EXISTS `dating_chat` (
  `id` int(10) unsigned NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=667 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_chat`
--

INSERT INTO `dating_chat` (`id`, `from`, `to`, `message`, `sent`, `recd`) VALUES
(1, '1', '8', 'Hi', '2014-11-13 07:03:02', 1),
(2, '8', '1', 'Hello', '2014-11-13 07:04:41', 1),
(3, '1', '8', 'How are you?', '2014-11-13 07:04:55', 1),
(4, '1', '8', '??', '2014-11-13 07:05:27', 1),
(5, '1', '8', 'hi', '2014-11-13 07:06:43', 1),
(6, '8', '1', 'hello', '2014-11-13 07:13:09', 1),
(7, '8', '1', 'Hello...', '2014-11-13 07:21:02', 1),
(8, '1', '8', 'hi', '2014-11-13 07:21:11', 1),
(9, '1', '8', 'hello', '2014-11-13 07:23:10', 1),
(10, '8', '1', 'hi', '2014-11-13 07:24:02', 1),
(11, '8', '1', 'hi', '2014-11-13 07:40:46', 1),
(12, '1', '8', 'hello', '2014-11-13 07:46:34', 1),
(13, '8', '1', 'hi', '2014-11-13 07:47:01', 1),
(14, '8', '1', 'hello', '2014-11-13 07:49:05', 1),
(15, '1', '8', 'hi', '2014-11-13 07:49:12', 1),
(16, '8', '1', 'hi', '2014-11-13 07:52:43', 1),
(17, '1', '4', 'hello', '2014-11-13 07:52:53', 1),
(18, '1', '8', 'hello.............', '2014-11-13 07:53:25', 1),
(19, '8', '1', 'hi', '2014-11-13 07:53:45', 1),
(20, '1', '8', 'hi', '2014-11-13 07:55:00', 1),
(21, '1', '8', 'hola', '2014-11-13 23:26:56', 1),
(22, '1', '8', 'how are you', '2014-11-13 23:27:03', 1),
(23, '1', '3', 'I don''t know', '2014-11-14 01:06:52', 1),
(24, '1', '8', 'hello..this is a test for language change', '2014-11-14 01:18:09', 1),
(25, '1', '8', 'hi', '2014-11-19 02:29:18', 1),
(26, '1', '8', 'hi', '2014-11-19 05:12:01', 1),
(27, '1', '8', 'hi', '2014-11-28 23:40:41', 1),
(28, '1', '8', 'hola baby girl', '2014-11-29 21:48:33', 1),
(29, '8', '1', ':)', '2014-12-06 06:40:36', 1),
(30, '1', '3', 'hello xx', '2014-12-11 14:03:46', 1),
(31, '1', '3', 'hello', '2014-12-11 14:03:59', 1),
(32, '4', '1', 'hi', '2015-02-12 05:37:32', 1),
(33, '4', '1', 'hi', '2015-02-12 05:41:53', 1),
(34, '1', '4', 'dfdfdf', '2015-02-12 05:57:22', 1),
(35, '1', '4', 'hrllo', '2015-02-12 05:57:26', 1),
(36, '1', '4', 'name', '2015-02-12 05:57:35', 1),
(37, '1', '4', 'hello', '2015-02-19 22:24:39', 1),
(38, '1', '4', 'Hello', '2015-02-19 22:24:50', 1),
(39, '12', '11', 'ki re', '2015-02-25 06:05:05', 1),
(40, '11', '12', 'bol bhai', '2015-02-25 06:05:17', 1),
(41, '11', '12', 'kamon achis?', '2015-02-25 06:05:24', 1),
(42, '12', '11', 'ha ha ha', '2015-02-25 06:05:42', 1),
(43, '11', '12', 'ohhh', '2015-02-25 06:05:55', 1),
(44, '12', '11', 'bujhlam', '2015-02-25 06:06:03', 1),
(45, '11', '12', 'hey', '2015-02-25 06:11:06', 1),
(46, '1', '5', 'hello', '2015-03-11 10:15:51', 1),
(47, '1', '5', 'hello', '2015-03-11 10:16:02', 1),
(48, '1', '4', 'hi', '2015-03-13 06:55:28', 1),
(49, '1', '4', ':)', '2015-03-13 06:55:59', 1),
(50, '1', '4', 'dgdfdfdsf', '2015-03-13 06:56:41', 1),
(51, '1', '4', 'ki', '2015-04-06 12:29:12', 1),
(52, '1', '5', 'hello', '2015-04-07 03:03:04', 1),
(53, '1', '4', 'hello', '2015-04-09 11:36:02', 1),
(54, '1', '4', 'kl', '2015-04-13 08:03:10', 1),
(55, '1', '4', 'hello', '2015-04-13 23:05:16', 1),
(56, '1', '4', 'hello', '2015-04-13 23:17:05', 1),
(57, '1', '5', 'hello', '2015-05-19 00:57:37', 1),
(58, '1', '5', 'jguggh', '2015-05-19 15:06:52', 1),
(59, '1', '5', 'hey buddy', '2015-05-25 01:17:20', 1),
(60, '5', '1', 'how are you?', '2015-05-25 01:17:33', 1),
(61, '1', '5', 'I am good', '2015-05-25 01:17:55', 1),
(62, '4', '1', 'hi damon', '2015-05-29 08:11:08', 1),
(63, '4', '1', 'how are you ?', '2015-05-29 08:11:16', 1),
(64, '1', '4', 'i''m good thanks and you?', '2015-05-29 09:36:32', 1),
(65, '1', '5', 'hii', '2015-06-03 00:03:56', 1),
(66, '8', '1', 'hii', '2015-06-03 05:27:51', 1),
(67, '1', '8', 'hello', '2015-06-03 05:28:05', 1),
(68, '1', '8', 'how are you?', '2015-06-03 05:28:42', 1),
(69, '8', '1', 'I am fine how are you?', '2015-06-03 05:28:53', 1),
(70, '8', '1', 'hii', '2015-06-03 05:32:15', 1),
(71, '8', '1', 'hello', '2015-06-03 05:32:46', 1),
(72, '1', '8', 'how are you?', '2015-06-03 05:32:56', 1),
(73, '1', '8', 'hiiiiiiii', '2015-06-03 05:33:36', 1),
(74, '8', '1', 'hii', '2015-06-03 05:34:38', 1),
(75, '1', '8', 'hello', '2015-06-03 05:34:44', 1),
(76, '8', '1', 'how are you?', '2015-06-03 05:35:18', 1),
(77, '1', '8', 'I am fine', '2015-06-03 05:35:25', 1),
(78, '8', '1', 'hii', '2015-06-03 05:38:53', 1),
(79, '1', '8', 'hello', '2015-06-03 05:38:59', 1),
(80, '5', '1', 'hi', '2015-06-07 00:07:39', 1),
(81, '1', '4', 'hi', '2015-06-07 04:00:14', 1),
(82, '5', '1', 'hi', '2015-06-08 03:22:51', 1),
(83, '5', '1', 'hello', '2015-06-08 03:23:16', 1),
(84, '1', '5', 'hi', '2015-06-08 03:54:03', 1),
(85, '1', '4', 'helo', '2015-06-08 03:54:14', 1),
(86, '3', '5', 'hii', '2015-06-11 04:15:04', 1),
(87, '5', '3', 'hello', '2015-06-11 04:15:15', 1),
(88, '3', '5', 'how are you?', '2015-06-11 04:15:41', 1),
(89, '5', '3', 'I am fine', '2015-06-11 04:15:51', 1),
(90, '3', '5', 'cool', '2015-06-11 04:15:56', 1),
(91, '3', '5', 'hiiiii', '2015-06-11 04:16:48', 1),
(92, '5', '3', 'hello', '2015-06-11 04:16:57', 1),
(93, '3', '5', 'hello', '2015-06-11 04:20:11', 1),
(94, '3', '5', 'hiiii', '2015-06-11 04:20:38', 1),
(95, '5', '3', 'how are you?', '2015-06-11 04:20:46', 1),
(96, '5', '3', 'jghjghjkhgkj', '2015-06-11 04:53:24', 1),
(97, '3', '5', 'hi', '2015-06-11 05:03:24', 1),
(98, '3', '5', 'hi', '2015-06-11 05:05:53', 1),
(99, '3', '5', 'hi', '2015-06-11 05:06:37', 1),
(100, '3', '5', 'fggfg', '2015-06-11 05:06:44', 1),
(101, '3', '5', 'hgj', '2015-06-11 05:07:10', 1),
(102, '3', '5', 'gfhgh', '2015-06-11 05:09:11', 1),
(103, '3', '5', 'gh', '2015-06-11 05:09:32', 1),
(104, '3', '5', 'fchf', '2015-06-11 05:12:25', 1),
(105, '3', '5', 'dgdfgfdgfh', '2015-06-11 05:19:53', 1),
(106, '3', '5', 'hiiii', '2015-06-11 05:41:58', 1),
(107, '1', '5', 'hi', '2015-06-15 06:03:50', 1),
(108, '1', '5', 'hi', '2015-06-15 06:05:22', 1),
(109, '3', '5', 'hi', '2015-06-15 06:10:12', 1),
(110, '3', '5', 'vxsdfdsfs', '2015-06-15 06:10:39', 1),
(111, '3', '5', 'dsdsadsad', '2015-06-15 06:10:42', 1),
(112, '3', '5', 'gfhgfhgfh', '2015-06-15 06:11:33', 1),
(113, '3', '5', 'hi', '2015-06-15 06:18:50', 1),
(114, '3', '5', 'hi', '2015-06-15 06:22:21', 1),
(115, '3', '5', 'sdsfsaf', '2015-06-15 06:28:29', 1),
(116, '3', '5', 'sdsd', '2015-06-15 06:28:40', 1),
(117, '3', '5', 'vbfxcbfvbfd', '2015-06-15 06:40:19', 1),
(118, '3', '5', 'gfdgfdg'']', '2015-06-15 06:40:24', 1),
(119, '3', '5', 'dfvvdfsg', '2015-06-15 06:44:34', 1),
(120, '3', '5', 'i[io[io', '2015-06-15 06:44:37', 1),
(121, '3', '5', 'wewqewqe', '2015-06-15 06:55:56', 1),
(122, '3', '5', 'ewrewrewrew', '2015-06-15 06:55:58', 1),
(123, '3', '5', 'hi', '2015-06-15 08:19:29', 1),
(124, '3', '5', 'hi', '2015-06-15 08:23:30', 1),
(125, '8', '1', 'hi', '2015-06-30 07:22:35', 1),
(126, '8', '1', 'hello', '2015-06-30 07:23:42', 1),
(127, '8', '1', 'hii', '2015-06-30 07:27:17', 1),
(128, '8', '1', 'hello', '2015-06-30 07:29:24', 1),
(129, '1', '4', 'hi', '2015-07-02 01:20:14', 1),
(130, '1', '4', 'hi', '2015-07-02 01:27:21', 1),
(131, '1', '4', 'hi', '2015-07-02 02:30:43', 1),
(132, '1', '4', 'Hel', '2015-07-02 02:32:50', 1),
(133, '1', '4', 'sadsfsdf', '2015-07-02 02:38:18', 1),
(134, '1', '4', 'again', '2015-07-02 02:41:11', 1),
(135, '1', '4', 'hi', '2015-07-02 02:42:59', 1),
(136, '1', '4', 'hello', '2015-07-13 13:28:28', 1),
(137, '1', '4', 'hiiiiiiiiii', '2015-07-17 06:10:37', 1),
(138, '1', '4', 'hi', '2015-07-17 07:28:43', 1),
(139, '3', '5', 'hi', '2015-07-20 02:56:37', 1),
(140, '105', '5', 'kjhjhhjhj', '2015-08-09 13:47:55', 1),
(141, '5', '105', 'hola', '2015-08-09 13:48:47', 1),
(142, '105', '5', 'hola amor como estas', '2015-08-09 13:49:00', 1),
(143, '1', '5', 'hi', '2015-08-12 00:13:44', 1),
(144, '1', '5', 'hi', '2015-08-12 00:41:09', 1),
(145, '1', '5', 'hello', '2015-08-12 00:43:26', 1),
(146, '1', '5', 'he..............', '2015-08-12 00:51:48', 1),
(147, '1', '5', 'hi', '2015-08-12 00:58:07', 1),
(148, '1', '5', 'hello', '2015-08-12 02:41:44', 1),
(149, '1', '5', 'hhhhhhhhhhhhhhh', '2015-08-12 02:41:50', 1),
(150, '1', '5', 'hiiiii', '2015-08-12 02:43:25', 1),
(151, '5', '1', 'hi', '2015-08-12 02:45:22', 1),
(152, '1', '5', 'How are you?', '2015-08-12 02:51:46', 1),
(153, '5', '1', 'good', '2015-08-12 02:52:07', 1),
(154, '1', '5', 'hi hi hi hi hi hi hi hi hi hi', '2015-08-12 02:53:28', 1),
(155, '1', '5', 'are you ok?', '2015-08-12 03:00:12', 1),
(156, '5', '1', 'yes', '2015-08-12 03:01:49', 1),
(157, '1', '5', 'Thanks', '2015-08-12 03:06:55', 1),
(158, '1', '5', 'next plan?', '2015-08-12 03:08:52', 1),
(159, '5', '1', 'Will inform you..', '2015-08-12 03:10:33', 1),
(160, '1', '5', 'hi', '2015-08-12 03:20:42', 1),
(161, '5', '1', 'hi', '2015-08-12 04:35:23', 1),
(162, '5', '1', '??', '2015-08-12 04:35:36', 1),
(163, '5', '1', '??', '2015-08-12 04:35:37', 1),
(164, '5', '1', '??', '2015-08-12 04:35:39', 1),
(165, '5', '1', '???', '2015-08-12 04:35:40', 1),
(166, '5', '1', '???', '2015-08-12 04:35:41', 1),
(167, '5', '1', 'hi', '2015-08-24 02:34:15', 1),
(168, '5', '1', 'hi', '2015-08-26 05:52:22', 1),
(169, '105', '1', 'hello', '2015-08-29 11:20:01', 1),
(170, '105', '1', '=)', '2015-08-29 11:20:20', 1),
(171, '1', '105', 'hola hermosa', '2015-08-29 11:20:20', 1),
(172, '105', '1', 'te amo hermoso', '2015-08-29 11:22:57', 1),
(173, '105', '1', 'hello', '2015-08-29 11:23:25', 1),
(174, '1', '105', 'Hello darling girl', '2015-08-29 11:31:04', 1),
(175, '105', '1', 'tu no love me nada', '2015-08-29 11:32:11', 1),
(176, '105', '1', 've tal', '2015-08-29 11:32:13', 1),
(177, '1', '105', 'tu quieres comprar un muffin', '2015-08-29 11:44:06', 1),
(178, '1', '4', 'hola', '2015-08-31 10:30:23', 1),
(179, '1', '4', 'hello', '2015-09-01 11:51:02', 1),
(180, '1', '4', 'hola', '2015-09-05 07:44:41', 1),
(181, '1', '4', 'hi', '2015-09-06 22:59:29', 1),
(182, '1', '4', 'hi', '2015-09-06 23:13:23', 1),
(183, '1', '4', 'how are you', '2015-09-06 23:13:31', 1),
(184, '4', '1', 'hi', '2015-09-06 23:15:35', 1),
(185, '1', '4', 'hi', '2015-09-06 23:16:03', 1),
(186, '4', '1', 'hi', '2015-09-06 23:20:13', 1),
(187, '4', '1', 'hellooo', '2015-09-06 23:20:34', 1),
(188, '4', '1', 'are you there?', '2015-09-06 23:22:35', 1),
(189, '1', '4', 'hi', '2015-09-07 00:18:27', 1),
(190, '1', '4', 'how are you', '2015-09-07 00:18:36', 1),
(191, '4', '1', 'hi', '2015-09-07 00:21:01', 1),
(192, '4', '1', 'i am fine', '2015-09-07 00:22:41', 1),
(193, '4', '1', 'how are you?', '2015-09-07 00:22:48', 1),
(194, '1', '4', 'i am good', '2015-09-07 00:25:41', 1),
(195, '1', '4', 'what about u ?', '2015-09-07 00:27:45', 1),
(196, '1', '4', 'r u there?', '2015-09-07 00:29:21', 1),
(197, '1', '4', 'hey', '2015-09-07 00:34:37', 1),
(198, '1', '4', 'hi', '2015-09-07 00:36:36', 1),
(199, '1', '4', 'hello', '2015-09-07 00:36:43', 1),
(200, '1', '4', 'hey', '2015-09-07 00:38:41', 1),
(201, '1', '4', 'new one', '2015-09-07 00:40:09', 1),
(202, '1', '4', 'hey', '2015-09-07 07:42:43', 1),
(203, '4', '1', 'hi', '2015-09-07 07:45:08', 1),
(204, '1', '4', 'hello', '2015-09-07 08:02:26', 1),
(205, '1', '4', 'hi', '2015-09-07 13:39:27', 1),
(206, '4', '1', 'hello', '0000-00-00 00:00:00', 1),
(207, '4', '1', 'hey', '0000-00-00 00:00:00', 1),
(208, '4', '1', 'hello', '0000-00-00 00:00:00', 1),
(209, '4', '1', 'r u therer', '0000-00-00 00:00:00', 1),
(210, '1', '4', 'i am here', '2015-09-07 13:53:19', 1),
(211, '4', '1', 'why site is not working?', '0000-00-00 00:00:00', 1),
(212, '1', '4', 'how can i know?', '2015-09-07 13:55:39', 1),
(213, '1', '4', 'you should know', '2015-09-07 13:59:00', 1),
(214, '4', '1', 'i don''t think so', '2015-09-07 14:00:36', 1),
(215, '1', '5', 'hi', '2015-09-07 16:36:20', 1),
(216, '124', '5', 'hi', '2015-09-07 17:21:13', 1),
(217, '5', '124', 'hello', '2015-09-07 17:21:52', 1),
(218, '5', '105', 'Te Amo', '2015-09-07 14:33:59', 1),
(219, '105', '5', 'que tal', '2015-09-07 14:33:29', 1),
(220, '4', '1', ':)', '2015-09-09 13:59:58', 1),
(221, '4', '1', ':-)', '2015-09-09 15:08:29', 1),
(222, '4', '1', ':)', '2015-09-09 15:12:21', 1),
(223, '1', '4', 'hiiii', '2015-09-09 15:26:15', 1),
(224, '4', '1', 'heloo', '2015-09-09 15:26:23', 1),
(225, '4', '1', ':)', '2015-09-09 15:26:26', 1),
(226, '4', '1', ':)', '2015-09-09 15:35:06', 1),
(227, '4', '1', ':-)', '2015-09-09 15:35:14', 1),
(228, '4', '1', ':(', '2015-09-09 15:36:25', 1),
(229, '4', '1', ':)', '2015-09-09 15:57:26', 1),
(230, '1', '4', 'dhur', '2015-09-09 15:57:40', 1),
(231, '1', '4', ':(', '2015-09-09 16:00:55', 1),
(232, '4', '1', ':)', '2015-09-09 16:01:28', 1),
(233, '4', '1', ':)', '2015-09-09 16:21:41', 1),
(234, '1', '4', ':)', '2015-09-09 16:25:20', 1),
(235, '1', '4', ':(', '2015-09-09 16:27:16', 1),
(236, '4', '1', ':)', '2015-09-09 16:30:14', 1),
(237, '1', '4', ':(', '2015-09-09 16:33:04', 1),
(238, '', '4', ':)', '2015-09-09 16:35:59', 1),
(239, '1', '4', ':)', '2015-09-09 16:36:55', 1),
(240, '1', '4', ':(', '2015-09-09 16:37:21', 1),
(241, '4', '1', ':)', '2015-09-09 16:37:30', 1),
(242, '4', '1', ':(', '2015-09-09 16:37:38', 1),
(243, '1', '4', ':)', '2015-09-09 16:37:49', 1),
(244, '1', '4', ':(', '2015-09-09 16:39:27', 1),
(245, '4', '1', ':)', '2015-09-09 16:39:45', 1),
(246, '4', '1', ':-P', '2015-09-09 16:39:57', 1),
(247, '4', '1', '<3', '2015-09-09 16:40:11', 1),
(248, '4', '1', ':)', '2015-09-09 16:40:24', 1),
(249, '4', '1', ':(', '2015-09-09 16:40:30', 1),
(250, '1', '4', ':)', '2015-09-09 16:40:40', 1),
(251, '4', '1', ':)', '2015-09-09 16:40:58', 1),
(252, '4', '1', ':) :)', '2015-09-09 16:42:59', 1),
(253, '1', '4', ':)', '2015-09-09 16:43:35', 1),
(254, '4', '1', ':(', '2015-09-09 16:46:57', 1),
(255, '1', '4', ':)', '2015-09-09 16:47:09', 1),
(256, '4', '1', ':)', '2015-09-09 16:47:22', 1),
(257, '4', '1', ':)', '2015-09-09 16:53:58', 1),
(258, '4', '1', ':)', '2015-09-09 16:55:50', 1),
(259, '4', '1', ':)', '2015-09-09 17:01:25', 1),
(260, '1', '4', ':(', '2015-09-09 17:01:48', 1),
(261, '1', '4', ':(', '2015-09-09 17:01:51', 1),
(262, '1', '4', ':)', '2015-09-09 17:02:06', 1),
(263, '4', '1', ':)', '2015-09-09 19:09:41', 1),
(264, '4', '1', 'hii', '2015-09-09 19:18:05', 1),
(265, '1', '4', 'hii', '2015-09-09 19:58:50', 1),
(266, '26', '105', 'hello :-)', '2015-09-09 14:12:09', 1),
(267, '105', '26', 'hello', '2015-09-09 14:11:33', 1),
(268, '26', '105', 'te amo', '2015-09-09 14:12:47', 1),
(269, '105', '26', 'no', '2015-09-09 14:12:00', 1),
(270, '5', '1', 'hi', '2015-09-10 11:18:57', 1),
(271, '1', '5', ':)', '2015-09-10 11:23:14', 1),
(272, '1', '5', 'hi', '2015-09-10 12:05:39', 1),
(273, '1', '5', ':-)', '2015-09-10 12:06:06', 1),
(274, '1', '5', ':-) hello :-Ãž', '2015-09-10 12:09:23', 1),
(275, '1', '5', ':&gt;=]', '2015-09-10 12:09:31', 1),
(276, '1', '5', 'hi=D', '2015-09-10 12:09:35', 1),
(277, '1', '5', ';) ;)', '2015-09-10 12:10:07', 1),
(278, '1', '5', 'hello 8O', '2015-09-10 12:10:13', 1),
(279, '1', '5', '=/', '2015-09-10 12:10:17', 1),
(280, '1', '5', ':-)', '2015-09-10 12:11:41', 1),
(281, '1', '5', ':o)', '2015-09-10 12:11:45', 1),
(282, '5', '1', ':-\\', '2015-09-10 12:14:09', 1),
(283, '1', '5', 'XD', '2015-09-10 12:22:53', 1),
(284, '1', '5', ':P', '2015-09-10 12:22:58', 1),
(285, '1', '5', 'fgdgdfgdfgdfgdf fdg dfg dfgfdgdfg dfgdfg gdfg  :/', '2015-09-10 12:27:58', 1),
(286, '1', '5', 'fdg fdg dfgd XD', '2015-09-10 12:28:05', 1),
(287, '5', '1', ';D', '2015-09-10 12:28:40', 1),
(288, '5', '1', 'hello  :-)', '2015-09-10 12:28:47', 1),
(289, '1', '5', 'dsf sdfsdf  8D', '2015-09-10 12:45:19', 1),
(290, '5', '1', ';D', '2015-09-10 12:45:56', 1),
(291, '1', '5', 'hiu', '2015-09-10 12:55:50', 1),
(292, '5', '1', 'from karun', '2015-09-10 12:58:50', 1),
(293, '5', '1', 'hello', '2015-09-10 13:11:23', 1),
(294, '5', '1', 'hjgjghj', '2015-09-10 13:11:40', 1),
(295, '5', '1', 'ghjhg', '2015-09-10 13:11:41', 1),
(296, '5', '1', 'jghj', '2015-09-10 13:11:41', 1),
(297, '5', '1', 'ghj', '2015-09-10 13:11:41', 1),
(298, '5', '1', 'ghjgh', '2015-09-10 13:11:42', 1),
(299, '5', '1', 'jhg', '2015-09-10 13:11:42', 1),
(300, '5', '1', 'jghj', '2015-09-10 13:11:43', 1),
(301, '5', '1', 'ghjghjgh', '2015-09-10 13:11:46', 1),
(302, '5', '1', 'hgj', '2015-09-10 13:11:46', 1),
(303, '5', '1', 'ghj', '2015-09-10 13:11:46', 1),
(304, '5', '1', 'ghj', '2015-09-10 13:11:47', 1),
(305, '5', '1', 'j', '2015-09-10 13:11:47', 1),
(306, '5', '1', 'ghjg', '2015-09-10 13:11:47', 1),
(307, '5', '1', 'dfgfdgd', '2015-09-10 13:14:25', 1),
(308, '5', '1', 'fdgfd', '2015-09-10 13:14:27', 1),
(309, '5', '1', 'fdgdf', '2015-09-10 13:14:29', 1),
(310, '5', '1', 'fdgfd', '2015-09-10 13:14:31', 1),
(311, '5', '1', 'dfgfdg', '2015-09-10 13:14:33', 1),
(312, '5', '1', 'dfg', '2015-09-10 13:14:47', 1),
(313, '5', '1', 'fdg', '2015-09-10 13:14:49', 1),
(314, '5', '1', 'fdgdf', '2015-09-10 13:14:51', 1),
(315, '5', '1', 'fdgdfg', '2015-09-10 13:14:52', 1),
(316, '5', '1', 'hgjghj', '2015-09-10 13:15:10', 1),
(317, '5', '1', 'vbnvbn', '2015-09-10 13:16:27', 1),
(318, '5', '1', 'vbnvn', '2015-09-10 13:16:32', 1),
(319, '5', '1', 'ghjghj', '2015-09-10 13:16:38', 1),
(320, '5', '1', ':)', '2015-09-10 13:17:55', 1),
(321, '1', '5', 'hii', '2015-09-10 13:17:55', 1),
(322, '5', '1', ':o)', '2015-09-10 13:17:59', 1),
(323, '1', '5', ':)', '2015-09-10 13:17:59', 1),
(324, '1', '5', 'great', '2015-09-10 13:18:03', 1),
(325, '1', '5', ':o)', '2015-09-10 13:18:08', 1),
(326, '5', '26', ':-\\', '2015-09-10 13:37:03', 1),
(327, '5', '1', 'test :O', '2015-09-10 14:09:21', 1),
(328, '5', '105', 'hola darling', '2015-09-15 12:26:34', 1),
(329, '105', '5', ':-)', '2015-09-15 12:26:12', 1),
(330, '5', '105', 'hola', '2015-09-15 12:27:35', 1),
(331, '5', '105', 'ugugiuhiuh', '2015-09-15 12:29:54', 0),
(332, '5', '1', 'hi', '2015-09-17 17:00:29', 1),
(333, '1', '105', ':o)', '2015-11-27 11:36:17', 0),
(334, '1', '105', '8-) 8-)', '2015-11-27 11:41:36', 0),
(335, '1', '4', 'hi', '2016-06-01 16:04:31', 1),
(336, '1', '4', 'hi', '2016-06-01 16:06:06', 1),
(337, '1', '8', 'hi', '2016-06-01 16:12:24', 1),
(338, '26', '86', 'Te Amo', '2016-06-05 17:59:33', 1),
(339, '86', '26', 'no mentirosito', '2016-06-05 17:59:42', 1),
(340, '86', '26', '8O', '2016-06-05 17:59:52', 1),
(341, '86', '26', ':-p', '2016-06-05 18:00:01', 1),
(342, '26', '86', '=(', '2016-06-05 18:05:45', 1),
(343, '26', '86', 'TE AMO OK', '2016-06-05 18:05:55', 1),
(344, '86', '26', 've que tal', '2016-06-05 18:06:24', 1),
(345, '26', '86', 'hello darling', '2016-06-05 18:10:30', 1),
(346, '26', '86', 'is this all coming through ok', '2016-06-05 18:10:38', 1),
(347, '86', '26', 'hello', '2016-06-05 20:24:13', 1),
(348, '86', '26', 'te amo mi hermoso', '2016-06-05 20:26:47', 1),
(349, '5', '86', 'hola chica', '2016-06-06 10:51:54', 1),
(350, '86', '5', ':/', '2016-06-06 10:51:58', 1),
(351, '26', '5', 'hola', '2016-08-30 09:11:19', 1),
(352, '5', '26', 'hola', '2016-08-30 09:12:11', 1),
(353, '26', '5', 'porque no puedo hablar con jojo me gusta ella', '2016-08-30 09:13:57', 1),
(354, '5', '26', 'hola', '2016-08-30 09:14:48', 1),
(355, '26', '5', '=(', '2016-08-30 09:19:03', 1),
(356, '5', '26', 'hola bikash', '2016-08-30 11:21:43', 1),
(357, '26', '5', 'hola', '2016-08-30 11:21:53', 1),
(358, '26', '5', 'estas bueno', '2016-08-30 11:21:57', 1),
(359, '26', '5', ':O', '2016-08-30 11:22:11', 1),
(360, '26', '5', 'jjj', '2016-08-30 11:31:41', 1),
(361, '1', '26', 'dfgdgfdgf', '0000-00-00 00:00:00', 1),
(362, '1', '26', 'uittuytuyt', '0000-00-00 00:00:00', 1),
(363, '26', '5', 'si tu eres soltera vamos a darle candela....pegaito ahi', '2016-08-31 10:19:09', 1),
(364, '26', '5', 'una vaina loca que me lleva a la gloria....cuando yo siento eso..cucuccuc', '2016-08-31 10:19:45', 1),
(365, '26', '5', 'si lo hacemos en mi caroo va ser brummmm', '2016-08-31 10:20:01', 1),
(366, '26', '5', 'ella me descontrola cuando estamos a solass', '2016-08-31 10:20:14', 1),
(367, '5', '26', ':Ãž :c) :^) :-D :-( :-X :&gt;', '2016-08-31 10:27:06', 1),
(368, '26', '5', '8O', '2016-08-31 10:31:42', 1),
(369, '5', '26', 'ibihoiu', '2016-08-31 10:40:21', 1),
(370, '1', '5', 'hgfhgfh', '0000-00-00 00:00:00', 1),
(371, '26', '5', 'hhhhh', '0000-00-00 00:00:00', 1),
(372, '1', '4', 'hi..', '0000-00-00 00:00:00', 1),
(373, '1', '4', 'hi..', '0000-00-00 00:00:00', 1),
(374, '4', '1', 'hi..', '2016-09-02 17:37:00', 1),
(375, '4', '1', 'hello...are there???', '2016-09-02 17:37:31', 1),
(376, '4', '1', 'yes....i''m here', '2016-09-02 17:39:46', 1),
(377, '1', '4', 'hello', '0000-00-00 00:00:00', 1),
(378, '1', '4', 'hiiiii...', '0000-00-00 00:00:00', 1),
(379, '1', '4', 'dfgdgdf', '0000-00-00 00:00:00', 1),
(380, '1', '4', 'hiiii...', '0000-00-00 00:00:00', 1),
(381, '4', '1', 'hiiii..', '2016-09-02 17:48:10', 1),
(382, '1', '4', 'ftsdfsdfjhsdfgjsdfh', '0000-00-00 00:00:00', 1),
(383, '26', '5', 'hola amor mio', '0000-00-00 00:00:00', 1),
(384, '26', '5', 'como estas?', '0000-00-00 00:00:00', 1),
(385, '26', '5', 'kjhgghjbnm', '0000-00-00 00:00:00', 1),
(386, '26', '5', 'bvcbvc', '0000-00-00 00:00:00', 1),
(387, '', '26', 'hola Bikash', '2016-09-05 16:41:18', 1),
(388, '26', '', 'tu hola', '0000-00-00 00:00:00', 1),
(389, '26', '', 'kÃ±jsdfhsb', '0000-00-00 00:00:00', 1),
(390, '26', '', 'NBDCMNBZNMCB', '0000-00-00 00:00:00', 1),
(391, '26', '', 'ASBJAHDGSDB', '0000-00-00 00:00:00', 1),
(392, '', '26', 'vhvuhb', '2016-09-05 16:48:20', 1),
(393, '26', '', 'kjadfckjsdb', '0000-00-00 00:00:00', 1),
(394, '26', '', 'xcbzmncb mnzb', '0000-00-00 00:00:00', 1),
(395, '173', '172', 'fdgfdgdgfd', '2016-09-07 15:02:26', 1),
(396, '172', '173', '161', '2016-09-07 15:02:50', 1),
(397, '172', '173', '2131', '2016-09-07 15:02:52', 1),
(398, '172', '173', '123', '2016-09-07 15:02:54', 1),
(399, '173', '172', 'dfsdfgdgfdgfd', '2016-09-07 15:03:17', 1),
(400, '172', '173', '21', '2016-09-07 15:04:00', 1),
(401, '172', '173', '213', '2016-09-07 15:04:01', 1),
(402, '172', '173', '12312', '2016-09-07 15:04:02', 1),
(403, '172', '173', '12312', '2016-09-07 15:04:03', 1),
(404, '5', '86', 'hello', '2016-09-08 11:15:44', 1),
(405, '5', '86', 'guygiuhiuh', '2016-09-08 11:16:13', 1),
(406, '86', '5', ':Ãž', '2016-09-08 11:19:22', 1),
(407, '5', '86', 'porque tu no va hablar conmigo ?', '2016-09-08 11:22:41', 1),
(408, '86', '5', 'porque yo necesito hacer face primero', '2016-09-08 11:23:29', 1),
(409, '5', '86', 'Tu no ama mi o que?', '2016-09-08 11:24:45', 1),
(410, '86', '5', 'no', '2016-09-08 11:27:48', 1),
(411, '86', '5', 'http://www.mundofitness.com/rutina-de-tonificar-brazos-para-mujeres/', '2016-09-08 11:27:51', 1),
(412, '86', '5', 'hola', '2016-09-08 13:33:53', 1),
(413, '86', '5', ';)', '2016-09-08 14:08:33', 1),
(414, '86', '26', 'estas muy bueno', '2016-09-08 15:51:04', 1),
(419, '3', '5', 'qqq', '2016-09-16 12:45:10', 1),
(420, '86', '26', 'test 4545', '2016-09-16 12:58:06', 1),
(421, '86', '26', 'test uioo', '2016-09-16 12:58:35', 1),
(422, '86', '26', 'test 333', '2016-09-16 12:59:32', 1),
(423, '86', '26', 'trrewewrwr', '2016-09-16 12:59:51', 1),
(424, '86', '26', 'bdfbdbdbfdbdf', '2016-09-16 12:59:58', 1),
(425, '86', '26', 'aaaasa', '2016-09-16 13:00:21', 1),
(426, '86', '26', 'rewrwrewrw', '2016-09-16 13:00:36', 1),
(427, '86', '26', 'casasassasasafas', '2016-09-16 13:01:56', 1),
(428, '86', '26', 'bdbfbfdb', '2016-09-16 13:03:28', 1),
(429, '86', '26', 'eee', '2016-09-16 13:20:34', 1),
(430, '26', '86', 'daadwdwqd', '2016-09-16 13:21:01', 1),
(431, '26', '86', 'fesfeffe', '2016-09-16 13:23:04', 1),
(432, '26', '86', 'sasaasdsfaa', '2016-09-16 14:11:05', 1),
(433, '86', '26', 'dasadasad', '2016-09-16 14:11:15', 1),
(434, '86', '26', 'sdcdscscdscs', '2016-09-16 14:12:12', 1),
(435, '26', '86', 'ddwqqdq', '2016-09-16 14:12:39', 1),
(436, '26', '86', 'asfasadad', '2016-09-16 14:13:05', 1),
(437, '26', '86', 'sssss', '2016-09-16 14:13:24', 1),
(438, '26', '86', 'dsadadsad', '2016-09-16 14:13:55', 1),
(439, '86', '26', 'jhgjgjkgjk', '2016-09-16 14:17:46', 1),
(440, '86', '26', 'csaacacsa', '2016-09-16 14:18:02', 1),
(441, '86', '26', 'qqqqq', '2016-09-16 14:18:30', 1),
(442, '86', '26', 'aaaaaaaaaaa', '2016-09-16 14:18:44', 1),
(443, '86', '26', 'ddddddddddd', '2016-09-16 14:18:59', 1),
(444, '86', '26', 'asasasasas', '2016-09-16 14:19:14', 1),
(445, '26', '86', 'qqqqq', '2016-09-16 14:22:58', 1),
(446, '26', '86', 'rwrwewerwe', '2016-09-16 14:26:16', 1),
(447, '26', '86', 'asasasas', '2016-09-16 14:26:38', 1),
(448, '26', '86', 'hi', '2016-09-16 14:26:58', 1),
(449, '26', '86', 'dsfssdds', '2016-09-16 14:27:23', 1),
(450, '26', '86', 'dawdada', '2016-09-16 14:27:38', 1),
(451, '26', '86', 'ghfhfhgf', '2016-09-16 14:28:00', 1),
(452, '86', '26', ':-D', '2016-09-16 14:47:48', 1),
(453, '86', '26', 'trtt', '2016-09-16 15:08:19', 1),
(454, '86', '26', 'yuuyuu', '2016-09-16 15:14:09', 1),
(455, '5', '26', 'hola', '2016-09-16 13:53:58', 1),
(456, '26', '5', 'hola', '2016-09-16 13:54:10', 1),
(457, '26', '5', 'dff', '2016-09-19 10:55:53', 1),
(458, '5', '26', 'sddsdsds', '2016-09-19 10:56:17', 1),
(459, '26', '5', 'asdfdas', '2016-09-19 10:56:39', 1),
(460, '5', '26', 'HOLA HERMOSO ESPOSO', '2016-09-20 13:51:52', 1),
(461, '5', '26', 'te amo hermoso', '2016-09-20 13:51:58', 1),
(462, '5', '26', 'sos mi todo', '2016-09-20 13:52:02', 1),
(463, '5', '26', '=(', '2016-09-20 13:53:25', 1),
(464, '5', '26', 'uh que tal', '2016-09-20 13:53:32', 1),
(465, '26', '5', 'te amo hermosita', '2016-09-20 14:01:03', 1),
(466, '5', '26', 'tu buche mada facka', '2016-09-20 14:01:28', 1),
(467, '26', '5', 'bullshit mother fucker you', '2016-09-20 14:02:03', 1),
(468, '5', '26', 'you', '2016-09-20 14:02:10', 1),
(469, '5', '3', 'test1', '2016-09-26 17:44:40', 0),
(470, '5', '3', 'test2', '2016-09-26 17:44:54', 0),
(471, '26', '5', 'hola gediondito', '2016-09-27 10:07:06', 1),
(472, '5', '26', 'hermosita te amo mas que yo ama esta vaso', '2016-09-27 10:11:08', 1),
(473, '26', '5', 'i know', '2016-09-27 10:14:36', 1),
(474, '26', '5', 'HOLA HERMOSITO', '2016-09-27 14:31:31', 1),
(475, '26', '5', 'jhhhh', '2016-09-27 14:40:25', 1),
(476, '26', '5', 'jbjhbjk', '2016-09-27 14:41:44', 1),
(477, '26', '5', 'yo no me siento bien', '2016-09-27 16:05:06', 1),
(478, '26', '5', ':[', '2016-09-27 16:05:22', 1),
(479, '26', '5', 'tu no mi amas nada', '2016-09-27 16:16:33', 1),
(480, '1', '5', 'jdfsjhdfda', '0000-00-00 00:00:00', 1),
(481, '26', '5', 'kajsdbfkjabckjdvkjd', '2016-10-04 12:00:19', 1),
(482, '4', '1', 'Hi', '2016-10-06 10:16:03', 1),
(483, '1', '4', 'Hello', '0000-00-00 00:00:00', 1),
(484, '1', '26', 'hi', '0000-00-00 00:00:00', 1),
(485, '1', '26', 'hi', '0000-00-00 00:00:00', 1),
(486, '1', '26', 'kjlkj', '0000-00-00 00:00:00', 1),
(487, '5', '1', 'guyguyhiu', '2016-10-10 09:06:01', 1),
(488, '1', '5', 'yuyuyuy', '0000-00-00 00:00:00', 1),
(489, '1', '5', 'sakjhakj', '0000-00-00 00:00:00', 1),
(490, '1', '5', 'hola care bola', '0000-00-00 00:00:00', 1),
(491, '1', '5', 'papapapapap', '0000-00-00 00:00:00', 1),
(492, '1', '5', 'Hi', '2016-10-17 18:27:50', 1),
(493, '5', '1', 'hello', '2016-10-17 18:28:04', 1),
(494, '1', '5', 'hi', '2016-10-18 15:24:13', 1),
(495, '4', '1', 'Hi', '2016-10-18 16:56:29', 1),
(496, '4', '1', 'Hello', '2016-10-18 16:57:58', 1),
(497, '4', '1', 'hmm', '2016-10-18 16:58:40', 1),
(498, '4', '1', 'hiiiiiiiiii', '2016-10-18 17:05:44', 1),
(499, '4', '1', 'hellooooooo', '2016-10-18 17:09:18', 1),
(500, '4', '1', 'hiiiiiiiiiiii', '2016-10-18 17:12:47', 1),
(501, '4', '1', ';lk;l', '2016-10-18 17:14:22', 1),
(502, '4', '1', 'dfgd', '2016-10-18 17:15:58', 1),
(503, '4', '1', 'fdhgfgh', '2016-10-18 17:20:10', 1),
(504, '4', '1', 'fdg', '2016-10-18 17:20:21', 1),
(505, '4', '1', 'hi', '2016-10-18 17:24:46', 1),
(506, '4', '1', 'jj', '2016-10-18 17:26:17', 1),
(507, '4', '1', 'Hi', '2016-10-18 17:30:01', 1),
(508, '4', '1', 'joy', '2016-10-18 17:33:52', 1),
(509, '4', '1', 'ppp', '2016-10-18 17:34:35', 1),
(510, '4', '1', 'oooooooo', '2016-10-18 17:35:33', 1),
(511, '4', '1', 'ooo', '2016-10-18 17:36:13', 1),
(512, '4', '1', 'pppppppp', '2016-10-18 17:36:34', 1),
(513, '4', '1', 'kjlkj', '2016-10-18 17:38:11', 1),
(514, '4', '1', 'hi', '2016-10-18 17:44:08', 1),
(515, '4', '1', 'hmm', '2016-10-18 17:44:43', 1),
(516, '4', '1', 'fghfg', '2016-10-18 17:45:26', 1),
(517, '5', '26', 'hi', '2016-10-20 16:33:40', 1),
(518, '5', '26', 'hellooo', '2016-10-20 16:33:59', 1),
(519, '5', '26', 'hiiii', '2016-10-20 16:34:20', 1),
(520, '5', '26', 'dfgdfg', '2016-10-20 16:34:29', 1),
(521, '5', '26', 'dfgdfg', '2016-10-20 16:34:30', 1),
(522, '5', '26', 'fdg', '2016-10-20 16:34:31', 1),
(523, '5', '26', 'dfg', '2016-10-20 16:34:31', 1),
(524, '5', '1', 'ffghfg', '2016-10-20 16:34:44', 1),
(525, '5', '26', 'fghfgh', '2016-10-20 16:34:48', 1),
(526, '5', '26', 'dfgdfg', '2016-10-20 16:40:18', 1),
(527, '5', '26', 'fdgd', '2016-10-20 16:40:20', 1),
(528, '5', '26', 'dfg', '2016-10-20 16:40:20', 1),
(529, '5', '26', 'fdg', '2016-10-20 16:40:21', 1),
(530, '5', '26', 'ddfg', '2016-10-20 16:40:21', 1),
(531, '5', '26', 'dfg', '2016-10-20 16:40:22', 1),
(532, '5', '26', 'dfg', '2016-10-20 16:40:23', 1),
(533, '5', '26', 'ffdg', '2016-10-20 16:40:24', 1),
(534, '26', '5', 'dfgd', '0000-00-00 00:00:00', 1),
(535, '26', '5', 'dfg', '0000-00-00 00:00:00', 1),
(536, '26', '5', 'fdg', '0000-00-00 00:00:00', 1),
(537, '26', '5', 'fdg', '0000-00-00 00:00:00', 1),
(538, '26', '5', 'fdg', '0000-00-00 00:00:00', 1),
(539, '1', '5', 'ohojoij', '0000-00-00 00:00:00', 1),
(540, '5', '1', 'hola', '2016-10-21 10:00:55', 1),
(541, '5', '1', 'uhjhjk', '2016-10-21 10:10:36', 1),
(542, '1', '5', 'hgvhg h', '0000-00-00 00:00:00', 1),
(543, '5', '1', 'ghjgjhg', '2016-10-21 14:12:03', 1),
(544, '26', '5', 'dadwdwdw', '0000-00-00 00:00:00', 1),
(545, '26', '5', 'fewfewfw', '0000-00-00 00:00:00', 1),
(546, '5', '26', 'adadwdwq', '2016-10-27 18:41:45', 1),
(547, '5', '26', 'dwaddwdw', '2016-10-27 18:44:57', 1),
(548, '5', '26', 'fefewfwf', '2016-10-27 18:45:23', 1),
(549, '5', '26', 'dwwqdwqdwqd', '2016-10-27 18:45:48', 1),
(550, '5', '26', 'khgjkhjk', '2016-10-27 18:52:41', 1),
(551, '26', '5', 'dqwdwqdwq', '0000-00-00 00:00:00', 1),
(552, '26', '5', 'ergregeger', '0000-00-00 00:00:00', 1),
(553, '26', '5', 'water', '0000-00-00 00:00:00', 1),
(554, '26', '5', 'dsdsds', '0000-00-00 00:00:00', 1),
(555, '26', '5', 'tyeyyryrt', '0000-00-00 00:00:00', 1),
(556, '26', '5', 'dwqdwqdwq', '0000-00-00 00:00:00', 1),
(557, '26', '5', 'dawwdwqd', '0000-00-00 00:00:00', 1),
(558, '5', '26', 'hola', '2016-11-01 15:10:45', 1),
(559, '5', '26', 'algo', '2016-11-01 15:12:20', 1),
(560, '26', '5', 'hi', '0000-00-00 00:00:00', 1),
(561, '26', '5', 'helloo', '0000-00-00 00:00:00', 1),
(562, '26', '5', 'hiiiiiiiiii', '0000-00-00 00:00:00', 1),
(563, '5', '26', 'Hi Damon', '2016-11-02 18:42:25', 1),
(564, '26', '5', 'yes I can see this chat box', '0000-00-00 00:00:00', 1),
(565, '26', '5', 'but your small profile has the chat button on the left, it should always be on the right', '0000-00-00 00:00:00', 1),
(566, '26', '5', 'also there are still two buttons below that say english and spanish... what are these ??', '0000-00-00 00:00:00', 1),
(567, '5', '26', 'ok', '2016-11-02 18:44:36', 1),
(568, '5', '26', 'where did you get those button?', '2016-11-02 18:44:52', 1),
(569, '5', '26', 'can you please send me the link?', '2016-11-02 18:45:10', 1),
(570, '26', '5', 'on this chat box below where I am typing it says english and spanish (two buttons)', '0000-00-00 00:00:00', 1),
(571, '5', '26', 'can you see my video?', '2016-11-02 18:45:38', 1),
(572, '26', '5', 'no', '0000-00-00 00:00:00', 1),
(573, '26', '5', 'I will try and video you', '0000-00-00 00:00:00', 1),
(574, '26', '5', 'hola', '0000-00-00 00:00:00', 1),
(575, '5', '26', 'hola}', '2016-11-02 09:04:36', 1),
(576, '1', '26', 'hola', '0000-00-00 00:00:00', 1),
(577, '26', '1', 'hola hermosita', '0000-00-00 00:00:00', 1),
(578, '5', '1', 'hi', '2016-11-08 15:36:34', 0),
(579, '5', '1', 'helloo', '2016-11-08 15:36:38', 0),
(580, '5', '26', 'Hola', '2016-11-08 15:48:32', 1),
(581, '26', '5', 'hi', '0000-00-00 00:00:00', 1),
(582, '5', '26', 'hola', '2016-11-08 15:52:05', 1),
(583, '26', '5', 'hi', '0000-00-00 00:00:00', 1),
(584, '26', '5', 'helloooooooo', '0000-00-00 00:00:00', 1),
(585, '5', '26', 'hiii', '2016-11-08 15:55:55', 1),
(586, '5', '26', 'hola', '2016-11-08 15:56:21', 1),
(587, '5', '26', 'heyyy', '2016-11-08 16:01:09', 1),
(588, '5', '26', 'hola', '2016-11-08 16:01:27', 1),
(589, '26', '5', 'hola', '0000-00-00 00:00:00', 1),
(590, '26', '5', 'hi', '0000-00-00 00:00:00', 1),
(591, '26', '5', '1111111111', '0000-00-00 00:00:00', 1),
(592, '5', '26', '22222', '2016-11-08 16:05:59', 1),
(593, '5', '26', 'dsfsdf', '2016-11-08 16:06:32', 1),
(594, '26', '5', '44444444444444', '2016-11-08 10:37:14', 1),
(595, '26', '5', 'hiiii', '2016-11-08 11:03:33', 1),
(596, '5', '26', 'hola', '2016-11-08 11:07:05', 1),
(597, '26', '5', 'Hola! Â¿CÃ³mo estÃ¡s', '2016-11-08 11:09:18', 1),
(598, '26', '5', 'Hi', '2016-11-08 12:41:13', 1),
(599, '26', '5', 'Bikash Here', '2016-11-08 12:41:17', 1),
(600, '5', '26', 'hi', '2016-11-08 12:41:25', 1),
(601, '5', '26', 'hello bikash', '2016-11-08 12:41:46', 1),
(602, '26', '5', 'can you see the translation buttons', '2016-11-08 12:41:49', 1),
(603, '26', '5', 'to translate the chat', '2016-11-08 12:41:56', 1),
(604, '5', '26', 'the chat box was sitting under my tool bar', '2016-11-08 12:42:25', 1),
(605, '5', '26', 'but I have moved the chat box and I see, English/Spanish buttons', '2016-11-08 12:42:43', 1),
(606, '26', '5', 'oh. Its draggable', '2016-11-08 12:42:48', 1),
(607, '26', '5', 'you can drag that anywhere', '2016-11-08 12:43:04', 1),
(608, '5', '26', 'ok great', '2016-11-08 12:43:12', 1),
(609, '5', '26', 'I see two buttons, but should this not be purchase translation token button ?', '2016-11-08 12:43:42', 1),
(610, '26', '5', 'then only compatiability function needs to be fixed', '2016-11-08 12:44:55', 1),
(611, '5', '26', 'has Karuna got an account that doesn''t need to purchase translations ?', '2016-11-08 12:45:39', 1),
(612, '26', '5', 'no. Every user have to purchase translation', '2016-11-08 12:46:24', 1),
(613, '26', '5', 'male and female both?', '2016-11-08 12:46:33', 1),
(614, '26', '5', 'am i right?', '2016-11-08 12:46:39', 1),
(615, '5', '26', 'yes', '2016-11-08 12:46:40', 1),
(616, '5', '26', 'why does Karuna have the english and spanish buttons then and not the purchase translation button', '2016-11-08 12:47:05', 1),
(617, '26', '5', 'oh. I have buyed the translation for testing', '2016-11-08 12:47:36', 1),
(618, '26', '5', 'you can check another user', '2016-11-08 12:47:49', 1),
(619, '26', '5', 'or by creating new user', '2016-11-08 12:47:59', 1),
(620, '5', '26', 'oh ok great, yes it is working well.. one thing though can you seperate the buttons a little they are touching each other', '2016-11-08 12:48:12', 1),
(621, '26', '5', 'ok.', '2016-11-08 12:48:30', 1),
(622, '5', '26', 'well done, bikash it is working well... you purchased the translations how ?', '2016-11-08 12:49:02', 1),
(623, '5', '26', 'how do I set that up for future users when they purchase the translations ?', '2016-11-08 12:49:26', 1),
(624, '26', '5', 'Iif translation has not been pruchased then you will get a Purchase translatioin button', '2016-11-08 12:49:46', 1),
(625, '26', '5', 'there you will get three payment gateway', '2016-11-08 12:50:01', 1),
(626, '26', '5', 'paypal,creadit card', '2016-11-08 12:50:06', 1),
(627, '26', '5', 'did not got your point', '2016-11-08 12:50:33', 1),
(628, '26', '5', 'what do you mean by setup?', '2016-11-08 12:50:42', 1),
(629, '26', '5', 'do you want to set a translation package from admin?', '2016-11-08 12:51:09', 1),
(630, '5', '26', 'but once the user has paid, how do i automate there translations through the google api, do I need to do that manualy ?', '2016-11-08 12:51:32', 1),
(631, '26', '5', 'oh you mean the translation api', '2016-11-08 12:52:52', 1),
(632, '5', '26', 'do I need to purchase the translations or will they translate automatically ?', '2016-11-08 12:53:01', 1),
(633, '26', '5', 'we have used bing translation api', '2016-11-08 12:53:06', 1),
(634, '26', '5', 'after limit you have to pay for that', '2016-11-08 12:53:26', 1),
(635, '5', '26', 'why bing is this better than google for translation ?', '2016-11-08 12:53:45', 1),
(636, '26', '5', 'yes you have to payfor that', '2016-11-08 12:54:08', 1),
(637, '5', '26', 'what I need to know though is once I have reached my limit then how does the bing account and the miamor platform talk to each other or have you got this setup already ?', '2016-11-08 12:55:04', 1),
(638, '26', '5', 'for google you have to add card before use', '2016-11-08 12:55:06', 1),
(639, '26', '5', 'and in bing there is a certain limit for free every day', '2016-11-08 12:55:39', 1),
(640, '26', '5', 'bing and google both are about same', '2016-11-08 12:55:51', 1),
(641, '26', '5', 'I have separate the translation buttons', '2016-11-08 12:58:40', 1),
(642, '26', '5', 'please reload to check', '2016-11-08 12:58:48', 1),
(643, '5', '26', 'so if I set up a bing or google account, this will plug in fine to the platform', '2016-11-08 12:58:52', 1),
(644, '26', '5', 'yes both have api', '2016-11-08 12:59:31', 1),
(645, '26', '5', 'we have to integrate in our webite', '2016-11-08 12:59:44', 1),
(646, '26', '5', '*website', '2016-11-08 12:59:49', 1),
(647, '5', '26', 'yes the buttons are good now thank you', '2016-11-08 13:01:15', 1),
(648, '26', '5', 'welcome', '2016-11-08 13:01:42', 1),
(649, '5', '26', 'can I click on links now in this chat box ?', '2016-11-08 13:01:49', 1),
(650, '5', '26', 'last time I was not able to', '2016-11-08 13:01:56', 1),
(651, '26', '5', 'i have to check', '2016-11-08 13:02:14', 1),
(652, '26', '5', 'https://www.google.co.in', '2016-11-08 13:02:20', 1),
(653, '5', '26', 'no I can''t', '2016-11-08 13:02:30', 1),
(654, '26', '5', 'nope', '2016-11-08 13:02:33', 1),
(655, '5', '26', 'and because I can''t copy I can access the link', '2016-11-08 13:02:51', 1),
(656, '26', '5', 'Ok. I will check if i can do this better', '2016-11-08 13:03:17', 1),
(657, '5', '26', 'a couple of things with the Chat box, can a user log out of chat, so they don''t get people sending them messages ?', '2016-11-08 13:03:47', 1),
(658, '5', '26', 'and can you turn off the sound of the messages arriving or have the sound if you choose to', '2016-11-08 13:04:27', 1),
(659, '26', '5', 'It will be tough but i will try', '2016-11-08 13:05:33', 1),
(660, '5', '26', 'Thank you very much bikash... good work', '2016-11-08 13:05:50', 1),
(661, '5', '26', 'i will go through the document later today and check all the changes', '2016-11-08 13:06:07', 1),
(662, '26', '5', 'Thanks', '2016-11-08 13:06:13', 1),
(663, '26', '5', 'Please check the doc', '2016-11-08 13:06:20', 1),
(664, '5', '26', 'I will thanks, bye for now', '2016-11-08 13:06:33', 1),
(665, '26', '5', 'ok. Thanks', '2016-11-08 13:06:38', 1),
(666, '26', '5', 'bye', '2016-11-08 13:06:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dating_compatibility_setting`
--

CREATE TABLE IF NOT EXISTS `dating_compatibility_setting` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `looking_for` varchar(255) NOT NULL,
  `partner_looking_for` varchar(255) NOT NULL,
  `looking_for_important` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `partner_marital_status` varchar(255) NOT NULL,
  `marital_status_important` varchar(255) NOT NULL,
  `believe_love` varchar(255) NOT NULL,
  `partner_believe_love` varchar(255) NOT NULL,
  `believe_love_important` varchar(255) NOT NULL,
  `have_kids` varchar(255) NOT NULL,
  `partner_have_kids` varchar(255) NOT NULL,
  `have_kids_important` varchar(255) NOT NULL,
  `family_importance` varchar(255) NOT NULL,
  `partner_family_importance` varchar(255) NOT NULL,
  `family_importance_important` varchar(255) NOT NULL,
  `loyal_in_relation` varchar(255) NOT NULL,
  `partner_loyal_in_relation` varchar(255) NOT NULL,
  `loyal_in_relation_important` varchar(255) NOT NULL,
  `romantic_person` varchar(255) NOT NULL,
  `partner_romantic_person` varchar(255) NOT NULL,
  `romantic_person_important` varchar(255) NOT NULL,
  `longest_relation` varchar(255) NOT NULL,
  `partner_longest_relation` varchar(255) NOT NULL,
  `longest_relation_important` varchar(255) NOT NULL,
  `religion_importance` varchar(255) NOT NULL,
  `partner_religion_importance` varchar(255) NOT NULL,
  `religion_importance_important` varchar(255) NOT NULL,
  `willing_relocate` varchar(255) NOT NULL,
  `partner_willing_relocate` varchar(255) NOT NULL,
  `willing_relocate_important` varchar(255) NOT NULL,
  `new_experience` varchar(255) NOT NULL,
  `partner_new_experience` varchar(255) NOT NULL,
  `new_experience_important` varchar(255) NOT NULL,
  `show_affection` varchar(255) NOT NULL,
  `partner_show_affection` varchar(255) NOT NULL,
  `show_affection_important` varchar(255) NOT NULL,
  `friend_with_ex` varchar(255) NOT NULL,
  `partner_friend_with_ex` varchar(255) NOT NULL,
  `friend_with_ex_important` varchar(255) NOT NULL,
  `financially_secure` varchar(255) NOT NULL,
  `partner_financially_secure` varchar(255) NOT NULL,
  `financially_secure_important` varchar(255) NOT NULL,
  `most_enjoy` varchar(255) NOT NULL,
  `partner_most_enjoy` varchar(255) NOT NULL,
  `most_enjoy_important` varchar(255) NOT NULL,
  `smoke` varchar(255) NOT NULL,
  `partner_smoke` varchar(255) NOT NULL,
  `smoke_important` varchar(255) NOT NULL,
  `drink` varchar(255) NOT NULL,
  `partner_drink` varchar(255) NOT NULL,
  `drink_important` varchar(255) NOT NULL,
  `exercise` varchar(255) NOT NULL,
  `partner_exercise` varchar(255) NOT NULL,
  `exercise_important` varchar(255) NOT NULL,
  `bill_pay` varchar(255) NOT NULL,
  `partner_bill_pay` varchar(255) NOT NULL,
  `bill_pay_important` varchar(255) NOT NULL,
  `want_from_relationship` varchar(255) NOT NULL,
  `partner_want_from_relationship` varchar(255) NOT NULL,
  `want_from_relationship_important` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_compatibility_setting`
--

INSERT INTO `dating_compatibility_setting` (`id`, `user_id`, `looking_for`, `partner_looking_for`, `looking_for_important`, `marital_status`, `partner_marital_status`, `marital_status_important`, `believe_love`, `partner_believe_love`, `believe_love_important`, `have_kids`, `partner_have_kids`, `have_kids_important`, `family_importance`, `partner_family_importance`, `family_importance_important`, `loyal_in_relation`, `partner_loyal_in_relation`, `loyal_in_relation_important`, `romantic_person`, `partner_romantic_person`, `romantic_person_important`, `longest_relation`, `partner_longest_relation`, `longest_relation_important`, `religion_importance`, `partner_religion_importance`, `religion_importance_important`, `willing_relocate`, `partner_willing_relocate`, `willing_relocate_important`, `new_experience`, `partner_new_experience`, `new_experience_important`, `show_affection`, `partner_show_affection`, `show_affection_important`, `friend_with_ex`, `partner_friend_with_ex`, `friend_with_ex_important`, `financially_secure`, `partner_financially_secure`, `financially_secure_important`, `most_enjoy`, `partner_most_enjoy`, `most_enjoy_important`, `smoke`, `partner_smoke`, `smoke_important`, `drink`, `partner_drink`, `drink_important`, `exercise`, `partner_exercise`, `exercise_important`, `bill_pay`, `partner_bill_pay`, `bill_pay_important`, `want_from_relationship`, `partner_want_from_relationship`, `want_from_relationship_important`) VALUES
(3, 1, '2', '2', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '4', '4', '1', '4', '3', '1', '1', '1', '1', '1', '1', '1', '3', '2', '1', '2', '2', '1', '1', '2', '1', '1', '1', '1', '2', '2', '1', '2', '2', '1', '1', '1', '1', '1', '4', '1', '2', '2', '2'),
(9, 26, '3', '4', '2', '1', '1', '', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 4, '3', '3', '2', '3', '2', '1', '2', '2', '3', '1', '2', '3', '2', '3', '2', '1', '3', '1', '2', '3', '1', '1', '5', '1', '2', '2', '3', '2', '4', '2', '2', '2', '3', '2', '4', '2', '2', '3', '2', '2', '2', '1', '2', '2', '3', '1', '1', '3', '1', '1', '3', '1', '2', '1', '1', '4', '2', '4', '5', '3'),
(26, 168, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 5, '1', '1', '2', '2', '1', '1', '2', '1', '3', '1', '2', '3', '1', '3', '2', '2', '3', '1', '3', '3', '1', '2', '5', '1', '3', '2', '3', '3', '4', '2', '3', '2', '3', '3', '4', '2', '1', '3', '2', '3', '2', '1', '1', '2', '3', '2', '1', '3', '2', '1', '3', '4', '2', '1', '3', '4', '2', '5', '5', '3'),
(21, 163, '1', '2', '2', '1', '2', '1', '1', '1', '2', '1', '1', '1', '2', '2', '', '1', '1', '2', '2', '', '', '2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 3, '5', '1,2,3', '2', '4', '1,3,4', '1', '1', '1,2', '3', '1', '1,2', '3', '2', '1,2,3', '2', '3', '1,2,3', '1', '1', '1,2,3', '1', '3', '4,5', '1', '4', '1,2', '3', '4', '1,2,4', '2', '1', '1,2', '3', '1', '1,2,3,4', '2', '3', '2,3', '2', '1', '2', '1', '1', '1,2', '3', '2', '1', '3', '1', '1', '3', '2', '1,2', '1', '2', '3,4', '2', '2', '1,4,5', '3'),
(7, 6, '4', '1,2,3', '2', '5', '1,3,4', '1', '2', '1,2', '3', '2', '1,2', '3', '3', '1,2,3', '2', '4', '1,2,3', '1', '3', '1,2,3', '1', '5', '4,5', '1', '1', '1,2', '3', '1', '1,2,4', '2', '3', '1,2', '3', '4', '1,2,3,4', '2', '2', '2,3', '2', '2', '2', '1', '3', '1,2', '3', '3', '1', '3', '3', '1', '3', '1', '1,2', '1', '4', '3,4', '2', '1', '1,4,5', '3'),
(8, 8, '6', '1,2,3', '1', '1', '1,3', '3', '1', '1,2', '3', '2', '1,2', '3', '3', '2,3', '1', '2', '2', '1', '1', '1,3', '1', '2', '2,3', '1', '4', '4', '1', '', '', '1', '', '', '1', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1,2', '3'),
(10, 104, '2', '1', '1', '1', '5', '1', '1', '1', '2', '2', '1', '2', '2', '2', '2', '4', '1', '1', '', '1', '2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 110, '4', '4', '2', '1', '2', '1', '1', '2', '1', '2', '1', '2', '3', '2', '1', '1', '2', '3', '3', '3', '3', '3', '2', '1', '1', '1', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '4', '3'),
(12, 115, '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2'),
(13, 116, '2', '1', '1', '1', '2', '1', '1', '1', '1', '1', '2', '1', '1', '2', '3', '3', '2', '1', '2', '2', '2', '1', '2', '2', '4', '1', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '3', '3', '3', '3', '2', '1', '1', '1', '1', '1', '1', '1'),
(14, 89, '4', '4', '1', '4', '5', '1', '2', '2', '1', '1', '1', '2', '2', '1', '1', '2', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '2', '2', '2', '2', '1', '3', '1', '2', '3', '3', '2', '1', '1', '2', '1', '3', '2', '2', '2', '2', '2', '3', '1', '1', '1', '1', '1'),
(15, 118, '5', '4', '2', '1', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '1', '1', '1', '2', '2', '2', '1', '1', '2', '1', '1', '1', '1', '1', '1', '1', '2', '1', '1', '2', '3', '2', '1', '1', '3', '2', '1', '1', '1', '1', '1', '2', '3', '3', '2', '2', '3', '2', '3', '1', '2', '3', '1', '2', '3'),
(16, 105, '2', '2', '1', '1', '1', '1', '1', '1', '2', '1', '1', '2', '1', '1', '3', '1', '1', '2', '2', '2', '1', '4', '4', '1', '2', '2', '1', '2', '1', '2', '1', '1', '2', '1', '1', '2', '3', '3', '2', '2', '1', '2', '1', '1', '1', '1', '1', '2', '2', '2', '1', '1', '1', '2', '4', '4', '2', '3', '5', '1'),
(17, 54, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(18, 102, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '2', '2', '2', '', '2', '2', '', '', '', '', '1', '1', '', '', '', '', '', '', '4', '1', '', '2', '1', '2', '4', '4', ''),
(19, 157, '2', '2', '1', '1', '1', '1', '1', '1', '1', '2', '1', '2', '1', '1', '3', '1', '1', '3', '1', '1', '2', '5', '2', '1', '3', '3', '1', '1', '1', '1', '1', '1', '2', '3', '', '1', '2', '2', '1', '1', '1', '2', '2', '2', '1', '2', '2', '1', '2', '2', '1', '1', '2', '2', '4', '4', '1', '2', '2', '2'),
(20, 88, '2', '2', '1', '1', '1', '2', '1', '2', '1', '1', '2', '2', '1', '1', '3', '1', '1', '2', '1', '1', '1', '2', '', '1', '2', '2', '1', '2', '1', '1', '', '', '', '1', '1', '1', '3', '3', '2', '1', '1', '1', '2', '2', '1', '1', '1', '2', '2', '2', '1', '1', '2', '2', '4', '4', '2', '5', '1', '2'),
(22, 161, '5', '5', '2', '4', '5', '2', '2', '1', '2', '2', '1', '2', '1', '2', '3', '4', '2', '2', '3', '2', '2', '2', '3', '2', '3', '1', '2', '2', '1', '2', '1', '2', '1', '1', '2', '3', '3', '2', '1', '1', '2', '3', '1', '1', '1', '1', '2', '3', '1', '2', '3', '1', '2', '3', '3', '2', '1', '1', '2', '3'),
(23, 120, '1', '1', '2', '2', '1', '2', '2', '2', '1', '1', '2', '1', '1', '2', '2', '1', '1', '2', '1', '2', '2', '1', '1', '1', '2', '3', '1', '1', '2', '3', '1', '1', '3', '2', '2', '2', '2', '3', '3', '3', '2', '1', '1', '1', '2', '1', '2', '2', '1', '1', '2', '2', '1', '1', '', '', '', '', '', ''),
(25, 22, '3', '3', '2', '1', '1', '2', '1', '1', '2', '1', '1', '2', '1', '1', '2', '1', '1', '2', '1', '1', '3', '2', '1', '2', '1', '1', '2', '1', '1', '2', '1', '1', '1', '3', '4', '3', '1', '1', '2', '2', '1', '2', '1', '1', '2', '1', '2', '2', '1', '2', '3', '1', '2', '1', '4', '4', '3', '5', '5', '2'),
(27, 172, '3', '3', '2', '1', '1', '3', '1', '1', '2', '1', '1', '3', '2', '2', '2', '1', '2', '2', '2', '2', '1', '1', '2', '1', '1', '2', '2', '3', '1', '2', '1', '2', '1', '3', '3', '2', '3', '3', '1', '1', '1', '2', '1', '1', '2', '1', '2', '2', '1', '2', '2', '1', '3', '2', '4', '1', '3', '5', '5', '2'),
(28, 173, '3', '1', '2', '1', '1', '2', '1', '1', '2', '2', '2', '2', '1', '1', '2', '1', '2', '2', '1', '1', '2', '1', '2', '2', '1', '1', '2', '1', '1', '2', '1', '1', '2', '2', '2', '2', '2', '2', '1', '1', '2', '2', '2', '2', '2', '2', '1', '2', '2', '2', '2', '4', '2', '2', '1', '1', '2', '1', '1', '2'),
(29, 86, '2', '2', '1', '2', '1', '1', '1', '1', '2', '1', '1', '2', '1', '1', '3', '1', '1', '2', '2', '2', '1', '4', '', '1', '2', '2', '1', '2', '1', '2', '1', '1', '1', '3', '', '1', '3', '3', '2', '1', '1', '2', '2', '2', '1', '1', '1', '2', '2', '2', '1', '1', '1', '2', '3', '3', '2', '5', '5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `dating_draft`
--

CREATE TABLE IF NOT EXISTS `dating_draft` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_draft`
--

INSERT INTO `dating_draft` (`id`, `user_id`, `to_id`, `message`, `date`) VALUES
(71, 1, 3, 'Hi,\r\nAbhishek. How are you?\r\n\r\nAbhi', '2014-11-29 06:57:35'),
(74, 1, 0, 'Test message', '2014-11-29 07:04:32'),
(82, 1, 3, 'Test Message...', '2014-12-06 06:57:47'),
(84, 105, 0, 'nonononoo', '2015-08-29 18:16:28'),
(85, 26, 0, 'h', '2016-08-30 16:38:11'),
(90, 1, 0, '', '2016-08-31 07:56:30'),
(96, 1, 0, 'trrytrytrytr', '2016-09-02 06:13:10'),
(97, 1, 0, 'tr', '2016-09-02 06:13:10'),
(98, 1, 0, 'gfhgfhgfhhgffhg', '2016-09-02 06:27:08'),
(99, 1, 0, 'gfhgfhgfh', '2016-09-02 06:27:08'),
(100, 1, 0, 'gfhgfhgfhhgffhgf', '2016-09-02 06:27:08'),
(101, 1, 0, 'gfhgf', '2016-09-02 06:27:07'),
(103, 1, 0, 'gdfgdfgdfg', '2016-09-02 06:31:39'),
(104, 1, 0, 'gdfg', '2016-09-02 06:31:39'),
(105, 1, 0, 'gdfg', '2016-09-02 06:31:39'),
(106, 1, 0, 'ytfhghfhgf', '2016-09-02 06:35:28'),
(107, 1, 0, 'ytf', '2016-09-02 06:35:28'),
(108, 1, 0, 'ytf', '2016-09-02 06:35:28'),
(109, 1, 0, 'ytfhg', '2016-09-02 06:35:28'),
(110, 1, 0, 'ytfhgh', '2016-09-02 06:35:28'),
(111, 1, 0, 'ytfhghf', '2016-09-02 06:35:28'),
(112, 1, 0, 'dfdgdgf', '2016-09-02 06:53:46'),
(113, 1, 0, 'dfd', '2016-09-02 06:53:46'),
(114, 1, 0, 'dfdg', '2016-09-02 06:53:46'),
(116, 1, 0, 'fdg', '2016-09-02 06:54:25'),
(117, 1, 0, 'fdg', '2016-09-02 06:54:25'),
(118, 1, 0, 'fdgdg', '2016-09-02 06:54:25'),
(121, 5, 0, 'sdfegerg', '2016-09-03 13:20:48'),
(122, 5, 0, 'sdf', '2016-09-03 13:20:48'),
(123, 5, 0, 'sdfegergerg', '2016-09-03 13:21:07'),
(124, 5, 0, 'sdf', '2016-09-03 13:20:48'),
(125, 5, 0, 'h', '2016-09-03 13:22:07'),
(126, 5, 0, 'he', '2016-09-03 13:22:07'),
(127, 5, 0, 'hell', '2016-09-03 13:22:08'),
(128, 5, 0, 'hel', '2016-09-03 13:22:08'),
(129, 5, 0, 'hello', '2016-09-03 13:22:08'),
(133, 26, 5, 'noinoin', '2016-09-20 20:02:37'),
(135, 26, 0, '', '2016-09-28 17:53:36'),
(136, 26, 0, 'asdf', '2016-09-28 17:53:19'),
(137, 26, 0, 'asdf', '2016-09-28 17:53:19'),
(138, 1, 86, 'jgkgjhbnmbhvhgv jbvhghv ', '2016-10-21 20:03:41');

-- --------------------------------------------------------

--
-- Table structure for table `dating_friend_requests`
--

CREATE TABLE IF NOT EXISTS `dating_friend_requests` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `new_request` int(11) NOT NULL,
  `accept` tinyint(1) NOT NULL DEFAULT '0',
  `deny` tinyint(1) NOT NULL DEFAULT '0',
  `is_alert` int(11) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_friend_requests`
--

INSERT INTO `dating_friend_requests` (`id`, `from_id`, `to_id`, `new_request`, `accept`, `deny`, `is_alert`, `date`) VALUES
(20, 11, 12, 0, 1, 0, 0, '2015-02-25 06:04:24'),
(21, 1, 3, 1, 0, 0, 0, '2015-03-14 23:59:52'),
(22, 3, 8, 1, 0, 0, 0, '2015-05-29 14:56:29'),
(23, 1, 6, 1, 0, 0, 0, '2015-05-31 18:48:12'),
(60, 5, 78, 1, 0, 0, 1, '2016-10-04 14:14:37'),
(44, 5, 49, 1, 0, 0, 0, '2016-09-03 13:49:34'),
(71, 5, 3, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(37, 1, 86, 0, 1, 0, 0, '2016-06-01 16:29:55'),
(39, 26, 159, 1, 0, 0, 0, '2016-06-05 22:09:07'),
(45, 26, 4, 1, 0, 0, 0, '2016-09-05 21:08:06'),
(51, 86, 4, 1, 0, 0, 0, '2016-09-08 20:39:46'),
(61, 5, 1, 0, 1, 0, 0, '2016-10-04 15:09:28'),
(70, 5, 120, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(62, 1, 87, 1, 0, 0, 1, '2016-10-05 15:30:42'),
(63, 1, 4, 0, 1, 0, 0, '2016-10-06 04:44:51'),
(65, 1, 172, 1, 0, 0, 1, '2016-10-06 11:56:53'),
(69, 26, 5, 0, 1, 0, 0, '2016-10-07 17:15:14'),
(72, 5, 118, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(73, 5, 174, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(74, 5, 4, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(75, 26, 8, 1, 0, 0, 1, '1970-01-01 05:00:00'),
(76, 26, 1, 0, 1, 0, 0, '1970-01-01 05:00:00'),
(77, 5, 168, 1, 0, 0, 1, '1970-01-01 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dating_interest`
--

CREATE TABLE IF NOT EXISTS `dating_interest` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_interest`
--

INSERT INTO `dating_interest` (`id`, `user_id`, `type`, `title`, `link`) VALUES
(3, 1, 1, 'My Music 2', 'www.youtube.com/watch?v=eytE4BU9Atk'),
(7, 1, 3, 'Test TV show 1', 'http://www.google.com'),
(10, 1, 4, 'Fear and Loathing in Las Vegas', 'http://www.amazon.com/Fear-Loathing-Las-Vegas-American/dp/0679785892'),
(11, 1, 1, 'I LOVE MUSIC', ''),
(12, 7, 1, 'You Tube', 'https://www.youtube.com/'),
(13, 7, 1, 'Find partners for fun, dating and long term relationships', 'https://www.youtube.com/'),
(14, 1, 2, 'Scarface', ''),
(15, 5, 1, 'I LOVE MUSIC', ''),
(16, 1, 1, 'fat freddys drop', 'youtube'),
(17, 1, 2, 'a clockwork orange', 'amozon'),
(18, 1, 3, 'love hate', 'youtube'),
(19, 1, 4, 'don juan tales of a yaqui indian', 'amazon'),
(20, 86, 1, 'se formo la gozadera', 'https://youtu.be/VMp55KH_3wo'),
(21, 26, 1, 'la bicicleta', 'https://www.youtube.com/watch?v=-UV0QGLmYys'),
(22, 26, 1, 'jose', ''),
(23, 26, 2, 'hgjkl', ''),
(24, 26, 3, 'two and a half  men', ''),
(25, 86, 1, 'Enrique iglesias', 'https://www.youtube.com/watch?v=xFutjZEBTXs'),
(26, 86, 4, 'el libro de la vida', 'https://www.youtube.com/watch?v=JvIvF8ST8CY'),
(27, 86, 2, 'ajjhhsgfjhkgf', ''),
(28, 86, 4, 'NHVHGJHG', ''),
(29, 86, 1, 'amor mio', ''),
(30, 5, 1, 'ybuybuhb', ''),
(31, 26, 1, 'amor amor', ''),
(32, 26, 2, 'jhonmhjg', ''),
(33, 5, 1, 'guygiuh', ''),
(34, 26, 1, 'ggg', 'ghfhgf'),
(35, 86, 1, 'hgvghhjjj', ''),
(36, 86, 2, 'kjbjhbh', ''),
(37, 86, 4, 'el amor de mi vida', ''),
(38, 5, 1, 'jhgjhgjh', ''),
(39, 26, 3, 'tu voz estereo', ''),
(40, 26, 2, 'skjdbfsd', ''),
(41, 5, 1, 'Quiero ser', 'https://youtu.be/oGn-k8qW8VM'),
(42, 1, 3, 'Two and a half men', 'https://youtu.be/3xWojaDG2tY'),
(43, 1, 3, 'ewqee', 'efewfewfw');

-- --------------------------------------------------------

--
-- Table structure for table `dating_kiss`
--

CREATE TABLE IF NOT EXISTS `dating_kiss` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `form_status` tinyint(1) NOT NULL DEFAULT '1',
  `to_status` tinyint(1) NOT NULL DEFAULT '1',
  `from_view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''1'' - Viewed, ''0'' - Not Viewed',
  `to_view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''1'' - Viewed, ''0'' - Not Viewed'
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_kiss`
--

INSERT INTO `dating_kiss` (`id`, `from_id`, `to_id`, `message`, `date`, `form_status`, `to_status`, `from_view`, `to_view`) VALUES
(1, 3, 1, '', '2014-10-14 12:09:09', 1, 0, 0, 1),
(15, 1, 5, '', '2015-02-06 18:36:14', 1, 1, 0, 1),
(14, 1, 3, '', '2014-12-10 22:02:50', 1, 1, 0, 0),
(13, 5, 1, '', '2014-11-07 15:50:04', 1, 0, 0, 1),
(12, 1, 3, '', '2014-11-07 00:18:23', 1, 1, 0, 0),
(11, 1, 4, '', '2014-11-07 00:10:52', 1, 1, 0, 1),
(10, 1, 3, '', '2014-11-07 00:10:48', 1, 1, 0, 0),
(16, 1, 4, '', '2015-06-07 11:00:09', 1, 1, 0, 1),
(17, 5, 78, '', '2015-08-09 20:36:57', 1, 1, 0, 0),
(18, 1, 8, '', '2015-08-24 07:28:56', 1, 1, 0, 1),
(19, 1, 5, '', '2015-08-28 20:39:00', 1, 1, 0, 1),
(20, 1, 105, '', '2015-08-29 18:34:30', 1, 1, 0, 1),
(21, 1, 4, '', '2015-09-01 07:11:41', 1, 1, 0, 1),
(22, 1, 4, '', '2015-09-04 06:38:55', 1, 1, 0, 1),
(23, 1, 0, '', '2015-09-04 07:41:16', 1, 1, 0, 0),
(24, 1, 0, '', '2015-09-04 07:43:28', 1, 1, 0, 0),
(25, 1, 0, '', '2015-09-04 07:46:23', 1, 1, 0, 0),
(26, 124, 0, '', '2015-09-07 14:54:46', 1, 1, 0, 0),
(27, 124, 0, '', '2015-09-07 15:09:30', 1, 1, 0, 0),
(28, 1, 4, '', '2016-06-01 16:35:20', 1, 1, 0, 1),
(29, 1, 4, '', '2016-06-01 16:35:29', 1, 1, 0, 1),
(30, 1, 105, '', '2016-06-01 16:49:36', 1, 1, 0, 0),
(31, 1, 4, '', '2016-06-01 21:03:58', 1, 1, 0, 1),
(32, 1, 4, '', '2016-06-01 21:04:00', 1, 1, 0, 1),
(33, 1, 4, '', '2016-06-01 21:04:01', 1, 1, 0, 1),
(34, 1, 4, '', '2016-06-01 21:04:04', 1, 1, 0, 1),
(35, 1, 4, '', '2016-06-01 21:06:27', 1, 1, 0, 1),
(36, 1, 4, '', '2016-06-01 21:06:28', 1, 1, 0, 1),
(37, 1, 8, '', '2016-06-01 21:21:35', 1, 1, 0, 1),
(38, 1, 8, '', '2016-06-01 21:21:36', 1, 1, 0, 1),
(39, 1, 8, '', '2016-06-01 21:21:36', 1, 1, 0, 1),
(40, 1, 8, '', '2016-06-01 21:21:37', 1, 1, 0, 1),
(41, 1, 8, '', '2016-06-01 21:21:39', 1, 1, 0, 1),
(42, 1, 8, '', '2016-06-01 21:21:40', 1, 1, 0, 1),
(43, 1, 8, '', '2016-06-01 21:21:53', 1, 1, 0, 1),
(44, 1, 8, '', '2016-06-01 21:21:57', 1, 1, 0, 1),
(45, 1, 8, '', '2016-06-01 21:21:58', 1, 1, 0, 1),
(46, 1, 8, '', '2016-06-01 21:21:59', 1, 1, 0, 1),
(47, 1, 5, '', '2016-06-01 22:43:29', 1, 1, 0, 1),
(48, 1, 5, '', '2016-06-01 22:43:37', 1, 0, 0, 0),
(49, 26, 5, '', '2016-06-06 01:22:42', 1, 1, 0, 1),
(50, 86, 26, '', '2016-06-06 16:03:01', 1, 1, 0, 1),
(51, 26, 86, '', '2016-08-29 19:45:20', 1, 1, 0, 1),
(52, 1, 26, '', '2016-09-05 13:28:11', 1, 0, 0, 1),
(53, 1, 26, '', '2016-09-05 13:28:37', 1, 1, 0, 1),
(54, 1, 8, '', '2016-09-05 13:38:02', 1, 1, 0, 1),
(55, 26, 86, '', '2016-09-05 21:39:03', 1, 1, 0, 1),
(56, 26, 86, '', '2016-09-06 17:20:22', 1, 1, 0, 1),
(57, 26, 1, '', '2016-09-07 05:45:53', 1, 0, 0, 1),
(58, 26, 1, '', '2016-09-07 13:02:23', 1, 0, 0, 1),
(59, 26, 1, '', '2016-09-07 13:05:52', 1, 0, 0, 1),
(60, 26, 86, '', '2016-09-08 06:41:50', 1, 1, 0, 1),
(61, 86, 1, '', '2016-09-08 18:03:08', 1, 0, 0, 1),
(62, 86, 5, '', '2016-09-08 19:54:55', 1, 0, 0, 1),
(65, 5, 86, '', '2016-09-27 15:45:18', 1, 1, 0, 0),
(66, 26, 5, '', '2016-09-28 20:06:14', 1, 1, 0, 1),
(67, 4, 1, '', '2016-10-06 05:39:14', 1, 0, 0, 1),
(68, 1, 4, '', '2016-10-06 05:39:40', 1, 1, 0, 1),
(69, 4, 1, '', '2016-10-06 10:28:37', 1, 1, 0, 1),
(70, 4, 1, '', '1969-12-31 18:30:00', 1, 1, 0, 1),
(71, 5, 1, '', '1970-01-01 05:00:00', 1, 1, 0, 1),
(72, 1, 86, '', '1970-01-01 05:00:00', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_languages`
--

CREATE TABLE IF NOT EXISTS `dating_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_translated` varchar(55) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_languages`
--

INSERT INTO `dating_languages` (`id`, `name`, `name_translated`) VALUES
(3, 'English', 'inglés'),
(4, 'Spanish', 'español'),
(5, 'French', 'francés'),
(6, 'Arabic', 'arabico'),
(7, 'Italian', 'italiano'),
(8, 'German', 'german'),
(9, 'Indian', ' Indian '),
(10, 'Chinese', 'Chino'),
(11, 'Other', 'otro');

-- --------------------------------------------------------

--
-- Table structure for table `dating_moreimages`
--

CREATE TABLE IF NOT EXISTS `dating_moreimages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_moreimages`
--

INSERT INTO `dating_moreimages` (`id`, `user_id`, `title`, `image`) VALUES
(18, 86, '', '8d97938777dd6a69794b6f385f72ece04c23d9f839f.jpg'),
(7, 1, 'Title1', '703e67a097e2b98c330035efed0c4462images12.jpg'),
(19, 86, '', 'dfb768a6c3091e4a9c884bf22af77271corazon-rosa-de-madera_267489792.jpg'),
(8, 8, 'Title1', 'cafe51eb07b1224ad07ad951f8dd7704goodone of kelsandmom.jpg'),
(9, 8, 'Title2', '2a830e2dd3ce61286cca05f98a068e5eHydrangeas.jpg'),
(14, 8, 'Title3', '167deb95c71c9cba9a94c6a3b908a3a0up-im.png'),
(11, 8, 'Title4', '0e00ca92905d78197d3921a3d7453a2fPenguins.jpg'),
(13, 1, 'Title3', '054b4fc61248a40603f3baf6e894d8aeup-im.png'),
(15, 8, 'Title4', '9aa232dcc8d88423e450642d8d802985sample.png'),
(16, 8, '', '2dff0b2a4cf83a4c5c790ff21d0a7cb5happy-face.jpg'),
(17, 105, '', 'c413b428f2c7e7b5c0bd0de591889d0bmothers-day-588087_640.jpg'),
(20, 1, '', '97940f7357814893eee9adc2b018da75Tulips.jpg'),
(21, 1, '', '6b51f22087b51f638dc02bcb1d2c7f8bLighthouse.jpg'),
(22, 5, '', 'e7d74bb72db7058296d8cec148fa30853d8b31d0c5fdea3b4c294e3cae4c1c90.jpg'),
(23, 5, '', 'cc240c267c6d9acca6c126218ab9341f6Asombrate-con-Cano-Cristales.jpg'),
(24, 1, '', 'e4a35a63db704296abbdc48534dd7bb46898998880_7d3c207fe7_b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dating_newsletter`
--

CREATE TABLE IF NOT EXISTS `dating_newsletter` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `user` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dating_reference`
--

CREATE TABLE IF NOT EXISTS `dating_reference` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `ref_user_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_reference`
--

INSERT INTO `dating_reference` (`id`, `ref_id`, `ref_user_id`, `user_id`) VALUES
(2, 'miamor000001', 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `dating_report`
--

CREATE TABLE IF NOT EXISTS `dating_report` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_for` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_report`
--

INSERT INTO `dating_report` (`id`, `user_id`, `report_for`, `message`, `date`) VALUES
(2, 1, 5, 'This is a test report...', '2015-01-21 05:30:58'),
(4, 1, 8, 'Test report...', '2015-01-21 05:54:33'),
(6, 1, 3, 'test report for payle', '2015-01-21 06:06:06'),
(8, 1, 5, 'test report...k', '2015-01-21 06:15:47'),
(9, 1, 4, 'test report...', '2015-01-21 06:21:01'),
(10, 1, 5, 'giuygiuhu', '2015-07-20 23:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `dating_sendnewsletter`
--

CREATE TABLE IF NOT EXISTS `dating_sendnewsletter` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_sendnewsletter`
--

INSERT INTO `dating_sendnewsletter` (`id`, `title`, `description`, `user`, `date`) VALUES
(1, 'This is the tile', 'desc', 0, '2015-03-11 06:31:22'),
(2, 'Title', '<p>\r\n	attchement with mail</p>\r\n', 0, '2015-03-11 07:07:38'),
(3, 'Find partners for fun, dating and long term relationships', '<p>\r\n	Find partners for fun, dating and long term relationships</p>\r\n', 0, '2015-03-11 07:09:40'),
(4, 'Find partners for fun, dating and long term relationships', '<p>\r\n	Find partners for fun, dating and long term relationships</p>\r\n', 0, '2015-03-11 07:13:08'),
(5, 'Find perfect match from miamor apps', '<p>\r\n	Find perfect match from miamor apps.&nbsp;<br />\r\n	with the attachement .</p>\r\n', 0, '2015-03-11 07:16:17'),
(6, 'Lorem test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		Lorem Ipsum Doller sit amet</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-06-09 12:36:27'),
(7, 'test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		Test</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-14 04:53:40'),
(8, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		Hi this is a test news.</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		Lorem ipsum doller sit amet</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 11:47:30'),
(9, 'The standard Lorem Ipsum passage, used since the 1500s', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		Lorem Ipsum doller sit amet</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 11:51:08'),
(10, 'Lorem Ipsum Doller Sit Amet', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<h1 style="color: blue; text-align: center;">\r\n		<span style="font-size:36px;"><strong>Lorem Ipsum Test Message</strong></span></h1>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p style="text-align: justify;">\r\n		Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n	<p style="text-align: justify;">\r\n		<img alt="" src="http://108.179.225.244/~nationalit/team2/miamor/images/bg1.jpg" style="width: 600px;" /></p>\r\n	<p style="text-align: justify;">\r\n		&nbsp;</p>\r\n	<p style="text-align: justify;">\r\n		There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words</p>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 12:00:50'),
(11, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df sdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 12:14:33'),
(12, 'Test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		TEST</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 09:49:38'),
(13, 'Test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		TEST</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-17 09:53:30'),
(14, 'Lorem Testing Mail', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		Lorem Ipsum Doller Sit Amet</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-20 12:56:40'),
(15, 'Lorem Ipsum Doller Sit Amet', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<h1>\r\n		<span style="font-size:48px;">Lorem Ipsum</span></h1>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-20 01:00:57'),
(16, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<h1 style="text-align: center;">\r\n		Lorem Ipsum Is A test News</h1>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-20 01:01:50'),
(17, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div style="text-align: center;">\r\n		<span style="font-size:48px;">Lorem Ipsum Doller Sit Amet</span></div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 06:30:15'),
(18, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf s</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 06:39:36'),
(19, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<p>\r\n	sd fsdf sdfsd</p>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 06:54:03'),
(20, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdfsdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 06:58:56'),
(21, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		vbnv bnv nbv</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:22:59'),
(22, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		vbnv bnv nbv</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:25:28'),
(23, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:27:39'),
(24, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:28:25'),
(25, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:29:55'),
(26, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:32:01'),
(27, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:34:44'),
(28, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		g gh fgh f</div>\r\n	<div>\r\n		fg fgh</div>\r\n	<div>\r\n		gf hfgh</div>\r\n	<div>\r\n		&nbsp;f</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:35:59'),
(29, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		g gh fgh f</div>\r\n	<div>\r\n		fg fgh</div>\r\n	<div>\r\n		gf hfgh</div>\r\n	<div>\r\n		&nbsp;f</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:38:43'),
(30, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		g gh fgh f</div>\r\n	<div>\r\n		fg fgh</div>\r\n	<div>\r\n		gf hfgh</div>\r\n	<div>\r\n		&nbsp;f</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:39:55'),
(31, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;ddfg dfgdfgd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:40:42'),
(32, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		cxv xcv xcv</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&nbsp;</div>\r\n', 0, '2015-07-21 07:44:54'),
(33, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		cx xdf dfg dfgdg</div>\r\n</div>\r\n', 0, '2015-07-21 07:47:05'),
(34, 'Lorem Ipsum Is A test News', '<div style="min-height:200px;width:100%;">\r\n	<div> fdg dfg dgfdf\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:47:52'),
(35, 'Lorem Ipsum Is A test News', '<div style="min-height:200px;width:100%;">\r\n	<div> fdg dfg dgdfg\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:48:34'),
(36, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://108.179.225.244/~nationalit/team2/miamor/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sd fsdf sdf sdf dsf dsf sdf dsf</div>\r\n	<div>\r\n		&nbsp;sdfsdfsdfds</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:50:39'),
(37, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://108.179.225.244/~nationalit/team2/miamor/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	d sd asd asdasd asdasd</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:54:09'),
(38, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://i.imgur.com/u0LR7dp.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df sdf sdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:55:55'),
(39, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		ds fsdf sdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:57:36'),
(40, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df dfg dfgdgdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 07:59:31'),
(41, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sf dfdfg dfg df</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:00:04'),
(42, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df dfg dfg dfgd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:02:32'),
(43, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="logo" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="logo" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df gdfg dfg dfg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:03:58'),
(44, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	</div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>ds fsdf\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:06:41'),
(45, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" alt="Miamor" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:07:58'),
(46, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		dds sdf sd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:09:44'),
(47, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" alt="Miamor" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:11:49'),
(48, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sd sdf dsf ds fsdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:13:47'),
(49, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" alt="Miamor" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 08:15:49'),
(50, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:21:33'),
(51, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sd fsdfssd sdf</div>\r\n	<div>\r\n		sd sdf sdfdfsdfsdfsd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:22:41'),
(52, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:23:58'),
(53, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sdfsdfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:25:05'),
(54, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:26:22'),
(55, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		Lorem Ipsum</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:29:29'),
(56, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img alt="Miamor" src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	ghj ghjghj gh\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:30:32'),
(57, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	&nbsp;</div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df gdg dfg df</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:31:42'),
(58, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		fd gdfg dfgdfg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:32:47'),
(59, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		f fd gdgdfgdfgdfg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:33:37'),
(60, 'Lorem Ipsum With Attached File', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		s dasd asasdasd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:50:12'),
(61, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<p>\r\n	&nbsp;dsf sdf sdfsd</p>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:51:22'),
(62, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	xcvfgdfggdfgdf\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:52:02'),
(63, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		sdf sf sdf sd</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:52:52'),
(64, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		f gdfg dfg d</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-21 09:53:05'),
(65, 'test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-22 10:20:02'),
(66, 'test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-07-22 11:38:48'),
(67, 'Lorem Ipsum From MiAmor', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		AJs a das dasd asd fsdgfsd gdfg df</div>\r\n	<div>\r\n		gf hfgh fghfgh</div>\r\n	<div>\r\n		gf hfgh</div>\r\n	<div>\r\n		&nbsp;gfh</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		fg&nbsp; hgffffffffffffffffffffffffffffffffffffg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-24 07:34:57'),
(68, 'Lorem Ipsum From MiAmor', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		AJs a das dasd asd fsdgfsd gdfg df</div>\r\n	<div>\r\n		gf hfgh fghfgh</div>\r\n	<div>\r\n		gf hfgh</div>\r\n	<div>\r\n		&nbsp;gfh</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		fg&nbsp; hgffffffffffffffffffffffffffffffffffffg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-24 07:35:02'),
(69, 'Lorem Ipsum Doller ', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		df dfg dg f</div>\r\n	<div>\r\n		dfg df</div>\r\n	<div>\r\n		gdf</div>\r\n	<div>\r\n		gfd</div>\r\n	<div>\r\n		&nbsp;df</div>\r\n	<div>\r\n		sgdfgd dfg dsg dsfg dstret rt</div>\r\n	<div>\r\n		re ert ertertdfgfdg dfg dfg dfgfdg dfg dfg dfg</div>\r\n	<div>\r\n		dfg dfg</div>\r\n	<div>\r\n		fdg dfgdfgerterter&nbsp; tertetdfsdf sdf sd fsdfsdf sdf</div>\r\n	<div>\r\n		sd fsdf sdf sdf sdsf werwer</div>\r\n	<div>\r\n		sd fsdrewrwerwerwesdf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sdrew rwer wer wfdss fs df</div>\r\n	<div>\r\n		sf sdf</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-24 07:37:42'),
(70, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;KAs asd oiu asdasodiu JUoi s dasd asoid sd a. au&nbsp; asdoiu a as iw eui&nbsp; n,sdjg fjsdgf u sdf su7yf8ejwbj sdgf sdfgus sdfsdhhds fhsd fnsdjfhksdf. sd f iuewrjkhkdsf sdfhisdyxmcbudhy. sfkjhsdif dsf isafxcvmbzx diufsdf xcv isdh.</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:37:50'),
(71, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://team3.nationalitsolution.co.in/dating/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;sd ff sdf sdf sdf sdf sdf sdf sdf</div>\r\n	<div>\r\n		sd fsdf sdf sdf s</div>\r\n	<div>\r\n		sd sdf s</div>\r\n	<div>\r\n		sd fsdfs d</div>\r\n	<div>\r\n		sd sdf sdf</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:47:05'),
(72, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://www.miamor.com.co/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		dfg dfg dfg</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		df gd</div>\r\n	<div>\r\n		fgdf</div>\r\n	<div>\r\n		g</div>\r\n	<div>\r\n		dfgd fg</div>\r\n	<div>\r\n		dffd gdfg</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:53:30'),
(73, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://www.miamor.com.co/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		fg hfgh fgh fgh sduif hs fsdf sdkjf hsdf ksdjfh iuesdj fsdhf ksjdhifuwe ifsdjbfkjsd fjkshfdsdfsdf</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sd fsdjfjsdflkjsdlf sd</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		sd fjsdlfjksld jfksd fosdjf oij esmdlkfj sdlj lfsjdf sdoifujowefkjsdlfj ssdjf lskjdflsdkjfs</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:55:30'),
(74, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://www.miamor.com.co/images/logo.png" title="Miamor" /></div>\r\n<p>\r\n	dfg dfgdfg</p>\r\n<p>\r\n	fdg</p>\r\n<p>\r\n	df</p>\r\n<p>\r\n	gdfg</p>\r\n<p>\r\n	dfg</p>\r\n<p>\r\n	fd</p>\r\n<p>\r\n	&nbsp;g</p>\r\n<p>\r\n	dfgdfgt et ert ert ert er fd g sdfgersfderser re</p>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:56:43'),
(75, 'Lorem Ipsum Is A test News', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://www.miamor.com.co/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		f gdfg df</div>\r\n	<div>\r\n		gdf g</div>\r\n	<div>\r\n		dfgdfg dfg dfjgdjg oidfjg dfj gdfg</div>\r\n	<div>\r\n		df gdf;kg ;dkfgl;dfkg dfk gldfkg d</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-26 11:57:24'),
(76, 'test', '<div style="text-align:center;background:#243745;padding:10px 0px;width:100%">\r\n	<img src="http://www.miamor.com.co/images/logo.png" title="Miamor" /></div>\r\n<div style="min-height:200px;width:100%;">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		Test</div>\r\n</div>\r\n<div style="text-align:center;background:#243745;padding:10px 0px;width:100%;color:#FFF">\r\n	&copy; 2014 miamor, Inc. Terms Privacy Policy</div>\r\n', 0, '2015-08-29 04:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `dating_sharing`
--

CREATE TABLE IF NOT EXISTS `dating_sharing` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sharing_text` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_sharing`
--

INSERT INTO `dating_sharing` (`id`, `name`, `sharing_text`) VALUES
(1, 'facebook', 'Test Facebook Text'),
(2, 'twitter', 'Test Twitter Text');

-- --------------------------------------------------------

--
-- Table structure for table `dating_sms`
--

CREATE TABLE IF NOT EXISTS `dating_sms` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mobilenumber` varchar(255) NOT NULL,
  `verifycode` varchar(255) NOT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_sms`
--

INSERT INTO `dating_sms` (`id`, `user_id`, `mobilenumber`, `verifycode`, `is_verified`) VALUES
(10, 3, '+917044352633', '1827282914', 0),
(6, 1, '+573142928688', '2142475828', 1),
(9, 5, '+919002561033', '550626946', 1),
(11, 49, '+919331861603', '2079533980', 0),
(12, 0, '+573142928688', '1015192544', 0),
(13, 154, '+573209172443', '973487884', 1),
(14, 157, '+573209172443', '1533339275', 1),
(15, 26, '919775777617', '1627375106', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_template`
--

CREATE TABLE IF NOT EXISTS `dating_template` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_template`
--

INSERT INTO `dating_template` (`id`, `subject`, `content`) VALUES
(1, 'Welcome to MiAmor', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [FIRST NAME]</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Please Click on this link below </font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">[LINK]</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">On behalf of the Company</font><br />\r\n																<font style="font-family:Arial, sans-serif; font-size:18px; color: #717171;">[FIRST NAME]</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright 2015 MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(3, 'You have received a message from', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME] ,</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																You have recieved a message from [SENDERNAME].Please login to see the message. [USERS]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(4, 'You Have Been Viewed By', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																You have been viewed by these people....</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																[USERS]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(5, 'These are matches we think you will like', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																These are matches we think you will like -</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																[USERS]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(6, 'Your Compatibility matches', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Your Compatibility matches...</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																[USERS]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(7, 'We haven’t seen you for awhile? we think you would like these singles…', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<br />\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Your Compatibility matches...</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																[USERS]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright [YEAR] MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n'),
(8, 'Thank You For Subscribing with us', '<table bgcolor="#e8e8e8" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#718ea0" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td style="text-align:left;" width="90%">\r\n												<img alt="" height="49" src="http://www.miamor.com.co/images/logo.png" width="199" /></td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:30px; color: #343434;">Hi,</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #717171;">Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#0099ff" height="70">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="600">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td height="70" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff;">Copyright 2015 MiAmor</font></td>\r\n															<td align="right" width="50%">\r\n																<font style="font-family:Arial, sans-serif; font-size:14px; color: #fff; text-align:right;"><a href="" style="text-decoration:none; color:#fff;">Terms and Conditions</a> | <a href="" style="text-decoration:none; color:#fff;">Privacy Policy</a></font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `dating_translation_package`
--

CREATE TABLE IF NOT EXISTS `dating_translation_package` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `days` int(5) NOT NULL,
  `price` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_translation_package`
--

INSERT INTO `dating_translation_package` (`id`, `name`, `days`, `price`) VALUES
(1, 'Package 1', 7, 15),
(2, 'Package 2', 14, 30),
(3, 'Package 3', 21, 45),
(4, 'package 4', 30, 55);

-- --------------------------------------------------------

--
-- Table structure for table `dating_userimages`
--

CREATE TABLE IF NOT EXISTS `dating_userimages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_img` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_userimages`
--

INSERT INTO `dating_userimages` (`id`, `user_id`, `title`, `image`, `main_img`) VALUES
(1, 8, '', '07cae5548f849831574d856f1867ed6bPenguins.jpg', 1),
(2, 8, '', 'a99af7d5ce1b1de9362e86b66c0434c8Lighthouse.jpg', 0),
(5, 8, '', 'be2b06fa5253a1963dec66de6327db56Tulips.jpg', 0),
(12, 26, '', '269f49c0e5768cf33dafde21724201a9design.png', 0),
(13, 87, '', '33ead1c375fc5c0ccee455a0727e8fe1logo.png', 1),
(17, 1, '', '1c1f13fca12205c56d443c71ef2a48eb230_OL_product.jpg', 1),
(18, 5, '', 'e2cea19eb97125e9c6c3f02f2b762404default.png', 0),
(19, 11, '', '32cdeded708ceddc5a61b8e71288c7f8test111.jpg', 1),
(20, 86, '', 'e655812a77bb5e0bf067bc71d68cf04cDollarphotoclub_36455021.jpg', 0),
(21, 161, '', '979153b84a9e90cbff596f76c851d0c6Shawn_Tok_Profile.jpg', 1),
(22, 163, '', '29856c8bc55ccb9e69fdcf14410e4c39images.jpeg', 1),
(23, 120, '', '48608e4682a4bca687e431641354ef06profile-pics-18.jpg', 1),
(24, 26, '', '8bf5cf33fc1d5bffcdbb1879b98fc3b93d8b31d0c5fdea3b4c294e3cae4c1c90.jpg', 0),
(25, 86, '', 'b8643e92fe62888914f5e2f60b5657832c57bee25a2fd4e11d94cd841588a406.jpg', 0),
(26, 86, '', 'dd26471a44a71f1c1966f145a18830f61385710016_kak-virtualnaya-lyubov-mozhet-stat-realnoy.jpg', 1),
(27, 4, '', '9e91bbc6b5e104d79382e29a3e697416Koala.jpg', 0),
(28, 5, '', 'cb7a8692b1f68218a4b34182d20a4d351_-cartagena.jpg', 1),
(29, 1, '', '9eeeb7f98f280cd2daa4a4706eb1b86aSunrise-Jump.jpg', 0),
(31, 26, '', 'c81241b1b04e33beaac544934c2157d0youth-570881_960_720.jpg', 1),
(32, 26, '', '3b638adc468b2638f3ee8e1a389fd865champix.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_user_video`
--

CREATE TABLE IF NOT EXISTS `dating_user_video` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dating_user_video`
--

INSERT INTO `dating_user_video` (`id`, `user_id`, `video`) VALUES
(2, 1, 'www.youtube.com/watch?v=eytE4BU9Atk'),
(3, 1, 'https://www.youtube.com/watch?v=WH9ZJu4wRUE'),
(4, 5, 'https://www.youtube.com/watch?v=4VaJRci4rl8'),
(7, 5, 'https://www.youtube.com/watch?v=MjqRd2ovOxw'),
(8, 4, 'https://youtu.be/yGJXt6RV4eY'),
(9, 1, 'https://youtu.be/yGJXt6RV4eY'),
(11, 4, 'https://youtu.be/yGJXt6RV4eY'),
(12, 26, 'https://www.youtube.com/watch?v=MjqRd2ovOxw'),
(13, 1, 'https://youtu.be/a5fHoAx12DY'),
(14, 32, 'https://www.youtube.com/watch?v=7aHl0srdVEo'),
(15, 32, 'https://www.youtube.com/watch?v=px900Y_uxQU'),
(16, 32, 'https://www.youtube.com/watch?v=dlfjLWac6qY');

-- --------------------------------------------------------

--
-- Table structure for table `dating_videosessionchat`
--

CREATE TABLE IF NOT EXISTS `dating_videosessionchat` (
  `id` int(11) NOT NULL,
  `to_id` int(11) DEFAULT NULL,
  `from_id` int(11) DEFAULT NULL,
  `sessionid` text,
  `tok` text,
  `is_session` tinyint(1) NOT NULL DEFAULT '0',
  `quit` tinyint(1) DEFAULT '0',
  `is_finished` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=233 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_videosessionchat`
--

INSERT INTO `dating_videosessionchat` (`id`, `to_id`, `from_id`, `sessionid`, `tok`, `is_session`, `quit`, `is_finished`) VALUES
(60, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2NjczODk0NH5nZXc0OTdaTTA3WWN2L2tzODhFVnFud0t-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NWQzZDA1OTkxNTBjOWFhMGY3MjY4YjFiMDkxMjU4NTE2MzliNTdiNjpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTmpjek9EazBOSDVuWlhjME9UZGFUVEEzV1dOMkwydHpPRGhGVm5GdWQwdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NjczOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY2NzM5LjAxMTIyMjY5MDM0NSZleHBpcmVfdGltZT0xNDMxMDc3NTM5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(61, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzA0NTMxNn4yTEVEOWhxNjRiRWlreUNrMHpUZmZkMFJ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZDAwNDg0YWNiODViZGFhNmE2MGFhYjczMDY3ODgyZTgxMWZjM2Y4MjpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpBME5UTXhObjR5VEVWRU9XaHhOalJpUldscmVVTnJNSHBVWm1aa01GSi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzA0NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MDQ1LjM3NzkzNzQ5Mjk4JmV4cGlyZV90aW1lPTE0MzEwNzc4NDUmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(62, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2NzI1NTkzNX5MQTNISUpCVnJHZDdzK0RYS0lhdzFWUWN-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MTM1Nzk5ZGZlMGYyMTcyYjBmZGZmYTA0OGY4ODkwYWI2OWRmYjBhNTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpJMU5Ua3pOWDVNUVROSVNVcENWbkpIWkRkekswUllTMGxoZHpGV1VXTi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzI1NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MjU2MTg0MTAzNjcyOCZleHBpcmVfdGltZT0xNDMxMDc4MDU1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(63, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzM0MjAxOH4vYU15blFMMEluTzhqK1dQUzlXc3VhVGV-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9Nzk3OGMzNTcyM2Q3NTNiMzkzYWYyODQ1MTNlMjA0MzExMzk0NWM0YTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpNME1qQXhPSDR2WVUxNWJsRk1NRWx1VHpocUsxZFFVemxYYzNWaFZHVi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzM0MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3MzQyLjA4NzA3ODk1MDkmZXhwaXJlX3RpbWU9MTQzMTA3ODE0MiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(64, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzQxOTAyM35rUHZtZ1BVQzNRb1I3Y1k3Y3c3V1dwK0x-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZGZkM2JmNjhjNjY4ZTY1OGU2ZTA1NjA1MWIwNTY5YTRmM2IxYzczMDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpReE9UQXlNMzVyVUhadFoxQlZRek5SYjFJM1kxazNZM2MzVjFkd0sweC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzQxOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3NDE5LjA4MjE0NTU1Mzk3NyZleHBpcmVfdGltZT0xNDMxMDc4MjE5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(65, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzUzNDkzNX5HS0VxVGd4VnFXSmFNUXNNMmRrSlZFYkx-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NzY4ZGVjYmY1YjM3MGRhNDI4NTc4Y2E1ODdiMmVmMmI4MzhmOTBiZDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnpVek5Ea3pOWDVIUzBWeFZHZDRWbkZYU21GTlVYTk5NbVJyU2xaRllreC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzUzNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3NTM1MTMwNDI0MjQ1MyZleHBpcmVfdGltZT0xNDMxMDc4MzM0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(66, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2NzkwNDE4N35LZ2hGUGVEL0k2RDluVUJ5bklTdTVadGx-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MzIyMTU4ODM0YzIzNGU5NDI1NzU3MzQzZWE4MTEyMDczNmRjMWQyODpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprd05ERTROMzVMWjJoR1VHVkVMMGsyUkRsdVZVSjVia2xUZFRWYWRHeC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2NzkwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTA0LjI0MTg5MzkwMTM0MCZleHBpcmVfdGltZT0xNDMxMDc4NzA0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(67, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2Nzk2NjI4M354RGhnT09ybUN0Ym8xV1FPd1o4T21qRVB-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9M2I2ZWMyNWJiNDM3ODc2ODg0MzYxNGY0NzUxMTc4NzIxMjcyNTI0NzpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprMk5qSTRNMzU0UkdoblQwOXliVU4wWW04eFYxRlBkMW80VDIxcVJWQi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2Nzk2NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTY2LjM0MTk0NTQxNzYzJmV4cGlyZV90aW1lPTE0MzEwNzg3NjYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(68, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTA2Nzk5NzE5NH5GN2xla25sc2JYeWEybmFHV1U0c2EydkR-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NWNkYjViYjhjZTM3MGYyZWUzNTczMTA1MzEyZDA4ZDExYTQ3NDU3MDpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyTnprNU56RTVOSDVHTjJ4bGEyNXNjMkpZZVdFeWJtRkhWMVUwYzJFeWRrUi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2Nzk5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY3OTk3LjI2MTMxMjA5OTc5MiZleHBpcmVfdGltZT0xNDMxMDc4Nzk3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(69, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2ODA3ODkyNX52RG1qZ25HTURubE1xWnAydHkxdFBBSE1-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NmE3ODA0MjE1Yzk4YTRlZDk3YWI2NDc5MzhmMTgxZDcwY2JiMmRmMTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RBM09Ea3lOWDUyUkcxcVoyNUhUVVJ1YkUxeFduQXlkSGt4ZEZCQlNFMS1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODA3OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MDc4Ljk5NTM2NTE0ODk2JmV4cGlyZV90aW1lPTE0MzEwNzg4NzgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(70, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA2ODEwNTk5N34wNzdtUmlCWSt1bnhpM0xUWWZoSm9Tbit-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZTNkOWQxOTY0M2VkNjZiOWUyZGI5YTczMTFkZGI2ZDk2OTA0MTYxMTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RFd05UazVOMzR3TnpkdFVtbENXU3QxYm5ocE0weFVXV1pvU205VGJpdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODEwNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MTA2LjA2MTM1MDQzMjQxOCZleHBpcmVfdGltZT0xNDMxMDc4OTA2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(71, 1, 5, '1_MX40NTIzMTAxMn5-MTQzMTA2ODE4MTE0Nn5oYXp5TTBLSzlZY2dlYmpLTlY0b0VNTUd-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9OTdhYjkyNjZjNGMwZTlkOTE1MTE0Njc2Zjg0NzJiZWJhNjVkZjdmYzpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVEEyT0RFNE1URTBObjVvWVhwNVRUQkxTemxaWTJkbFltcExUbFkwYjBWTlRVZC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA2ODE4MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDY4MTgxLjIxMTM3MDkwMzcyOCZleHBpcmVfdGltZT0xNDMxMDc4OTgxJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(72, 1, 5, '2_MX40NTIzMTAxMn5-MTQzMTA5MTYzMzc1OX54UlRWaXRJeDdqRU9BWVJRcktGYUV0N1B-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9NDBjM2I0NzY0ZmFjOGM3NTA3NGQ4Yjc1M2RjNmRmMmJiODJlOWQ3MDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEE1TVRZek16YzFPWDU0VWxSV2FYUkplRGRxUlU5QldWSlJja3RHWVVWME4xQi1VSDQmY3JlYXRlX3RpbWU9MTQzMTA5MTYzMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDkxNjMzLjgyOTE4ODEwMjY5JmV4cGlyZV90aW1lPTE0MzExMDI0MzMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(73, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTA5MjAzNjQyMH5ncGtqejJjc0NsY2ZSTHBWcXdPR3hZa0t-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZjJiNjdiOTY4MGM2ZTU4YzExMjM3NDA3NWM4ZjY1OGQzZmI2NjA2NTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVEE1TWpBek5qUXlNSDVuY0d0cWVqSmpjME5zWTJaU1RIQldjWGRQUjNoWmEwdC1VSDQmY3JlYXRlX3RpbWU9MTQzMTA5MjAzNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMDkyMDM2LjQ4MTAxMzUxNTYwNCZleHBpcmVfdGltZT0xNDMxMTAyODM2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(74, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMTMyMzYyNzc2MX45eWlIWUdDVEpOSzJIVE8yWldVYWJiVHF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9Y2RkNTk0NWE4YzA4NmFjOGEzMzcyNDU5OGY0OGJjYzM4MDY2ODE4MzpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVE15TXpZeU56YzJNWDQ1ZVdsSVdVZERWRXBPU3pKSVZFOHlXbGRWWVdKaVZIRi1VSDQmY3JlYXRlX3RpbWU9MTQzMTMyMzYyNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzIzNjI3LjgyMTgxNjcxNzkyNSZleHBpcmVfdGltZT0xNDMxMzM0NDI3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(75, 1, 5, '2_MX40NTIzMTAxMn5-MTQzMTMyMzY5NDgzNn5EVG5qc2dOeEJKcWJpbDUxRm85cTRaL2x-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9OGE5ZjUxMjUwNGNjN2MxM2Y2OTk2ZTI5MGU1Nzc1YjIzNTM1OGY5NDpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNVE15TXpZNU5EZ3pObjVFVkc1cWMyZE9lRUpLY1dKcGJEVXhSbTg1Y1RSYUwyeC1VSDQmY3JlYXRlX3RpbWU9MTQzMTMyMzY5NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzIzNjk0LjkyMDI2Mjk0OTI3JmV4cGlyZV90aW1lPTE0MzEzMzQ0OTQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(76, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMTM3ODkwNDM0MH41QTU3VVNOQ0t4Qkh4akpmazd5SjRocGR-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZDdiNTE1MDUyN2I3YTM4NDc3OTdiZGI0MmUxMGYyZjhlYTBjNjQ1NTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNVE0zT0Rrd05ETTBNSDQxUVRVM1ZWTk9RMHQ0UWtoNGFrcG1hemQ1U2pSb2NHUi1VSDQmY3JlYXRlX3RpbWU9MTQzMTM3ODkwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMxMzc4OTA0LjQxNzc2OTUwMDMwJmV4cGlyZV90aW1lPTE0MzEzODk3MDQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(77, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMjA0NDYwNDAzOX5ObGxLTkZIVWRGaDRzdnVIU2RRWnJoWEF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9N2RjMzg5N2U2MzI2YzlhODJhNmQyYjYwZDk5ZTVlNGU1YWZlMjY0YTpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNakEwTkRZd05EQXpPWDVPYkd4TFRrWklWV1JHYURSemRuVklVMlJSV25Kb1dFRi1VSDQmY3JlYXRlX3RpbWU9MTQzMjA0NDYwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyMDQ0NjA0LjE3NzczODYzMjAmZXhwaXJlX3RpbWU9MTQzMjA1NTQwNCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(78, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMjA3MzEzMzM5MH4vbzRHeUJDUDY0SEZJYzNQbGNSUUxqSnF-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MzhiYzFiNzNhYjIwNjlmNGQwYjgxYmY5MmVhZjU0NDMwNmRkMmM4YjpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNakEzTXpFek16TTVNSDR2YnpSSGVVSkRVRFkwU0VaSll6TlFiR05TVVV4cVNuRi1VSDQmY3JlYXRlX3RpbWU9MTQzMjA3MzEzMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyMDczMTMzLjQ1MTU5OTg0MzQwMSZleHBpcmVfdGltZT0xNDMyMDgzOTMzJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(79, 1, 5, '1_MX40NTIzMTAxMn5-MTQzMjU0MTg5NzMxM341VDV0S2pFSitVbXhpQ2RaRExEY3dIREZ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZWE3N2QxYWQzN2U4YjA4Nzk0NTExODYwMGFkYzY5MjM0N2MyMTlkYjpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNalUwTVRnNU56TXhNMzQxVkRWMFMycEZTaXRWYlhocFEyUmFSRXhFWTNkSVJFWi1VSDQmY3JlYXRlX3RpbWU9MTQzMjU0MTg5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMyNTQxODk3LjM3MTY3MDYwMjQ3MCZleHBpcmVfdGltZT0xNDMyNTUyNjk3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(80, 5, 1, '2_MX40NTIzMTAxMn5-MTQzMzEwMDE0ODM5MH5mM3pCZjVFb0UvWXpoWHpQUmI2TUpuVUZ-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9MWE2ODA4MzQ3Yzk4MzZhYjAxMzljYWE3MmYwYmE2MDcyYWNkOGY2ODpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNekV3TURFME9ETTVNSDVtTTNwQ1pqVkZiMFV2V1hwb1dIcFFVbUkyVFVwdVZVWi1VSDQmY3JlYXRlX3RpbWU9MTQzMzEwMDE0OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMTAwMTQ4LjQ1NTgyMzg0MTcyJmV4cGlyZV90aW1lPTE0MzMxMTA5NDgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(81, 5, 1, '1_MX40NTIzMTAxMn5-MTQzMzE2MzYzNzA4N345Zyt4ZldJSmxiUW53eHp1dXpWcGNhQnp-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZTYzYTkzYzRiNzU4ZDJhYmRkMWZhMTlkZTlhYjNmOTA2ZmU0MTFjMTpzZXNzaW9uX2lkPTFfTVg0ME5USXpNVEF4TW41LU1UUXpNekUyTXpZek56QTROMzQ1Wnl0NFpsZEpTbXhpVVc1M2VIcDFkWHBXY0dOaFFucC1VSDQmY3JlYXRlX3RpbWU9MTQzMzE2MzYzNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMTYzNjM3LjE1MjAwNDE0NTgxJmV4cGlyZV90aW1lPTE0MzMxNzQ0MzcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(82, 1, 8, '2_MX40NTIzMTAxMn5-MTQzMzMzNDc4ODQ2NH5IZEVDOU1rNi9Ubk44bExDbHUzdFowQVp-UH4', 'T1==cGFydG5lcl9pZD00NTIzMTAxMiZzaWc9ZGYyMDJkOTRlNjM4YjNjNzM1YjJlYTMyZmQ0YzU4NjBhNDllZTZhMzpzZXNzaW9uX2lkPTJfTVg0ME5USXpNVEF4TW41LU1UUXpNek16TkRjNE9EUTJOSDVJWkVWRE9VMXJOaTlVYms0NGJFeERiSFV6ZEZvd1FWcC1VSDQmY3JlYXRlX3RpbWU9MTQzMzMzNDc4OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDMzMzM0Nzg4LjUyOTczNTQwODc4JmV4cGlyZV90aW1lPTE0MzMzNDU1ODgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(83, 5, 1, '2_MX40NTMwOTQyMn5-MTQzOTM3NjQzOTY5MX5la3JFQzJ6SlJGeUFSajV4TVRBNEhBbXZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9Mjc5MTExMmY2ODU5Mjk1NzdlMjdmNzAyNWNiMGU0ZjMxM2M1MDU3YzpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUXpPVE0zTmpRek9UWTVNWDVsYTNKRlF6SjZTbEpHZVVGU2FqVjRUVlJCTkVoQmJYWi1VSDQmY3JlYXRlX3RpbWU9MTQzOTM3NjQzOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDM5Mzc2NDM5LjY5NTM2NzIwNDMmZXhwaXJlX3RpbWU9MTQzOTM4NzIzOSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(84, 5, 1, '2_MX40NTMwOTQyMn5-MTQzOTM3ODA0MTA3NH53Ly9rZmQwZFFrdit0V0pXZ3YyR3RzNnF-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9N2UwZGQ1YzY2MDJkNTlmZjNkZTBmNGM0YTQyODczOTMwOTY0Yjg1OTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUXpPVE0zT0RBME1UQTNOSDUzTHk5clptUXdaRkZyZGl0MFYwcFhaM1l5UjNSek5uRi1VSDQmY3JlYXRlX3RpbWU9MTQzOTM3ODA0MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDM5Mzc4MDQxLjA2MTIyMjQ3MDkyNiZleHBpcmVfdGltZT0xNDM5Mzg4ODQxJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(85, 5, 1, '2_MX40NTMwOTQyMn5-MTQ0MDQwOTA3MjcyNn5XRUcrMENVdDJIYXZEaHpYQ3l0b1NrUXl-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NjA1MWQ1ZDU2ZmNiMjE2MjgxMzdkYjIyYTk1MjAyNDdjZTZjMzAyMzpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNRFF3T1RBM01qY3lObjVYUlVjck1FTlZkREpJWVhaRWFIcFlRM2wwYjFOclVYbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MDQwOTA3MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQwNDA5MDcyLjcyMjYzMTQ2NDQ2JmV4cGlyZV90aW1lPTE0NDA0MTk4NzImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(86, 5, 1, '2_MX40NTMwOTQyMn5-MTQ0MDU5MzU1MDcxN35LQzJrS2U5aHNUYzRnc2NvVUl1cUtuQkZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NTlhZWYzNmQzZWFlMGI0MjY5MWM0N2Y4MGIyNzZkZGQ2YmFmOGY0YTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNRFU1TXpVMU1EY3hOMzVMUXpKclMyVTVhSE5VWXpSbmMyTnZWVWwxY1V0dVFrWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MDU5MzU1MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQwNTkzNTUwLjcyMTI3MDM1OTQyMiZleHBpcmVfdGltZT0xNDQwNjA0MzUwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(87, 8, 1, '1_MX40NTMwOTQyMn5-MTQ0MDc5Mzk2NDQ3MX5rbXBaOE13bDdGQnBIQ09HZjNSbzJTMmV-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NjhlZTAyMzVlNTNjMjRjNTdmYWMwNThhMGEyMjFlYzUxZjVlY2NkZjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNRGM1TXprMk5EUTNNWDVyYlhCYU9FMTNiRGRHUW5CSVEwOUhaak5TYnpKVE1tVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MDc5Mzk2NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQwNzkzOTY0LjQ4Nzc5NjUxOTQyJmV4cGlyZV90aW1lPTE0NDA4MDQ3NjQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(88, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MDc5NDIxODYyN343MVkvRkw2bmNFRkRCbGNCTTR2Kzc2azl-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NGQ3NjUzYjY1Y2VlZGJiODM0NzEyNDk0ZGNlNmQ0NWUyMzg4MzcyNjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNRGM1TkRJeE9EWXlOMzQzTVZrdlJrdzJibU5GUmtSQ2JHTkNUVFIyS3pjMmF6bC1VSDQmY3JlYXRlX3RpbWU9MTQ0MDc5NDIxOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQwNzk0MjE4LjYxNjA3Njg1MyZleHBpcmVfdGltZT0xNDQwODA1MDE4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(89, 1, 105, '1_MX40NTMwOTQyMn5-MTQ0MDg3MjU4NDU0Nn52N0R1R2ExS3hpZ0JJczhBVHh5Sm8xOUV-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NTIxYmM3NmI1ZmE5NWZkNGY4NzBkMGJmMDNhYzc2MmEzMjExMTIyNjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNRGczTWpVNE5EVTBObjUyTjBSMVIyRXhTM2hwWjBKSmN6aEJWSGg1U204eE9VVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MDg3MjU4NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQwODcyNTg0LjUzMTAwMTE2MTM5MCZleHBpcmVfdGltZT0xNDQwODgzMzg0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(90, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTA0MjQxNTgwN35ITFRWNzJGazI2KzV3RGNncGxCME50Rll-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NmMzMjI0NWRmMTIxMzcyZTIyMWUxNjFjZGVlY2YxNWJkYjEyMWE0MzpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEEwTWpReE5UZ3dOMzVJVEZSV056SkdhekkyS3pWM1JHTm5jR3hDTUU1MFJsbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTA0MjQxNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMDQyNDE1Ljc5MTU5MTQ3Mzg5MyZleHBpcmVfdGltZT0xNDQxMDUzMjE1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(91, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTA4OTQ2MzQyN35QeDl6MWJLR3V2emxpNU9uOVlNVmQrakh-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZDNiOWJiOGZiOTM1MTVlMTAyZmI3YWIyODAyZGYzZmJmZTNmMTZmNTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEE0T1RRMk16UXlOMzVRZURsNk1XSkxSM1YyZW14cE5VOXVPVmxOVm1RcmFraC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTA4OTQ2MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMDg5NDYzLjQyMzU5NzAzNDY1JmV4cGlyZV90aW1lPTE0NDExMDAyNjMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(92, 105, 1, '1_MX40NTMwOTQyMn5-MTQ0MTEzMTY3MjEzN35Genp3OEI5eHBxdHlnTUdqSDQrTVFTMkJ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MzEyNDc4OTYxNTVjMDEzZTk0YTMxODJiM2Y2NjNjM2EwOTFhMzM0NTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEV6TVRZM01qRXpOMzVHZW5wM09FSTVlSEJ4ZEhsblRVZHFTRFFyVFZGVE1rSi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTEzMTY3MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMTMxNjcyLjEyMjEzNjcxMDYxMCZleHBpcmVfdGltZT0xNDQxMTQyNDcyJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(93, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTI3NDc1MDk3OX4wa3lzZjh5QU0vaUNaZk43SXhQcStvOTR-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZTAyY2NjZjEwOTQ2OTgzN2UyOTUwOTFkZjE2ZGVjOGFkZGRhYmRlOTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEkzTkRjMU1EazNPWDR3YTNselpqaDVRVTB2YVVOYVprNDNTWGhRY1N0dk9UUi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTI3NDc1MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMjc0NzUwLjk3MTgwNDYwMjg0NiZleHBpcmVfdGltZT0xNDQxMjg1NTUwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(94, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTI3NjAwMzAxOH5WUUJLc3JJdklQU0lxV0dsWkJPeEFzNUZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZGY2OTYyMGQwOTRhYzI3NzFjOWMzZDlhNTQyNWQwNGY3ODllZmI1NjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEkzTmpBd016QXhPSDVXVVVKTGMzSkpka2xRVTBseFYwZHNXa0pQZUVGek5VWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTI3NjAwMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMjc2MDAzLjAxNDg2Mjk5MTIwJmV4cGlyZV90aW1lPTE0NDEyODY4MDMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(95, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTI3Nzc5NDEwOH41SUUxc3h2aHRsVEhTSlMwN0xOd2tyT1d-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NjM5ODc4MDc3ZWRkMjJmZTcyYTM0NTJhZjY2NDIwZjFjYzBiYTA2YjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVEkzTnpjNU5ERXdPSDQxU1VVeGMzaDJhSFJzVkVoVFNsTXdOMHhPZDJ0eVQxZC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTI3Nzc5NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMjc3Nzk0LjA5MjAxNDgyNTAxJmV4cGlyZV90aW1lPTE0NDEyODg1OTQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(96, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTM1NDU3NDM4MH5nUnkyWVg4cU0wTEJhZG8zYW9ZYVBoUzZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9Yjg3NWIyZTVlZjQ3ZDdjNGUxNDI0Y2RhODM5Zjg3YjA5Mjg2MTRhNTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTkRVM05ETTRNSDVuVW5reVdWZzRjVTB3VEVKaFpHOHpZVzlaWVZCb1V6Wi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NDU3NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU0NTc0LjM3NDY3NzYwMDkmZXhwaXJlX3RpbWU9MTQ0MTM2NTM3NCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(97, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTM1NDU4Nzc5OH50aXJjUWppa0k3RXVvNHhqWkY1TEs0enV-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZDExNDhiNTA3YjVjNTQ0NzE4MjQ0MjRkMWVmYzMyYmRmMmQ3OTJmYTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTkRVNE56YzVPSDUwYVhKalVXcHBhMGszUlhWdk5IaHFXa1kxVEVzMGVuVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NDU4NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU0NTg3Ljc4MTcyNjM4MjE1NyZleHBpcmVfdGltZT0xNDQxMzY1Mzg3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(98, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTM1NDcxMjQxOH5tcWVQbWhKQjlsalQ3QzZnRERNQzgreWl-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9YWU4MWI1NjdlMjljMTA1MmMzMWVkN2U5NmViMzUxYzFmZTUwZTY4MDpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTkRjeE1qUXhPSDV0Y1dWUWJXaEtRamxzYWxRM1F6Wm5SRVJOUXpncmVXbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NDcxMiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU0NzEyLjQ5NjY4OTU1NjgmZXhwaXJlX3RpbWU9MTQ0MTM2NTUxMiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(99, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTM1NDkxMzk2OH5HTWVLUEI5UjRKRW9WMGEycUFQU0gvU2x-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZjcyMjU0MzA2YTNkMjAyYzI2NTQ0ZjVhNTFmNWIxMWE4MDU5OTI2ZjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTkRreE16azJPSDVIVFdWTFVFSTVValJLUlc5V01HRXljVUZRVTBndlUyeC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NDkxMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU0OTEzLjk0MTk3MzczMjM3OCZleHBpcmVfdGltZT0xNDQxMzY1NzEzJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(100, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTM1NDkyNTg1NX5NT2RLNWFBVXNIcTNSRG9NcmxlOWVpbzh-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NzJiZTExYTUzYzNmNzM5MGNiZjVlODMwZWZkNDViYjhmNzFiNjI1MjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTkRreU5UZzFOWDVOVDJSTE5XRkJWWE5JY1ROU1JHOU5jbXhsT1dWcGJ6aC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NDkyNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU0OTI1LjgzMTg0Mjk5NDg4MyZleHBpcmVfdGltZT0xNDQxMzY1NzI1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(101, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTM1NTAxNjc4NX5nZmtYc2s4SmJ6QlFrTzJDTmxsTWpIOW9-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9Njc5ZGVkYmE1NWFiNWJjM2UwNjM3MjM0NjlhMjMzNGE4NzdiZDFjYjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTlRBeE5qYzROWDVuWm10WWMyczRTbUo2UWxGclR6SkRUbXhzVFdwSU9XOS1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NTAxNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU1MDE2Ljc2MjE0NTMzMzk0JmV4cGlyZV90aW1lPTE0NDEzNjU4MTYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(102, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTM1NTA4NDkzNH5UMnk4dmw2Y2RKcUNqdnRkQ09tMU9udVB-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NDAyOTZiZDNlYjNkOTg0ZTQwZDAzNTk4MzA5YmI5NjE1M2VkN2EwYTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVE0xTlRBNE5Ea3pOSDVVTW5rNGRtdzJZMlJLY1VOcWRuUmtRMDl0TVU5dWRWQi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTM1NTA4NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxMzU1MDg0LjkxMTg2MzcxMzI3NCZleHBpcmVfdGltZT0xNDQxMzY1ODg0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(103, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTQ2NDM3MzAxMH5DbE5Ea2dhcEtYTkN6eC9pZlo2T2FtcGx-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZWEyZTRkNzBiNWJkNDA4OGVjZDFmZDM4MTMyZmExNGM5YzJkNjk4NDpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFEyTkRNM016QXhNSDVEYkU1RWEyZGhjRXRZVGtONmVDOXBabG8yVDJGdGNHeC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTQ2NDM3MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNDY0MzcyLjk5MTM5MTY5NzM2NyZleHBpcmVfdGltZT0xNDQxNDc1MTcyJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(104, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTQ2NTAyODYyMH4wK0NKODNScFlyL0pHNkpMOWRpVlFDUDJ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZGNhOTgwNzE0ODIzODAxZWE4OTNiMmNiOGRlOTc4MjI1N2Q2YjNjYTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFEyTlRBeU9EWXlNSDR3SzBOS09ETlNjRmx5TDBwSE5rcE1PV1JwVmxGRFVESi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTQ2NTAyOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNDY1MDI4LjU5MTA2MDU1MzkyOCZleHBpcmVfdGltZT0xNDQxNDc1ODI4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(105, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTQ2NTA2NjU0NX5XV0lJeHk5a3IrRnN1T2RZYzRsN1dwUXF-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MmJjNWUwMGUyZmNkMmJlZmY1YmNjMWFiMWMwZTg4YTc5OTRhNzU0NjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFEyTlRBMk5qVTBOWDVYVjBsSmVIazVhM0lyUm5OMVQyUlpZelJzTjFkd1VYRi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTQ2NTA2NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNDY1MDY2LjUyNTYxMzMyODYmZXhwaXJlX3RpbWU9MTQ0MTQ3NTg2NiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(106, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTU0Mjc3MTI5OH5TWk9hckVmYVFHWTVyamZFSWpXdncvelN-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NTY5OGNlZjMxNmFkNmNlMjlmYzg2MDJjMGE2ODFhMDA4Yzg4YmY5ZDpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFUwTWpjM01USTVPSDVUV2s5aGNrVm1ZVkZIV1RWeWFtWkZTV3BYZG5jdmVsTi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTU0Mjc3MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNTQyNzcxLjI5MTUzMTczNDU3MiZleHBpcmVfdGltZT0xNDQxNTUzNTcxJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(107, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTU0Mjc5NzgyNn56VXRiNER2WUJiVkl2WlJURGxLeXBTcHh-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MTZmZmU5NmVlOTk3MmRmZDMyYjJmMmVkZDI2NmUzMWZmN2M3NWY5NTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFUwTWpjNU56Z3lObjU2VlhSaU5FUjJXVUppVmtsMldsSlVSR3hMZVhCVGNIaC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTU0Mjc5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNTQyNzk3LjgxMTA0NjgzMjczMyZleHBpcmVfdGltZT0xNDQxNTUzNTk3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(108, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYxNzg4NTY3MX5VYkM4b3lmeEtFTkJKN3JQbER5eTVHZEx-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NzM5OGJkZmY3ZTBhMzcyMTAwODNhMzhhYzljZmNlNzdiZWRiZTczNjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4TnpnNE5UWTNNWDVWWWtNNGIzbG1lRXRGVGtKS04zSlFiRVI1ZVRWSFpFeC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxNzg4NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE3ODg1LjY2MTU5MzQ4NzQ3NCZleHBpcmVfdGltZT0xNDQxNjI4Njg1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(109, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYxODAxOTM4OH5CYUpQbzBvR1lXditFRk0vczhVRlJOcUt-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9M2E2NmE5NTgyMTFmYzY4NzcyOTEwOGQ4NTMxMTk1NWRmM2E3YjRmZDpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T0RBeE9UTTRPSDVDWVVwUWJ6QnZSMWxYZGl0RlJrMHZjemhWUmxKT2NVdC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxODAxOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE4MDE5LjM3MjU0OTEwMTAwJmV4cGlyZV90aW1lPTE0NDE2Mjg4MTkmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(110, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYxODQwOTU1OX41YVowMUZoNkdPTlhueTZuQ3A5ei94eVp-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NDgxNjU1MDNjMGNkNTA4NDg0ZWRiZjJkZjE5MWMzMTk2OWM2OGM4NTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T0RRd09UVTFPWDQxWVZvd01VWm9Oa2RQVGxodWVUWnVRM0E1ZWk5NGVWcC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxODQwOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE4NDA5LjU0MTQxMDk0OTkyMiZleHBpcmVfdGltZT0xNDQxNjI5MjA5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(111, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYxODYyNzMwOH42WDhpMVY2QlBmazcreUJBZUpFM1FWQVJ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZmMxYmIxN2ZmNTA1NGJjYzBhZTMwYjg3ZGUwN2JhZGZhYmYzZmEzZjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T0RZeU56TXdPSDQyV0RocE1WWTJRbEJtYXpjcmVVSkJaVXBGTTFGV1FWSi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxODYyNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE4NjI3LjI5MjA2MTg1MzEzMyZleHBpcmVfdGltZT0xNDQxNjI5NDI3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(112, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYxODc0NjE4N35hd1NRemJXcUt1WnJMRHJoakVnVngwK2h-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MDcyOWVkOTFjNGNmOTFhNmQ4YTU1MzZkN2U1MjU2NThhY2UyNmRkMTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T0RjME5qRTROMzVoZDFOUmVtSlhjVXQxV25KTVJISm9ha1ZuVm5nd0syaC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxODc0NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE4NzQ2LjE3MjAzMDA5ODQzMyZleHBpcmVfdGltZT0xNDQxNjI5NTQ2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(113, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTYxODc3ODQxNH5JTUNKZXdUUjdHSmJaMGNsdXhxWDVoNGF-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9YTUyYzdjYjRhYWFiZDI2MjkwNjA1N2FlYjRlNDM3Njg4ZjdiYjRmYzpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T0RjM09EUXhOSDVKVFVOS1pYZFVVamRIU21KYU1HTnNkWGh4V0RWb05HRi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxODc3OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE4Nzc4LjM5MTM2MDkzOTY5MyZleHBpcmVfdGltZT0xNDQxNjI5NTc4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(114, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTYxOTI0MTkxMH45cjR5aDhwbUZZdG03aWNrZ1htOE1UWG5-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MGRjMjk2ZjI2NjgxZmNhOTY1NzljOWQ3N2ExMTk0NTc3MzQyOTE2MTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T1RJME1Ua3hNSDQ1Y2pSNWFEaHdiVVpaZEcwM2FXTnJaMWh0T0UxVVdHNS1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxOTI0MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE5MjQxLjg5MTkyOTk0MTExMSZleHBpcmVfdGltZT0xNDQxNjMwMDQxJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(115, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYxOTQ3NTA5NX5GUEpCOWV4SnM1MnN2TjhlVHppakN5QjZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZGYxMmNmM2IzOWRhN2EzMjA5MjIzYmE0MGZlZTAwMjVlOTE3OTQyZjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T1RRM05UQTVOWDVHVUVwQ09XVjRTbk0xTW5OMlRqaGxWSHBwYWtONVFqWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxOTQ3NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE5NDc1LjA3ODQ5NDAwODYyJmV4cGlyZV90aW1lPTE0NDE2MzAyNzUmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(116, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTYxOTYwODI2Nn45eGQ2bzloWlVSN3JtTGFQanhsQlE0MkN-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NDZkNTcwN2EyYTIxZGFlOWNmZmE1NGQ2NDcwMmQ5MDdjNGM5Njg2ZTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T1RZd09ESTJObjQ1ZUdRMmJ6bG9XbFZTTjNKdFRHRlFhbmhzUWxFME1rTi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxOTYwOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE5NjA4LjI0MTI2NTEwNjQxOSZleHBpcmVfdGltZT0xNDQxNjMwNDA4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(117, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYxOTc2MDc4OX5ObDFYMFI4UmxnU0JQRGg3V2JSc1JlUWZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9Y2Y1ODc5MWNkZGY1NGFiNDVmMzhlOWRiNTdiNzJlYTQ3OTAxNzJhNzpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T1RjMk1EYzRPWDVPYkRGWU1GSTRVbXhuVTBKUVJHZzNWMkpTYzFKbFVXWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxOTc2MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE5NzYwLjc3MTUzMzU5NjE3NyZleHBpcmVfdGltZT0xNDQxNjMwNTYwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(118, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYxOTkyNTk0Mn5IZFZjbUEydHNveUVtaGlERXRuOVFnUDR-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NWViMDVkYjg4YTUyZmM0MTIxZDllNGZkMjU0OThhNmFhYjg2MDAzNjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl4T1RreU5UazBNbjVJWkZaamJVRXlkSE52ZVVWdGFHbEVSWFJ1T1ZGblVEUi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYxOTkyNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjE5OTI1LjkyMTY5NDA2MjQ4OCZleHBpcmVfdGltZT0xNDQxNjMwNzI1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(119, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYyMDAyMDAxM35oQlVhRzlSZ0JJK0hhRm5wS3dlYVdMT2t-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NWJjYzU4NTQxMDU4ZjZlNjVmMzJlNjViODE1N2YyOTZmYTA5ZDVhNjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURBeU1EQXhNMzVvUWxWaFJ6bFNaMEpKSzBoaFJtNXdTM2RsWVZkTVQydC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDAxOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwMDIwMTczOTU5NTc1MSZleHBpcmVfdGltZT0xNDQxNjMwODE5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(120, 1, 4, '1_MX40NTMwOTQyMn5-MTQ0MTYyMDE3MjU5N35Pc1kyRkFMWFpkbkZMYVNyN29Dcy82Yk9-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MzUwZmYwYjA2OTlmY2Q1ZGY1MjQ1ZTJiNTYzOTUyMGM3NjMwMzEzNDpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURFM01qVTVOMzVQYzFreVJrRk1XRnBrYmtaTVlWTnlOMjlEY3k4MllrOS1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDE3MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwMTcyLjU3MTUxOTc1NjU4MiZleHBpcmVfdGltZT0xNDQxNjMwOTcyJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(121, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTYyMDIxMjcwOX5jSW1mbXhXR3ZyWFhwZUFxaGRxOCtJN0Z-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZTY4MTkxNTA4NTljYjUyZjk1NzllYTQwMGNmNWRmYzMxOTMzMjQ1ZDpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURJeE1qY3dPWDVqU1cxbWJYaFhSM1p5V0Zod1pVRnhhR1J4T0N0Sk4wWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDIxMiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwMjEyLjY5NDI3NjczMTczJmV4cGlyZV90aW1lPTE0NDE2MzEwMTImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(122, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYyMDQwMjYxMX5NVzNUY0FjbUFCcXFzR3ltekR4c2IxWVd-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MDIyYzliOTk1MzQ3YTEzMjY5ZGFjMzk4MDIzZjU1MmQ5NmUyZjcwMjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURRd01qWXhNWDVOVnpOVVkwRmpiVUZDY1hGelIzbHRla1I0YzJJeFdWZC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDQwMiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwNDAyLjU5OTExNDk4OTEmZXhwaXJlX3RpbWU9MTQ0MTYzMTIwMiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(123, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYyMDUzODg4MX5HYXN1NG5tOXNTTkR1U0NDNUdUUzZQYWR-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MmY5NmM5Y2I5NjAxMWZlNWYwM2U1MzE5YTc4YWNkYzU1OWFkMGYzYzpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURVek9EZzRNWDVIWVhOMU5HNXRPWE5UVGtSMVUwTkROVWRVVXpaUVlXUi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDUzOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwNTM4Ljg2MTg4NDE4NTA4OCZleHBpcmVfdGltZT0xNDQxNjMxMzM4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(124, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTYyMDY0OTkwNX5rQmxCVzRKZVJTWDVTS1JoUmxzN1R3OEN-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NWVmMjNmNjdmZmM4Y2Q1ODVkZDhhYzhhNWNkMGQ2NjYwMzkxMGFhYzpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURZME9Ua3dOWDVyUW14Q1Z6UktaVkpUV0RWVFMxSm9VbXh6TjFSM09FTi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDY0OSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwNjQ5Ljg5Njc3ODk3MDUwJmV4cGlyZV90aW1lPTE0NDE2MzE0NDkmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(125, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTYyMDcwNzEyMX5KRW1mczBub1AwUmJ2cXNZd01WTlNCZ1p-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9YTFkNGUyZjZiZWM3NzBjMmJhNjE3YTI0ZjA2M2JmY2U2NmVlNTk5YzpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURjd056RXlNWDVLUlcxbWN6QnViMUF3VW1KMmNYTlpkMDFXVGxOQ1oxcC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDcwNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwNzA3LjEzNzE0MTgyNSZleHBpcmVfdGltZT0xNDQxNjMxNTA3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(126, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYyMDkwNDczNH41VTdtVlgvbFNGbHh3azVkV3dFRDhBUC9-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MjcxNGRmYTg1MThlYzdiYmEyNTI5M2YxMTIzMTdlZWJkMDBmZGIxNzpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TURrd05EY3pOSDQxVlRkdFZsZ3ZiRk5HYkhoM2F6VmtWM2RGUkRoQlVDOS1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMDkwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIwOTA0LjcxMjAyMzIxNzQyNCZleHBpcmVfdGltZT0xNDQxNjMxNzA0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(127, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYyMTExODcxMn42aGJlYTAvS0VQTGlXWmZVRHN6NHFYcFh-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9YWQ3ZWU2NDM1MGRkYjQ5M2MyMTZiOTgwYjdiOWI4NDBkNTc1OWZiYjpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TVRFeE9EY3hNbjQyYUdKbFlUQXZTMFZRVEdsWFdtWlZSSE42TkhGWWNGaC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMTExOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIxMTE4LjY5MTc5NzA0MjIyMyZleHBpcmVfdGltZT0xNDQxNjMxOTE4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(128, 4, 1, '2_MX40NTMwOTQyMn5-MTQ0MTYyMTE2MTQyMX4yditJSnBDSUw2a1RlNkFxVFU1YjhoRlZ-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MThkMDEzOWJhMzhlNzdlY2Q0NzI2NjlmYWUxYzRhMzUxMmNjZjc5NDpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TVRFMk1UUXlNWDR5ZGl0SlNuQkRTVXcyYTFSbE5rRnhWRlUxWWpob1JsWi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMTE2MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIxMTYxLjQxMDI3OTk3MDAxJmV4cGlyZV90aW1lPTE0NDE2MzE5NjEmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(129, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYyMTIxMTM5N35zN0NkVFltakpJVmFPb2R4K2JlMS9CZ01-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9NDMyYWJlMTMzZjkwZmQ0MTNjMjBlYjg1ODliZjAzNDQxYzM0NGE0ODpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TVRJeE1UTTVOMzV6TjBOa1ZGbHRha3BKVm1GUGIyUjRLMkpsTVM5Q1owMS1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMTIxMSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIxMjExLjM3NDUxOTE3OTU5JmV4cGlyZV90aW1lPTE0NDE2MzIwMTEmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(130, 4, 1, '1_MX40NTMwOTQyMn5-MTQ0MTYyMTU4NDI2N350MTc1LzQ1NUx2WTVCSGIrWm8xVy9nK0l-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MDIxYjRlZDM0Y2M0N2EwZGVjNWMyM2Y2OGNmZDllYzExOGFmMTk5NTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TVRVNE5ESTJOMzUwTVRjMUx6UTFOVXgyV1RWQ1NHSXJXbTh4Vnk5bkswbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMTU4NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIxNTg0LjI0MTg3NTI4NjYxOCZleHBpcmVfdGltZT0xNDQxNjMyMzg0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(131, 1, 4, '2_MX40NTMwOTQyMn5-MTQ0MTYyMTgwNjk0OH52MmV3RWtpS3lxMDcyK3NSRndNUnBDR3N-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9MmU0Zjc1ZDM5ZjMxOWZiYzk2YzVkMTczMWYxZTgwZjcxZjZkMTc1ZTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFl5TVRnd05qazBPSDUyTW1WM1JXdHBTM2x4TURjeUszTlNSbmROVW5CRFIzTi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTYyMTgwNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjIxODA2LjkyMTI5MDU4NTc0NCZleHBpcmVfdGltZT0xNDQxNjMyNjA2JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(132, 105, 5, '2_MX40NTMwOTQyMn5-MTQ0MTY1NDUxNDExOX55ZzRRS0JFNTY3UGIvLzluZE96ZTM0T1V-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9YWVmMDJkZmY4MTE0ZGI1N2IxMDIyM2M1OWU2Mzk0MGYxOWRjNzM0OTpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVFkxTkRVeE5ERXhPWDU1WnpSUlMwSkZOVFkzVUdJdkx6bHVaRTk2WlRNMFQxVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTY1NDUxNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxNjU0NTE0LjEyMDYwNTQxNDc2JmV4cGlyZV90aW1lPTE0NDE2NjUzMTQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(133, 5, 26, '2_MX40NTMwOTQyMn5-MTQ0MTgyMTk0NDkzMn5ZL0drK05UcXRxRjdVeGMzMDlGU0ZHVDV-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZWFjYmNmZTMyYzUzOWRhNGY2ZTAwMjBiMjIxMGNiMjgzNWY3NGZmZjpzZXNzaW9uX2lkPTJfTVg0ME5UTXdPVFF5TW41LU1UUTBNVGd5TVRrME5Ea3pNbjVaTDBkckswNVVjWFJ4UmpkVmVHTXpNRGxHVTBaSFZEVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTgyMTk0NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxODIxOTQ0LjkyODAxNDM5MDgxJmV4cGlyZV90aW1lPTE0NDE4MzI3NDQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(134, 105, 26, '1_MX40NTMwOTQyMn5-MTQ0MTk5NzM4MzM1NX5DdS9SVnE3RlNFdzhuVWZndEdjYXQweDV-UH4', 'T1==cGFydG5lcl9pZD00NTMwOTQyMiZzaWc9ZTgzMjdhYWRlZDEwNmVkNjE0N2U1N2IzNDM3OThhMDczMTdkNTEzNTpzZXNzaW9uX2lkPTFfTVg0ME5UTXdPVFF5TW41LU1UUTBNVGs1TnpNNE16TTFOWDVEZFM5U1ZuRTNSbE5GZHpodVZXWm5kRWRqWVhRd2VEVi1VSDQmY3JlYXRlX3RpbWU9MTQ0MTk5NzM4MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQxOTk3MzgzLjM2NDEyMDA4Njk3NDQyJmV4cGlyZV90aW1lPTE0NDIwMDgxODMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(135, 1, 4, '1_MX40NTM0MjYyMn5-MTQ0MjMwNDUxMzA5NX5acFM4dHZ2Sy8zSUp1MGpDK1c2eTJ0Tll-UH4', 'T1==cGFydG5lcl9pZD00NTM0MjYyMiZzaWc9NTU0OGY3NDkxM2U3OGVhODY3MWMzYTE1ZjAzNGRiMWIwMDk4NGRlMDpzZXNzaW9uX2lkPTFfTVg0ME5UTTBNall5TW41LU1UUTBNak13TkRVeE16QTVOWDVhY0ZNNGRIWjJTeTh6U1VwMU1HcERLMWMyZVRKMFRsbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MjMwNDUxMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQyMzA0NTEzLjA3NjI4MDg1MDY5NDgmZXhwaXJlX3RpbWU9MTQ0MjMxNTMxMyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(136, 78, 5, '1_MX40NTM0MjYyMn5-MTQ0MjM1NDIzODYzN34xZ3JVWExsaEpLdVpEVnlWVWJVaDBZamR-UH4', 'T1==cGFydG5lcl9pZD00NTM0MjYyMiZzaWc9ZjVjNDg3MTgxMGM4M2M2NDBmZjBkZGQyMzY3OGYwMmQ5NzNkZDM1ZDpzZXNzaW9uX2lkPTFfTVg0ME5UTTBNall5TW41LU1UUTBNak0xTkRJek9EWXpOMzR4WjNKVldFeHNhRXBMZFZwRVZubFdWV0pWYURCWmFtUi1VSDQmY3JlYXRlX3RpbWU9MTQ0MjM1NDIzOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQyMzU0MjM4LjY0MTc3OTAwMjQ5MTYmZXhwaXJlX3RpbWU9MTQ0MjM2NTAzOCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(137, 1, 5, '1_MX40NTM0MjYyMn5-MTQ0MjQ5MjE5MDI4Nn5ONkcwOVh1L3EwSzVYWWR2dmpvNHNNNkl-UH4', 'T1==cGFydG5lcl9pZD00NTM0MjYyMiZzaWc9YzZhMmViOTJlYTZiNzcwMzM1YjM4YTIwYzRkNTE5YjkyZDIzMjYyZTpzZXNzaW9uX2lkPTFfTVg0ME5UTTBNall5TW41LU1UUTBNalE1TWpFNU1ESTRObjVPTmtjd09WaDFMM0V3U3pWWVdXUjJkbXB2TkhOTk5rbC1VSDQmY3JlYXRlX3RpbWU9MTQ0MjQ5MjE5MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDQyNDkyMTkwLjI1NTE4MDU0OTU3NzAmZXhwaXJlX3RpbWU9MTQ0MjUwMjk5MCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(138, 8, 1, '1_MX40NTY0NDEzMn5-MTQ3MjIxOTAyOTQ4Nn5pSzNQdTJDeGM4Y1laeXFOOGxkTkZjb0J-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZTVmYWM5ZTJhY2FmYThjNGM0MTM2ZTU1YzFiMTExNjEzNWU4NWZhODpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNakl4T1RBeU9UUTRObjVwU3pOUWRUSkRlR000WTFsYWVYRk9PR3hrVGtaamIwSi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjIxODk3NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyMjE4OTc3LjMwNDIxNjgwODk3Mzk0JmV4cGlyZV90aW1lPTE0NzIyMjk3NzcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 0, 0, 0),
(139, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3MjU2NjQ1ODk0Mn5uMkFFeHNUMXdIamdUaXNQUGd0SXNUVXR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MjIyNDQwZGRmNjYzYTM3YzVjYTMxMjc4ODkxNDRhOWVmMzcxODRhMTpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNalUyTmpRMU9EazBNbjV1TWtGRmVITlVNWGRJYW1kVWFYTlFVR2QwU1hOVVZYUi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjU2NjQ1OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNTY2NDU4Ljk1MzkxNzc0NzM3MyZleHBpcmVfdGltZT0xNDcyNTc3MjU4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(140, 5, 26, '2_MX40NTY0NDEzMn5-MTQ3MjU2Njc1MzU4M35Pamk1UmtYQU1BMHBGZUtBRFhBVi9sL3Z-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZjJkMjQzNzlkNjIyMzc0YTQ2ZDIyMDQ4ZjA2MmJmYzhlNDgwNDY1YTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNalUyTmpjMU16VTRNMzVQYW1rMVVtdFlRVTFCTUhCR1pVdEJSRmhCVmk5c0wzWi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjU2Njc1MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNTY2NzUzLjU4NDYxOTg1ODc4NzE1JmV4cGlyZV90aW1lPTE0NzI1Nzc1NTMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(141, 26, 5, '2_MX40NTY0NDEzMn5-MTQ3MjU3NDE0ODUzN35pVkZDRGtJMTNodzd0enA5VHNOUlZ4aFp-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NTYzMWQ2NjJiMjI4NDA2ZTI4MTJiN2ZhYWNiN2I5ZDI1OTk5MjQ1MTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNalUzTkRFME9EVXpOMzVwVmtaRFJHdEpNVE5vZHpkMGVuQTVWSE5PVWxaNGFGcC1VSDQmY3JlYXRlX3RpbWU9MTQ3MjU3NDE0OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNTc0MTQ4LjU1MDYxMzE0NjA4NDgmZXhwaXJlX3RpbWU9MTQ3MjU4NDk0OCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(142, 5, 26, '2_MX40NTY0NDEzMn5-MTQ3MjU3NDIyMjMwN35xdWsweWp3dkVuU1h4dTM4Sy8xVk14Qjh-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MzgyMTAwZGJlZmEzNmE4MzNmYjA1YWMyNDA4YWViZDNhOTdmMjhhZDpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNalUzTkRJeU1qTXdOMzV4ZFdzd2VXcDNka1Z1VTFoNGRUTTRTeTh4VmsxNFFqaC1VSDQmY3JlYXRlX3RpbWU9MTQ3MjU3NDE2NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNTc0MTY3LjQ3MzMxMTMwMDQ0MzM3JmV4cGlyZV90aW1lPTE0NzI1ODQ5NjcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(143, 5, 26, '2_MX40NTY0NDEzMn5-MTQ3MjU3NDIyMDUwOH4rRXFSZVhJemFVQmV1TTBYdVpzMFNGSXl-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NTc1ZTJkZmY4YWQ0MTYyODUzOGZhOGUyZmQyZjA5YTEwZDE3ZDcyYjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNalUzTkRJeU1EVXdPSDRyUlhGU1pWaEplbUZWUW1WMVRUQllkVnB6TUZOR1NYbC1VSDQmY3JlYXRlX3RpbWU9MTQ3MjU3NDIyMCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNTc0MjIwLjUyNDUxNzc1NDg0ODE1JmV4cGlyZV90aW1lPTE0NzI1ODUwMjAmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(144, 8, 1, '1_MX40NTY0NDEzMn5-MTQ3MjYzNzc2NDk0OH56U3J5NURteE1KSVFJbFBvTEJMNmNVNXR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZWQyMmE0YTBkNmJlZjExMmE0ODZhY2VkNGY0OThkMGM5OWZjOTVjYjpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNall6TnpjMk5EazBPSDU2VTNKNU5VUnRlRTFLU1ZGSmJGQnZURUpNTm1OVk5YUi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjYzNzcwOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNjM3NzA5LjY0ODkzODY4MzU3MDQmZXhwaXJlX3RpbWU9MTQ3MjY0ODUwOSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 0, 0, 0),
(145, 26, 1, '2_MX40NTY0NDEzMn5-MTQ3MjYzNzkwMTQyMn5DWVRyek9WajlUMHdxM3hVRmhPZFQ4ckR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NmUzYjBlMDdlN2ZmYWJiMzU4ZTQ2YzZlNzQ0ZTNiNWZkNGRjNjZjZTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNall6Tnprd01UUXlNbjVEV1ZSeWVrOVdhamxVTUhkeE0zaFZSbWhQWkZRNGNrUi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjYzNzg0NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNjM3ODQ2LjEwMDYyOTcwODUzNTImZXhwaXJlX3RpbWU9MTQ3MjY0ODY0NiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(146, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3MjY1NzIzMjczOX5rb0FGSUVFRGx2MmtJMGVyUnBCM1JIQkV-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NmM5ZjI4OWQ4NzY1M2RmZjA5YmYxMTI4ZjU2ZTY1ZWMyMGRlNjI0ZjpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNalkxTnpJek1qY3pPWDVyYjBGR1NVVkZSR3gyTW10Sk1HVnlVbkJDTTFKSVFrVi1VSDQmY3JlYXRlX3RpbWU9MTQ3MjY1NzIzMiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDcyNjU3MjMyLjc1MjcxNzg0NDEyOTQxJmV4cGlyZV90aW1lPTE0NzI2NjgwMzImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(147, 86, 5, '1_MX40NTY0NDEzMn5-MTQ3MzM1MTM4MjE4Nn5kNzlaMGgxLzhwQmphZ3BTNURDd1FWYU5-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9Yzc1MWNhMGYzZmEwNWIzNWE4MzMxOTA1ODM0MzFjOTVlMWU0ZjQxNzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNek0xTVRNNE1qRTRObjVrTnpsYU1HZ3hMemh3UW1waFozQlROVVJEZDFGV1lVNS1VSDQmY3JlYXRlX3RpbWU9MTQ3MzM1MTM4MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDczMzUxMzgyLjIwODcxNzQwNjI0OTg0JmV4cGlyZV90aW1lPTE0NzMzNjIxODImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(148, 5, 86, '1_MX40NTY0NDEzMn5-MTQ3MzM1MTQxMjAzMX5LdWFBeGxaM3pmL0MxL3BaYXdHd2ZsdGF-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YmJmOGVjYjczZmZiZWY4OTU4Y2FlZTMyZWI1Y2U5ZTZhY2QxY2MyMDpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNNek0xTVRReE1qQXpNWDVMZFdGQmVHeGFNM3BtTDBNeEwzQmFZWGRIZDJac2RHRi1VSDQmY3JlYXRlX3RpbWU9MTQ3MzM1MTQxMiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDczMzUxNDEyLjAzMTQ0OTcxNTI1OTUmZXhwaXJlX3RpbWU9MTQ3MzM2MjIxMiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(149, 86, 5, '2_MX40NTY0NDEzMn5-MTQ3MzM1MTYyOTc0M35IM3ZDK1I2enovaVlURysrZWRCTGxwcGx-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZjRmMGYzNGIzNTM5Yjg4MGY5M2EzMzZjYzQ3NTU2ODdjM2RhYjc1ZDpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNek0xTVRZeU9UYzBNMzVJTTNaREsxSTJlbm92YVZsVVJ5c3JaV1JDVEd4d2NHeC1VSDQmY3JlYXRlX3RpbWU9MTQ3MzM1MTU4NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDczMzUxNTg3LjQ0Mzc2NzE4MTgwMiZleHBpcmVfdGltZT0xNDczMzYyMzg3JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(150, 5, 86, '2_MX40NTY0NDEzMn5-MTQ3MzM1MTgyOTAxMX5GMFhRaE5iUVpPWGxEK2NsUEZFNzAvckN-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NDhlNWMxMjI0ZDJkNjc5NzI4ODJkZDdlOTlkZDEzNWU1OTAyNDI5YjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNNek0xTVRneU9UQXhNWDVHTUZoUmFFNWlVVnBQV0d4RUsyTnNVRVpGTnpBdmNrTi1VSDQmY3JlYXRlX3RpbWU9MTQ3MzM1MTc4NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDczMzUxNzg2LjcxMjQxMDYyMTg5NjImZXhwaXJlX3RpbWU9MTQ3MzM2MjU4NiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(151, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxMTYyNDA5OH5rTUsxb2orKzNwQXpTS24rcmxKRUlZOUR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZTU4OTZlYTIzYTlmZDI0MzZlMjdjN2ExMDdkZGEyNDIzZmZiZmI5MDpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TVRZeU5EQTVPSDVyVFVzeGIyb3JLek53UVhwVFMyNHJjbXhLUlVsWk9VUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxMTYyNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDExNjI0LjEwMjI3MDA1MTEwNTMmZXhwaXJlX3RpbWU9MTQ3NDAyMjQyNCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(152, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzI5NzI3M35FcTdmdFNCMjBWMGxjakVBSkEzRDkxSDh-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9OGE3OTUxY2FjYTRhOTg4MjQ5ZmU1NmI3ZGUwMWEyMjQ5NjIzY2FiZDpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpJNU56STNNMzVGY1RkbWRGTkNNakJXTUd4amFrVkJTa0V6UkRreFNEaC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzI5NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3Mjk3LjI4NDUxNzgzMjYyOTA0JmV4cGlyZV90aW1lPTE0NzQwMjgwOTcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(153, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzMzNDAxN35tQ2NxN1J4L1JWY0RXQzlHU2wxRTJBNy9-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ODhlZTZkMGQyYmU0NDg2NmNlODA4ODY4MGE1YjY4MjQxMTZiYWZmNjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpNek5EQXhOMzV0UTJOeE4xSjRMMUpXWTBSWFF6bEhVMnd4UlRKQk55OS1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzMzNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3MzM0LjAwOTY5MTgzMDY1NDEmZXhwaXJlX3RpbWU9MTQ3NDAyODEzNCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(154, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzM1MjE3Mn51VU9CaUEzbXpKeCtNZENQU05tMnRRWVZ-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZDA2NzU4NDg0YjcyYjk2ZWViMWQwNTUzMWY4NjcxY2EzODAyMGJmODpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpNMU1qRTNNbjUxVlU5Q2FVRXpiWHBLZUN0TlpFTlFVMDV0TW5SUldWWi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzM1MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3MzUyLjE2MzgxOTc1MDk1MjEwJmV4cGlyZV90aW1lPTE0NzQwMjgxNTImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(155, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzM2NjQ2Mn4rUUtEYjhMSU1NWHVuT0pPZys3c0JFT2h-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MTA5YjA1ODJlZTgzMTkzODIyN2I4NDU1NGNlOTcxZmJmZTA3YmExZDpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpNMk5qUTJNbjRyVVV0RVlqaE1TVTFOV0hWdVQwcFBaeXMzYzBKRlQyaC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzM2NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3MzY2LjQ3MzQxMzIxOTAzNzc2JmV4cGlyZV90aW1lPTE0NzQwMjgxNjYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(156, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzM3Njc4Nn5TREh1SUVEY3pITk5OSmhoOExNeHIrT3N-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9OTE3MTNlN2U5NDFmNmQxODYxZjdmZGUzYmNkZmYxZjE3YzgzN2I4MTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpNM05qYzRObjVUUkVoMVNVVkVZM3BJVGs1T1NtaG9PRXhOZUhJclQzTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzM3NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3Mzc2Ljc5MzMxNzY2OTQyOTY4JmV4cGlyZV90aW1lPTE0NzQwMjgxNzYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(157, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzQwMTEyNX4vRGhCMkdBYzBjajZ5R3RmRE1ydzVmT2F-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZDA2NGU3YmZkYmQ0ZDY1ZTNjZDMxMzJiYjA1MmMwMjYyMzNhMWVlNzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpRd01URXlOWDR2UkdoQ01rZEJZekJqYWpaNVIzUm1SRTF5ZHpWbVQyRi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQwMSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDAxLjExNzIyODEyOTczODYmZXhwaXJlX3RpbWU9MTQ3NDAyODIwMSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(158, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzQxMTAzNX5NYkdib0E0anI4blplRVlvY1Y1U1NNMmJ-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MTg4M2MxMWM1MmFhMGNiZDNlMjNjMzc3ZjFjMWZmZDQ5N2I4YTM4ZjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpReE1UQXpOWDVOWWtkaWIwRTBhbkk0YmxwbFJWbHZZMVkxVTFOTk1tSi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQxMSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDExLjAyNzExMjQ1MDU4NjQ5JmV4cGlyZV90aW1lPTE0NzQwMjgyMTEmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(159, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzQyMzQ2MX55T25ubk1rODQyOEI0a0E1WVovNXdHdmR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YzIzODc4ODczMzgyMzRiZmNlZDMzMDA4NDNkOWVhNzcxZmFmMzBjMzpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpReU16UTJNWDU1VDI1dWJrMXJPRFF5T0VJMGEwRTFXVm92TlhkSGRtUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQyMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDIzLjQ1MjUxODU2MTgxMCZleHBpcmVfdGltZT0xNDc0MDI4MjIzJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(160, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxNzQzNTUyNX40dFB0bkl1RlNTVkMyOVc5VXdQV2loZTB-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MzZhMGY1ZmYxNTEzN2M2ZjE1NDk5NDI0NmVkZTc3ZmYzOTI0ZjYzYzpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpRek5UVXlOWDQwZEZCMGJrbDFSbE5UVmtNeU9WYzVWWGRRVjJsb1pUQi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQzNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDM1LjUzMjY1MTI0MTEzNCZleHBpcmVfdGltZT0xNDc0MDI4MjM1JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(161, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzQ0NzgzM35hNGFSMHlqMzJEMGJRMDdCL1drczdYUE5-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ODI0YzlkZjNhZjAwNDdkZjQzZDg3ZjcwYjM4NmNkMmVmNjE3YTgwNjpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpRME56Z3pNMzVoTkdGU01IbHFNekpFTUdKUk1EZENMMWRyY3pkWVVFNS1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQ0NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDQ3Ljg0MDE4NzQ4ODgyNTUmZXhwaXJlX3RpbWU9MTQ3NDAyODI0NyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(162, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzQ3MzAwM35nVTZGSnlWdVZsSnFWSHhYSmNDUG5JODV-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NTdhN2ZmMzZiMDUwNTNkODJlYjRiMzI5MjI2YmY1MGI4YjM2NWIyNTpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpRM016QXdNMzVuVlRaR1NubFdkVlpzU25GV1NIaFlTbU5EVUc1Sk9EVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzQ3MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NDcyLjk5NDkxNzY2NTg4NDU5JmV4cGlyZV90aW1lPTE0NzQwMjgyNzImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(163, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzUwMzM0N341K3FnbklZUnFTSGNQWDdIcFhTTEEvNU1-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MTU4YzU4NDUwZWNjZmJhZDFhY2FiZmY1ZTViZjVlZWNiYjUwYTdhNTpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpVd016TTBOMzQxSzNGbmJrbFpVbkZUU0dOUVdEZEljRmhUVEVFdk5VMS1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzUwMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NTAzLjMzOTYyOTM5NTgzMzkmZXhwaXJlX3RpbWU9MTQ3NDAyODMwMyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(164, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzU1MzI5M35DcTE3N1dza1o2VVcyWGdURFJzZUR5NHR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NGY4NTIwOGIxN2FhY2MzMGQ1YjM5NTk4MDZhMTBmMWQ1ZDUxMjVmMTpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpVMU16STVNMzVEY1RFM04xZHphMW8yVlZjeVdHZFVSRkp6WlVSNU5IUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzU1MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NTUzLjMwMDUxNDcxNjQ1Mzg1JmV4cGlyZV90aW1lPTE0NzQwMjgzNTMmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(165, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxNzU3MTgzMX5hRS9XMXVXbGo5VVFLT1ZlQzVKbExHbVR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MDE5NjZiZjg1MTNkNzg3NTExZTRmNDMxMDliMjM0MjRjMmQ0ZGYwMzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4TnpVM01UZ3pNWDVoUlM5WE1YVlhiR281VlZGTFQxWmxRelZLYkV4SGJWUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxNzU3MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE3NTcxLjgyMzU1MjI4NzU2ODcmZXhwaXJlX3RpbWU9MTQ3NDAyODM3MSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0);
INSERT INTO `dating_videosessionchat` (`id`, `to_id`, `from_id`, `sessionid`, `tok`, `is_session`, `quit`, `is_finished`) VALUES
(166, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAxODMwMDA5Mn5iUDdTdDBmUElCNzBtdXRQS1UyYTlDU0R-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YmVkMGY2MjQ3MDZlYTczNTY3Yzk1NDAwN2M1ODk0NWRjYjk1YzM5ODpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4T0RNd01EQTVNbjVpVURkVGREQm1VRWxDTnpCdGRYUlFTMVV5WVRsRFUwUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxODMwMCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE4MzAwLjA4NTIxMTQ1OTEyMzM2JmV4cGlyZV90aW1lPTE0NzQwMjkxMDAmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(167, 86, 26, '1_MX40NTY0NDEzMn5-MTQ3NDAxODcyODUzMn5EZzFBbDRrdnRoZjZGdGM4V05pbVladmJ-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ODg0ZGMyOWNmZDQ1NWJiM2EzZjBjZWU0YTA2OTUwNWM5MTc0YTc0MzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4T0RjeU9EVXpNbjVFWnpGQmJEUnJkblJvWmpaR2RHTTRWMDVwYlZsYWRtSi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxODcyOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE4NzI4LjUyNTQ2MjM4MTk4NCZleHBpcmVfdGltZT0xNDc0MDI5NTI4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(168, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAxODk3MzkwMH5IdkYzV0J2TndPblBYR2p3ZDAzbU1lWTZ-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9M2U3YzkzZWEzMWViYzA5MjUyOGYxNjhkZGM4OWY3ZDBkMjNmZGUwZjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4T0RrM016a3dNSDVJZGtZelYwSjJUbmRQYmxCWVIycDNaREF6YlUxbFdUWi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxODk3MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE4OTczLjkwOTg0MjYxODkxNjgmZXhwaXJlX3RpbWU9MTQ3NDAyOTc3MyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(169, 86, 26, '2_MX40NTY0NDEzMn5-MTQ3NDAxOTA1OTk5OX5WWXNBVHZ5b2h3SWZIUGlLYVRySFpHUWJ-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YjQwNGQ1MzU3ODE2NjVhMDU0Y2ZiYWQzOTFmMTJhNTMwYzYxNzc1ZTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF4T1RBMU9UazVPWDVXV1hOQlZIWjViMmgzU1daSVVHbExZVlJ5U0ZwSFVXSi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAxOTA1OSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDE5MDU5Ljk5MjMxMjc4MDc3ODM3JmV4cGlyZV90aW1lPTE0NzQwMjk4NTkmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(170, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAyNjY5OTI5N34yVVo5RWxCb2ZZMVRqM0ZlUkRKamNVS3V-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NWViNWYyMzc3NTFhZDkyZTQ5ODBmYWI1YTAyYTJhYWJjMDU4ZDI5YTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF5TmpZNU9USTVOMzR5VlZvNVJXeENiMlpaTVZScU0wWmxVa1JLYW1OVlMzVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAyNjY5OSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDI2Njk5LjI5MDQxODg0ODg1NTY0JmV4cGlyZV90aW1lPTE0NzQwMzc0OTkmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(171, 26, 86, '1_MX40NTY0NDEzMn5-MTQ3NDAyNzkzMzg3Nn40VEIraVdHWU1lOVNZK3gvK3F1Q0k1Wnh-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MTIwNWU1YTRmYmI2MGVlNGFjZDcxOTkxMGUwMzY0ZmZlYTJkMGQ1YzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREF5Tnprek16ZzNObjQwVkVJcmFWZEhXVTFsT1ZOWkszZ3ZLM0YxUTBrMVduaC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAyNzkzMyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDI3OTMzLjg2OTI3NDg5OTM0MzYmZXhwaXJlX3RpbWU9MTQ3NDAzODczMyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(172, 26, 86, '2_MX40NTY0NDEzMn5-MTQ3NDAyOTIxODUxNn5YU0JhK3hPRzViTmR5YmEzei9mZXFyQ2Z-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YTE3NmIzYjg2Y2VjMmRjZjM2NGNkZjNhMDY0M2M0MjBmMWZiOTM2MzpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREF5T1RJeE9EVXhObjVZVTBKaEszaFBSelZpVG1SNVltRXplaTltWlhGeVEyWi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDAyOTIxOCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDI5MjE4LjUyNTY4ODcyMDcxOTYmZXhwaXJlX3RpbWU9MTQ3NDA0MDAxOCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(173, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3NDA1MjA2NzA3NX5JeGE1aVgwK2d4bGl1MXBoeUtIY1RVNWR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9Y2JhODY1Mjc4ZGMzNzk2NjNjYmQ1MTc0M2Y1N2MzMzA2NTlhOWI1NDpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREExTWpBMk56QTNOWDVKZUdFMWFWZ3dLMmQ0YkdsMU1YQm9lVXRJWTFSVk5XUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDA1MjA2NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDUyMDY3LjEwNzM0OTc5MTU5MjImZXhwaXJlX3RpbWU9MTQ3NDA2Mjg2NyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(174, 26, 5, '2_MX40NTY0NDEzMn5-MTQ3NDA1MjA4MzY4OH5jUXNLaEErci90M29JMGRVQnlYaklpSjR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YmY4ODg0ZjMzZjNlZTU3NGNmNWEzYjg4ODFjODA2ZGI5YTY5ZTIxNzpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREExTWpBNE16WTRPSDVqVVhOTGFFRXJjaTkwTTI5Sk1HUlZRbmxZYWtscFNqUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDA1MjA4MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDUyMDgzLjY3NTUyNzUyNTE3OTEmZXhwaXJlX3RpbWU9MTQ3NDA2Mjg4MyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(175, 5, 26, '1_MX40NTY0NDEzMn5-MTQ3NDA1MjE5MzU4OH5MdGhlSGloOXNvOERwc0FNMHF0eGRFc2x-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MDMxMDRiZmMzNGUxZWJjZmQyNDVlOGFjOTE3NmY4ODFkZGFjZTQzYzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREExTWpFNU16VTRPSDVNZEdobFNHbG9PWE52T0VSd2MwRk5NSEYwZUdSRmMyeC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDA1MjE5MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MDUyMTkzLjYwNzY0OTgzMjk3NCZleHBpcmVfdGltZT0xNDc0MDYyOTkzJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(176, 5, 26, '2_MX40NTY0NDEzMn5-MTQ3NDI2MjQ5MjczOH5sM3pMbTJXeEVlWlFRSzJLSXErS0dZeDh-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZTc0ZTYwZDIzYzgzOGYxMzM3OTI2OWUwODc5ODI4ZmZhNTdlZThlOTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREkyTWpRNU1qY3pPSDVzTTNwTWJUSlhlRVZsV2xGUlN6SkxTWEVyUzBkWmVEaC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDI2MjQ5MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MjYyNDkyLjczMTE1MDg0OTc5MTQmZXhwaXJlX3RpbWU9MTQ3NDI3MzI5MiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(177, 26, 5, '2_MX40NTY0NDEzMn5-MTQ3NDI2MjgwNzQyOX5sNHowVkZxU1BweHRXN2lzYWNMTU9IQVl-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NDViNzRmZWI4ZWM4ODA1YjgxYzIzZjgzN2ZiZjk4NDg5MjRkNWJhYzpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREkyTWpnd056UXlPWDVzTkhvd1ZrWnhVMUJ3ZUhSWE4ybHpZV05NVFU5SVFWbC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDI2MjgwNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MjYyODA3LjQyMDMyMDIzODg4NTQzJmV4cGlyZV90aW1lPTE0NzQyNzM2MDcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(178, 5, 26, '1_MX40NTY0NDEzMn5-MTQ3NDI2Mjg1MTA1NX5CaC9FYkt1bk54MmxER2x2UnZTYzJjTFl-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ODdjNjZjNGExMmZiZTRhNzQxODE5MDM5YTExNjRkNDI0MGY0NjczYjpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNOREkyTWpnMU1UQTFOWDVDYUM5RllrdDFiazU0TW14RVIyeDJVblpUWXpKalRGbC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDI2Mjg1MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MjYyODUxLjA0NTgzOTAxMjU4MDUmZXhwaXJlX3RpbWU9MTQ3NDI3MzY1MSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(179, 5, 26, '2_MX40NTY0NDEzMn5-MTQ3NDI2Mjg3MTQwOX54OXdWNEg5YTQ3N2lQaFB5S1l3TFpaVnB-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YTY0MjJlMjllODIwYmM1OTBkZjlhNDRlNGNkMTUzNjZlOTkwNGViODpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNOREkyTWpnM01UUXdPWDU0T1hkV05FZzVZVFEzTjJsUWFGQjVTMWwzVEZwYVZuQi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDI2Mjg3MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0MjYyODcxLjM4NjE1OTU2NDUwOTgmZXhwaXJlX3RpbWU9MTQ3NDI3MzY3MSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(180, 5, 26, '1_MX40NTY0NDEzMn5-MTQ3NDM5NzUyNjI0NX5KQkF4Ni8rMUV4QTNPVG5ENXhRRzZGbGR-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YjZlYzc4MmFkNDZhNTc4NGQwZWVhNTA0M2RkNjY4YjdiMDczMTkwODpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNORE01TnpVeU5qSTBOWDVLUWtGNE5pOHJNVVY0UVROUFZHNUVOWGhSUnpaR2JHUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5NzUyNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk3NTI2LjI1NzQ3ODI5ODY2MTQmZXhwaXJlX3RpbWU9MTQ3NDQwODMyNiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(181, 26, 5, '2_MX40NTY0NDEzMn5-MTQ3NDM5NzU0MjI4NX5PaG12WXBmelV5cmh5aS9EZ3g5WnJ3L0p-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9YjNhMDBhMDZlMjRhYThiZDRiNmE4ZDAyZTNiM2MzYmM4YTA2OTY2YjpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNORE01TnpVME1qSTROWDVQYUcxMldYQm1lbFY1Y21oNWFTOUVaM2c1V25KM0wwcC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5NzU0MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk3NTQyLjI4MTgxOTgyNzUwOTA4JmV4cGlyZV90aW1lPTE0NzQ0MDgzNDImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(182, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3NDM5NzU0NzU0OH5Tc1ZFaW5wSDRDaUNUNElvTGhiRnpmUnN-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MDU1OWQ1MmU1NjI4OTdjZjU4MWM4OTNhYWRjNTkzM2RlMzU0NTQ4NzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNORE01TnpVME56VTBPSDVUYzFaRmFXNXdTRFJEYVVOVU5FbHZUR2hpUm5wbVVuTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5NzU0NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk3NTQ3LjU2MzcxNzQ2ODUyMzg5JmV4cGlyZV90aW1lPTE0NzQ0MDgzNDcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(183, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3NDM5NzU1NTI4NX5lcTZaMDgvak1lT051bVBrWU5DRkR0Ymx-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9M2YxMDU3ODZmZjRlNDBmY2U1YWNhYzJmOTFmMjY1NjI4N2IxYWUzODpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNORE01TnpVMU5USTROWDVsY1RaYU1EZ3ZhazFsVDA1MWJWQnJXVTVEUmtSMFlteC1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5NzU1NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk3NTU1LjI5NTQxNzE1NTcwMTA0JmV4cGlyZV90aW1lPTE0NzQ0MDgzNTUmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(184, 26, 5, '2_MX40NTY0NDEzMn5-MTQ3NDM5ODA5MTI0MH55eUxzL2NIM1Z3Q2tzdmJXaXBvREt1dEV-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9M2I2N2RiZGU3NjNkNmJiYzdjMjNmMGJiOTcwYzE2NTcwMzNiYTZkOTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNORE01T0RBNU1USTBNSDU1ZVV4ekwyTklNMVozUTJ0emRtSlhhWEJ2UkV0MWRFVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5ODA5MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk4MDkxLjI0ODE1OTA4NDQzODgmZXhwaXJlX3RpbWU9MTQ3NDQwODg5MSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(185, 5, 26, '1_MX40NTY0NDEzMn5-MTQ3NDM5ODEwNTUzMn5xMzArQ0dSbm96WXByUVNqTFd0SGtVNDF-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9ZTc3MDJmMWE5MWE2NjNiYWE4ZThlMTI0ZGZkOTEwM2U3MGM1MGQyMDpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNORE01T0RFd05UVXpNbjV4TXpBclEwZFNibTk2V1hCeVVWTnFURmQwU0d0Vk5ERi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5ODEwNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk4MTA1LjUzOTUxMTc0MzU0NjA1JmV4cGlyZV90aW1lPTE0NzQ0MDg5MDUmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(186, 26, 5, '1_MX40NTY0NDEzMn5-MTQ3NDM5ODYyMDg0M35vSVJlZHdLK2dhbDRHWWtNSXM1UHZTd01-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9MDJmY2JhNmEzOWQ1OTQ3Y2MzNTY0OTNiNzIwODgwY2NiNWI3NWE5NzpzZXNzaW9uX2lkPTFfTVg0ME5UWTBOREV6TW41LU1UUTNORE01T0RZeU1EZzBNMzV2U1ZKbFpIZExLMmRoYkRSSFdXdE5TWE0xVUhaVGQwMS1VSDQmY3JlYXRlX3RpbWU9MTQ3NDM5ODYyMCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Mzk4NjIwLjg1MTg1OTAwMTYyMSZleHBpcmVfdGltZT0xNDc0NDA5NDIwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(187, 3, 5, '2_MX40NTY0NDEzMn5-MTQ3NDY4OTY2MjgzN340aEg3TGlJcUxDQWxBQVlkQTRsSTh6dkN-UH4', 'T1==cGFydG5lcl9pZD00NTY0NDEzMiZzaWc9NzFiNzg0MDU4ZjRlYzIxYjQ4M2UzMDY4M2Q4MWMxMTA3MzEwOTBkNTpzZXNzaW9uX2lkPTJfTVg0ME5UWTBOREV6TW41LU1UUTNORFk0T1RZMk1qZ3pOMzQwYUVnM1RHbEpjVXhEUVd4QlFWbGtRVFJzU1RoNmRrTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NDY4OTY2MiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc0Njg5NjYyLjgzNTQxNTY1NzkzNzAyJmV4cGlyZV90aW1lPTE0NzQ3MDA0NjImY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 0, 0, 0),
(188, 26, 1, '2_MX40NTY5Nzg5Mn5-MTQ3NTc1OTMyNjUwN35GNkRoUVRPd092WGxEZDJCOVhoYjlaWEN-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9N2M0OGNmN2FhZjJjZjk1YjZmMzI1YzMxZTdjNmNlY2I3MmNhMTc1ODpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOVGMxT1RNeU5qVXdOMzVHTmtSb1VWUlBkMDkyV0d4RVpESkNPVmhvWWpsYVdFTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NTc1OTMyNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc1NzU5MzI2LjQ5MTMxNjg0NTEyMzc1JmV4cGlyZV90aW1lPTE0NzU3NzAxMjYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 0, 0),
(189, 1, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NjMwMjM4NTY5MH5RZlI3K1RKaHA0S2lkTXpKejc4MUxWd3N-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MjU0NjQ2M2VmZmRiY2Y4ZjVhZDEwOWJiZjNkZGNkYmM3M2U3MjhiOTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOak13TWpNNE5UWTVNSDVSWmxJM0sxUkthSEEwUzJsa1RYcEtlamM0TVV4V2QzTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NjMwMjM4NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2MzAyMzg1LjcwMTE4NzI0NjAzNzQmZXhwaXJlX3RpbWU9MTQ3NjMxMzE4NSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(190, 5, 1, '2_MX40NTY5Nzg5Mn5-MTQ3NjcwODY4ODU3Mn40NG5UOGFFVW56T3JoU2xVQWtLelFob1d-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OTQzZGRjMjVlNTUwZjUzYjRiZjIyM2MyNWU4Y2Q3ZGUxNzI3ZjZiMjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOamN3T0RZNE9EVTNNbjQwTkc1VU9HRkZWVzU2VDNKb1UyeFZRV3RMZWxGb2IxZC1VSDQmY3JlYXRlX3RpbWU9MTQ3NjcwODY4OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2NzA4Njg4LjIyMzI3MjUwMDE1NTMmZXhwaXJlX3RpbWU9MTQ3NjcxOTQ4OCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(191, 5, 1, '1_MX40NTY5Nzg5Mn5-MTQ3NjcwODg5NDgwM35mWFhhV1VNZ2dENUxrZ0QrR2RHa3VDTWt-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YjBhMzZjMmY1NzcwYmQ1YmM2N2FmMmRlOTc1ZTJmMTQwNGJmNTEzNzpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOamN3T0RnNU5EZ3dNMzVtV0ZoaFYxVk5aMmRFTlV4clowUXJSMlJIYTNWRFRXdC1VSDQmY3JlYXRlX3RpbWU9MTQ3NjcwODg5NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2NzA4ODk0LjQ1Mzk2NDU3Mjg4NSZleHBpcmVfdGltZT0xNDc2NzE5Njk0JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(192, 5, 1, '1_MX40NTY5Nzg5Mn5-MTQ3NjcwOTA5MDk2MX43Z0g2Qi9UVit3Z3AxWUlCSzFUaXQvbzd-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NDExZjg0MDM5ZmVjNmU4MjE0MGE5MDFlY2QyNDY4NWRiMzQ2NGMzMDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOamN3T1RBNU1EazJNWDQzWjBnMlFpOVVWaXQzWjNBeFdVbENTekZVYVhRdmJ6ZC1VSDQmY3JlYXRlX3RpbWU9MTQ3NjcwOTA5MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2NzA5MDkwLjYyNDg5NjM1MTM5ODcmZXhwaXJlX3RpbWU9MTQ3NjcxOTg5MCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(193, 5, 1, '2_MX40NTY5Nzg5Mn5-MTQ3NjcwOTEwOTYwM35wM2dld2tEV2xtVUZUdXgyRm5mcXBrUG9-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NDdmODRiNjE1OTA0ODE5ODJkNjFiY2VjY2VmYzBhNDQ4ZTdiMjBmODpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOamN3T1RFd09UWXdNMzV3TTJkbGQydEVWMnh0VlVaVWRYZ3lSbTVtY1hCclVHOS1VSDQmY3JlYXRlX3RpbWU9MTQ3NjcwOTEwOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2NzA5MTA5LjI2NjE1MTYzODA3ODEmZXhwaXJlX3RpbWU9MTQ3NjcxOTkwOSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(194, 5, 1, '2_MX40NTY5Nzg5Mn5-MTQ3Njc4NTcwNjUyOH5LUllWRXc0Z0FwZ05MQzZVeVdteXdVLzV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YWJkOGExMTFjY2JiNDY3ODMzNGZkMDA0ZGE4ZWFiZjkzN2NjM2Y3YTpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOamM0TlRjd05qVXlPSDVMVWxsV1JYYzBaMEZ3WjA1TVF6WlZlVmR0ZVhkVkx6Vi1VSDQmY3JlYXRlX3RpbWU9MTQ3Njc4NTcwNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2Nzg1NzA2LjE5NTExOTM3MzcyOTE3JmV4cGlyZV90aW1lPTE0NzY3OTY1MDYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(195, 5, 1, '1_MX40NTY5Nzg5Mn5-MTQ3Njc4NjY4ODI4NH5SWlYzY3B1WXRZbkRzRkd0RCtOc3VkL2R-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MjNmN2MxZDdmZDk4ZjJhMjAwZDQzMzEwNjkyNTMwNWJhODU2ZDRhYjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOamM0TmpZNE9ESTROSDVTV2xZelkzQjFXWFJaYmtSelJrZDBSQ3RPYzNWa0wyUi1VSDQmY3JlYXRlX3RpbWU9MTQ3Njc4NjY4NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2Nzg2Njg3LjkzMzY0NjgwOTE0NDEmZXhwaXJlX3RpbWU9MTQ3Njc5NzQ4NyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(196, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3Njk2MTg4MTAwMX5PQ2lSWEdwNnFQc29aelBSaEFjRUYyS0t-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MTg2NWVlNTU4MjQ2MTY0NTg5MmMwZWU2M2MyM2NhNjkwYTE2MGZmZjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOamsyTVRnNE1UQXdNWDVQUTJsU1dFZHdObkZRYzI5YWVsQlNhRUZqUlVZeVMwdC1VSDQmY3JlYXRlX3RpbWU9MTQ3Njk2MTg4MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc2OTYxODgwLjY3NDU0NTY5NjAwNzMmZXhwaXJlX3RpbWU9MTQ3Njk3MjY4MCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(197, 5, 1, '2_MX40NTY5Nzg5Mn5-MTQ3NzA2MjM1Mzg0M35sdzhOejVYTytwTHRUakQwakM5d0pvYVR-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZDgxOThkYTQzOWZiMzRlM2E3MGU2ODNiYzQzOGYzNzAxNDIxZDU2ODpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOekEyTWpNMU16ZzBNMzVzZHpoT2VqVllUeXR3VEhSVWFrUXdha001ZDBwdllWUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzA2MjM1MyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3MDYyMzUzLjQ5NDE4Mzk2ODM3NzEmZXhwaXJlX3RpbWU9MTQ3NzA3MzE1MyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(198, 1, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzA2MjcwNzk2MH5Ec0xSOFVDK01ocU5ZbFVMalgrUmQvL2V-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MDkzODIwMDA4MzAzOGNkMjkyM2EzMWUyOWJlM2FjYTAyMjY5ODBmNjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOekEyTWpjd056azJNSDVFYzB4U09GVkRLMDFvY1U1WmJGVk1hbGdyVW1RdkwyVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzA2MjcwNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3MDYyNzA3LjYzMjYxNzU3ODE3ODQ2JmV4cGlyZV90aW1lPTE0NzcwNzM1MDcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(199, 1, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzA2Mjc1NjMzOX5oWWdEUmhpRUZTZ2c5MXRtWjdHcWZyblN-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9Mjk5ODc4N2FjOWUzOWJmY2UzZTRmMDNkMGQ3YmI5MmExNGYyODA4MjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOekEyTWpjMU5qTXpPWDVvV1dkRVVtaHBSVVpUWjJjNU1YUnRXamRIY1daeWJsTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzA2Mjc1NSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3MDYyNzU1Ljk4NjUzOTMzNDk2NzMmZXhwaXJlX3RpbWU9MTQ3NzA3MzU1NSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(200, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3Mzk4Njk0M340c1dsUW5pQVFtZFROMkJyZU5kQTdqZjd-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OWZlNGE4ZjZmNDAyMTI3NDBjZDE5MjA3ZjFmYjA2MDUzNzk2OTYwNTpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTXprNE5qazBNMzQwYzFkc1VXNXBRVkZ0WkZST01rSnlaVTVrUVRkcVpqZC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3Mzk4NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTczOTg2LjYwNjQ1NTY0NDc5NDImZXhwaXJlX3RpbWU9MTQ3NzU4NDc4NiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(201, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDEwNTIyNX5WSloyanRFRmZ5S1Z2YVV4TE5iUlA0QlV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NTM0MTdhNWMyM2YxYmJlZmMyZTcyMTM0NzVmODJlNGIzZjc3OWRhMzpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRFd05USXlOWDVXU2xveWFuUkZSbVo1UzFaMllWVjRURTVpVWxBMFFsVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDEwNCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MTA0LjkxMzYxOTA2MTcxODk4JmV4cGlyZV90aW1lPTE0Nzc1ODQ5MDQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(202, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDEyODEyNn5CeHh0eThrcDVrTDAyaDdQK2k3c1RZWUx-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OWEwZWM1MGNhMWJiYjhiNmNmZDM1YzA2NGE4MDAxZDBkYzk0MWFlMjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRFeU9ERXlObjVDZUhoMGVUaHJjRFZyVERBeWFEZFFLMmszYzFSWldVeC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDEyNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MTI3LjgxNjMzOTIwOTI2OTUmZXhwaXJlX3RpbWU9MTQ3NzU4NDkyNyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(203, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDE1NzgyMX5vS2pHNEltMFFIWFRpbU1Zc3hINnBISXB-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OWYxNTU3YjczYWQ2NjhmNDczMWE3MjAzNTYxOTJkNWU0ZTEzMjI0NzpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRFMU56Z3lNWDV2UzJwSE5FbHRNRkZJV0ZScGJVMVpjM2hJTm5CSVNYQi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDE1NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MTU3LjUwNjkxMzA5NDk4MzY2JmV4cGlyZV90aW1lPTE0Nzc1ODQ5NTcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(204, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NDE3NTI1Mn5lU0VvNmFpaklScSt4V1RDNXhPanJxKzB-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NGVjOWE1ZDAyMTdkMzYxZWVjMGZlNDc0OTVjMzRjYTI4NTU1MDZlNjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRFM05USTFNbjVsVTBWdk5tRnBha2xTY1N0NFYxUkROWGhQYW5KeEt6Qi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDE3NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MTc0LjkwNTgxMjYwMjg0OTQ0JmV4cGlyZV90aW1lPTE0Nzc1ODQ5NzQmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(205, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NDE5MTU1MH5nNjA4SmlpNkVjL3daSjUyVVF4SGlyLzN-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MzliNjgyZjBlZTg2MDlhYjBmZjQ3M2ZhZTU1Zjc5NzI5NTUwNWI4NjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRFNU1UVTFNSDVuTmpBNFNtbHBOa1ZqTDNkYVNqVXlWVkY0U0dseUx6Ti1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDE5MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MTkxLjIwNzQyMDk5NDk2MTQ4JmV4cGlyZV90aW1lPTE0Nzc1ODQ5OTEmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(206, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NDIxMTc5Nn40czV4alBVbExIL1FwWUg5U1orR0REeVV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NWQwOWJmZDY3NWQ1MmVkNDFiMmE3YmE3MjUyMzYwZTYzMjMxMGEwODpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRJeE1UYzVObjQwY3pWNGFsQlZiRXhJTDFGd1dVZzVVMW9yUjBSRWVWVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDIxMSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MjExLjQ4MjUxOTEyOTcyMjAmZXhwaXJlX3RpbWU9MTQ3NzU4NTAxMSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(207, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDIzMDkyNH42VzB1TndVT3E4bWFtZGNDR2M5KzVYbkx-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NzU3ODRiMzkxNDMyMTExODEyMzVlYzA4MDcwOGExYjYyNDBkMTNmYTpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRJek1Ea3lOSDQyVnpCMVRuZFZUM0U0YldGdFpHTkRSMk01S3pWWWJreC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDIzMCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MjMwLjYwOTkxMjgxOTg0NDAwJmV4cGlyZV90aW1lPTE0Nzc1ODUwMzAmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(208, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDI1MTE2MH5JMEI0b2w1dm83OEtPMXpyeThuWnUrd3B-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ODZkNWJiMjMzZGE2OWNmZDQ1NjNiNzI5OTA3Mzc4NWZiYzY5NjgwNjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRJMU1URTJNSDVKTUVJMGIydzFkbTgzT0V0UE1YcHllVGh1V25VcmQzQi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDI1MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0MjUwLjgxMzUxOTQ5MjM1Nzk4JmV4cGlyZV90aW1lPTE0Nzc1ODUwNTAmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(209, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NDU0MTkwOX5XUzNVTzJ5OVE3WUYyUFhZdTNmanQvQXV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YTc4NTA0NzQyMmQzZGZiYjBiYjczYmQ3NzdhOTVkY2VlYWE4OTZkZjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRVME1Ua3dPWDVYVXpOVlR6SjVPVkUzV1VZeVVGaFpkVE5tYW5RdlFYVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDU0MSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0NTQxLjU5NzQ2NjIzNjIzMjAmZXhwaXJlX3RpbWU9MTQ3NzU4NTM0MSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(210, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDU2NzU5M35uM0ZvWEhLellpaU8vNkpLUmU1YkJxd3N-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZmU1NzJmMWNjNzc1NWNkZDUzNWE0ZmQxMDdiNWM4Y2MzY2E3MDEwNzpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRVMk56VTVNMzV1TTBadldFaExlbGxwYVU4dk5rcExVbVUxWWtKeGQzTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDU2NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0NTY3LjI3OTExMTA1NTg4MTczJmV4cGlyZV90aW1lPTE0Nzc1ODUzNjcmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(211, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NDYyNjY1Nn5PL1ZHbnh4QVYzM1ZkbWx3TnpKUWtLZmF-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZjVlMzEzODMzNzFkMmExMzQ1Y2NjMmFhNDNhZmE2OTc0ZWU3ZmRmMTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRZeU5qWTFObjVQTDFaSGJuaDRRVll6TTFaa2JXeDNUbnBLVVd0TFptRi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDYyNiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0NjI2LjMxMDMyNjQzMDY5MzMmZXhwaXJlX3RpbWU9MTQ3NzU4NTQyNiZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(212, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NDYzOTQwOX5GR01jTTVoMjlzTStTT05zZUxBcWx3c3h-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YzViNzk4MDljYjZiODkyMWZmMmVmYjA4ZTcxZmRiNzUwMDExZDI0MDpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTkRZek9UUXdPWDVHUjAxalRUVm9Namx6VFN0VFQwNXpaVXhCY1d4M2MzaC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NDYzOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc0NjM5LjA2NjEzOTM1NzE1NzgmZXhwaXJlX3RpbWU9MTQ3NzU4NTQzOSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(213, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3NzU3NTA2MDMzMH5wdVFOYUxnWHpjVjZUTGZKS0w0TGZPaS9-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YzRlN2ZiMjcyZjQyZjMzNDZiOWU1MTFiMzk4NTNlMjIxZGJkNDcyMzpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTlRBMk1ETXpNSDV3ZFZGT1lVeG5XSHBqVmpaVVRHWktTMHcwVEdaUGFTOS1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NTA2MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc1MDYwLjAxNjExOTMzMDU1MiZleHBpcmVfdGltZT0xNDc3NTg1ODYwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(214, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NTExNzc1OX5XcEx5YnFoQmtIbVFCRTRKdmM4V2FzNnF-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YWY5OWI4N2M4ZThmOWI5ZTQ3YTlkMjZhMDVhODcxY2IyYTZjMzA0NTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTlRFeE56YzFPWDVYY0V4NVluRm9RbXRJYlZGQ1JUUktkbU00VjJGek5uRi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NTExNyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc1MTE3LjQxMjY1MDQ3NTA2MjcmZXhwaXJlX3RpbWU9MTQ3NzU4NTkxNyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(215, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NTE0MTA2Mn41NmlKaTJ5K2Zqc0tWRkFZUW53R1BTNEp-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NDA5ZDMxMDE5NWNhOTlkMzQ2ZDNiMTY1MGVmMzY1NjM0NWU4YTUwYjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTlRFME1UQTJNbjQxTm1sS2FUSjVLMlpxYzB0V1JrRlpVVzUzUjFCVE5FcC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NTE0MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc1MTQwLjc0OTcxMzU5NDcyNzY3JmV4cGlyZV90aW1lPTE0Nzc1ODU5NDAmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(216, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3NzU3NTE1OTUzN35nT0IzMml6ZlJXRm1QNHVmUFZCcCtlY3N-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZDczOGVjMWUwYmEyN2I0MmY3OTUzYmFmYjJiODVhMWNjMWQ5NzlhODpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelUzTlRFMU9UVXpOMzVuVDBJek1tbDZabEpYUm0xUU5IVm1VRlpDY0N0bFkzTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzU3NTE1OSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NTc1MTU5LjIyMjcyNDYwNjg1OTQmZXhwaXJlX3RpbWU9MTQ3NzU4NTk1OSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(217, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzY0NTc1NzM2NH5ydVVxNk1RK3RPVXRkenptSjBMVEJnVkd-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OTQzNTNkYTY0OTk3ZTA5N2U3OGNiYjRkYjA4Yjk4ZWVkNWQzZDRkNzpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelkwTlRjMU56TTJOSDV5ZFZWeE5rMVJLM1JQVlhSa2VucHRTakJNVkVKblZrZC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY0NTc1NyZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjQ1NzU3LjAxODI0OTY2NDcwMzEmZXhwaXJlX3RpbWU9MTQ3NzY1NjU1NyZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(218, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzY1NzQ5MDgxNn5YRDRFZUpNT215VVNvREs3dDg4YnZtWlV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9M2I3MzUzYmYxNjcyMjFkMjliMTI2MGIxYzM0NGE0OTIyZWEzNzg2ZjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelkxTnpRNU1EZ3hObjVZUkRSRlpVcE5UMjE1VlZOdlJFczNkRGc0WW5adFdsVi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY1NzQ5MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjU3NDkwLjQ2OTE1NjY1ODA4MTYmZXhwaXJlX3RpbWU9MTQ3NzY2ODI5MCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(219, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzY1NzU3OTIxOX5VcjU5MzJYeVRyeEZ4aWRaNWJ3MjdJcmJ-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OTQ4MWNlZDE2MGVmYjY0OGE4ODAxYjUwMzdlY2FjNWU2NDBkNWNkMDpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelkxTnpVM09USXhPWDVWY2pVNU16SlllVlJ5ZUVaNGFXUmFOV0ozTWpkSmNtSi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY1NzU3OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjU3NTc4LjkwNjExMTA2OTI1NiZleHBpcmVfdGltZT0xNDc3NjY4Mzc4JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(220, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzY1NzY4NTA2Nn5jc1FkWStNSUl0UW9SQkNScTlRc01FZ2R-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZGM5MGFlZWJjMjc4M2NmMDY0OTU4YjA1YzYxNWJlYzJkNmFkMmQ3ZDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelkxTnpZNE5UQTJObjVqYzFGa1dTdE5TVWwwVVc5U1FrTlNjVGxSYzAxRloyUi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY1NzY4NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjU3Njg0LjcxODczNDExMzM0NzMmZXhwaXJlX3RpbWU9MTQ3NzY2ODQ4NCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(221, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3NzY1NzgxNTQ1NX51REorUFlwS0Y1WHdoL1N3bGhEaHY1T1N-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9M2E3OTBkZDAzYzI1NjkwN2EzYzdiNzgyZTNjMWY5Y2UxZmQzMjAwNDpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNOelkxTnpneE5UUTFOWDUxUkVvclVGbHdTMFkxV0hkb0wxTjNiR2hFYUhZMVQxTi1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY1NzgxNSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjU3ODE1LjE0NDcyNzYwOTYzNjAmZXhwaXJlX3RpbWU9MTQ3NzY2ODYxNSZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 0, 0),
(222, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3NzY2OTE2ODQyN35FRGlHSnhrVDZXM2RFa240UW1xQjJlTUt-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MWQxYWU4MGY3MDMwODM3YmRjMjYzNGE1M2U3MTM1NmUyYzlhOThhYjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNOelkyT1RFMk9EUXlOMzVGUkdsSFNuaHJWRFpYTTJSRmEyNDBVVzF4UWpKbFRVdC1VSDQmY3JlYXRlX3RpbWU9MTQ3NzY2OTE2OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc3NjY5MTY4LjExMTkxMTU2OTQ5MjUyJmV4cGlyZV90aW1lPTE0Nzc2Nzk5NjgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(223, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODAzMDg3MDQ0NX44RWpPZW5wTERRZ0R5Y2FnRzdnN24rend-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9M2VhMTJkMDM5ZTQ5Zjc4ODM3ZjEyMWNkNTA2M2RhMmMzZjA0OTE4MTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREF6TURnM01EUTBOWDQ0UldwUFpXNXdURVJSWjBSNVkyRm5SemRuTjI0cmVuZC1VSDQmY3JlYXRlX3RpbWU9MTQ3ODAzMDg3MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDMwODcwLjEwNzk4OTk5OTUzNiZleHBpcmVfdGltZT0xNDc4MDQxNjcwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(224, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODAzMTEwOTU1OH4zTHluUW9wdU1hY2FSb0N6U0tZV2tmU25-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9OWNkNDE4OGViODEwNDcyY2UyODJlOGU4NzFkMzQwZTM3NDgxMTI4YTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREF6TVRFd09UVTFPSDR6VEhsdVVXOXdkVTFoWTJGU2IwTjZVMHRaVjJ0bVUyNS1VSDQmY3JlYXRlX3RpbWU9MTQ3ODAzMTEwOSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDMxMTA5LjIxMDYxOTc4MzM1OTU5JmV4cGlyZV90aW1lPTE0NzgwNDE5MDkmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(225, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3ODAzMTE0OTIyM35pcVMybjVoNGNUbmlkQzd1T0lKSkdZV2p-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9Y2Q4MGYwNDA4ZmY5YTA2ZDRlNTAyOWM4MzIzN2Y0ZWM1NzdlOWM0YjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNPREF6TVRFME9USXlNMzVwY1ZNeWJqVm9OR05VYm1sa1F6ZDFUMGxLU2tkWlYycC1VSDQmY3JlYXRlX3RpbWU9MTQ3ODAzMTE0OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDMxMTQ4Ljg3NDgxMzI4MDkyNzI2JmV4cGlyZV90aW1lPTE0NzgwNDE5NDgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(226, 26, 5, '1_MX40NTY5Nzg5Mn5-MTQ3ODA5MjE2MTMzMn5RdjJsMitMQ1F1ZTErZ2I3S29JSlhKMzF-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ODQxZGJlNzNlMjU3YTM5MTY2Yjk3Y2ZkN2Y1ZmQxY2E0OTI4MzViMjpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TWpFMk1UTXpNbjVSZGpKc01pdE1RMUYxWlRFcloySTNTMjlKU2xoS016Ri1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5MjE2MCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDkyMTYwLjk4NTU5OTg1MTQ2NDUmZXhwaXJlX3RpbWU9MTQ3ODEwMjk2MCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(227, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODA5MjIyMTMzMn5pTnB0ZlNnTjVUbmZJMHY2TDk4dE01UEV-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9MDA1NDI1MzZjNmE2YjEwYmMxNTA4NDExNTcwOTcyYzBiZDdjMWEyNTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TWpJeU1UTXpNbjVwVG5CMFpsTm5UalZVYm1aSk1IWTJURGs0ZEUwMVVFVi1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5MjIyMCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDkyMjIwLjk4NjU3MjI1MzMzOCZleHBpcmVfdGltZT0xNDc4MTAzMDIwJmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 0, 0),
(228, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODA5MjU2MDA4M35YQVh6M21iSUZKOGttOEx3YmRnSWV4ZVR-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9NDI2N2U0YmU1ZmJhZTc4ODJiYTM4NmM0Y2QxYTBhMmYyMjE0OTIzNDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TWpVMk1EQTRNMzVZUVZoNk0yMWlTVVpLT0d0dE9FeDNZbVJuU1dWNFpWUi1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5MjU1OSZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDkyNTU5Ljc0MTYwNDc2NTM5MiZleHBpcmVfdGltZT0xNDc4MTAzMzU5JmNvbm5lY3Rpb25fZGF0YT11c2VyTmFtZSUzREJvYg==', 1, 1, 0),
(229, 5, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODA5MjY2ODU4MX41RXpxMHovZnZSV3pOWUlwQnFwbGxVM0Z-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9YTE5ZTkwY2NhZGU1NGVmYWIxMTA3NzNlYjMzYWQ2NTkxZjQ2ZTRkMDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TWpZMk9EVTRNWDQxUlhweE1Ib3ZablpTVjNwT1dVbHdRbkZ3Ykd4Vk0wWi1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5MjY2OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDkyNjY4LjIzNDkxODc4NzIwNTM5JmV4cGlyZV90aW1lPTE0NzgxMDM0NjgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(230, 26, 5, '2_MX40NTY5Nzg5Mn5-MTQ3ODA5NTQ4NDkxM343bEpLS2tZUk1qSGRSbGc1MlBLOFc5Ukh-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZmI5OWIwNGFiZGY2OTM3NmMxZDY2MGMwZWNlMzBmYWM0NDdkNDIyMjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TlRRNE5Ea3hNMzQzYkVwTFMydFpVazFxU0dSU2JHYzFNbEJMT0ZjNVVraC1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5NTQ4NCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDk1NDg0LjU2NTcxNzk2MzE2ODYmZXhwaXJlX3RpbWU9MTQ3ODEwNjI4NCZjb25uZWN0aW9uX2RhdGE9dXNlck5hbWUlM0RCb2I=', 1, 1, 0),
(231, 5, 26, '2_MX40NTY5Nzg5Mn5-MTQ3ODA5NTQ5ODk2NH5WTFIxbFFWNHU5eHpGODZxaUNBTmtRa3p-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZWY4MWFlNDI1YjkyMGZhYWU2M2VjNDAwMGFjNjFkOGU0NjAxNDllYjpzZXNzaW9uX2lkPTJfTVg0ME5UWTVOemc1TW41LU1UUTNPREE1TlRRNU9EazJOSDVXVEZJeGJGRldOSFU1ZUhwR09EWnhhVU5CVG10UmEzcC1VSDQmY3JlYXRlX3RpbWU9MTQ3ODA5NTQ5OCZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MDk1NDk4LjYxODMxNjM3MjM2Mzk2JmV4cGlyZV90aW1lPTE0NzgxMDYyOTgmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0),
(232, 1, 26, '1_MX40NTY5Nzg5Mn5-MTQ3ODEwMTE3NjUxMX5SeGNNa3lIRThGM2ZDbzNSb1pXckJPdDd-UH4', 'T1==cGFydG5lcl9pZD00NTY5Nzg5MiZzaWc9ZTJhYzQ3OTQ1YjU0NjRkNWQ4ZWZjZjM3OWZmZjY4Nzc1MDFhNGY1MTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOemc1TW41LU1UUTNPREV3TVRFM05qVXhNWDVTZUdOTmEzbElSVGhHTTJaRGJ6TlNiMXBYY2tKUGREZC1VSDQmY3JlYXRlX3RpbWU9MTQ3ODEwMTE3NiZyb2xlPXB1Ymxpc2hlciZub25jZT0xNDc4MTAxMTc2LjE3MDExMDUwMTkwODg1JmV4cGlyZV90aW1lPTE0NzgxMTE5NzYmY29ubmVjdGlvbl9kYXRhPXVzZXJOYW1lJTNEQm9i', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dating_wink`
--

CREATE TABLE IF NOT EXISTS `dating_wink` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `form_status` tinyint(1) NOT NULL DEFAULT '1',
  `to_status` tinyint(1) NOT NULL DEFAULT '1',
  `from_view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''1'' - Viewed, ''0'' - Not Viewed',
  `to_view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''1'' - Viewed, ''0'' - Not Viewed'
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dating_wink`
--

INSERT INTO `dating_wink` (`id`, `from_id`, `to_id`, `message`, `date`, `form_status`, `to_status`, `from_view`, `to_view`) VALUES
(1, 1, 4, '', '2016-10-05 12:15:43', 1, 0, 0, 0),
(2, 4, 1, '', '2016-10-06 05:39:12', 1, 0, 0, 1),
(3, 1, 4, '', '2016-10-06 05:39:39', 1, 1, 0, 1),
(4, 4, 1, '', '2016-10-06 10:28:10', 1, 1, 0, 1),
(5, 1, 78, '', '2016-10-06 10:30:41', 1, 1, 0, 0),
(6, 26, 5, '', '2016-10-07 17:11:52', 1, 1, 0, 1),
(7, 4, 1, '', '1969-12-31 18:30:00', 1, 1, 0, 1),
(8, 5, 1, '', '1970-01-01 05:00:00', 1, 1, 0, 1),
(9, 5, 78, '', '1970-01-01 05:00:00', 1, 0, 0, 0),
(10, 5, 3, '', '1970-01-01 05:00:00', 1, 1, 0, 0),
(11, 5, 118, '', '1970-01-01 05:00:00', 1, 1, 0, 0),
(12, 5, 4, '', '1970-01-01 05:00:00', 1, 1, 0, 0),
(13, 1, 86, '', '1970-01-01 05:00:00', 1, 1, 0, 0),
(14, 5, 26, '', '1970-01-01 05:00:00', 1, 1, 0, 1),
(15, 26, 1, '', '1970-01-01 05:00:00', 1, 1, 0, 1),
(16, 26, 5, '', '1969-12-31 18:30:00', 1, 1, 0, 1),
(17, 5, 26, '', '1969-12-31 18:30:00', 1, 1, 0, 1),
(18, 5, 26, '', '1970-01-01 05:00:00', 1, 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dateing_add`
--
ALTER TABLE `dateing_add`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_add_pages`
--
ALTER TABLE `dateing_add_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_advertise`
--
ALTER TABLE `dateing_advertise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_assign_ads`
--
ALTER TABLE `dateing_assign_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_bank`
--
ALTER TABLE `dateing_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_banner`
--
ALTER TABLE `dateing_banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `status_2` (`status`),
  ADD KEY `status_3` (`status`);

--
-- Indexes for table `dateing_billing`
--
ALTER TABLE `dateing_billing`
  ADD PRIMARY KEY (`billing_id`);

--
-- Indexes for table `dateing_blogs`
--
ALTER TABLE `dateing_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_bodytype`
--
ALTER TABLE `dateing_bodytype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_booking`
--
ALTER TABLE `dateing_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_carmakers`
--
ALTER TABLE `dateing_carmakers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_carmodels`
--
ALTER TABLE `dateing_carmodels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_car_moreimage`
--
ALTER TABLE `dateing_car_moreimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_category`
--
ALTER TABLE `dateing_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_cms`
--
ALTER TABLE `dateing_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_countries`
--
ALTER TABLE `dateing_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_ethnicity`
--
ALTER TABLE `dateing_ethnicity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_favourite`
--
ALTER TABLE `dateing_favourite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_homecms`
--
ALTER TABLE `dateing_homecms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_homeimages`
--
ALTER TABLE `dateing_homeimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_individual_adds`
--
ALTER TABLE `dateing_individual_adds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_invoice`
--
ALTER TABLE `dateing_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_message`
--
ALTER TABLE `dateing_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_order`
--
ALTER TABLE `dateing_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_package`
--
ALTER TABLE `dateing_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_package_women`
--
ALTER TABLE `dateing_package_women`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_payment`
--
ALTER TABLE `dateing_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_port`
--
ALTER TABLE `dateing_port`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_quickpics`
--
ALTER TABLE `dateing_quickpics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_religion`
--
ALTER TABLE `dateing_religion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_sentnewsletter`
--
ALTER TABLE `dateing_sentnewsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_settings`
--
ALTER TABLE `dateing_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_shipping`
--
ALTER TABLE `dateing_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_social`
--
ALTER TABLE `dateing_social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_story`
--
ALTER TABLE `dateing_story`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_tbladmin`
--
ALTER TABLE `dateing_tbladmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_tblorderdetails`
--
ALTER TABLE `dateing_tblorderdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_tblorders`
--
ALTER TABLE `dateing_tblorders`
  ADD PRIMARY KEY (`orderid`);

--
-- Indexes for table `dateing_transactions`
--
ALTER TABLE `dateing_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_user`
--
ALTER TABLE `dateing_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_userquery`
--
ALTER TABLE `dateing_userquery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_user_1`
--
ALTER TABLE `dateing_user_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_user_billing`
--
ALTER TABLE `dateing_user_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_viewedme`
--
ALTER TABLE `dateing_viewedme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dateing_wishlist`
--
ALTER TABLE `dateing_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_block_reason`
--
ALTER TABLE `dating_block_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_block_user`
--
ALTER TABLE `dating_block_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_chat`
--
ALTER TABLE `dating_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_compatibility_setting`
--
ALTER TABLE `dating_compatibility_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_draft`
--
ALTER TABLE `dating_draft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_friend_requests`
--
ALTER TABLE `dating_friend_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_interest`
--
ALTER TABLE `dating_interest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_kiss`
--
ALTER TABLE `dating_kiss`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_languages`
--
ALTER TABLE `dating_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_moreimages`
--
ALTER TABLE `dating_moreimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_newsletter`
--
ALTER TABLE `dating_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_reference`
--
ALTER TABLE `dating_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_report`
--
ALTER TABLE `dating_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_sendnewsletter`
--
ALTER TABLE `dating_sendnewsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_sharing`
--
ALTER TABLE `dating_sharing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_sms`
--
ALTER TABLE `dating_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_template`
--
ALTER TABLE `dating_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_translation_package`
--
ALTER TABLE `dating_translation_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_userimages`
--
ALTER TABLE `dating_userimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_user_video`
--
ALTER TABLE `dating_user_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_videosessionchat`
--
ALTER TABLE `dating_videosessionchat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dating_wink`
--
ALTER TABLE `dating_wink`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dateing_add`
--
ALTER TABLE `dateing_add`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `dateing_add_pages`
--
ALTER TABLE `dateing_add_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dateing_advertise`
--
ALTER TABLE `dateing_advertise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `dateing_assign_ads`
--
ALTER TABLE `dateing_assign_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `dateing_bank`
--
ALTER TABLE `dateing_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dateing_banner`
--
ALTER TABLE `dateing_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dateing_billing`
--
ALTER TABLE `dateing_billing`
  MODIFY `billing_id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `dateing_blogs`
--
ALTER TABLE `dateing_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dateing_bodytype`
--
ALTER TABLE `dateing_bodytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_booking`
--
ALTER TABLE `dateing_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dateing_carmakers`
--
ALTER TABLE `dateing_carmakers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dateing_carmodels`
--
ALTER TABLE `dateing_carmodels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dateing_car_moreimage`
--
ALTER TABLE `dateing_car_moreimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `dateing_category`
--
ALTER TABLE `dateing_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `dateing_cms`
--
ALTER TABLE `dateing_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dateing_countries`
--
ALTER TABLE `dateing_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dateing_ethnicity`
--
ALTER TABLE `dateing_ethnicity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `dateing_favourite`
--
ALTER TABLE `dateing_favourite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `dateing_homecms`
--
ALTER TABLE `dateing_homecms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dateing_homeimages`
--
ALTER TABLE `dateing_homeimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dateing_individual_adds`
--
ALTER TABLE `dateing_individual_adds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `dateing_invoice`
--
ALTER TABLE `dateing_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dateing_message`
--
ALTER TABLE `dateing_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `dateing_order`
--
ALTER TABLE `dateing_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dateing_package`
--
ALTER TABLE `dateing_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dateing_package_women`
--
ALTER TABLE `dateing_package_women`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dateing_payment`
--
ALTER TABLE `dateing_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dateing_port`
--
ALTER TABLE `dateing_port`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_quickpics`
--
ALTER TABLE `dateing_quickpics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dateing_religion`
--
ALTER TABLE `dateing_religion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_sentnewsletter`
--
ALTER TABLE `dateing_sentnewsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `dateing_settings`
--
ALTER TABLE `dateing_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dateing_shipping`
--
ALTER TABLE `dateing_shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_social`
--
ALTER TABLE `dateing_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `dateing_story`
--
ALTER TABLE `dateing_story`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dateing_tbladmin`
--
ALTER TABLE `dateing_tbladmin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dateing_tblorderdetails`
--
ALTER TABLE `dateing_tblorderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `dateing_tblorders`
--
ALTER TABLE `dateing_tblorders`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_transactions`
--
ALTER TABLE `dateing_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `dateing_user`
--
ALTER TABLE `dateing_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `dateing_userquery`
--
ALTER TABLE `dateing_userquery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dateing_user_1`
--
ALTER TABLE `dateing_user_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dateing_user_billing`
--
ALTER TABLE `dateing_user_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dateing_viewedme`
--
ALTER TABLE `dateing_viewedme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1201;
--
-- AUTO_INCREMENT for table `dateing_wishlist`
--
ALTER TABLE `dateing_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dating_block_reason`
--
ALTER TABLE `dating_block_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `dating_block_user`
--
ALTER TABLE `dating_block_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dating_chat`
--
ALTER TABLE `dating_chat`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=667;
--
-- AUTO_INCREMENT for table `dating_compatibility_setting`
--
ALTER TABLE `dating_compatibility_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `dating_draft`
--
ALTER TABLE `dating_draft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `dating_friend_requests`
--
ALTER TABLE `dating_friend_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `dating_interest`
--
ALTER TABLE `dating_interest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `dating_kiss`
--
ALTER TABLE `dating_kiss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `dating_languages`
--
ALTER TABLE `dating_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `dating_moreimages`
--
ALTER TABLE `dating_moreimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `dating_newsletter`
--
ALTER TABLE `dating_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dating_reference`
--
ALTER TABLE `dating_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dating_report`
--
ALTER TABLE `dating_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dating_sendnewsletter`
--
ALTER TABLE `dating_sendnewsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `dating_sharing`
--
ALTER TABLE `dating_sharing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dating_sms`
--
ALTER TABLE `dating_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dating_template`
--
ALTER TABLE `dating_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `dating_translation_package`
--
ALTER TABLE `dating_translation_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dating_userimages`
--
ALTER TABLE `dating_userimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `dating_user_video`
--
ALTER TABLE `dating_user_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `dating_videosessionchat`
--
ALTER TABLE `dating_videosessionchat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT for table `dating_wink`
--
ALTER TABLE `dating_wink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
