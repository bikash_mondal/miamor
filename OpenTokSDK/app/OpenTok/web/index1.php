<?php
session_start();
$_SESSION['PREV_CHAT_PAGE'] = $_SERVER['HTTP_REFERER'];

ob_start();
ini_set("display_errors",'1');
error_reporting(E_ALL);

         $autoloader = __DIR__.'/../vendor/autoload.php';

	if(!file_exists($autoloader)){
		die('You must run `composer install` in the sample app directory');
	}
        
	require($autoloader);

	use OpenTok\OpenTok;
	use OpenTok\Role;
	
	include('../../../../administrator/includes/config.php');

	$opentok = new OpenTok(API_KEY,API_SECRET);
	$session = $opentok->createSession();
	$sessionId = $session->getSessionId();
	
	$metadata = "userName=Bob";
	$role = Role::PUBLISHER;

	$properties = array(
		'role' => Role::PUBLISHER,
		'expireTime' => strtotime(date('Y-m-d H:i:s').' +180 minutes'),
		'data' => $metadata
	);
     
       $tok= $opentok->generateToken($sessionId,$properties);
       header("location:".VIDEOCHAT_SITE_URL."videochatsession.php?to_id=".$_REQUEST['id']."&session_id=".$sessionId."&tok=".$tok);


 ?>       
       
