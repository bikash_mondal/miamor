<?php
ob_start();
session_start();
include('administrator/includes/config.php');
include('class.phpmailer.php');
include('includes/language.php');
if ($_SESSION['user_id'] == '') {
    header('location:index.php');
    exit;
}

if ($_POST['sub']) {
    $partner_looking_for = '';
    $partner_marital_status = '';
    $partner_believe_love = '';
    $partner_have_kids = '';
    $partner_family_importance = '';
    $partner_loyal_in_relation = '';
    $partner_romantic_person = '';
    $partner_longest_relation = '';
    $partner_religion_importance = '';
    $partner_willing_relocate = '';
    $partner_new_experience = '';
    $partner_show_affection = '';
    $partner_friend_with_ex = '';
    $partner_financially_secure = '';
    $partner_most_enjoy = '';
    $partner_smoke = '';
    $partner_drink = '';
    $partner_exercise = '';
    $partner_bill_pay = '';
    $partner_want_from_relationship = '';

    //echo "<pre>"; print_r($_POST); exit;
    
    
    $looking_for = isset($_POST['looking_for']) ? $_POST['looking_for'] : '';
    if (!empty($_POST['partner_looking_for'])) {
        foreach ($_POST['partner_looking_for'] as $v) {
            $partner_looking_for.=$v . ',';
        }
        $partner_looking_for = trim($partner_looking_for, ',');
    }
    $looking_for_important = isset($_POST['looking_for_important']) ? $_POST['looking_for_important'] : '';

    
    
    
    
    
    
    $marital_status = isset($_POST['marital_status']) ? $_POST['marital_status'] : '';
    if (!empty($_POST['partner_marital_status'])) {
        foreach ($_POST['partner_marital_status'] as $v) {
            $partner_marital_status.=$v . ',';
        }
        $partner_marital_status = trim($partner_marital_status, ',');
    }
    $marital_status_important = isset($_POST['marital_status_important']) ? $_POST['marital_status_important'] : '';

    $believe_love = isset($_POST['believe_love']) ? $_POST['believe_love'] : '';
    if (!empty($_POST['partner_believe_love'])) {
        foreach ($_POST['partner_believe_love'] as $v) {
            $partner_believe_love.=$v . ',';
        }
        $partner_believe_love = trim($partner_believe_love, ',');
    }
    $believe_love_important = isset($_POST['believe_love_important']) ? $_POST['believe_love_important'] : '';

    $have_kids = isset($_POST['have_kids']) ? $_POST['have_kids'] : '';
    if (!empty($_POST['partner_have_kids'])) {
        foreach ($_POST['partner_have_kids'] as $v) {
            $partner_have_kids.=$v . ',';
        }
        $partner_have_kids = trim($partner_have_kids, ',');
    }
    $have_kids_important = isset($_POST['have_kids_important']) ? $_POST['have_kids_important'] : '';

    $family_importance = isset($_POST['family_importance']) ? $_POST['family_importance'] : '';
    if (!empty($_POST['partner_family_importance'])) {
        foreach ($_POST['partner_family_importance'] as $v) {
            $partner_family_importance.=$v . ',';
        }
        $partner_family_importance = trim($partner_family_importance, ',');
    }
    $family_importance_important = isset($_POST['family_importance_important']) ? $_POST['family_importance_important'] : '';

    $loyal_in_relation = isset($_POST['loyal_in_relation']) ? $_POST['loyal_in_relation'] : '';
    if (!empty($_POST['partner_loyal_in_relation'])) {
        foreach ($_POST['partner_loyal_in_relation'] as $v) {
            $partner_loyal_in_relation.=$v . ',';
        }
        $partner_loyal_in_relation = trim($partner_loyal_in_relation, ',');
    }
    $loyal_in_relation_important = isset($_POST['loyal_in_relation_important']) ? $_POST['loyal_in_relation_important'] : '';

    $romantic_person = isset($_POST['romantic_person']) ? $_POST['romantic_person'] : '';
    if (!empty($_POST['partner_romantic_person'])) {
        foreach ($_POST['partner_romantic_person'] as $v) {
            $partner_romantic_person.=$v . ',';
        }
        $partner_romantic_person = trim($partner_romantic_person, ',');
    }
    $romantic_person_important = isset($_POST['romantic_person_important']) ? $_POST['romantic_person_important'] : '';

    $longest_relation = isset($_POST['longest_relation']) ? $_POST['longest_relation'] : '';
    if (!empty($_POST['partner_longest_relation'])) {
        foreach ($_POST['partner_longest_relation'] as $v) {
            $partner_longest_relation.=$v . ',';
        }
        $partner_longest_relation = trim($partner_longest_relation, ',');
    }
    $longest_relation_important = isset($_POST['longest_relation_important']) ? $_POST['longest_relation_important'] : '';

    $religion_importance = isset($_POST['religion_importance']) ? $_POST['religion_importance'] : '';
    if (!empty($_POST['partner_religion_importance'])) {
        foreach ($_POST['partner_religion_importance'] as $v) {
            $partner_religion_importance.=$v . ',';
        }
        $partner_religion_importance = trim($partner_religion_importance, ',');
    }
    $religion_importance_important = isset($_POST['religion_importance_important']) ? $_POST['religion_importance_important'] : '';

    $willing_relocate = isset($_POST['willing_relocate']) ? $_POST['willing_relocate'] : '';
    if (!empty($_POST['partner_willing_relocate'])) {
        foreach ($_POST['partner_willing_relocate'] as $v) {
            $partner_willing_relocate.=$v . ',';
        }
        $partner_willing_relocate = trim($partner_willing_relocate, ',');
    }
    $willing_relocate_important = isset($_POST['willing_relocate_important']) ? $_POST['willing_relocate_important'] : '';

    $new_experience = isset($_POST['new_experience']) ? $_POST['new_experience'] : '';
    if (!empty($_POST['partner_new_experience'])) {
        foreach ($_POST['partner_new_experience'] as $v) {
            $partner_new_experience.=$v . ',';
        }
        $partner_new_experience = trim($partner_new_experience, ',');
    }
    $new_experience_important = isset($_POST['new_experience_important']) ? $_POST['new_experience_important'] : '';

    $show_affection = isset($_POST['show_affection']) ? $_POST['show_affection'] : '';
    if (!empty($_POST['partner_show_affection'])) {
        foreach ($_POST['partner_show_affection'] as $v) {
            $partner_show_affection.=$v . ',';
        }
        $partner_show_affection = trim($partner_show_affection, ',');
    }
    $show_affection_important = isset($_POST['show_affection_important']) ? $_POST['show_affection_important'] : '';

    $friend_with_ex = isset($_POST['friend_with_ex']) ? $_POST['friend_with_ex'] : '';
    if (!empty($_POST['partner_friend_with_ex'])) {
        foreach ($_POST['partner_friend_with_ex'] as $v) {
            $partner_friend_with_ex.=$v . ',';
        }
        $partner_friend_with_ex = trim($partner_friend_with_ex, ',');
    }
    $friend_with_ex_important = isset($_POST['friend_with_ex_important']) ? $_POST['friend_with_ex_important'] : '';

    $financially_secure = isset($_POST['financially_secure']) ? $_POST['financially_secure'] : '';
    if (!empty($_POST['partner_financially_secure'])) {
        foreach ($_POST['partner_financially_secure'] as $v) {
            $partner_financially_secure.=$v . ',';
        }
        $partner_financially_secure = trim($partner_financially_secure, ',');
    }
    $financially_secure_important = isset($_POST['financially_secure_important']) ? $_POST['financially_secure_important'] : '';

    $most_enjoy = isset($_POST['most_enjoy']) ? $_POST['most_enjoy'] : '';
    if (!empty($_POST['partner_most_enjoy'])) {
        foreach ($_POST['partner_most_enjoy'] as $v) {
            $partner_most_enjoy.=$v . ',';
        }
        $partner_most_enjoy = trim($partner_most_enjoy, ',');
    }
    $most_enjoy_important = isset($_POST['most_enjoy_important']) ? $_POST['most_enjoy_important'] : '';

    $smoke = isset($_POST['smoke']) ? $_POST['smoke'] : '';
    if (!empty($_POST['partner_smoke'])) {
        foreach ($_POST['partner_smoke'] as $v) {
            $partner_smoke.=$v . ',';
        }
        $partner_smoke = trim($partner_smoke, ',');
    }
    $smoke_important = isset($_POST['smoke_important']) ? $_POST['smoke_important'] : '';

    $drink = isset($_POST['drink']) ? $_POST['drink'] : '';
    if (!empty($_POST['partner_drink'])) {
        foreach ($_POST['partner_drink'] as $v) {
            $partner_drink.=$v . ',';
        }
        $partner_drink = trim($partner_drink, ',');
    }
    $drink_important = isset($_POST['drink_important']) ? $_POST['drink_important'] : '';

    $exercise = isset($_POST['exercise']) ? $_POST['exercise'] : '';
    if (!empty($_POST['partner_exercise'])) {
        foreach ($_POST['partner_exercise'] as $v) {
            $partner_exercise.=$v . ',';
        }
        $partner_exercise = trim($partner_exercise, ',');
    }
    $exercise_important = isset($_POST['exercise_important']) ? $_POST['exercise_important'] : '';

    $bill_pay = isset($_POST['bill_pay']) ? $_POST['bill_pay'] : '';
    if (!empty($_POST['partner_bill_pay'])) {
        foreach ($_POST['partner_bill_pay'] as $v) {
            $partner_bill_pay.=$v . ',';
        }
        $partner_bill_pay = trim($partner_bill_pay, ',');
    }
    $bill_pay_important = isset($_POST['bill_pay_important']) ? $_POST['bill_pay_important'] : '';

    $want_from_relationship = isset($_POST['want_from_relationship']) ? $_POST['want_from_relationship'] : '';
    if (!empty($_POST['partner_want_from_relationship'])) {
        foreach ($_POST['partner_want_from_relationship'] as $v) {
            $partner_want_from_relationship.=$v . ',';
        }
        $partner_want_from_relationship = trim($partner_want_from_relationship, ',');
    }
    $want_from_relationship_important = isset($_POST['want_from_relationship_important']) ? $_POST['want_from_relationship_important'] : '';

    $sql = mysql_query("SELECT * FROM `dating_compatibility_setting` WHERE `user_id`='" . $_SESSION['user_id'] . "'");
    $tot = mysql_num_rows($sql);
    if ($tot == 0) {
        $fields = array(
            'user_id' => $_SESSION['user_id'],
            'looking_for' => mysql_real_escape_string($looking_for),
            'partner_looking_for' => mysql_real_escape_string($partner_looking_for),
            'looking_for_important' => mysql_real_escape_string($looking_for_important),
            'marital_status' => mysql_real_escape_string($marital_status),
            'partner_marital_status' => mysql_real_escape_string($partner_marital_status),
            'marital_status_important' => mysql_real_escape_string($marital_status_important),
            'believe_love' => mysql_real_escape_string($believe_love),
            'partner_believe_love' => mysql_real_escape_string($partner_believe_love),
            'believe_love_important' => mysql_real_escape_string($believe_love_important),
            'have_kids' => mysql_real_escape_string($have_kids),
            'partner_have_kids' => mysql_real_escape_string($partner_have_kids),
            'have_kids_important' => mysql_real_escape_string($have_kids_important),
            'family_importance' => mysql_real_escape_string($family_importance),
            'partner_family_importance' => mysql_real_escape_string($partner_family_importance),
            'family_importance_important' => mysql_real_escape_string($family_importance_important),
            'loyal_in_relation' => mysql_real_escape_string($loyal_in_relation),
            'partner_loyal_in_relation' => mysql_real_escape_string($partner_loyal_in_relation),
            'loyal_in_relation_important' => mysql_real_escape_string($loyal_in_relation_important),
            'romantic_person' => mysql_real_escape_string($romantic_person),
            'partner_romantic_person' => mysql_real_escape_string($partner_romantic_person),
            'romantic_person_important' => mysql_real_escape_string($romantic_person_important),
            'longest_relation' => mysql_real_escape_string($longest_relation),
            'partner_longest_relation' => mysql_real_escape_string($partner_longest_relation),
            'longest_relation_important' => mysql_real_escape_string($longest_relation_important),
            'religion_importance' => mysql_real_escape_string($religion_importance),
            'partner_religion_importance' => mysql_real_escape_string($partner_religion_importance),
            'religion_importance_important' => mysql_real_escape_string($religion_importance_important),
            'willing_relocate' => mysql_real_escape_string($willing_relocate),
            'partner_willing_relocate' => mysql_real_escape_string($partner_willing_relocate),
            'willing_relocate_important' => mysql_real_escape_string($willing_relocate_important),
            'new_experience' => mysql_real_escape_string($new_experience),
            'partner_new_experience' => mysql_real_escape_string($partner_new_experience),
            'new_experience_important' => mysql_real_escape_string($new_experience_important),
            'show_affection' => mysql_real_escape_string($show_affection),
            'partner_show_affection' => mysql_real_escape_string($partner_show_affection),
            'show_affection_important' => mysql_real_escape_string($show_affection_important),
            'friend_with_ex' => mysql_real_escape_string($friend_with_ex),
            'partner_friend_with_ex' => mysql_real_escape_string($partner_friend_with_ex),
            'friend_with_ex_important' => mysql_real_escape_string($friend_with_ex_important),
            'financially_secure' => mysql_real_escape_string($financially_secure),
            'partner_financially_secure' => mysql_real_escape_string($partner_financially_secure),
            'financially_secure_important' => mysql_real_escape_string($financially_secure_important),
            'most_enjoy' => mysql_real_escape_string($most_enjoy),
            'partner_most_enjoy' => mysql_real_escape_string($partner_most_enjoy),
            'most_enjoy_important' => mysql_real_escape_string($most_enjoy_important),
            'smoke' => mysql_real_escape_string($smoke),
            'partner_smoke' => mysql_real_escape_string($partner_smoke),
            'smoke_important' => mysql_real_escape_string($smoke_important),
            'drink' => mysql_real_escape_string($drink),
            'partner_drink' => mysql_real_escape_string($partner_drink),
            'drink_important' => mysql_real_escape_string($drink_important),
            'exercise' => mysql_real_escape_string($exercise),
            'partner_exercise' => mysql_real_escape_string($partner_exercise),
            'exercise_important' => mysql_real_escape_string($exercise_important),
            'bill_pay' => mysql_real_escape_string($bill_pay),
            'partner_bill_pay' => mysql_real_escape_string($partner_bill_pay),
            'bill_pay_important' => mysql_real_escape_string($bill_pay_important),
            'want_from_relationship' => mysql_real_escape_string($want_from_relationship),
            'partner_want_from_relationship' => mysql_real_escape_string($partner_want_from_relationship),
            'want_from_relationship_important' => mysql_real_escape_string($want_from_relationship_important));

        $fieldsListname = array();
        $fieldsListval = array();
        foreach ($fields as $field => $value) {
            $fieldsListname[] = '' . $field . '';
            $fieldsListval[] = '"' . $value . '"';
        }
        $editQuery = "INSERT into `dating_compatibility_setting`(" . implode(', ', $fieldsListname) . ") values (" . implode(', ', $fieldsListval) . ")";
    } else {
        $fields = array(
            'looking_for' => mysql_real_escape_string($looking_for),
            'partner_looking_for' => mysql_real_escape_string($partner_looking_for),
            'looking_for_important' => mysql_real_escape_string($looking_for_important),
            'marital_status' => mysql_real_escape_string($marital_status),
            'partner_marital_status' => mysql_real_escape_string($partner_marital_status),
            'marital_status_important' => mysql_real_escape_string($marital_status_important),
            'believe_love' => mysql_real_escape_string($believe_love),
            'partner_believe_love' => mysql_real_escape_string($partner_believe_love),
            'believe_love_important' => mysql_real_escape_string($believe_love_important),
            'have_kids' => mysql_real_escape_string($have_kids),
            'partner_have_kids' => mysql_real_escape_string($partner_have_kids),
            'have_kids_important' => mysql_real_escape_string($have_kids_important),
            'family_importance' => mysql_real_escape_string($family_importance),
            'partner_family_importance' => mysql_real_escape_string($partner_family_importance),
            'family_importance_important' => mysql_real_escape_string($family_importance_important),
            'loyal_in_relation' => mysql_real_escape_string($loyal_in_relation),
            'partner_loyal_in_relation' => mysql_real_escape_string($partner_loyal_in_relation),
            'loyal_in_relation_important' => mysql_real_escape_string($loyal_in_relation_important),
            'romantic_person' => mysql_real_escape_string($romantic_person),
            'partner_romantic_person' => mysql_real_escape_string($partner_romantic_person),
            'romantic_person_important' => mysql_real_escape_string($romantic_person_important),
            'longest_relation' => mysql_real_escape_string($longest_relation),
            'partner_longest_relation' => mysql_real_escape_string($partner_longest_relation),
            'longest_relation_important' => mysql_real_escape_string($longest_relation_important),
            'religion_importance' => mysql_real_escape_string($religion_importance),
            'partner_religion_importance' => mysql_real_escape_string($partner_religion_importance),
            'religion_importance_important' => mysql_real_escape_string($religion_importance_important),
            'willing_relocate' => mysql_real_escape_string($willing_relocate),
            'partner_willing_relocate' => mysql_real_escape_string($partner_willing_relocate),
            'willing_relocate_important' => mysql_real_escape_string($willing_relocate_important),
            'new_experience' => mysql_real_escape_string($new_experience),
            'partner_new_experience' => mysql_real_escape_string($partner_new_experience),
            'new_experience_important' => mysql_real_escape_string($new_experience_important),
            'show_affection' => mysql_real_escape_string($show_affection),
            'partner_show_affection' => mysql_real_escape_string($partner_show_affection),
            'show_affection_important' => mysql_real_escape_string($show_affection_important),
            'friend_with_ex' => mysql_real_escape_string($friend_with_ex),
            'partner_friend_with_ex' => mysql_real_escape_string($partner_friend_with_ex),
            'friend_with_ex_important' => mysql_real_escape_string($friend_with_ex_important),
            'financially_secure' => mysql_real_escape_string($financially_secure),
            'partner_financially_secure' => mysql_real_escape_string($partner_financially_secure),
            'financially_secure_important' => mysql_real_escape_string($financially_secure_important),
            'most_enjoy' => mysql_real_escape_string($most_enjoy),
            'partner_most_enjoy' => mysql_real_escape_string($partner_most_enjoy),
            'most_enjoy_important' => mysql_real_escape_string($most_enjoy_important),
            'smoke' => mysql_real_escape_string($smoke),
            'partner_smoke' => mysql_real_escape_string($partner_smoke),
            'smoke_important' => mysql_real_escape_string($smoke_important),
            'drink' => mysql_real_escape_string($drink),
            'partner_drink' => mysql_real_escape_string($partner_drink),
            'drink_important' => mysql_real_escape_string($drink_important),
            'exercise' => mysql_real_escape_string($exercise),
            'partner_exercise' => mysql_real_escape_string($partner_exercise),
            'exercise_important' => mysql_real_escape_string($exercise_important),
            'bill_pay' => mysql_real_escape_string($bill_pay),
            'partner_bill_pay' => mysql_real_escape_string($partner_bill_pay),
            'bill_pay_important' => mysql_real_escape_string($bill_pay_important),
            'want_from_relationship' => mysql_real_escape_string($want_from_relationship),
            'partner_want_from_relationship' => mysql_real_escape_string($partner_want_from_relationship),
            'want_from_relationship_important' => mysql_real_escape_string($want_from_relationship_important));

        $fieldsList = array();
        foreach ($fields as $field => $value) {
            $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
        }

        $editQuery = "UPDATE `dating_compatibility_setting` SET " . implode(', ', $fieldsList)
                . " WHERE `user_id` = '" . $_SESSION['user_id'] . "'";
    }

    if (mysql_query($editQuery)) {
        $_SESSION['msg'] = "Compatibility Setting Updated Successfully";
    } else {
        $_SESSION['msg'] = "Error occuried while updating Compatibility";
    }
	$_SESSION['NOTIFY_MESSAGE'] = COMP_SETTING;
    header('Location:compatibility_setting.php');
    exit();
}

$sqlupdated = mysql_query("SELECT * FROM `dating_compatibility_setting` WHERE `user_id`='" . $_SESSION['user_id'] . "'");
$totupdated = mysql_num_rows($sqlupdated);
$resupdated = mysql_fetch_array($sqlupdated)
?>										

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <?php echo COMPT_SETTING ?>
        <meta name="" content="">
        <link rel="stylesheet" href="css/style.css">
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    </head>
    <body style="background: url(images/bg-main.jpg) center top no-repeat fixed; margin: 0 0 15px 0;">
<?php include('includes/header.php'); ?>
        <div class="container">
            <div class="profile_body">
<?php include('includes/left-panel.php'); ?>
                <div class="right_menu">
                    <div class="bred_cumb">
                        <img src="images/step3.png" alt="" />
                        <ul>
                            <li><a href="edit_profile.php"><?php echo CREATE_PROFILE ?></a></li>
                            <li><a href="upload_photo.php"><?php echo UPLOAD_PHOTO ?></a></li>
                            <li><a href="compatibility_setting.php"><?php echo COMPT_SETTING ?></a></li>
                        </ul>
                    </div>
                    <div class="tab_based search_page">

                        <div class="tab_container">
                            <div id="tab2" class="tab_content">
                                <form action="" method="POST">
                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LOOKING_FOR ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 1) {
    echo 'checked';
} ?>/><b><?php echo AFRIEND ?></b></p>
                                            <p><input type="radio" value="2" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 2) {
    echo 'checked';
} ?>/><b><?php echo SOMETHINGCASUAL ?></b></p>
                                            <p><input type="radio" value="3" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 3) {
    echo 'checked';
} ?>/><b><?php echo ARELATION ?></b></p>
                                            <p><input type="radio" value="4" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 4) {
    echo 'checked';
} ?>/><b><?php echo MARRIGE ?></b></p>
                                            <p><input type="radio" value="5" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 5) {
    echo 'checked';
} ?>/><b><?php echo SUGAR ?></b></p>
                                            <p><input type="radio" value="6" name="looking_for" <?php if (isset($resupdated['looking_for']) && $resupdated['looking_for'] == 6) {
    echo 'checked';
} ?>/><b><?php echo UNSURE ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
        <?php
        $partner_looking_for_exp = array();
        if (isset($resupdated['partner_looking_for']) && $resupdated['partner_looking_for'] != '') {
            $partner_looking_for_exp = explode(',', $resupdated['partner_looking_for']);
        }
        ?>
                                            <p><input type="radio" value="1" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('1', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo AFRIEND ?></b></p>
                                            <p><input type="radio" value="2" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('2', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo SOMETHINGCASUAL ?></b></p>
                                            <p><input type="radio" value="3" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('3', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo ARELATION ?></b></p>
                                            <p><input type="radio" value="4" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('4', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo MARRIGE ?></b></p>
                                            <p><input type="radio" value="5" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('5', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo SUGAR ?></b></p>
                                            <p><input type="radio" value="6" name="partner_looking_for[]" <?php if (!empty($partner_looking_for_exp) && in_array('6', $partner_looking_for_exp)) {
            echo 'checked';
        } ?>/><b><?php echo UNSURE ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="looking_for_important" <?php if (isset($resupdated['looking_for_important']) && $resupdated['looking_for_important'] == 1) {
            echo 'checked';
        } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="looking_for_important" <?php if (isset($resupdated['looking_for_important']) && $resupdated['looking_for_important'] == 2) {
            echo 'checked';
        } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="looking_for_important" <?php if (isset($resupdated['looking_for_important']) && $resupdated['looking_for_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo MATSTATUS ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="marital_status" <?php if (isset($resupdated['marital_status']) && $resupdated['marital_status'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SINGLE ?></b></p>
                                            <p><input type="radio" value="2" name="marital_status" <?php if (isset($resupdated['marital_status']) && $resupdated['marital_status'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MARRIED ?></b></p>
                                            <p><input type="radio" value="3" name="marital_status" <?php if (isset($resupdated['marital_status']) && $resupdated['marital_status'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SEPARETED ?></b></p>
                                            <p><input type="radio" value="4" name="marital_status" <?php if (isset($resupdated['marital_status']) && $resupdated['marital_status'] == 4) {
                                                echo 'checked';
                                            } ?>/><b><?php echo DIVORCED ?></b></p>
                                            <p><input type="radio" value="5" name="marital_status" <?php if (isset($resupdated['marital_status']) && $resupdated['marital_status'] == 5) {
                                                echo 'checked';
                                            } ?>/><b><?php echo WIDOWED ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNER_SHOULD ?></lable>
                                        <div class="input auto_height">
<?php
$partner_marital_status_exp = array();
if (isset($resupdated['partner_marital_status']) && $resupdated['partner_marital_status'] != '') {
    $partner_marital_status_exp = explode(',', $resupdated['partner_marital_status']);
}
?>
                                            <p><input type="radio" value="1" name="partner_marital_status[]" <?php if (!empty($partner_marital_status_exp) && in_array('1', $partner_marital_status_exp)) {
    echo 'checked';
} ?>/><b><?php echo SINGLE ?></b></p>
                                            <p><input type="radio" value="2" name="partner_marital_status[]" <?php if (!empty($partner_marital_status_exp) && in_array('2', $partner_marital_status_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MARRIED ?></b></p>
                                            <p><input type="radio" value="3" name="partner_marital_status[]" <?php if (!empty($partner_marital_status_exp) && in_array('3', $partner_marital_status_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SEPARETED ?></b></p>
                                            <p><input type="radio" value="4" name="partner_marital_status[]" <?php if (!empty($partner_marital_status_exp) && in_array('4', $partner_marital_status_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo DIVORCED ?></b></p>
                                            <p><input type="radio" value="5" name="partner_marital_status[]" <?php if (!empty($partner_marital_status_exp) && in_array('5', $partner_marital_status_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo WIDOWED ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="marital_status_important" <?php if (isset($resupdated['marital_status_important']) && $resupdated['marital_status_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="marital_status_important" <?php if (isset($resupdated['marital_status_important']) && $resupdated['marital_status_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="marital_status_important" <?php if (isset($resupdated['marital_status_important']) && $resupdated['marital_status_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo DOYOUBELIVE ?>?</lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="believe_love" <?php if (isset($resupdated['believe_love']) && $resupdated['believe_love'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="believe_love" <?php if (isset($resupdated['believe_love']) && $resupdated['believe_love'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_believe_love_exp = array();
if (isset($resupdated['partner_believe_love']) && $resupdated['partner_believe_love'] != '') {
    $partner_believe_love_exp = explode(',', $resupdated['partner_believe_love']);
}
?>
                                            <p><input type="radio" value="1" name="partner_believe_love[]" <?php if (!empty($partner_believe_love_exp) && in_array('1', $partner_believe_love_exp)) {
    echo 'checked';
} ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="partner_believe_love[]" <?php if (!empty($partner_believe_love_exp) && in_array('2', $partner_believe_love_exp)) {
    echo 'checked';
} ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="believe_love_important" <?php if (isset($resupdated['believe_love_important']) && $resupdated['believe_love_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="believe_love_important" <?php if (isset($resupdated['believe_love_important']) && $resupdated['believe_love_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="believe_love_important" <?php if (isset($resupdated['believe_love_important']) && $resupdated['believe_love_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo DOYOUWANTTO ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="have_kids" <?php if (isset($resupdated['have_kids']) && $resupdated['have_kids'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="have_kids" <?php if (isset($resupdated['have_kids']) && $resupdated['have_kids'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_have_kids_exp = array();
if (isset($resupdated['partner_have_kids']) && $resupdated['partner_have_kids'] != '') {
    $partner_have_kids_exp = explode(',', $resupdated['partner_believe_love']);
}
?>
                                            <p><input type="radio" value="1" name="partner_have_kids[]" <?php if (!empty($partner_have_kids_exp) && in_array('1', $partner_have_kids_exp)) {
    echo 'checked';
} ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="partner_have_kids[]" <?php if (!empty($partner_have_kids_exp) && in_array('2', $partner_have_kids_exp)) {
    echo 'checked';
} ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="have_kids_important" <?php if (isset($resupdated['have_kids_important']) && $resupdated['have_kids_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="have_kids_important" <?php if (isset($resupdated['have_kids_important']) && $resupdated['have_kids_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="have_kids_important" <?php if (isset($resupdated['have_kids_important']) && $resupdated['have_kids_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">   
                                        <li>
                                        <lable><?php echo HOWIMPORTANTFAMILY ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="family_importance" <?php if (isset($resupdated['family_importance']) && $resupdated['family_importance'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="family_importance" <?php if (isset($resupdated['family_importance']) && $resupdated['family_importance'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="family_importance" <?php if (isset($resupdated['family_importance']) && $resupdated['family_importance'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
                                            <?php
                                            $partner_family_importance_exp = array();
                                            if (isset($resupdated['partner_family_importance']) && $resupdated['partner_family_importance'] != '') {
                                                $partner_family_importance_exp = explode(',', $resupdated['partner_family_importance']);
                                            }
                                            ?>
                                            <p><input type="radio" value="1" name="partner_family_importance[]" <?php if (!empty($partner_family_importance_exp) && in_array('1', $partner_family_importance_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="partner_family_importance[]" <?php if (!empty($partner_family_importance_exp) && in_array('2', $partner_family_importance_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="partner_family_importance[]" <?php if (!empty($partner_family_importance_exp) && in_array('3', $partner_family_importance_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="family_importance_important" <?php if (isset($resupdated['family_importance_important']) && $resupdated['family_importance_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="family_importance_important" <?php if (isset($resupdated['family_importance_important']) && $resupdated['family_importance_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="family_importance_important" <?php if (isset($resupdated['family_importance_important']) && $resupdated['family_importance_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo ROYALRELATIONSHIP ?>?</lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="loyal_in_relation" <?php if (isset($resupdated['loyal_in_relation']) && $resupdated['loyal_in_relation'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo ALWAYS ?></b></p>
                                            <p><input type="radio" value="2" name="loyal_in_relation" <?php if (isset($resupdated['loyal_in_relation']) && $resupdated['loyal_in_relation'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MOSTLY ?></b></p>
                                            <p><input type="radio" value="3" name="loyal_in_relation" <?php if (isset($resupdated['loyal_in_relation']) && $resupdated['loyal_in_relation'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SOMETIME ?></b></p>
                                            <p><input type="radio" value="4" name="loyal_in_relation" <?php if (isset($resupdated['loyal_in_relation']) && $resupdated['loyal_in_relation'] == 4) {
                                                echo 'checked';
                                            } ?>/><b><?php echo WHENSUTI ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_loyal_in_relation_exp = array();
if (isset($resupdated['partner_loyal_in_relation']) && $resupdated['partner_loyal_in_relation'] != '') {
    $partner_loyal_in_relation_exp = explode(',', $resupdated['partner_loyal_in_relation']);
}
?>
                                            <p><input type="radio" value="1" name="partner_loyal_in_relation[]" <?php if (!empty($partner_loyal_in_relation_exp) && in_array('1', $partner_loyal_in_relation_exp)) {
    echo 'checked';
} ?>/><b><?php echo ALWAYS ?></b></p>
                                            <p><input type="radio" value="2" name="partner_loyal_in_relation[]" <?php if (!empty($partner_loyal_in_relation_exp) && in_array('2', $partner_loyal_in_relation_exp)) {
    echo 'checked';
} ?>/><b><?php echo MOSTLY ?></b></p>
                                            <p><input type="radio" value="3" name="partner_loyal_in_relation[]" <?php if (!empty($partner_loyal_in_relation_exp) && in_array('3', $partner_loyal_in_relation_exp)) {
    echo 'checked';
} ?>/><b><?php echo SOMETIME ?></b></p>
                                            <p><input type="radio" value="4" name="partner_loyal_in_relation[]" <?php if (!empty($partner_loyal_in_relation_exp) && in_array('4', $partner_loyal_in_relation_exp)) {
    echo 'checked';
} ?>/><b><?php echo WHENSUTI ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="loyal_in_relation_important" <?php if (isset($resupdated['loyal_in_relation_important']) && $resupdated['loyal_in_relation_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="loyal_in_relation_important" <?php if (isset($resupdated['loyal_in_relation_important']) && $resupdated['loyal_in_relation_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="loyal_in_relation_important" <?php if (isset($resupdated['loyal_in_relation_important']) && $resupdated['loyal_in_relation_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>

                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo AREYOUROMENTIC ?>?</lable>
                                        <div class="input auto_height">
                                            <p><span style="float:left;"><input type="radio" value="1" name="romantic_person" <?php if (isset($resupdated['romantic_person']) && $resupdated['romantic_person'] == 1) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LOVE_ROMENTIC ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="romantic_person" <?php if (isset($resupdated['romantic_person']) && $resupdated['romantic_person'] == 2) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo OCCA_ROMENTIC ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="romantic_person" <?php if (isset($resupdated['romantic_person']) && $resupdated['romantic_person'] == 3) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo NO_ROMENTIC ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNER_SHOULD ?></lable>
                                        <div class="input auto_height">
                                            <?php
                                            $partner_romantic_person_exp = array();
                                            if (isset($resupdated['partner_romantic_person']) && $resupdated['partner_romantic_person'] != '') {
                                                $partner_romantic_person_exp = explode(',', $resupdated['partner_romantic_person']);
                                            }
                                            ?>
                                            <p><span style="float:left;"><input type="radio" value="1" name="partner_romantic_person[]" <?php if (!empty($partner_romantic_person_exp) && in_array('1', $partner_romantic_person_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LOVE_ROMENTIC ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_romantic_person[]" <?php if (!empty($partner_romantic_person_exp) && in_array('2', $partner_romantic_person_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo OCCA_ROMENTIC ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="partner_romantic_person[]" <?php if (!empty($partner_romantic_person_exp) && in_array('3', $partner_romantic_person_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo NO_ROMENTIC ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="romantic_person_important" <?php if (isset($resupdated['romantic_person_important']) && $resupdated['romantic_person_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="romantic_person_important" <?php if (isset($resupdated['romantic_person_important']) && $resupdated['romantic_person_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="romantic_person_important" <?php if (isset($resupdated['romantic_person_important']) && $resupdated['romantic_person_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>

                                    <div class="clearfix"></div>

                                    <ul class="search_field">     

                                        <li>
                                        <lable><?php echo LONGEST_RELATION ?>?</lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="longest_relation" <?php if (isset($resupdated['longest_relation']) && $resupdated['longest_relation'] == 1) {
                                                echo 'checked';
                                            } ?>/><b> 6 <?php echo MONTH ?></b></p>
                                            <p><input type="radio" value="2" name="longest_relation" <?php if (isset($resupdated['longest_relation']) && $resupdated['longest_relation'] == 2) {
                                                echo 'checked';
                                            } ?>/><b>6 <?php echo MONTH ?> - 1 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="3" name="longest_relation" <?php if (isset($resupdated['longest_relation']) && $resupdated['longest_relation'] == 3) {
                                                echo 'checked';
                                            } ?>/><b>1 <?php echo YEAR ?> - 3 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="4" name="longest_relation" <?php if (isset($resupdated['longest_relation']) && $resupdated['longest_relation'] == 4) {
                                                echo 'checked';
                                            } ?>/><b>3 <?php echo YEAR ?> - 6 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="5" name="longest_relation" <?php if (isset($resupdated['longest_relation']) && $resupdated['longest_relation'] == 5) {
                                                echo 'checked';
                                            } ?>/><b>6 <?php echo YEAR ?> +</b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_longest_relation_exp = array();
if (isset($resupdated['partner_longest_relation']) && $resupdated['partner_longest_relation'] != '') {
    $partner_longest_relation_exp = explode(',', $resupdated['partner_longest_relation']);
}
?>
                                            <p><input type="radio" value="1" name="partner_longest_relation[]" <?php if (!empty($partner_longest_relation_exp) && in_array('1', $partner_longest_relation_exp)) {
    echo 'checked';
} ?>/><b> 6 <?php echo MONTH ?></b></p>
                                            <p><input type="radio" value="2" name="partner_longest_relation[]" <?php if (!empty($partner_longest_relation_exp) && in_array('2', $partner_longest_relation_exp)) {
    echo 'checked';
} ?>/><b>6 <?php echo MONTH ?> - 1 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="3" name="partner_longest_relation[]" <?php if (!empty($partner_longest_relation_exp) && in_array('3', $partner_longest_relation_exp)) {
    echo 'checked';
} ?>/><b>1 <?php echo YEAR ?> - 3 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="4" name="partner_longest_relation[]" <?php if (!empty($partner_longest_relation_exp) && in_array('4', $partner_longest_relation_exp)) {
                                                echo 'checked';
                                            } ?>/><b>3 <?php echo YEAR ?> - 6 <?php echo YEAR ?></b></p>
                                            <p><input type="radio" value="5" name="partner_longest_relation[]" <?php if (!empty($partner_longest_relation_exp) && in_array('5', $partner_longest_relation_exp)) {
                                                echo 'checked';
                                            } ?>/><b>6 <?php echo YEAR ?> +</b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="longest_relation_important" <?php if (isset($resupdated['longest_relation_important']) && $resupdated['longest_relation_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="longest_relation_important" <?php if (isset($resupdated['longest_relation_important']) && $resupdated['longest_relation_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="longest_relation_important" <?php if (isset($resupdated['longest_relation_important']) && $resupdated['longest_relation_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">                                
                                        <li>
                                        <lable><?php echo IS_RELIGION ?>?</lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="religion_importance" <?php if (isset($resupdated['religion_importance']) && $resupdated['religion_importance'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="religion_importance" <?php if (isset($resupdated['religion_importance']) && $resupdated['religion_importance'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SOMEWHAT ?></b></p>
                                            <p><input type="radio" value="3" name="religion_importance" <?php if (isset($resupdated['religion_importance']) && $resupdated['religion_importance'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTREALLY ?></b></p>
                                            <p><input type="radio" value="4" name="religion_importance" <?php if (isset($resupdated['religion_importance']) && $resupdated['religion_importance'] == 4) {
                                                echo 'checked';
                                            } ?>/><b><?php echo IAMNOTREL ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNER_SHOULD ?></lable>
                                        <div class="input auto_height">
<?php
$partner_religion_importance_exp = array();
if (isset($resupdated['partner_religion_importance']) && $resupdated['partner_religion_importance'] != '') {
    $partner_religion_importance_exp = explode(',', $resupdated['partner_religion_importance']);
}
?>
                                            <p><input type="radio" value="1" name="partner_religion_importance[]" <?php if (!empty($partner_religion_importance_exp) && in_array('1', $partner_religion_importance_exp)) {
    echo 'checked';
} ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="partner_religion_importance[]" <?php if (!empty($partner_religion_importance_exp) && in_array('2', $partner_religion_importance_exp)) {
    echo 'checked';
} ?>/><b><?php echo SOMEWHAT ?></b></p>
                                            <p><input type="radio" value="3" name="partner_religion_importance[]" <?php if (!empty($partner_religion_importance_exp) && in_array('3', $partner_religion_importance_exp)) {
    echo 'checked';
} ?>/><b><?php echo NOTREALLY ?></b></p>
                                            <p><input type="radio" value="4" name="partner_religion_importance[]" <?php if (!empty($partner_religion_importance_exp) && in_array('4', $partner_religion_importance_exp)) {
    echo 'checked';
} ?>/><b><?php echo IAMNOTREL ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="religion_importance_important" <?php if (isset($resupdated['religion_importance_important']) && $resupdated['religion_importance_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="religion_importance_important" <?php if (isset($resupdated['religion_importance_important']) && $resupdated['religion_importance_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="religion_importance_important" <?php if (isset($resupdated['religion_importance_important']) && $resupdated['religion_importance_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_ARE_YOU_WILLING_RELOCATE ?></lable>
                                        <div class="input auto_height">
                                            <p><span style="float:left;"><input type="radio" value="1" name="willing_relocate" <?php if (isset($resupdated['willing_relocate']) && $resupdated['willing_relocate'] == 1) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_WILLING_TO_RELOCATE_TO_ANOTHER_COUNTRY ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="willing_relocate" <?php if (isset($resupdated['willing_relocate']) && $resupdated['willing_relocate'] == 2) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_WILLING_TO_RELOCATE_WITHIN_MY_COUNTRY ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="willing_relocate" <?php if (isset($resupdated['willing_relocate']) && $resupdated['willing_relocate'] == 3) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_NOT_SURE_RELOCATING ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="4" name="willing_relocate" <?php if (isset($resupdated['willing_relocate']) && $resupdated['willing_relocate'] == 4) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_NOT_WILLING_TO_RELOCATING ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
                                            <?php
                                            $partner_willing_relocate_exp = array();
                                            if (isset($resupdated['partner_willing_relocate']) && $resupdated['partner_willing_relocate'] != '') {
                                                $partner_willing_relocate_exp = explode(',', $resupdated['partner_willing_relocate']);
                                            }
                                            ?>
                                            <p><span style="float:left;"><input type="radio" value="1" name="partner_willing_relocate[]" <?php if (!empty($partner_willing_relocate_exp) && in_array('1', $partner_willing_relocate_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_WILLING_TO_RELOCATE_TO_ANOTHER_COUNTRY ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_willing_relocate[]" <?php if (!empty($partner_willing_relocate_exp) && in_array('2', $partner_willing_relocate_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_WILLING_TO_RELOCATE_WITHIN_MY_COUNTRY ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="partner_willing_relocate[]" <?php if (!empty($partner_willing_relocate_exp) && in_array('3', $partner_willing_relocate_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_NOT_SURE_RELOCATING ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="4" name="partner_willing_relocate[]" <?php if (!empty($partner_willing_relocate_exp) && in_array('4', $partner_willing_relocate_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_NOT_WILLING_TO_RELOCATING ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="willing_relocate_important" <?php if (isset($resupdated['willing_relocate_important']) && $resupdated['willing_relocate_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="willing_relocate_important" <?php if (isset($resupdated['willing_relocate_important']) && $resupdated['willing_relocate_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="willing_relocate_important" <?php if (isset($resupdated['willing_relocate_important']) && $resupdated['willing_relocate_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo LANG_ARE_YOU_OPEN_NEW_EXPERIENCE ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="new_experience" <?php if (isset($resupdated['new_experience']) && $resupdated['new_experience'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="new_experience" <?php if (isset($resupdated['new_experience']) && $resupdated['new_experience'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SOMETIME ?></b></p>
                                            <p><input type="radio" value="3" name="new_experience" <?php if (isset($resupdated['new_experience']) && $resupdated['new_experience'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_RARE ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_new_experience_exp = array();
if (isset($resupdated['partner_new_experience']) && $resupdated['partner_new_experience'] != '') {
    $partner_new_experience_exp = explode(',', $resupdated['partner_new_experience']);
}
?>
                                            <p><input type="radio" value="1" name="partner_new_experience[]" <?php if (!empty($partner_new_experience_exp) && in_array('1', $partner_new_experience_exp)) {
    echo 'checked';
} ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="partner_new_experience[]" <?php if (!empty($partner_new_experience_exp) && in_array('2', $partner_new_experience_exp)) {
    echo 'checked';
} ?>/><b><?php echo SOMETIME ?></b></p>
                                            <p><input type="radio" value="3" name="partner_new_experience[]" <?php if (!empty($partner_new_experience_exp) && in_array('3', $partner_new_experience_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_RARE ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="new_experience_important" <?php if (isset($resupdated['new_experience_important']) && $resupdated['new_experience_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="new_experience_important" <?php if (isset($resupdated['new_experience_important']) && $resupdated['new_experience_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="new_experience_important" <?php if (isset($resupdated['new_experience_important']) && $resupdated['new_experience_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_HOW_DO_YOU_SHOW_AFFECTION ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="show_affection" <?php if (isset($resupdated['show_affection']) && $resupdated['show_affection'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_KISSING ?></b></p>
                                            <p><input type="radio" value="2" name="show_affection" <?php if (isset($resupdated['show_affection']) && $resupdated['show_affection'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_HOLDING_HANDS ?></b></p>
                                            <p><input type="radio" value="3" name="show_affection" <?php if (isset($resupdated['show_affection']) && $resupdated['show_affection'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_WALKING_SIDE ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="4" name="show_affection" <?php if (isset($resupdated['show_affection']) && $resupdated['show_affection'] == 4) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_DONT_SHOW_AFFECTION ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
                                            <?php
                                            $partner_show_affection_exp = array();
                                            if (isset($resupdated['partner_show_affection']) && $resupdated['partner_show_affection'] != '') {
                                                $partner_show_affection_exp = explode(',', $resupdated['partner_show_affection']);
                                            }
                                            ?>
                                            <p><input type="radio" value="1" name="partner_show_affection[]" <?php if (!empty($partner_show_affection_exp) && in_array('1', $partner_show_affection_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_KISSING ?></b></p>
                                            <p><input type="radio" value="2" name="partner_show_affection[]" <?php if (!empty($partner_show_affection_exp) && in_array('2', $partner_show_affection_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_HOLDING_HANDS ?></b></p>
                                            <p><input type="radio" value="3" name="partner_show_affection[]" <?php if (!empty($partner_show_affection_exp) && in_array('3', $partner_show_affection_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_WALKING_SIDE ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="4" name="partner_show_affection[]" <?php if (!empty($partner_show_affection_exp) && in_array('4', $partner_show_affection_exp)) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_DONT_SHOW_AFFECTION ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="show_affection_important" <?php if (isset($resupdated['show_affection_important']) && $resupdated['show_affection_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="show_affection_important" <?php if (isset($resupdated['show_affection_important']) && $resupdated['show_affection_important'] == 2) {
                                                echo 'checked';
                                            } ?>><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="show_affection_important" <?php if (isset($resupdated['show_affection_important']) && $resupdated['show_affection_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_ARE_YOU_FRIENDS_WITH_EXES ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="friend_with_ex" <?php if (isset($resupdated['friend_with_ex']) && $resupdated['friend_with_ex'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_YES_ALL ?></b></p>
                                            <p><input type="radio" value="2" name="friend_with_ex" <?php if (isset($resupdated['friend_with_ex']) && $resupdated['friend_with_ex'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_SOME_OG_THEM ?></b></p>
                                            <p><input type="radio" value="3" name="friend_with_ex" <?php if (isset($resupdated['friend_with_ex']) && $resupdated['friend_with_ex'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_friend_with_ex_exp = array();
if (isset($resupdated['partner_friend_with_ex']) && $resupdated['partner_friend_with_ex'] != '') {
    $partner_friend_with_ex_exp = explode(',', $resupdated['partner_friend_with_ex']);
}
?>
                                            <p><input type="radio" value="1" name="partner_friend_with_ex[]" <?php if (!empty($partner_friend_with_ex_exp) && in_array('1', $partner_friend_with_ex_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_YES_ALL ?></b></p>
                                            <p><input type="radio" value="2" name="partner_friend_with_ex[]" <?php if (!empty($partner_friend_with_ex_exp) && in_array('2', $partner_friend_with_ex_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_SOME_OG_THEM ?></b></p>
                                            <p><input type="radio" value="3" name="partner_friend_with_ex[]" <?php if (!empty($partner_friend_with_ex_exp) && in_array('3', $partner_friend_with_ex_exp)) {
    echo 'checked';
} ?>/><b><?php echo NO ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="friend_with_ex_important" <?php if (isset($resupdated['friend_with_ex_important']) && $resupdated['friend_with_ex_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="friend_with_ex_important" <?php if (isset($resupdated['friend_with_ex_important']) && $resupdated['friend_with_ex_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="friend_with_ex_important" <?php if (isset($resupdated['friend_with_ex_important']) && $resupdated['friend_with_ex_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">     
                                        <li>
                                        <lable><?php echo LANG_FINANCIALLY_SECURE ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="financially_secure" <?php if (isset($resupdated['financially_secure']) && $resupdated['financially_secure'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="financially_secure" <?php if (isset($resupdated['financially_secure']) && $resupdated['financially_secure'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SOMEWHAT ?></b></p>
                                            <p><input type="radio" value="3" name="financially_secure" <?php if (isset($resupdated['financially_secure']) && $resupdated['financially_secure'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTYET ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
                                            <?php
                                            $partner_financially_secure_exp = array();
                                            if (isset($resupdated['partner_financially_secure']) && $resupdated['partner_financially_secure'] != '') {
                                                $partner_financially_secure_exp = explode(',', $resupdated['partner_financially_secure']);
                                            }
                                            ?>
                                            <p><input type="radio" value="1" name="partner_financially_secure[]" <?php if (!empty($partner_financially_secure_exp) && in_array('1', $partner_financially_secure_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo YES ?></b></p>
                                            <p><input type="radio" value="2" name="partner_financially_secure[]" <?php if (!empty($partner_financially_secure_exp) && in_array('2', $partner_financially_secure_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo SOMEWHAT ?></b></p>
                                            <p><input type="radio" value="3" name="partner_financially_secure[]" <?php if (!empty($partner_financially_secure_exp) && in_array('3', $partner_financially_secure_exp)) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTYET ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="financially_secure_important" <?php if (isset($resupdated['financially_secure_important']) && $resupdated['financially_secure_important'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="financially_secure_important" <?php if (isset($resupdated['financially_secure_important']) && $resupdated['financially_secure_important'] == 2) {
                                                echo 'checked';
                                            } ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="financially_secure_important" <?php if (isset($resupdated['financially_secure_important']) && $resupdated['financially_secure_important'] == 3) {
                                                echo 'checked';
                                            } ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>

                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_AFTER_WORK_ENJOY ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="most_enjoy" <?php if (isset($resupdated['most_enjoy']) && $resupdated['most_enjoy'] == 1) {
                                                echo 'checked';
                                            } ?>/><b><?php echo LANG_RELAXING_HOME ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="most_enjoy"  <?php if (isset($resupdated['most_enjoy']) && $resupdated['most_enjoy'] == 2) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_GOING_OUT_DINNER ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="most_enjoy"  <?php if (isset($resupdated['most_enjoy']) && $resupdated['most_enjoy'] == 3) {
                                                echo 'checked';
                                            } ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_GOING_BAR ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_most_enjoy_exp = array();
if (isset($resupdated['partner_most_enjoy']) && $resupdated['partner_most_enjoy'] != '') {
    $partner_most_enjoy_exp = explode(',', $resupdated['partner_most_enjoy']);
}
?>
                                            <p><input type="radio" value="1" name="partner_most_enjoy[]" <?php if (!empty($partner_most_enjoy_exp) && in_array('1', $partner_most_enjoy_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_RELAXING_HOME ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_most_enjoy[]" <?php if (!empty($partner_most_enjoy_exp) && in_array('2', $partner_most_enjoy_exp)) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_GOING_OUT_DINNER ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="partner_most_enjoy[]" <?php if (!empty($partner_most_enjoy_exp) && in_array('3', $partner_most_enjoy_exp)) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_GOING_BAR ?></b></span></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="most_enjoy_important"  <?php if (isset($resupdated['most_enjoy_important']) && $resupdated['most_enjoy_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="most_enjoy_important" <?php if (isset($resupdated['most_enjoy_important']) && $resupdated['most_enjoy_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="most_enjoy_important" <?php if (isset($resupdated['most_enjoy_important']) && $resupdated['most_enjoy_important'] == 3) {
    echo 'checked';
} ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo LANG_DO_YOU_SMOKE ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="smoke" <?php if (isset($resupdated['smoke']) && $resupdated['smoke'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="smoke" <?php if (isset($resupdated['smoke']) && $resupdated['smoke'] == 2) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_SOMETIMES_SOCIAL_SITUATIONS ?></b></span></p>
                                            <p><input type="radio" value="3" name="smoke" <?php if (isset($resupdated['smoke']) && $resupdated['smoke'] == 3) {
    echo 'checked';
} ?>/><b><?php echo YESREG ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_smoke_exp = array();
if (isset($resupdated['partner_smoke']) && $resupdated['partner_smoke'] != '') {
    $partner_smoke_exp = explode(',', $resupdated['partner_smoke']);
}
?>
                                            <p><input type="radio" value="1" name="partner_smoke[]" <?php if (!empty($partner_smoke_exp) && in_array('1', $partner_smoke_exp)) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_smoke[]" <?php if (!empty($partner_smoke_exp) && in_array('2', $partner_smoke_exp)) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_SOMETIMES_SOCIAL_SITUATIONS ?></b></span></p>
                                            <p><input type="radio" value="3" name="partner_smoke[]" <?php if (!empty($partner_smoke_exp) && in_array('3', $partner_smoke_exp)) {
    echo 'checked';
} ?>/><b><?php echo YESREG ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="smoke_important" <?php if (isset($resupdated['smoke_important']) && $resupdated['smoke_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="smoke_important" <?php if (isset($resupdated['smoke_important']) && $resupdated['smoke_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="smoke_important" <?php if (isset($resupdated['smoke_important']) && $resupdated['smoke_important'] == 3) {
    echo 'checked';
} ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">    
                                        <li>
                                        <lable><?php echo LANG_DO_YOU_DRINK ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="drink" <?php if (isset($resupdated['drink']) && $resupdated['drink'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="drink" <?php if (isset($resupdated['drink']) && $resupdated['drink'] == 2) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_SOMETIMES_SOCIAL_SITUATIONS ?></b></span></p>
                                            <p><input type="radio" value="3" name="drink" <?php if (isset($resupdated['drink']) && $resupdated['drink'] == 3) {
    echo 'checked';
} ?>/><b><?php echo YESREG ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_drink_exp = array();
if (isset($resupdated['partner_drink']) && $resupdated['partner_drink'] != '') {
    $partner_drink_exp = explode(',', $resupdated['partner_drink']);
}
?>
                                            <p><input type="radio" value="1" name="partner_drink[]" <?php if (!empty($partner_drink_exp) && in_array('1', $partner_drink_exp)) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_drink[]" <?php if (!empty($partner_drink_exp) && in_array('2', $partner_drink_exp)) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_SOMETIMES_SOCIAL_SITUATIONS ?></b></span></p>
                                            <p><input type="radio" value="3" name="partner_drink[]" <?php if (!empty($partner_drink_exp) && in_array('3', $partner_drink_exp)) {
    echo 'checked';
} ?>/><b><?php echo YESREG ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="drink_important" <?php if (isset($resupdated['drink_important']) && $resupdated['drink_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="drink_important" <?php if (isset($resupdated['drink_important']) && $resupdated['drink_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="drink_important" <?php if (isset($resupdated['drink_important']) && $resupdated['drink_important'] == 3) {
    echo 'checked';
} ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">     
                                        <li>
                                        <lable><?php echo LANG_HOW_OFTEN_DO_YOU_EXERCISE ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="exercise" <?php if (isset($resupdated['exercise']) && $resupdated['exercise'] == 1) {
    echo 'checked';
} ?>/><b><?php echo EVERYDAY ?></b></p>
                                            <p><input type="radio" value="2" name="exercise" <?php if (isset($resupdated['exercise']) && $resupdated['exercise'] == 2) {
    echo 'checked';
} ?>/><b><?php echo FEW_TIMES_WEEK ?></b></p>
                                            <p><input type="radio" value="3" name="exercise" <?php if (isset($resupdated['exercise']) && $resupdated['exercise'] == 3) {
    echo 'checked';
} ?>/><b><?php echo NOT_OFETN ?></b></p>
                                            <p><input type="radio" value="4" name="exercise" <?php if (isset($resupdated['exercise']) && $resupdated['exercise'] == 4) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_exercise_exp = array();
if (isset($resupdated['partner_exercise']) && $resupdated['partner_exercise'] != '') {
    $partner_exercise_exp = explode(',', $resupdated['partner_exercise']);
}
?>
                                            <p><input type="radio" value="1" name="partner_exercise[]" <?php if (!empty($partner_exercise_exp) && in_array('1', $partner_exercise_exp)) {
    echo 'checked';
} ?>/><b><?php echo EVERYDAY ?></b></p>
                                            <p><input type="radio" value="2" name="partner_exercise[]" <?php if (!empty($partner_exercise_exp) && in_array('2', $partner_exercise_exp)) {
    echo 'checked';
} ?>/><b><?php echo FEW_TIMES_WEEK ?></b></p>
                                            <p><input type="radio" value="3" name="partner_exercise[]" <?php if (!empty($partner_exercise_exp) && in_array('3', $partner_exercise_exp)) {
    echo 'checked';
} ?>><b><?php echo NOT_OFETN ?></b></p>
                                            <p><input type="radio" value="4" name="partner_exercise[]" <?php if (!empty($partner_exercise_exp) && in_array('4', $partner_exercise_exp)) {
    echo 'checked';
} ?>/><b><?php echo NEVER ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="exercise_important" <?php if (isset($resupdated['exercise_important']) && $resupdated['exercise_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="exercise_important" <?php if (isset($resupdated['exercise_important']) && $resupdated['exercise_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="exercise_important" <?php if (isset($resupdated['exercise_important']) && $resupdated['exercise_important'] == 3) {
    echo 'checked';
} ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_WHEN_BILL_ARRIVES ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="bill_pay" <?php if (isset($resupdated['bill_pay']) && $resupdated['bill_pay'] == 1) {
    echo 'checked';
} ?>/><b><?php echo LANG_I_PAY ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="bill_pay" <?php if (isset($resupdated['bill_pay']) && $resupdated['bill_pay'] == 2) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_OTHER_PERSON_SHOULD_PAY ?></b></span></p>
                                            <p><span style="float:left;"><input type="radio" value="3" name="bill_pay" <?php if (isset($resupdated['bill_pay']) && $resupdated['bill_pay'] == 3) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_WE_WILL_SPLIT_BILL ?></b></span></p>
                                            <p><input type="radio" value="4" name="bill_pay" <?php if (isset($resupdated['bill_pay']) && $resupdated['bill_pay'] == 4) {
    echo 'checked';
} ?>/><b><?php echo LANG_DEPENDS_SITUATION ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_bill_pay_exp = array();
if (isset($resupdated['partner_bill_pay']) && $resupdated['partner_bill_pay'] != '') {
    $partner_bill_pay_exp = explode(',', $resupdated['partner_bill_pay']);
}
?>
                                            <p><input type="radio" value="1" name="partner_bill_pay[]" <?php if (!empty($partner_bill_pay_exp) && in_array('1', $partner_bill_pay_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_I_PAY ?></b></p>
                                            <p><span style="float:left;"><input type="radio" value="2" name="partner_bill_pay[]" <?php if (!empty($partner_bill_pay_exp) && in_array('2', $partner_bill_pay_exp)) {
    echo 'checked';
} ?>/></span><span style="float:left;width:158px"><b><?php echo LANG_OTHER_PERSON_SHOULD_PAY ?></b></span></p>
                                            <p><input type="radio" value="3" name="partner_bill_pay[]" <?php if (!empty($partner_bill_pay_exp) && in_array('3', $partner_bill_pay_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_WE_WILL_SPLIT_BILL ?></b></p>
                                            <p><input type="radio" value="4" name="partner_bill_pay[]" <?php if (!empty($partner_bill_pay_exp) && in_array('4', $partner_bill_pay_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_DEPENDS_SITUATION ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="bill_pay_important" <?php if (isset($resupdated['bill_pay_important']) && $resupdated['bill_pay_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="bill_pay_important" <?php if (isset($resupdated['bill_pay_important']) && $resupdated['bill_pay_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="bill_pay_important" <?php if (isset($resupdated['bill_pay_important']) && $resupdated['bill_pay_important'] == 3) {
    echo 'checked';
} ?>><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>    
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable><?php echo LANG_WHAT_YOU_WANT_MOST_FROM_YOUR_RELATIONSHIP ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="want_from_relationship" <?php if (isset($resupdated['want_from_relationship']) && $resupdated['want_from_relationship'] == 1) {
    echo 'checked';
} ?>/><b><?php echo LOVE ?></b></p>
                                            <p><input type="radio" value="2" name="want_from_relationship" <?php if (isset($resupdated['want_from_relationship']) && $resupdated['want_from_relationship'] == 2) {
    echo 'checked';
} ?>/><b><?php echo SEX ?></b></p>
                                            <p><input type="radio" value="3" name="want_from_relationship" <?php if (isset($resupdated['want_from_relationship']) && $resupdated['want_from_relationship'] == 3) {
    echo 'checked';
} ?>/><b><?php echo LANG_FINANCIAL_HELP ?></b></p>
                                            <p><input type="radio" value="4" name="want_from_relationship" <?php if (isset($resupdated['want_from_relationship']) && $resupdated['want_from_relationship'] == 4) {
    echo 'checked';
} ?>/><b><?php echo LANG_HUMOR ?></b></p>
                                            <p><input type="radio" value="5" name="want_from_relationship" <?php if (isset($resupdated['want_from_relationship']) && $resupdated['want_from_relationship'] == 5) {
    echo 'checked';
} ?>/><b><?php echo LANG_FRIENDSHIP ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo MYPARTNERS_SELECT ?></lable>
                                        <div class="input auto_height">
<?php
$partner_want_from_relationship_exp = array();
if (isset($resupdated['partner_want_from_relationship']) && $resupdated['partner_want_from_relationship'] != '') {
    $partner_want_from_relationship_exp = explode(',', $resupdated['partner_want_from_relationship']);
}
?>
                                            <p><input type="radio" value="1" name="partner_want_from_relationship[]" <?php if (!empty($partner_want_from_relationship_exp) && in_array('1', $partner_want_from_relationship_exp)) {
    echo 'checked';
} ?>/><b><?php echo LOVE ?></b></p>
                                            <p><input type="radio" value="2" name="partner_want_from_relationship[]" <?php if (!empty($partner_want_from_relationship_exp) && in_array('2', $partner_want_from_relationship_exp)) {
    echo 'checked';
} ?>/><b><?php echo SEX ?></b></p>
                                            <p><input type="radio" value="3" name="partner_want_from_relationship[]" <?php if (!empty($partner_want_from_relationship_exp) && in_array('3', $partner_want_from_relationship_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_FINANCIAL_HELP ?></b></p>
                                            <p><input type="radio" value="4" name="partner_want_from_relationship[]" <?php if (!empty($partner_want_from_relationship_exp) && in_array('4', $partner_want_from_relationship_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_HUMOR ?></b></p>
                                            <p><input type="radio" value="5" name="partner_want_from_relationship[]" <?php if (!empty($partner_want_from_relationship_exp) && in_array('5', $partner_want_from_relationship_exp)) {
    echo 'checked';
} ?>/><b><?php echo LANG_FRIENDSHIP ?></b></p>
                                        </div>
                                        </li>
                                        <li>
                                        <lable><?php echo HOWIMPORTANT ?></lable>
                                        <div class="input auto_height">
                                            <p><input type="radio" value="1" name="want_from_relationship_important" <?php if (isset($resupdated['want_from_relationship_important']) && $resupdated['want_from_relationship_important'] == 1) {
    echo 'checked';
} ?>/><b><?php echo NOTIMPORTANT ?></b></p>
                                            <p><input type="radio" value="2" name="want_from_relationship_important" <?php if (isset($resupdated['want_from_relationship_important']) && $resupdated['want_from_relationship_important'] == 2) {
    echo 'checked';
} ?>/><b><?php echo MODIMPORTANT ?></b></p>
                                            <p><input type="radio" value="3" name="want_from_relationship_important" <?php if (isset($resupdated['want_from_relationship_important']) && $resupdated['want_from_relationship_important'] == 3) {
    echo 'checked';
} ?>/><b><?php echo VERYIMPORTANT ?></b></p>
                                        </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <ul class="search_field">
                                        <li>
                                        <lable>&nbsp;</lable>
                                        <div class="input">
                                            <input type="submit" value="<?php echo SUBMIT; ?>" class="btn_sub" name="sub"/>
                                        </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
<?php include('includes/footer.php'); ?>
            </div>
        </div>
    <style>
        .input p{height:20px !important; padding: 14px 0;}
        .input p input[type="checkbox"]{margin-top:0px}
        .input p input[type="radio"]{margin-right:5px}
        .input p b{line-height:17px;margin-left:0px}
        .search_field li lable{height:auto;width:200px;margin-right:20px;line-height:19px;margin-top: 10px;}
        .search_field{width: 827px;}
        .search_field .input{width:580px;}
        .search_field .input p{width: 170px;}
    </style>
    <script>
        $(document).ready(function () {
            //When page loads...
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content

            //On Click Event
            $("ul.tabs li").click(function () {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });
        });
    </script>
    <style>
        .search_field{border-bottom: 1px dashed #999;}
    </style>
</body>
</html>
