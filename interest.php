<?php
ob_start();
session_start();
include('administrator/includes/config.php');
include('class.phpmailer.php');
include('includes/language.php');
if($_SESSION['user_id']=='')
{
header('location:index.php');
exit;
}

if($_POST['sub'])
{
  $type = isset($_POST['type']) ? $_POST['type'] : '';
  $title = isset($_POST['title']) ? $_POST['title'] : '';
  $link = isset($_POST['link']) ? $_POST['link'] : '';
  $user_id = $_SESSION['user_id'];
  mysql_query('INSERT into dating_interest(user_id,type,title,link) values ('.$user_id.','.$type.',"'.$title.'","'.$link.'")');

 header("Location:interest.php?msg=add");	
}
																			
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><?php echo LANG_ADD_INTEREST; ?></title>
<meta name="" content="">
<link rel="stylesheet" href="css/style.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<style type="text/css">
	.success,.message, .cake-error, p.error, .error-message {
    clear: both;
    color: #FFF;
    background: -moz-linear-gradient(center top , #EE5F5B, #C43C35) repeat-x scroll 0% 0% #C43C35;
    text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}

.success, .message, .cake-error, .cake-debug, .notice, p.error, .error-message {
 background: -moz-linear-gradient(center top , #ffcc00, #e6b800) repeat-x scroll 0 0 #ffcc00;
    border-radius: 4px;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.25) inset;
    color: #404040;
    margin-bottom: 18px;
    padding: 7px 14px;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
}
.message {
    clear: both;
    color: #fff;
    font-size: 100%;
    font-weight: bold;
    margin: 0 0 1em;
    padding: 3px;	
	 background: #009933;
}

</style>

<script type="text/javascript">
$(document).ready(function(){       
		setTimeout(function() {
			$('.message').fadeOut('slow');
		}, 6000);
	});
	$(document).ready(function(){       
		setTimeout(function() {
			$('.success').fadeOut('slow');
		}, 6000);
	});
</script>
</head>
<body style="background: url(images/bg-main.jpg) center top no-repeat fixed; margin: 0 0 15px 0;">



	<?php include('includes/header.php');?>

	<div class="container">
	<?php if (isset($_GET['msg']) && $_GET['msg']=='add') { ?>

<div style="text-align:center;">
    <div id="flashMessage" class="message">
      <?php echo LANG_INTERESTADDMESS;?>
    </div>
</div>

<?php } ?>

		<div class="profile_body">
			<?php include('includes/left-panel.php');?>
			<div class="right_menu">
				<h2><?php echo LANG_ADD_INTEREST; ?></h2>
				<div class="tab_based search_page">
					<div class="tab_container">
					    <div id="tab1" class="tab_content">
                                            <form action="interest.php" method="POST">
					       <ul class="search_field">
						   <li>
						   	<lable><?php echo LANG_INTEREST_TYPE; ?></lable>
						   			<div class="input" style="height:auto">
										<div class="list_box list_5">
										  <select name="type" id="type">
										    <option value=""><?php echo SELECT ?></option>
										    <option value="1"><?php echo LANG_MUSIC; ?></option>
										    <option value="2"><?php echo LANG_MOVIE; ?></option>
										    <option value="3"><?php echo LANG_TV_SHOWS; ?></option>
										    <option value="4"><?php echo LANG_BOOK; ?></option>
										   </select>
										</div>
										<div style="clear:both"></div>
										<span id="type_err"><span>
									</div>
						   		</li>
								<li>
						   		<lable><?php echo LANG_TITLE; ?></lable>
						   		<div class="input" style="height:auto">
									<input class="input_text" type="text" name="title" id="title">
									<br/>
								        <span id="title_err"><span>
								</div>
						   		</li>
						   		<li>
						   		<lable><?php echo LANG_LINK; ?></lable>
						   		<div class="input" style="height:auto">
									<input class="input_text" type="text" name="link" id="link">
									<br/>
									<span style="color:#676767;font-size:14px">(<?php echo LANG_LINK_INFO; ?>)</span>
								</div>
						   		</li>
						   		<li>
						   			<lable></lable>
						   			<div class="input">
						   				<input type="submit" value="<?php echo LANG_ADD; ?>" name="sub" class="btn_sub" onclick="return validateinterest()"/>
						   			</div>
						   		</li>
						   </ul>
                                               </form>
					    </div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php include('includes/footer.php');?>
		</div>
	</div>
	<style>
           .input p{height:auto !important;}
	</style>
	<script>
	        function validateinterest()
	        {
	          var type=$('#type').val();
	          var title=$('#title').val();
	          if(type=='')
	          {
	            $('#type_err').html('<font color="red"><?php echo LANG_INTERESTALERT;?></font>');
	          }
	          else
	          {
	            $('#type_err').html('');
	          }
	          if(title=='')
	          {
	            $('#title_err').html('<font color="red"><?php echo LANG_TITLEALERT;?></font>');
	          }
	          else
	          {
	            $('#title_err').html('');
	          }
	          if(type!='' && title!='')
	          {
	            return true;
	          }
	          else
	          {
	            return false;
	          }
	        }
	        
		$(document).ready(function() {
		 //When page loads...
		 $(".tab_content").hide(); //Hide all content
		 $("ul.tabs li:first").addClass("active").show(); //Activate first tab
		 $(".tab_content:first").show(); //Show first tab content
		 
		 //On Click Event
		 $("ul.tabs li").click(function() {
		 
		  $("ul.tabs li").removeClass("active"); //Remove any "active" class
		  $(this).addClass("active"); //Add "active" class to selected tab
		  $(".tab_content").hide(); //Hide all tab content
		 
		  var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		  $(activeTab).fadeIn(); //Fade in the active ID content
		  return false;
		 });
		});
	</script>
</body>
</html>

























































