<!DOCTYPE html>
<!--[if lt IE 7]>

<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>

<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>

<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="js csstransitions skrollr skrollr-desktop" dir="ltr" lang="en">
  <!--
<![endif]-->
              <head>
                <meta http-equiv="content-type" content="text/html; charset=UTF-8">
                <title>
                  Welcome
                </title>
                <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes">
                <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:700' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="css/bootstrap.css">
                <link rel="stylesheet" href="css/style2.css">
                <link rel="stylesheet" href="css/style.css">
				<!--<link rel="stylesheet" href="images/responsive.css">-->
				<link rel="stylesheet" href="css/overlay.css">
				<!-- <link rel="stylesheet" href="css/pixeden-icons.css">-->
				<!--<link rel="stylesheet" href="images/styles.css">-->
				<!-- <link rel="stylesheet" href="images/jquery.css">-->
				<!-- <link rel="stylesheet" href="images/animate.css">-->
				<!--[if lt IE 9]>
				<script src="/js/html5shiv.js">
				</script>
				<script src="/js/respond.min.js">
				</script>
				<![endif]-->
              </head>
              <body data-twttr-rendered="true" class="loaded" itemscope="" itemtype="http://schema.org/Organization">
              	<div class="over_lay">
              		<div class="sign_up_box">
              			<h2>Sign in</h2>
              			<div class="left_form">
              				<ul>
							  	<li class="left_form_text">Email Address:</li>
							  	<li><input type="email" class="left_form_text_box" placeholder="Email Address"/></li>
							  	<li class="left_form_text">Password:</li>
							  	<li><input type="password" class="left_form_text_box" placeholder="Password"/></li>
							  	<li><a href="">Forgot password?</a></li>
							  	<li><input type="button" value="Sign In" class="sign_in_btn"/></li>
							</ul>
              			</div>
              			<div class="or">
              				<p>OR</p>          				
              			</div>
              			<div class="social_icon_hold">
              				<h3>Sign-in using</h3>
              				<ul>
							  	<li>
							  		<a href=""><img src="images/fb_sign.png" alt="" /></a>
							  		<span>Facebook</span>
							  	</li>
							  	<li>
								  	<a href=""><img src="images/tweet_sign.png" alt="" /></a>
								  	<span>Twitter</span>
							  	</li>
							  	<li>
								  	<a href=""><img src="images/gplus_sign.png" alt="" /></a>
								  	<span>Google</span>
							  	</li>
							</ul>
              			</div>
              		</div>
              	</div>
              	
                <nav class="mm-menu mm-horizontal mm-light mm-ismenu" id="menu">
                  <ul id="mm-m0-p0" class="mm-list mm-panel mm-opened mm-current">
                    <li>
                      <a href="#slide-1" title="Home">
                        Home
                      </a>
                    </li>
                    <li>
                      <a href="#slide-2" title="How it works">
                        How it works
                      </a>
                    </li>
                    <li>
                      <a href="#slide-3" title="Apps">
                        Apps
                      </a>
                    </li>
                    <li>
                      <a href="#slide-4" title="Tell a story">
                        Tell a story
                      </a>
                    </li>
                    <li>
                      <a href="#slide-5" title="About us">
                        About us
                      </a>
                    </li>
                    <li>
                      <a href="#slide-6" title="Contact">
                        contact
                      </a>
                    </li>
                  </ul>
                </nav>
                <!--[if lt IE 7]>
<p class="chromeframe">
You are using an 
<strong>
outdated
</strong>
browser. Please 
<a href="http://browsehappy.com/">
upgrade your browser
</a>
or 
<a href="http://www.google.com/chromeframe/?redirect=true">
activate Google Chrome Frame
</a>
to improve your experience.
</p>
<![endif]-->
                  <div class="mm-page">
                    <div id="preload">
                      <img src="images/bg1.jpg" alt="home!">
                      <img src="images/bg2.jpg" alt="Apps">
                      <img src="images/bg3.jpg" alt="About us">
                      <img src="images/bg4.jpg" alt="Contact">
                    </div>
                    <div id="skrollr-body">
                      <header id="home" class="header">
                        <div id="main-nav" class="navbar navbar-inverse bs-docs-nav menu hidden-xs hidden-sm" role="banner">
                          <nav class="navbar-collapse bs-navbar-collapse collapse" role="navigation">
                            <ul class="nav navbar-nav navbar-right responsive-nav main-nav-list">
                              <li class="menuList current">
                                <a href="#slide-1" title="Home">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    Home
                                  </span>
                                </a>
                              </li>
                              <li class="menuList">
                                <a href="#slide-2" title="How it works">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    How it works
                                  </span>
                                </a>
                              </li>
                              <li class="menuList">
                                <a href="#slide-3" title="Apps">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    Apps
                                  </span>
                                </a>
                              </li>
                              <li class="menuList">
                                <a href="#slide-4" title="Tell a story">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    Tell a story
                                  </span>
                                </a>
                              </li>
                              <li class="menuList">
                                <a href="#slide-5" title="About us">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    About us
                                  </span>
                                </a>
                              </li>
                              <li class="menuList">
                                <a href="#slide-6" title="Contact">
                                  <span class="menuCercle">
                                  </span>
                                  <span class="menuTitle">
                                    Contact
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </nav>
                        </div>
                      </header>
                      <section id="slide-1" class="homeSlide">
                        <div style="background-position: 50% -21.5152px;" class="bcg skrollable skrollable-between" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-bottom-top="background-position: 50% -100px;" data-anchor-target="#slide-1">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-1">
                              <div class="container">
                                <div class="menuCont">
                                  <div class="navbar-header responsive-logo">
                                    <a itemprop="url" href="http://cronnection.com/en/" class="navbar-brand left" title="Cronnection - a global collaborative consumption marketplace">
                                      <img src="images/logo.png" alt="Miamor white logo" class="logo">
                                    </a>
                                    <div class="right linksTop hidden-xs hidden-sm">
                                     <div class="navigation">
	                                	<a href=""><img src="images/tweet.png" alt="" /></a>
	                                	<a href=""><img src="images/fb.png" alt="" /></a>
		                                <a href="">Blog</a>
		                                <a href="">ES</a>
		                                <a href="">EN</a>
		                                <a href="">Login</a>
	                                </div>       
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="interiorSeccion container bannertext">
                                <h1>Find partners for fun, dating and long term relationships</h1>
                                <h2>Find partners for fun, dating and long term relationships</h2>
                                <input type="button" value="Sign up here for FREE"/>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section id="slide-2" class="homeSlide">
                        <div style="background-position: 50% 89.2586px;" class="bcg skrollable skrollable-between" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-bottom-top="background-position: 50% 100px;" data-anchor-target="#slide-2">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-2">
                              <div class="interiorSeccion how_it_works">
                              <h2> How it works</h2>
                               <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>
                               <ul>
							   		<li>
								   		<img src="images/create_profile.png" alt="" />
								   		<h2>Create profile</h2>
								   		<p>
								   			Lorem ipsum dolor sit amet, consectetur adipiscing elitsed do eiusmod tempor incididunt ut laboret... 

								   		</p>
							   		</li>
							   		<li><img src="images/create_sec.png" alt="" />
							   			<h2>Create selections</h2>
								   		<p>
								   			Lorem ipsum dolor sit amet, consectetur adipiscing elitsed do eiusmod tempor incididunt ut laboret... 

								   		</p>
							   		</li>
							   		<li><img src="images/contact_section.png" alt="" />
							   			<h2>Contact your selections</h2>
								   		<p>
								   			Lorem ipsum dolor sit amet, consectetur adipiscing elitsed do eiusmod tempor incididunt ut laboret... 

								   		</p>
							   		</li>
							   </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section id="slide-3" class="homeSlide">
                        <div style="background-position: 50% 0px;" class="bcg skrollable skrollable-before" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% 0px;" data-bottom-top="background-position: 50% 0px;" data-anchor-target="#slide-3">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-3">
                              <div class="interiorSeccion apps">
                                <img src="images/app.png" width="961" height="537"  alt=""/>
                                <aside>
                                	<h1>Find perfect match from miamor apps</h1>
                                	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
                                	<ul>
										<li><img src="images/google_play.png" alt="" /></li>
										<li><img src="images/appstore.png" alt="" /></li>
									</ul>
                                </aside>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section id="slide-4" class="homeSlide">
                        <div style="background-position: 50% 100px;" class="bcg skrollable skrollable-before" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -100px;" data-bottom-top="background-position: 50% 100px;" data-anchor-target="#slide-4">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-4">
                              <div class="interiorSeccion story">
                                <h2>Tell us your Dating story</h2>
                                <ul>
									<li>
									<img src="images/tel_story.png" alt="" />
									<aside>
										<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo”</p>
										<b>User Name</b>
										<span>Location of user</span>
										<div class="arrow" style="left:-14px"><img src="images/left_arrow.png" alt="" /></div>
									</aside>
									</li>
									<li>
									<aside style="float:left;">
										<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo”</p>
										<b>User Name</b>
										<span>Location of user</span>
										<div class="arrow" style="right:-14px"><img src="images/right_arrow.png" alt="" /></div>
									</aside>
									<img src="images/tel_story1.png" alt="" style="float:right"/>
									</li>
								</ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="overlay overlay-hugeinc overlay-blue boxTerms">
                        <button type="button" class="overlay-close">
                          Close
                        </button>
                      </div>
                      <section id="slide-5" class="homeSlide">
                        <div style="background-position: 50% 300px;" class="bcg skrollable skrollable-before" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -1px;" data-bottom-top="background-position: 50% 300px;" data-anchor-target="#slide-5">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-5">
                              <div class="interiorSeccion about">
                                <img src="images/about_pic.png" width="1145" height="461"  alt=""/>
                                <aside>
                                	<h2>About miamor.com</h2>
                                	<p>
                                	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </aside>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <section id="slide-6" class="homeSlide">
                        <div style="background-position: 50% 300px;" class="bcg skrollable skrollable-before" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -1px;" data-bottom-top="background-position: 50% 300px;" data-anchor-target="#slide-6">
                          <div class="hsContainer">
                            <div class="hsContent" data-anchor-target="#slide-6">
                              <div class="interiorSeccion home_footer">
                               <h1>	Get in Touch</h1>
                               	<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore. </h4>
                               	<div class="footer_search">
                               		<input type="text" value="Enter your email address"/><input type="button" value="SUBSCRIBE"/>
                               	</div>
                                <h3>Follow us on</h3>
                                <ul class="share_btn">
									<li><img src="images/share1.png" alt="" /></li>
									<li><img src="images/share2.png" alt="" /></li>
								</ul>
								<img src="images/logo.png" alt="" class="footer_logo"/>
								<ul class="footer_nav">
									<li><a href="">Home</a></li>
									<li><a href="">How it works</a></li>
									<li><a href="">About us</a></li>
									<li><a href="">The app</a></li>
									<li><a href="">Tell us</a></li>
									<li><a href="">Blog</a></li>
								</ul>
								<div class="clearfix"></div>
								<p>© 2014 miamor, Inc. Terms  Privacy policy</p>
                              </div>
                              <footer>
                                <div class="container footer">
                                  <div class="col-md-12 company-details">
                                    <div class="left">
                                      <p>
                                        <span itemprop="name">
                                        </span>
                                        <a href="http://blog.cronnection.com/" title="Cronnection Blog, a journal about collaborative consumption">
                                        </a>
                                        <a href="#" class="triggerModal" title="Terms and conditions" id="trigger-overlay">
                                          
                                        </a>
                                        <a href="mailto:hola@cronnection.com" itemprop="email" title="Send us an e-mail">
                                        </a>
                                      </p>
                                      <div class="right linksTop">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </footer>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                  <script src="js/modernizr.js">
                  </script>
                  <script type="text/javascript" src="js/jquery_002.js">
                  </script>
                  <script type="text/javascript" src="js/jquery_003.js">
                  </script>
                  <script type="text/javascript">
                    $(function() {
                      $('nav#menu').mmenu({
                        classes: 'mm-light'
                      }
                                         );
                    }
                     );
                  </script>
                  <!--<script src="images/bootstrap.js">
                  </script>-->
                  <!--<script src="images/classie.js">
                  </script>-->
                  <script type="text/javascript" src="js/main.js">
                  </script>
                  <script>
                    $(document).ready(function(){
                      $('.triggerModal').click(function(event){
                        event.preventDefault();
                      }
                                              );
                    }
                                     );
                    
                  </script>
                 <!-- <script src="images/iconic.js">
                  </script>-->
                  <!--<script src="images/wow.js">
                  </script>-->
                  <script src="js/zerif.js">
                  </script>
                  <script src="js/jquery_004.js">
                  </script>
                  
                  <!--<script src="images/smoothscroll.js">
                  </script>-->
                  <script src="js/jquery.js">
                  </script>
              </body>
</html>