<?php
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
echo $_SESSION['lang'];


if ($_POST['submit']) {
    $_SESSION['NOTIFY_MESSAGE'] = "Updated Successfully";
    
    $fname = isset($_POST['fname']) ? $_POST['fname'] : '';
    $lname = isset($_POST['lname']) ? $_POST['lname'] : '';
    $dpname = isset($_POST['dpname']) ? $_POST['dpname'] : '';
    if (!empty($_POST['year']) && !empty($_POST['month']) && !empty($_POST['date'])) {
        $dob = $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['date'];
    } else {
        $dob = '';
    }

    $height = isset($_POST['height']) ? $_POST['height'] : '';
    $sex = isset($_POST['sex']) ? $_POST['sex'] : '';
    $interest = isset($_POST['interest']) ? $_POST['interest'] : '';
    $relation = isset($_POST['relation']) ? $_POST['relation'] : '';
    $child = isset($_POST['child']) ? $_POST['child'] : '';
    $ethnicity = isset($_POST['ethnicity']) ? $_POST['ethnicity'] : '';
    $education = isset($_POST['education']) ? $_POST['education'] : '';
    $religion = isset($_POST['religion']) ? $_POST['religion'] : '';
    $smoking = isset($_POST['smoking']) ? $_POST['smoking'] : '';
    $drinking = isset($_POST['drinking']) ? $_POST['drinking'] : '';
    $bodytype = isset($_POST['bodytype']) ? $_POST['bodytype'] : '';
    $personality = isset($_POST['personality']) ? $_POST['personality'] : '';
    $look = isset($_POST['look']) ? $_POST['look'] : '';
    $about_me = isset($_POST['about_me']) ? $_POST['about_me'] : '';
    $me_doing = isset($_POST['me_doing']) ? $_POST['me_doing'] : '';
    $good_at = isset($_POST['good_at']) ? $_POST['good_at'] : '';
    $city = isset($_POST['city']) ? $_POST['city'] : '';
    $notice_about_me = isset($_POST['notice_about_me']) ? $_POST['notice_about_me'] : '';
    $perfect_match = isset($_POST['perfect_match']) ? $_POST['perfect_match'] : '';
    $ideal_date = isset($_POST['ideal_date']) ? $_POST['ideal_date'] : '';
    $facebook_link = isset($_POST['facebook_link']) ? $_POST['facebook_link'] : '';
    $twitter_link = isset($_POST['twitter_link']) ? $_POST['twitter_link'] : '';
    $dont_have_fbtwt = isset($_POST['dont_have_fbtwt']) ? $_POST['dont_have_fbtwt'] : '';
    if ($dont_have_fbtwt == 1) {

        $admin_verify = "1";
        $status = "1";

        //$_SESSION['msg'] = "Admin will approve there profile photos and video within 48 hrs,";
    } else {
        $admin_verify = "1";
        $status = "1";
    }

    if($sex=='M')
    {

        $gen="male";

    }
    else
    {

        $gen="female";

    }

    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';

    $creditcard_no = isset($_POST['creditcard_no']) ? $_POST['creditcard_no'] : '';
    $expire_month = isset($_POST['expire_month']) ? $_POST['expire_month'] : '';
    $expire_year = isset($_POST['expire_year']) ? $_POST['expire_year'] : '';
    $ccv = isset($_POST['ccv']) ? $_POST['ccv'] : '';

    $fields = array(
        'fname' => mysql_real_escape_string($fname),
        'lname' => mysql_real_escape_string($lname),
        'dpname' => mysql_real_escape_string($dpname),
        'height' => mysql_real_escape_string($height),
        'gender' => mysql_real_escape_string($sex),
        'sex' => mysql_real_escape_string($gen),
        'relation' => mysql_real_escape_string($relation),
        'child' => mysql_real_escape_string($child),
        'ethnicity' => mysql_real_escape_string($ethnicity),
        'education' => mysql_real_escape_string($education),
        'religion' => mysql_real_escape_string($religion),
        'smoking' => mysql_real_escape_string($smoking),
        'drinking' => mysql_real_escape_string($drinking),
        'bodytype' => mysql_real_escape_string($bodytype),
        'personality' => mysql_real_escape_string($personality),
        'about_me' => mysql_real_escape_string($about_me),
        'me_doing' => mysql_real_escape_string($me_doing),
        'good_at' => mysql_real_escape_string($good_at),
        'notice_about_me' => mysql_real_escape_string($notice_about_me),
        'lookfor' => mysql_real_escape_string($look),
        'perfect_match' => mysql_real_escape_string($perfect_match),
        'ideal_date' => mysql_real_escape_string($ideal_date),
        'facebook_link' => mysql_real_escape_string($facebook_link),
        'twitter_link' => mysql_real_escape_string($twitter_link),
        'dont_have_fbtwt' => mysql_real_escape_string($dont_have_fbtwt),
        'status' => mysql_real_escape_string($status),
        'admin_verify' => mysql_real_escape_string($admin_verify),
        'creditcard_no' => mysql_real_escape_string($creditcard_no),
        'expire_month' => mysql_real_escape_string($expire_month),
        'expire_year' => mysql_real_escape_string($expire_year),
        'ccv' => mysql_real_escape_string($ccv),
        'city' => mysql_real_escape_string($city),
        'phone' => mysql_real_escape_string($phone),
        'user_type' => mysql_real_escape_string($interest)
    );

    if (!empty($dob)) {
        $fields['dob'] = $dob;
    }


    $fieldsList = array();
    foreach ($fields as $field => $value) {
        $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
    }

    $editQuery = "UPDATE `dateing_user` SET " . implode(', ', $fieldsList)
            . " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";

    if (mysql_query($editQuery)) {
        foreach ($_FILES['image']['tmp_name'] as $key => $val) {
            if ($_FILES['image']['tmp_name'] != '') {
                $target_path = "upload/car/";
                $categoryRowsetfile_name = $_FILES['image']['name'][$key];
                $categoryRowsetfile_tmp = $_FILES['image']['tmp_name'][$key];
                $img_name = time() . $categoryRowsetfile_name;
                $img = $target_path . $img_name;
                move_uploaded_file($categoryRowsetfile_tmp, $img);
            } else {
                $img_name = $_REQUEST['hidimage'];
            }
            $image = mysql_query("UPDATE `dateing_user` SET `image`='" . $img_name . "' WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'");
        }

        $_SESSION['NOTIFY_MESSAGE'] = "Category Updated Successfully";
    } else {
        $_SESSION['NOTIFY_MESSAGE'] = "Error occuried while updating Category";
    }
    header('Location:add_user.php?id=' . $_REQUEST['id'] . '&action=edit');
    exit();
}
if ($_REQUEST['action'] == 'edit') {

    $categoryRowset = mysql_fetch_array(mysql_query("select * from `dateing_user` where `id`='" . $_REQUEST['id'] . "'"));
    $dobselect = explode('-', $categoryRowset['dob']);
    $preyear = $dobselect[0];
    $premonth = $dobselect[1];
    $predate = $dobselect[2];
    
    //$_SESSION['NOTIFY_MESSAGE'] = "Updated Successfully";
    
}
?>
<!DOCTYPE html>
<html>

    <head>
        <title>Edit User</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <style type="text/css">
            .input-xlarge { width:380px; }
        </style>
    </head>

    <body>
        <?php include('includes/header.php'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php include('includes/left_panel.php'); ?>
                <!--/span-->
                <div class="span9" id="content">
                    <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?>  User </div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                        <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />
                                        <input type="hidden" name="action" value="<?php echo $_REQUEST['action']; ?>" />
                                        <fieldset>
                                            <legend><?php echo $_REQUEST['action'] == 'edit' ? "Edit" : "Add"; ?> User</legend>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Name</label>
                                                <div class="controls">
                                                    <input type="text" name="fname" class="input-xlarge focused" placeholder="Your First Name" value="<?php echo $categoryRowset['fname'] ?>" required/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Display Name</label>
                                                <div class="controls">
                                                    <input type="text" name="dpname" id="focusedInput" class="input-xlarge focused" placeholder="Display Name" value="<?php echo $categoryRowset['dpname'] ?>" required/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Phone</label>
                                                <div class="controls">
                                                    <input type="text" name="phone" class="input-xlarge focused" placeholder="Tele Phone Number" value="<?php echo $categoryRowset['phone']; ?>" required/>

                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">City</label>
                                                <div class="controls">
                                                    <input type="text" name="city" class="input-xlarge focused" placeholder="Your City" value="<?php echo $categoryRowset['city']; ?>" required/>

                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Birthday</label>
                                                <div class="controls">
                                                    <select name="date" required <?php echo (isset($categoryRowset['dob']) && $categoryRowset['dob'] == '0000-00-00') ? '' : ''; ?>>
                                                        <option value="">Day</option>
                                                        <?php
                                                        //$date=date("Y");
                                                        for ($i = 1; $i <= 31; $i++) {
                                                            ?>
                                                            <option value="<?php echo $i ?>" <?php if ($predate == $i) { ?> selected <?php } ?>><?php echo $i ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                    <select name="month" <?php echo (isset($categoryRowset['dob']) && $categoryRowset['dob'] == '0000-00-00') ? '' : ''; ?>>
                                                        <option value="">Month</option>
                                                        <?php
                                                        //$date=date("Y");
                                                        for ($i = 1; $i <= 12; $i++) {
                                                            ?>
                                                            <option value="<?php echo $i ?>" <?php if ($premonth == $i) { ?> selected <?php } ?>><?php echo $i ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                    <select name="year" required <?php echo (isset($categoryRowset['dob']) && $categoryRowset['dob'] == '0000-00-00') ? '' : ''; ?>>
                                                        <option value="">Year</option>
                                                        <?php
                                                        $date = date("Y");
                                                        for ($i = 1950; $i <= $date; $i++) {
                                                            ?>
                                                            <option value="<?php echo $i ?>" <?php if ($preyear == $i) { ?> selected <?php } ?>><?php echo $i ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Height</label>
                                                <div class="controls">
                                                    <select name="height" required>
                                                        <?php
                                                        for ($i = 150; $i <= 200; $i++) {
                                                            ?>
                                                            <option value="<?php echo $i ?>" <?php if ($categoryRowset['height'] == $i) { ?> selected <?php } ?>><?php echo $i ?>&nbsp;cm</option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                            </div>



                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Sex</label>
                                                <div class="controls">
                                                    <select name="sex" required <?php echo (!empty($categoryRowset['gender']) ? '' : ''); ?> onchange="gender_check(this)">
                                                        <option value="M" <?php if ($categoryRowset['gender'] == "M") { ?> selected <?php } ?>><?php echo MALE ?></option>
                                                        <option value="F" <?php if ($categoryRowset['gender'] == "F") { ?> selected <?php } ?>><?php echo FEMALE ?></option>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Interested In</label>
                                                <div class="controls">
                                                    <select name="interest" required id="male_female">
                                                        <?php if (!empty($categoryRowset['gender']) && $categoryRowset['gender'] == 'M') {
                                                            ?>
                                                            <option value="Man Interested In Women" <?php if ($categoryRowset['user_type'] == 'Man Interested In Women') { ?> selected <?php } ?> ><?php echo LANG_MAN_LOOKING_FOR_WOMEN ?></option>
                                                            <option value="Man Interested In Men" <?php if ($categoryRowset['user_type'] == 'Man Interested In Men') { ?> selected <?php } ?> ><?php echo LANG_MAN_LOOKING_FOR_MAN ?></option>

                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="Woman Interested In Men" <?php if ($categoryRowset['user_type'] == 'Woman Interested In Men') { ?> selected <?php } ?> ><?php echo LANG_WOMEN_LOOKING_FOR_MAN ?></option>

                                                            <option value="Woman Interested In Women" <?php if ($categoryRowset['user_type'] == 'Woman Interested In Women') { ?> selected <?php } ?> ><?php echo LANG_WOMEN_LOOKING_FOR_WOMEN ?></option>
                                                        <?php } ?>
                                                        <option value="Interested In Men and Women" <?php if ($categoryRowset['user_type'] == 'Interested In Men and Women') { ?> selected <?php } ?>><?php echo LANG_INTERESTED_IN_MEN_WOMEN ?></option>

                                                    </select>

                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Relation</label>
                                                <div class="controls">
                                                    <select name="relation" required>
                                                        <option value="single" <?php if ($categoryRowset['relation'] == "single") { ?> selected <?php } ?>><?php echo SINGLE ?></option>
                                                        <option value="married" <?php if ($categoryRowset['relation'] == "married") { ?> selected <?php } ?>><?php echo MARRIED ?></option>
                                                        <option value="tell you later" <?php if ($categoryRowset['relation'] == "tell you later") { ?> selected <?php } ?>><?php echo TELL_YOU ?></option>
                                                        <option value="separeted" <?php if ($categoryRowset['relation'] == "separeted") { ?> selected <?php } ?>><?php echo LANG_SEPARATED; ?></option>
                                                        <option value="divorced" <?php if ($categoryRowset['relation'] == "divorced") { ?> selected <?php } ?>><?php echo DIVORCED ?></option>
                                                        <option value="widowed" <?php if ($categoryRowset['relation'] == "widowed") { ?> selected <?php } ?>><?php echo WIDOWED ?></option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Children</label>
                                                <div class="controls">
                                                    <select name="child" required>
                                                        <option value="no" <?php if ($categoryRowset['child'] == "no") { ?> selected <?php } ?>><?php echo NO ?></option>
                                                        <option value="yes" <?php if ($categoryRowset['child'] == "yes") { ?> selected <?php } ?>><?php echo YESCHLD ?></option>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Ethnicity</label>
                                                <div class="controls">
                                                    <select name="ethnicity" required>
                                                        <?php
                                                        $location = "select * from `dateing_countries`";
                                                        $locquery = mysql_query($location);
                                                        while ($rowloc = mysql_fetch_array($locquery)) {
                                                            if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'English') {
                                                                ?>
                                                                ?>
                                                                <option value="<?php echo $rowloc['id'] ?>" <?php if ($categoryRowset['ethnicity'] == $rowloc['id']) { ?> selected <?php } ?>><?php echo $rowloc['name'] ?></option>
                                                            <?php } elseif (isset($_SESSION['lang']) && $_SESSION['lang'] == 'Spanish') { ?>
                                                                <option value="<?php echo $rowloc['id'] ?>" <?php if ($categoryRowset['ethnicity'] == $rowloc['id']) { ?> selected <?php } ?>><?php echo utf8_encode($rowloc['translated_name']) ?></option>

                                                                <?php
                                                            } else {
                                                                ?>
                                                                <option value="<?php echo $rowloc['id'] ?>" <?php if ($categoryRowset['ethnicity'] == $rowloc['id']) { ?> selected <?php } ?>><?php echo utf8_encode($rowloc['name']) ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Education</label>
                                                <div class="controls">
                                                    <select name="education" required>
                                                        <option value="high school" <?php if ($categoryRowset['education'] == "high school") { ?> selected <?php } ?>><?php echo HIGH_SCHOOL ?></option>
                                                        <option value="college" <?php if ($categoryRowset['education'] == "college") { ?> selected <?php } ?>><?php echo COLLEGE ?></option>
                                                        <option value="college graduate" <?php if ($categoryRowset['education'] == "college graduate") { ?> selected <?php } ?>><?php echo COLLEGE_GRAT ?></option>


                                                        <option value="higher education" <?php if ($categoryRowset['education'] == "higher education") { ?> selected <?php } ?>><?php echo HIGHER_EDUCATION ?></option>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Religion</label>
                                                <div class="controls">
                                                    <select name="religion" required>
                                                        <option value="catholic" <?php if ($categoryRowset['religion'] == "catholic") { ?> selected <?php } ?>><?php echo CATHOLIC ?></option>
                                                        <option value="protestant" <?php if ($categoryRowset['religion'] == "protestant") { ?> selected <?php } ?>><?php echo PROTESTANT ?></option>
                                                        <option value="judaism" <?php if ($categoryRowset['religion'] == "judaism") { ?> selected <?php } ?>><?php echo JUDAISM ?></option>
                                                        <option value="muslim" <?php if ($categoryRowset['religion'] == "muslim") { ?> selected <?php } ?>><?php echo MUSLIM ?></option>
                                                        <option value="hindu" <?php if ($categoryRowset['religion'] == "hindu") { ?> selected <?php } ?>><?php echo HINDU ?></option>
                                                        <option value="restorationism" <?php if ($categoryRowset['religion'] == "restorationism") { ?> selected <?php } ?>><?php echo RESTORATIONISM ?></option>
                                                        <option value="buddhist" <?php if ($categoryRowset['religion'] == "buddhist") { ?> selected <?php } ?>><?php echo BUDDHIST ?></option>
                                                        <option value="other" <?php if ($categoryRowset['religion'] == "other") { ?> selected <?php } ?>><?php echo LANG_Other; ?></option>
                                                    </select>

                                                </div>
                                            </div>



                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Smoking</label>
                                                <div class="controls">
                                                    <select name="smoking" required>
                                                        <option value="no" <?php if ($categoryRowset['smoking'] == "no") { ?> selected <?php } ?>><?php echo NO ?></option>
                                                        <option value="yes socially" <?php if ($categoryRowset['smoking'] == "yes socially") { ?> selected <?php } ?>><?php echo YES ?></option>
                                                        <option value="yes regularly" <?php if ($categoryRowset['smoking'] == "yes regularly") { ?> selected <?php } ?>><?php echo YESREG ?></option>


                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Drinking</label>
                                                <div class="controls">
                                                    <select name="drinking" required>
                                                        <option value="no" <?php if ($categoryRowset['drinking'] == "no") { ?> selected <?php } ?>><?php echo NO ?></option>
                                                        <option value="yes socially" <?php if ($categoryRowset['drinking'] == "yes socially") { ?> selected <?php } ?>><?php echo YES ?></option>
                                                        <option value="yes regularly" <?php if ($categoryRowset['drinking'] == "yes regularly") { ?> selected <?php } ?>><?php echo YESREG ?></option>


                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Body Type</label>
                                                <div class="controls">
                                                    <select name="bodytype" required>
                                                        <option value="slim" <?php if ($categoryRowset['bodytype'] == "slim") { ?> selected <?php } ?>><?php echo SLIM ?></option>
                                                        <option value="athletic" <?php if ($categoryRowset['bodytype'] == "athletic") { ?> selected <?php } ?>><?php echo ATHLETIC ?></option>
                                                        <option value="curvy" <?php if ($categoryRowset['bodytype'] == "curvy") { ?> selected <?php } ?>><?php echo CURVY ?></option>
                                                        <option value="heavy" <?php if ($categoryRowset['bodytype'] == "heavy") { ?> selected <?php } ?>><?php echo HEAVY ?></option>

                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Personality</label>
                                                <div class="controls">
                                                    <select name="personality" required>
                                                        <option value="Pleasure Seeking and Sociable" <?php if ($categoryRowset['personality'] == "Pleasure Seeking and Sociable") { ?> selected <?php } ?>><?php echo LANG_PLEASURE_SEEKING ?></option>
                                                        <option value="Ambitious and Leader Like" <?php if ($categoryRowset['personality'] == "Ambitious and Leader Like") { ?> selected <?php } ?>><?php echo LANG_AMBICIOSSO_LIDER ?></option>
                                                        <option value="Analytical and Quiet" <?php if ($categoryRowset['personality'] == "Analytical and Quiet") { ?> selected <?php } ?>><?php echo LANG_ANALITICO_CALLADO ?></option>
                                                        <option value="Relaxed and Peaceful" <?php if ($categoryRowset['personality'] == "Relaxed and Peaceful") { ?> selected <?php } ?>><?php echo LANG_RELAJADO ?></option>
                                                        <option value="You Need to Find Out" <?php if ($categoryRowset['personality'] == "You Need to Find Out") { ?> selected <?php } ?>><?php echo LANG_USTED_NECESSITA ?></option>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Looking for</label>
                                                <div class="controls">
                                                    <p><input type="radio" value="penpal" name="look" <?php if ($categoryRowset['lookfor'] == "penpal") { ?> checked <?php } ?>/><span><?php echo PENPAL ?></span></p>
                                                    <p><input type="radio" value="a friend" name="look" <?php if ($categoryRowset['lookfor'] == "a friend") { ?> checked <?php } ?>/><span><?php echo AFRIEND ?></span></p>
                                                    <p><input type="radio" value="casual fun" name="look" <?php if ($categoryRowset['lookfor'] == "casual fun") { ?> checked <?php } ?> /><span><?php echo CASUAL ?></span></p>
                                                    <p><input type="radio" value="relationship" name="look" <?php if ($categoryRowset['lookfor'] == "relationship") { ?> checked <?php } ?>/><span><?php echo ARELATION ?></span></p>
                                                    <p><input type="radio" value="marriage" name="look" <?php if ($categoryRowset['lookfor'] == "marriage") { ?> checked <?php } ?>/><span><?php echo MARRIGE ?></span></p>
                                                    <p><input type="radio" value="sugar daddy" name="look" <?php if ($categoryRowset['lookfor'] == "sugar daddy") { ?> checked <?php } ?>/><span><?php echo SUGAR ?></span></p>

                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">About</label>
                                                <div class="controls">
                                                    <textarea name="about_me" class="input-xlarge focused"><?php echo $categoryRowset['about_me']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">What I'm doing with my life</label>
                                                <div class="controls">
                                                    <textarea name="me_doing" class="input-xlarge focused"><?php echo $categoryRowset['me_doing']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">I'm really good at</label>
                                                <div class="controls">
                                                    <textarea name="good_at" class="input-xlarge focused"><?php echo $categoryRowset['good_at']; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">The first thing people usually notice about me</label>
                                                <div class="controls">
                                                    <textarea name="notice_about_me"  class="input-xlarge focused"><?php echo $categoryRowset['notice_about_me']; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Perfect Match</label>
                                                <div class="controls">
                                                    <textarea name="perfect_match"  class="input-xlarge focused"><?php echo $categoryRowset['perfect_match']; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label" for="focusedInput">Ideal Date</label>
                                                <div class="controls">
                                                    <textarea name="ideal_date"  class="input-xlarge focused"><?php echo $categoryRowset['ideal_date']; ?></textarea>
                                                </div>
                                            </div>






                                            <!--                                            <div class="control-group">
                                                                                            <label class="control-label" for="focusedInput">Image</label>
                                                                                            <div class="controls">
                                                                                                <input type="file" name="image" > <?php if ($categoryRowset['image'] != '') { ?><br><a href="../upload/story/<?php echo $categoryRowset['image']; ?>" target="_blank">View Image</a><?php } ?>
                                                                                            </div>
                                                                                        </div>-->




                                            <div class="form-actions">
                                                <!--                                                <button type="submit" class="btn btn-primary"  name="submit">Save changes</button>-->
                                                <input type="submit" name="submit" value="Save changes" class="btn btn-primary">
                                                <button type="reset" class="btn" onClick="location.href = 'list_user.php'">Cancel</button>
                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>



                </div>
            </div>
            <hr>
            <?php include('includes/footer.php'); ?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>
        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />

        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>


        <script src="assets/scripts.js"></script>
        <script>
          function gender_check(obj)
        {
            var gender=$(obj).val();
             $.ajax({
                url: "ajax.php", 
                data: {gender : gender}, 
                success: function(result){
                    //alert(result);
        $("#male_female").html(result);
            }});
            
            

        }
        </script>
        <script>
                                                    $(function () {
                                                        $(".datepicker").datepicker();
                                                        $(".uniform_on").uniform();
                                                        $(".chzn-select").chosen();
                                                        $('.textarea').wysihtml5();

                                                        $('#rootwizard').bootstrapWizard({onTabShow: function (tab, navigation, index) {
                                                                var $total = navigation.find('li').length;
                                                                var $current = index + 1;
                                                                var $percent = ($current / $total) * 100;
                                                                $('#rootwizard').find('.bar').css({width: $percent + '%'});
                                                                // If it's the last tab then hide the last button and show the finish instead
                                                                if ($current >= $total) {
                                                                    $('#rootwizard').find('.pager .next').hide();
                                                                    $('#rootwizard').find('.pager .finish').show();
                                                                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                                                                } else {
                                                                    $('#rootwizard').find('.pager .next').show();
                                                                    $('#rootwizard').find('.pager .finish').hide();
                                                                }
                                                            }});
                                                        $('#rootwizard .finish').click(function () {
                                                            alert('Finished!, Starting over!');
                                                            $('#rootwizard').find("a[href*='tab1']").trigger('click');
                                                        });
                                                    });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>