<?php
ob_start();
session_start();
include('administrator/includes/config.php');
include('class.phpmailer.php');
include('includes/language.php');
if($_SESSION['user_id']=='')
{
 header('location:index.php');
 exit;
}										
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Online members</title>
<meta name="" content="">
<link rel="stylesheet" href="css/style.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<!--<script src="js/jquery-1.js"></script>-->
<script src="js/modernizr.js"></script>
<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>

</head>
<body style="background: url(images/bg-main.jpg) center top no-repeat fixed; margin: 0 0 15px 0;">
	<?php include('includes/header.php');?>
	<div class="container">
		<div class="profile_body">
			<?php include('includes/left-panel.php');?>
                    <div class="right_menu online_who" style="background:#fff;text-align:center;padding-top:30px">
                            <span><?php echo LANG_NO_PERMISSION_MESSAGE ?></span>
			  
			   <div class="demo-1 no-js">
                                <section id="grid" class="grid clearfix">

                               </section>
                           </div>
                         
			</div>
			<div class="clearfix"></div>
			<?php include('includes/footer.php');?>
		</div>
	</div>
	<link rel="stylesheet" type="text/css" href="css/component.css" />
       <script src="js/snap.svg-min.js"></script>
	<style>
	 .pub_prof:hover{text-decoration:underline}
	 .pub_prof{text-decoration:none;color:#0099ff}
	</style>
	<style>
         .list_img i {
          width: 12px;
          height: 12px;
          background: #41c439;
          border-radius: 100%;
          z-index: 999999;
          position:absolute;
          bottom:17px;right:31px;
          display:block;
          border:3px solid #0097fb;
        }
        .search_results{background:none;}
        .three_bttn input[type="button"]{float:none;margin-left:0px;margin-top:7px;}
        #f1_container {
          position: relative;
          margin: 13px 0px 13px 13px;
          width: 200px;
          height: 177px;
          z-index: 1;
          float:left;
        }
        .topimgdiv{float:left;padding-top: 70px;position:relative;}
        #f1_container {
          perspective: 1000;
        }
        #f1_card {
          width: 100%;
          height: 100%;
          transform-style: preserve-3d;
          transition: all 1.0s linear;
        }
         .usrimg{
          width: 120px;
          height: 120px;
          border-radius: 100%;
          border: 3px solid #0097FB;
          position: absolute;
          z-index: 999;
          left: 49px;
          top: 15px;
        }
        #f1_container:hover #f1_card {
          transform: rotateY(180deg);
          #box-shadow: -5px 5px 5px #aaa;
        }
        .face {
          position: absolute;
          width: 100%;
          height: 100%;
          backface-visibility: hidden;
          background-color: #fff;
          border-radius: 4px;
          box-shadow: 1px 2px 2px 0px #808080;
        }
        .face.back {
          display: block;
          transform: rotateY(180deg);
          box-sizing: border-box;
          #padding: 10px;
          color: #000;
          text-align: center;
          background-color: #fff;
          border-radius: 4px;
          box-shadow: 1px 2px 2px 0px #808080;
        }
        .profinfodiv
        {
         width:99.7%;
         height:81px;
         border:0px solid red;
         margin-top:55px;
         font-family:arial;
        }
        .profinfodivback
        {
         width:99.7%;
         height:106px;
         border:0px solid red;
         margin-top:65px;
         font-family:arial;
        }
        .proffavdiv{width:99.7%;height:26px;
         border:0px solid red;
         font-family:arial;}
        .profinfodiv h4{color:#6CC1FA;text-align:left;padding:7px 0 7px 10px}
        .profinfodiv .descdiv{float:left}
        .profinfodiv .descdiv h4{padding:2px 0 2px 10px}
        .profinfodiv .descdiv h4 span{font-size:12px !important;padding:2px 4px 2px 0px !important;}
        .profinfodiv p{text-align: left;padding: 0 10px 0 10px;font-size: 13px;margin-bottom:5px;color:#8B8B8B}
        .profinfodiv p span{font-weight:bold}
        
        .profinfodivback h4{color:#6CC1FA;text-align:left;padding:7px 0 7px 10px}
        .profinfodivback .descdiv{float:left}
        .profinfodivback .descdiv h4{padding:2px 0 2px 10px}
        .profinfodivback .descdiv h4 span{font-size:12px !important;padding:2px 4px 2px 0px !important;}
        .profinfodivback p{text-align: left;padding: 0 10px 0 10px;font-size: 13px;margin-bottom:5px;color:#8B8B8B}
        .profinfodivback p span{font-weight:bold}
        </style>
        <script>
		(function() {

			function init() {
				var speed = 250,
					easing = mina.easeinout;

				[].slice.call ( document.querySelectorAll( '#grid > a' ) ).forEach( function( el ) {
					var s = Snap( el.querySelector( 'svg' ) ), path = s.select( 'path' ),
						pathConfig = {
							from : path.attr( 'd' ),
							to : el.getAttribute( 'data-path-hover' )
						};

					el.addEventListener( 'mouseenter', function() {
						path.animate( { 'path' : pathConfig.to }, speed, easing );
					} );

					el.addEventListener( 'mouseleave', function() {
						path.animate( { 'path' : pathConfig.from }, speed, easing );
					} );
				} );
			}

			init();

		})();
		
		function redirect(id)
		{
                    var evt = window.event || arguments.callee.caller.arguments[0];
                     if($(evt.target).hasClass('chat-btn')==false)
                     {
                        window.location.href="details.php?id="+id;
                     }
		}
	</script>
	<script>
		$(document).ready(function() {
		 $(".tab_content").hide();
		 $("ul.tabs li:first").addClass("active").show();
		 $(".tab_content:first").show();
		 
		 $("ul.tabs li").click(function() {
		 
		  $("ul.tabs li").removeClass("active");
		  $(this).addClass("active");
		  $(".tab_content").hide();
		 
		  var activeTab = $(this).find("a").attr("href");
		  $(activeTab).fadeIn();
		  return false;
		 });
		});
		
		function make_fav(user_id){
			$.ajax({
			type: "post",
			url: "function/ajax.php?action=favourite",
			data: 'from_id=<?php echo $_SESSION['user_id'];?>&to_id='+user_id+'&message=',
			success: function(msg) { 
			if(msg.trim()=='suc'){
			   //$('.thank_u'+user_id).html('Added to Favourite..').show('slow');
			   //$('#message_text').html('');
			   //$("#msg").hide();
                           $('#favourite'+user_id).hide();
                           $('#favic'+user_id).show();
			  }
			 }	
			});
		  }
	</script>
</body>
</html>

