<?php 
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");


if(isset($_REQUEST['submit']))
{

	$name = isset($_POST['name']) ? $_POST['name'] : '';
	$make_by = isset($_POST['make_by']) ? $_POST['make_by'] : '';
	$model = isset($_POST['model']) ? $_POST['model'] : '';
	$desc = isset($_POST['desc']) ? $_POST['desc'] : '';
	$year = isset($_POST['year']) ? $_POST['year'] : '';
	$car_type = isset($_POST['car_type']) ? $_POST['car_type'] : '';
	$reg_year = isset($_POST['reg_year']) ? $_POST['reg_year'] : '';
	$mileage = isset($_POST['mileage']) ? $_POST['mileage'] : '';
	$vin = isset($_POST['vin']) ? $_POST['vin'] : '';
	$fuel_type = isset($_POST['fuel_type']) ? $_POST['fuel_type'] : '';
	$steering = isset($_POST['steering']) ? $_POST['steering'] : '';
	$door = isset($_POST['door']) ? $_POST['door'] : '';
	$transmission = isset($_POST['transmission']) ? $_POST['transmission'] : '';
	$passengers = isset($_POST['passengers']) ? $_POST['passengers'] : '';
	$price = isset($_POST['price']) ? $_POST['price'] : '';
	$kilometers = isset($_POST['kilometers']) ? $_POST['kilometers'] : '';
	$engine = isset($_POST['engine']) ? $_POST['engine'] : '';
	$warranties = isset($_POST['warranties']) ? $_POST['warranties'] : '';
	$brakes = isset($_POST['brakes']) ? $_POST['brakes'] : '';
	$exterior = isset($_POST['exterior']) ? $_POST['exterior'] : '';
	$interriors = isset($_POST['interriors']) ? $_POST['interriors'] : '';
	$seats = isset($_POST['seats']) ? $_POST['seats'] : '';
	$no_of_owners = isset($_POST['no_of_owners']) ? $_POST['no_of_owners'] : '';
	$color = isset($_POST['color']) ? $_POST['color'] : '';
	$datetime = date('Y-m-d H:i:s');
	
	

	$fields = array(
		'name' => mysql_real_escape_string($name),
		'make_by' => mysql_real_escape_string($make_by),
		'model' => mysql_real_escape_string($model),
		'desc' => mysql_real_escape_string($desc),
		'year' => mysql_real_escape_string($year),
		'car_type' => mysql_real_escape_string($car_type),
		'reg_year' => mysql_real_escape_string($reg_year),
		'mileage' => mysql_real_escape_string($mileage),
		'vin' => mysql_real_escape_string($vin),
		'fuel_type' => mysql_real_escape_string($fuel_type),
		'steering' => mysql_real_escape_string($steering),
		'door' => mysql_real_escape_string($door),
		'transmission' => mysql_real_escape_string($transmission),
		'passengers' => mysql_real_escape_string($passengers),
		'price' => mysql_real_escape_string($price),
		'kilometers' => mysql_real_escape_string($kilometers),
		'brakes' => mysql_real_escape_string($brakes),
		'warranties' => mysql_real_escape_string($warranties),
		'exterior' => mysql_real_escape_string($exterior),
		'interriors' => mysql_real_escape_string($interriors),
		'seats' => mysql_real_escape_string($seats),
		'no_of_owners' => mysql_real_escape_string($no_of_owners),
		'color' => mysql_real_escape_string($color),
		'engine' => mysql_real_escape_string($engine));

		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	  $editQuery = "UPDATE `dateing_car` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";

		if (mysql_query($editQuery)) {
		
		if($_FILES['image']['tmp_name']!='')
		{
		$target_path="../upload/car/";
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$img_name =time().$userfile_name;
		$img=$target_path.$img_name;
		move_uploaded_file($userfile_tmp, $img);
		
		$image =mysql_query("UPDATE `dateing_car` SET `img`='".$img_name."' WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'");
		}
		
		
			$_SESSION['msg'] = "Category Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Category";
		}

		header('Location:list_car.php');
		exit();
	
	 }
	 else
	 {
	 
	 $addQuery = "INSERT INTO `dateing_car` (`" . implode('`,`', array_keys($fields)) . "`)"
			. " VALUES ('" . implode("','", array_values($fields)) . "')";
			
			//exit;
		mysql_query($addQuery);
		$last_id=mysql_insert_id();
		if($_FILES['image']['tmp_name']!='')
		{
		$target_path="../upload/car/";
		$userfile_name = $_FILES['image']['name'];
		$userfile_tmp = $_FILES['image']['tmp_name'];
		$img_name =time().$userfile_name;
		$img=$target_path.$img_name;
		move_uploaded_file($userfile_tmp, $img);
		
		$image =mysql_query("UPDATE `dateing_car` SET `img`='".$img_name."' WHERE `id` = '" . $last_id . "'");
		}
		 
/*		if (mysql_query($addQuery)) {
		
			$_SESSION['msg'] = "Category Added Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while adding Category";
		}
		*/
		header('Location:list_car.php');
		exit();
	
	 }
				
				
}

if($_REQUEST['action']=='edit')
{
$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `dateing_car` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

}
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Add Car</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
       
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
         <?php include('includes/header.php');?>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Car</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal" method="post" action="add_car.php" enctype="multipart/form-data">
                                     <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                                     <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                                      <fieldset>
                                        <legend><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Car </legend>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Name</label>
                                          <div class="controls">
                      <input name="name" class="input-xlarge focused" required  id="focusedInput" type="text" value="<?php echo $categoryRowset['name'];?>">
                                          </div>
                                        </div>
										
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Make By </label>
                                        <div class="controls">
                                        <select id="selectError" name="make_by" >
											<option value="">Select One</option>
											<?php 
                                            $SQL ="SELECT * FROM `dateing_carmakers` order by `name`";
                                            $result = mysql_query($SQL);
                                            
                                            while($row1=mysql_fetch_array($result))
                                            { 
                                            ?>
                                            <option value="<?php echo $row1['id']; ?>" <?php if($categoryRowset['make_by']==$row1['id']) { echo "selected";}?> > <?php echo $row1['name']; ?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Model </label>
                                        <div class="controls">
                                        <select id="selectError" name="model" >
											<option value="">Select One</option>
											<?php 
                                            $SQL ="SELECT * FROM `dateing_carmodels` order by `name`";
                                            $result = mysql_query($SQL);
                                            
                                            while($row1=mysql_fetch_array($result))
                                            { 
                                            ?>
                                            <option value="<?php echo $row1['id']; ?>" <?php if($categoryRowset['model']==$row1['id']) { echo "selected";}?> > <?php echo $row1['name']; ?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        </div>
										
										
										
                                         
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Description</label>
                                        <div class="controls">
                                        <?php /*?><input name="description" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['description'];?>"><?php */?>
										<textarea name="desc" style="width:300px; height:100px;"><?php echo stripslashes($categoryRowset['desc']);?></textarea>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Year </label>
                                        <div class="controls">
                                        <select id="selectError" name="year" >
											<option value="">Select One</option>
											<?php for($i=1985;$i<=date('Y');$i++){?>
                                            <option value="<?php echo $i; ?>" <?php if($categoryRowset['year']==$i) { echo "selected";}?> ><?php echo $i; ?></option>
											<?php
											}
											?>
                                            
                                            </select>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Car Type </label>
                                        <div class="controls">
                                        <select id="selectError" name="car_type" >
											<option value="">Select One</option>
											<?php 
                                            $SQL ="SELECT * FROM `dateing_cartype` order by `name`";
                                            $result = mysql_query($SQL);
                                            
                                            while($row1=mysql_fetch_array($result))
                                            { 
                                            ?>
                                            <option value="<?php echo $row1['id']; ?>" <?php if($categoryRowset['car_type']==$row1['id']) { echo "selected";}?> > <?php echo $row1['name']; ?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Reg. Year</label>
                                        <div class="controls">
                                        <input name="reg_year" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['reg_year'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Mileage</label>
                                        <div class="controls">
                                        <input name="mileage" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['mileage'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Vehicle Identification Number</label>
                                        <div class="controls">
                                        <input name="vin" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['vin'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Fuel Type</label>
                                        <div class="controls">
                                        <input name="fuel_type" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['fuel_type'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Steering</label>
                                        <div class="controls">
                                        <input name="steering" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['steering'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Door</label>
                                        <div class="controls">
                                        <input name="door" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['door'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Transmission</label>
                                        <div class="controls">
                                        <input name="transmission" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['transmission'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">Number Of Passengers</label>
                                        <div class="controls">
                                        <input name="passengers" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['passengers'];?>">
                                        </div>
                                        </div>
										
									<div class="control-group">
                                        <label class="control-label" for="focusedInput">Price($)</label>
                                        <div class="controls">
                                        <input name="price" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['price'];?>">
                                        </div>
                                        </div>
										
										<div class="control-group">
                                        <label class="control-label" for="focusedInput">kilometers</label>
                                        <div class="controls">
                                        <input name="kilometers" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['kilometers'];?>">
                                        </div>
                                        </div>
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Engine</label>
                                        <div class="controls">
                                        <input name="engine" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['engine'];?>">
                                        </div>
                                        </div>
                                        
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Warranties</label>
                                        <div class="controls">
                                        <input name="warranties" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['warranties'];?>">
                                        </div>
                                        </div>
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Brakes</label>
                                        <div class="controls">
                                        <input name="brakes" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['brakes'];?>">
                                        </div>
                                        </div>
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Exterior</label>
                                        <div class="controls">
                                        <input name="exterior" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['exterior'];?>">
                                        </div>
                                        </div>
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Interriors</label>
                                        <div class="controls">
                                        <input name="interriors" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['interriors'];?>">
                                        </div>
                                        </div>
                                        
                                       
                                         <div class="control-group">
                                        <label class="control-label" for="focusedInput">Seats</label>
                                        <div class="controls">
                                        <input name="seats" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['seats'];?>">
                                        </div>
                                        </div>
                                        
                                         <div class="control-group">
                                        <label class="control-label" for="focusedInput">No. Of Owners</label>
                                        <div class="controls">
                                        <input name="no_of_owners" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['no_of_owners'];?>">
                                        </div>
                                        </div>
                                        
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Colors</label>
                                        <div class="controls">
                                        <input name="color" class="input-xlarge focused" type="text" value="<?php echo $categoryRowset['color'];?>">
                                        </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="control-group">
                                        <label class="control-label" for="focusedInput">Main Image</label>
                                        <div class="controls">
                                        <input type="file" name="image" > <?php if($categoryRowset['image']!=''){?><br><a href="../upload/category/<?php echo $categoryRowset['image'];?>" target="_blank">View</a><?php }?>
                                        </div>
                                        </div>



                                        
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary"  name="submit">Save changes</button>
                                          <button type="reset" class="btn" onClick="location.href='list_car.php'">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                

                </div>
            </div>
            <hr>
             <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>
        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        <link rel="stylesheet" href="css/colorpicker.css" type="text/css" />
        
        <script type="text/javascript" src="js/colorpicker.js"></script>
        <script type="text/javascript" src="js/eye.js"></script>
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>


        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>