<link rel="stylesheet" href="css/chat.css">
<link rel="stylesheet" href="css/screen.css">
<?php
$today = date('Y-m-d');

//echo "select * from `dateing_user` WHERE `id`='".$_SESSION['user_id']."' AND (`start_date`<='".$today."' AND `end_date`>='".$today."')";
$checkTranslatorPackage = mysql_num_rows(mysql_query("select * from `dateing_user` WHERE `id`='" . $_SESSION['user_id'] . "' AND (`start_date`<='" . $today . "' AND `end_date`>='" . $today . "')"));

if ($checkTranslatorPackage == 0) {
    ?>
    <script src="js/chat_membership.js" type="text/javascript"></script>
    <?php
} else {
    ?>
    <script src="js/chat.js" type="text/javascript"></script>
    <?php
}
?>
<script src="js/interact.js" type="text/javascript"></script>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
    interact('.draggable')
            .draggable({
                // allow dragging of multple elements at the same time
                max: Infinity,
                // call this function on every dragmove event
                onmove: function (event) {
                    var target = event.target,
                            // keep the dragged position in the data-x/data-y attributes
                            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                    // translate the element
                    target.style.webkitTransform =
                            target.style.transform =
                            'translate(' + x + 'px, ' + y + 'px)';

                    // update the posiion attributes
                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                },
                // call this function on every dragend event
                onend: function (event) {
                    var textEl = event.target.querySelector('p');

                    textEl && (textEl.textContent =
                            'moved a distance of '
                            + (Math.sqrt(event.dx * event.dx +
                                    event.dy * event.dy) | 0) + 'px');
                }
            })
            // enable inertial throwing
            .inertia(true)
            // keep the element within the area of it's parent
            .restrict({
                drag: "parent",
                endOnly: true,
                elementRect: {top: 0, left: 0, bottom: 1, right: 1}
            });

    // allow more than one interaction at a time
    interact.maxInteractions(Infinity);

    function twShare(url, title, descr, image, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://twitter.com/share?url=' + encodeURI(url) + '&text=' + encodeURI(title) + '', 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }

    function facebook_share()
    {
        FB.init({
            appId: '1578907722322033',
            cookie: true,
            status: true,
            xfbml: true
        });
        FB.ui(
                {
                    method: 'feed',
                    name: 'MiAmor',
                    link: 'http://nationalitsolution.in/team5/dating/',
                    picture: 'http://nationalitsolution.in/team5/dating/images/logo_dating.png',
                    caption: 'Miamor',
                    description: 'Miamor share'
                },
        function (response) {
            if (response && response.post_id) {
            } else {
            }
        })
    }
</script>

<div class="footer_all">
    <ul>
        <li class="logo_foot">
            <img src="images/logo.png" alt="" />
            <p>&copy; 2014 miamor, Inc. <a href="cms.php?pid=5" style="color:#fff;text-decoration:none">Terms</a>  <a href="cms.php?pid=6" style="color:#fff;text-decoration:none">Privacy Policy</a></p>
        </li>
        <li class="quick_link">
            <h2><?php echo QUICK_LINKS ?></h2>
            <a href="index.php"><?php echo HOME ?></a>     
            <a href="cms.php?pid=7"><?php echo HOW_IT_WORKS ?></a>    
            <a href="cms.php?pid=1"><?php echo ABOUT_US ?></a>       
            <a href="javascript:void(0)"><?php echo THE_APP ?></a>     
            <a href="cms.php?pid=11"><?php echo ADVERTISE ?></a>          
            <a href="cms.php?pid=8"><?php echo TELL_US ?></a>       
            <a href="blogs.php"><?php echo BLOG ?></a>
        </li>
        <li class="get_touch">
<?php
if ($_REQUEST['submit']) {
    $check = mysql_num_rows(mysql_query("select * from `dateing_sentnewsletter` where `email`='" . $_REQUEST['email'] . "'"));
    if ($check == 0) {
        $news = mysql_query("insert into `dateing_sentnewsletter`(`id`,`email`) values('','" . $_REQUEST['email'] . "')");
        header('location:index.php');
    } else {
        header('location:index.php');
    }
}
?>
            <?php $getintouch = mysql_fetch_array(mysql_query('select * from dateing_cms where id=10')); ?>
            <h2><?php echo GET_IN_TOUCH ?></h2>
            <p><?php
            if ($_SESSION['lang'] == 'Spanish' && (!empty($getintouch))) {
                echo $getintouch['description_translated'];
            } else {
                echo $getintouch['pagedetail'];
            }
            ?></p>
            <form action="" method="POST">
                <input type="text" name="email" placeholder="Enter your email address" required/>
                <input type="submit" value="<?php echo SUBSCRIBE ?>" name="submit" style="background: url(images/button_back.png) repeat-x scroll 0 0 rgba(0, 0, 0, 0);border: 0 none;border-radius: 5px;color: #fff;float: right;font-family: Noto Sans;font-size: 17px;height: 52px;width: 203px;margin-right: 45px;">
            </form>
        </li>
        <li class="follow">
            <h2><?php echo FOLLOW_US_ON ?></h2>
            <a href="javascript:void(0)" onclick="twShare('http://nationalitsolution.in/team5/dating/', 'Miamor Share', '', 'http://nationalitsolution.in/team5/dating/images/logo_dating.png', 520, 350)"><img src="images/new_share1.png" alt="twitter" /></a>
            <a href="javascript:void(0)" onclick="facebook_share()"><img src="images/new_share2.png" alt="facebook" /></a>
        </li>
    </ul>
</div>

<div id="fb-root"></div>
