<?php
if($_GET['pic']){
	$img = new img($_GET['pic']);	
	$img->resize($_GET['w'],$_GET['h']);
	$img->show();
}

class img {
	
	var $image = '';
	var $temp = '';
	var $arrOriginalDetails = '';
	
	function img($sourceFile){
		$this->arrOriginalDetails = getimagesize($sourceFile);
		
		if(file_exists($sourceFile)){
			if($this->arrOriginalDetails['mime']=='image/jpeg'){
				$this->image = ImageCreateFromJPEG($sourceFile);
			}
			else if($this->arrOriginalDetails['mime']=='image/png'){
				$this->image = imagecreatefrompng($sourceFile);
			}
			else if($this->arrOriginalDetails['mime']=='image/gif'){
				$this->image = imagecreatefromgif($sourceFile);
			}
		} else {
			$this->errorHandler();
		}
		return;
	}
	
	function resize($width,$height,$aspectradio='yes'){
		$o_wd = imagesx($this->image);
		$o_ht = imagesy($this->image);
		if(isset($aspectradio)&&$aspectradio) {
			/*$w = $o_wd;
			$h = $o_ht;
			if($o_wd > $width || $o_ht > $height) {
				if($o_ht > $o_wd) {
					$h = $height;
					$w = round($o_wd * $height / $o_ht);
					if($w > $width) {
						$w = $width;
						$h = round($h * $width / round($o_wd * $height / $o_ht));
					}
				} else if($o_wd > $o_ht) {
					$w = $width;
					$h = round($o_ht * $width / $o_wd);
					if($h > $height) {
						$h = $height;
						$w = round($w * $height / round($o_ht * $width / $o_wd));
					}
				} else {
					$h = $height;
					$w = round($o_wd * $height / $o_ht);
					if($w > $width) {
						$w = $width;
						$h = round($h * $width / round($o_wd * $height / $o_ht));
					}
				}
			}

			$width =& $w;
			$height =& $h;*/

			/*$scale = min($width/$o_wd, $height/$o_ht);
			$width1  = ceil($scale*$o_wd);
			$height1 = ceil($scale*$o_ht);
			*/
            /*if($_GET['w']==$_GET['h'])
			{
              if($_GET['w']==275)
			  {
			   $height=275;
			   $width=300;
			  }
			  else
			  {
                $height=275;
			    $width=350;
			  }
			}
			else
			{
              $ratio=$o_wd/$o_ht;
              $height=275;
			  $width=ceil(275*$ratio);
			}*/

			$heightRatio = $o_ht / $height;
			$widthRatio  = $o_wd /  $width;
		 
			if ($heightRatio < $widthRatio) {
				$optimalRatio = $heightRatio;
			} else {
				$optimalRatio = $widthRatio;
			}
		 
			$optimalHeight = $o_ht / $optimalRatio;
			$optimalWidth  = $o_wd  / $optimalRatio;

		}
		$this->temp = imagecreatetruecolor($optimalWidth, $optimalHeight);
        imagecopyresampled($this->temp, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $o_wd, $o_ht);

		// *** Find center - this will be used for the crop
		$cropStartX = ( $optimalWidth / 2) - ( $width /2 );
		$cropStartY = ( $optimalHeight/ 2) - ( $height/2 );
	 
		$crop = $this->temp;
	 
		// *** Now crop from center to exact requested size
		$this->temp = imagecreatetruecolor($width , $height);
		imagecopyresampled($this->temp, $crop , 0, 0, $cropStartX, $cropStartY, $width, $height , $width, $height);
		
		$this->sync();
		return;
	}
	
	function sync(){
		$this->image =& $this->temp;
		unset($this->temp);
		$this->temp = '';
		return;
	}
	
	function show(){
		$this->_sendHeader();
		if($this->arrOriginalDetails['mime']=='image/jpeg'){
			ImageJPEG($this->image);
			return;
		}
		else if($this->arrOriginalDetails['mime']=='image/png'){
			imagepng($this->image);
			return;
		}
		else if($this->arrOriginalDetails['mime']=='image/gif'){
			imagegif($this->image);
			return;
		}
	}
	
	function _sendHeader(){
		header('Content-Type: '.$this->arrOriginalDetails['mime']);
	}
	
	function errorHandler(){
		echo "error";
		exit();
	}
	
	function store($file){
		if($this->arrOriginalDetails['mime']=='image/jpeg'){
			ImageJPEG($this->image,$file);
			return;
		}
		else if($this->arrOriginalDetails['mime']=='image/png'){
			imagepng($this->image,$file);
			return;
		}
		else if($this->arrOriginalDetails['mime']=='image/gif'){
			imagegif($this->image,$file);
			return;
		}
	}
	
	function watermark($pngImage, $left = 0, $top = 0){
		ImageAlphaBlending($this->image, true);
		$layer = ImageCreateFromPNG($pngImage); 
		$logoW = ImageSX($layer); 
		$logoH = ImageSY($layer); 
		ImageCopy($this->image, $layer, $left, $top, 0, 0, $logoW, $logoH); 
	}
}
?>
