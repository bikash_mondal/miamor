<?php 
include_once("includes/session.php");
include_once("includes/config.php");

$from = '"';
	$to = '"';
	if($_POST)
	{
		//echo $_POST['ar_1'];
		$filename=dirname(__FILE__);
		$frpath = "includes/lang/sp.php";
		
		$enpath = "includes/lang/en.php";
		$enlines = file($enpath);//file in to an array
		$noofitem = count($enlines);
		$frlines[0]='<?php'."\n";
		$arlines[0]='<?php'."\n";
		$newenlines[0]='<?php'."\n";
		
		for($no=1;$no<$noofitem;$no++)
		{
			if(strpos($enlines[$no],'//') !== false)
			{
				$frlines[$no]=$enlines[$no];
				$arlines[$no]=$enlines[$no];
				$newenlines[$no]=$enlines[$no];
			}
			elseif(strpos($enlines[$no],'?>') !== false)
			{
				$frlines[$no]=$enlines[$no];
				$arlines[$no]=$enlines[$no];
				$newenlines[$no]=$enlines[$no];
			}
			else
			{
				$define=getDefineBetween($enlines[$no],$from,$to);
				$frlines[$no]='define("'.$define.'","'.stripslashes($_POST['fr_'.$no]).'");'."\n";
				$arlines[$no]='define("'.$define.'","'.stripslashes($_POST['ar_'.$no]).'");'."\n";
				$newenlines[$no]='define("'.$define.'","'.stripslashes($_POST['en_'.$no]).'");'."\n";
				//echo "<br>".$_POST['ar_'.$no];
				//echo "<br>".$arlines[$no];
			}
		}
		
		
		file_put_contents($frpath, $frlines);
		file_put_contents($arpath, $arlines);
		file_put_contents($enpath, $newenlines);
		/*file($frpath, $frlines);
		file($arpath, $arlines);
		$fpfr=fopen($frpath,'w');*/
		//$this->load->helper('url');
		//header('location:index.php');
	}
	
	function getDefineBetween($str,$from,$to)
{
    $sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
    $del = substr($sub,0,strpos($sub,$to));
	return $del;
}
?><!DOCTYPE html>
<html>
    
    <head>
        <title>Language Management </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        
   
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
             <?php include('includes/header.php');?>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php include('includes/left_panel.php');?>
                <!--/span-->
                <div class="span9" id="content">
                  
                  <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Language Management</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                	<form action='' method='post'>
  									<table class="table table-bordered">
						              <thead>
						                <tr>
						                  <th>#</th>
						                  <th>En</th>
                                          <th>Es</th>
						                </tr>
						              </thead>
						              <tbody>
                                      
                <?php 
                    //$page=SITE_URL."administrator/includes/lang/sp.php";
             		
					$enpath = "includes/lang/en.php";
					$enlines = file($enpath);//file in to an array
					$frpath = "includes/lang/sp.php";
					$frlines = file($frpath);//file in to an array
					
					$noofitem = count($enlines);
	
					for($no=1;$no<=$noofitem;$no++)
					{
						if(strpos($enlines[$no],'//') !== false)
						{
							echo '<tr><td><label>'.$no.'<label></td><td colspan="3">CommentLine</td></tr>';
						}
						elseif(strpos($enlines[$no],'?>') !== false)
						{
							break;
						}
						else
						{
						echo '<tr>';
						echo '<td><label>'.$no.'<label></td>';
						echo '<td><label><input type="text" name="en_'.$no.'" value="'.getStringBetween($enlines[$no],$from,$to).'" size="50"></label></td>';
						echo '<td><input type="text" name="fr_'.$no.'" value="'.getStringBetween($frlines[$no],$from,$to).'" size="50"></td>';
						
						echo '</tr>';
						}
					}
					
					
					function getStringBetween($str,$from,$to)
					{
						$sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
						$del = substr($sub,0,strpos($sub,$to));
						$str = str_replace('define("', '', $str);
						$str = str_replace('","', '', $str);
						$str = str_replace($del, '', $str);
						$str = str_replace('");', '', $str);
						return $str;
					}
                   
                 ?>
                  
                  <tr>
                  	<td colspan="3">
                    	<input type="submit" class="btn btn-primary" value="Save" name="submit">
                    </td>
                  </tr>
						              </tbody>
						            </table>
                                    </form>
                                </div>
                            </div>

                        </div>
                        <!-- /block -->
                        
                        
                    </div>

                    

                    

                    

                     


                </div>
            </div>
            <hr>
            <?php include('includes/footer.php');?>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
        <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/chat.js"></script>
    </body>

</html>