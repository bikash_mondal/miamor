<?php
ob_start();
session_start();
include('administrator/includes/config.php');
include('class.phpmailer.php');
include('includes/language.php');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>
 <?php 
    if(isset($_SESSION['lang']) && $_SESSION['lang']=='English'){ 
      echo $rs['title'];
    }
    elseif(isset($_SESSION['lang']) && $_SESSION['lang']=='Spanish')
    {
      echo $rs['title_translated'];
    }
    else
    {
      echo $rs['title'];
    }
 ?>
</title>
<meta name="" content="">
<link rel="stylesheet" href="css/style.css">
<!--<link href="gallery/css/galleriffic.css" rel="stylesheet" type="text/css" />-->
<!--<link href="colorbox/css/colorbox.css" rel="stylesheet" type="text/css" />-->
<link href="css/elastislide.css" rel="stylesheet" type="text/css" />
<!-- <link href="gallery/css/main.css" rel="stylesheet" type="text/css" />-->
<link href="lightbox/css/lightbox.css" rel="stylesheet" type="text/css" />

<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="gallery/js/jquery.history.js"></script>-->
<!--<script type="text/javascript" src="gallery/js/jquery.galleriffic.js"></script>-->
<!--<script type="text/javascript" src="gallery/js/jquery.opacityrollover.js"></script>-->
<!--<script type="text/javascript" src="gallery/js/main.js"></script>-->
<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>
<script type="text/javascript" src="js/jquery.elastislide.js"></script>
<!--<script type="text/javascript" src="colorbox/js/jquery.colorbox.js"></script>-->
<script type="text/javascript" src="lightbox/js/lightbox.js"></script>



<style>
	*{padding:0; margin:0}
	.modal_container{width:800px; margin:0 auto; position:relative; padding:10px 0; overflow:hidden; border:1px solid red;z-index:1000;}
	.subscribe_with{width:100%; float:left; margin-bottom:20px}
	.subscribe_with h1{float:left; margin-right:15px; font-size:24px;}
	.subscribe_with .select_bx{width:250px; height:34px; float:left; overflow:hidden; border:1px solid #CCC; background:url(images/black_arrow.png) no-repeat 96% 50%; border-radius:3px}
	.subscribe_with .select_bx select{background:none; border:none; height:34px; width:110%}
	.select_plan{width:100%; padding:2%; float:left;border-top:1px solid #dddddd; border-bottom:1px solid #dddddd}
	.select_plan h2{font-size:16px;}
	.select_plan ul{width:100%; margin:10px 0; float:left}
	.select_plan ul li{width:244px; height:155px; margin-right:10px; margin-bottom:12px;border:1px solid #dddddd; border-radius:4px; float:left; list-style-type:none; overflow:hidden}
	.select_plan ul li:hover{border:1px solid #00a7ff; cursor:pointer}
	.select_plan ul li:hover p.month{color:#00a7ff;}
	.select_plan ul li .top_area{width:100%; height:34px; float:left}
	.select_plan ul li .top_area h3{background:#00a7ff; font-size:16px; text-align:center; line-height:34px; color:#fff;}
	.select_plan ul li .bottom_area{width:100%; height:auto; padding:15px 0; float:left}
	.select_plan ul li p.red{color:#cc0000; text-align:center; margin:5px 0; font-size:16px;}
	.select_plan ul li p.month{color:#999; text-align:center; margin:5px 0; font-size:16px;}
	.select_plan ul li small{color:#a5a4a4; text-align:center; margin:5px 0; font-size:12px; display:block}
	
	.cr_card_dtl{width:96%; padding:2%; float:left; border-bottom:1px solid #dddddd; position:relative}
	.cr_card_dtl ul{width:100%; margin:10px 0; float:left}
	.cr_card_dtl ul li{width:100%; margin-bottom:10px; float:left; list-style-type:none;}
	.cr_card_dtl ul li p.desc{color:#333; text-align:left; padding:2px 0; font-size:14px;}
	.cr_card_dtl ul li .card_no{width:300px; float:left}
	.cr_card_dtl ul li .security_code{width:130px; float:left; margin-left:20px;}
	.cr_card_dtl ul li .month{width:110px; float:left;}
	.cr_card_dtl ul li .slash{width:10px; margin:0 10px; float:left; line-height:36px;}
	.cr_card_dtl ul li .date{width:90px; float:left;}
	.cr_card_dtl ul li .input_bx{height:34px;overflow:hidden; border:1px solid #CCC; border-radius:3px; position:relative}
	.cr_card_dtl ul li .input_bx .card_icon{height:22px; width:101px; position:absolute; top:6px; right:6px;}
	.cr_card_dtl ul li .input_bx input{height:34px; border:0; background:transparent; padding:0 5px;}
	.cr_card_dtl ul li .select_bx{height:34px;overflow:hidden; border:1px solid #CCC; background:url(images/black_arrow.png) no-repeat 95% 50%; border-radius:3px}
	.cr_card_dtl ul li .select_bx select{background:none; border:none; height:34px; width:120%}
	.cr_card_dtl ul li input[type="submit"]{background:#24c81d; color:#fff; padding:10px 50px; font-size:16px; border:0; cursor:pointer }
	.cr_card_dtl ul li span.cancel{margin-left:20px;}
</style>






</head>
<body style="background: url(images/bg-main.jpg) center top no-repeat fixed; margin: 0 0 15px 0;">
        <div class="over_lay" id="loginpage" style="display:none;">
      		<div class="sign_up_box" style="margin: 97px auto;">
      			<h2><?php echo SIGNIN ?><a href="javascript:void(0);" id="closee" style="float:right; color:#FFFFFF; font-size:15px; padding-right:10px;text-decoration:none"><?php echo CLOSE ?></a></h2>
      			<div class="left_form">
                <form action="index.php" method="POST" name="frm">
      				<ul>
						  	<li class="left_form_text"><?php echo EMAIL_ADDRESS ?>:</li>
						 <li><input type="email" name="login_email" class="left_form_text_box" placeholder="<?php echo EMAIL_ADDRESS ?>" required/></li>
						  	<li class="left_form_text"><?php echo PASSWORD ?>:</li>
						 <li><input type="password" name="login_password" class="left_form_text_box" placeholder="<?php echo PASSWORD ?>" required/></li>
						  	<li><a href="javascript:void(0)" style="text-decoration:none" onclick="open_forgot()"><?php echo FORGOT_PASSWORD ?>?</a></li>
						  	<li><input type="submit" name="login" value="<?php echo SIGNIN ?>" class="sign_in_btn"/></li>
						</ul>
                    </form>
                                                    <ul id="forgot_password_ul" style="display:none">
							 <li class="left_form_text">Forgot Password:</li>
							 <li>
							 <input type="email" name="login_email" class="left_form_text_box" placeholder="<?php echo EMAIL_ADDRESS ?>" id="forgot_email"/>
							 <span id="email_err" style="font-size:16px;float:left"></span>
							 </li>
							 <li>&nbsp;</li>
							 <li><input type="button" name="login" value="Submit" class="sign_in_btn" onclick="send_forgot_password()"/></li>
							</ul>
                    
      			</div>
      			<div class="or">
      				<p>OR</p>          				
      			</div>
      			<div class="social_icon_hold">
      				<h3><?php echo SIGN_IN_USING ?></h3>
      				<ul>
						  	<li>
						  		<a href="javascript:void(0)"><img src="images/fb_sign.png" alt="facebook" /></a>
						  <span><?php echo FACEBOOK ?></span>
						  	</li>
						  	<li>
							  	<a href="javascript:void(0)"><img src="images/tweet_sign.png" alt="twitter" /></a>
						    <span><?php echo TWITTER ?></span>
						  	</li>
						  	<li>
							  	<a href="javascript:void(0)"><img src="images/gplus_sign.png" alt="google" /></a>
					             <span><?php echo GOOGLE ?></span>
						  	</li>
						</ul>
      			</div>
      		</div> 
      	</div>
      	<div class="overlay_outer"></div>
	<?php include('includes/header.php');?>
	<div class="container">
		<div class="profile_body">
		    <div class="modal_container">
    	<div class="subscribe_with">
    		<h1>Subscribe with</h1>
            <div class="select_bx">
            	<select>
                	<option>Credit Debit Card</option>
                </select>
            </div>
        </div>
        <div class="select_plan">
        	<h2>Select a plan</h2>
            <ul>
            	<?php 

				  if($userDet['gender'] == 'F')
					{
						 $package=mysql_query("SELECT * FROM `dateing_package_women` WHERE id NOT IN (".$userPackage['id'].")"); 
				  while($rowpack=mysql_fetch_array($package)) {
				   ?>

				
					<li style="width:229px;">
						  		<h2><?php echo $rowpack['title'] ?></h2>
						  		<b style="margin-left: 44px;">Price:<span>$<?php echo $rowpack['price'] ?></span></b>
						  		<aside>
						  			<p>
						  				<?php echo $rowpack['description'] ?>
						  			</p>
						  		</aside>
						  		<div class="btn_suscribe">
                                <form action="pay2.php" method="POST">
                                <input type="hidden" name="price" value="<?php echo $rowpack['price']; ?>">
                                <input type="hidden" name="id" value="<?php echo $rowpack['id']; ?>">
                                <input type="hidden" name="u_id" value="<?php echo $_SESSION['inserted_user_id']; ?>">
						  			<input type="submit" value="SUSCRIBE" name="pack" class="subscribe_bttn" style="margin-left: 50px;"/></form>  
						  		</div>
						  	</li>


				  <?php }

					}else {

				  //$package=mysql_query("SELECT * FROM `dateing_package` WHERE id NOT IN (1,".$userPackage['id'].")"); 
                                  $package=mysql_query("SELECT * FROM `dateing_package` WHERE id NOT IN (1)"); 
                                  $i=0;
				  while($rowpack=mysql_fetch_array($package))  
				  {
				  ?>
						  	<li style="width:229px;text-align:center;">
						  		<h2 style="color:#fff;text-indent: 0px;"><?php echo $rowpack['duration'].' Month'; ?> (<?php echo trim($rowpack['title']); ?>)</h2>
						  		<b>Price:<span>$<?php echo $rowpack['price'] ?></span></b>
						  		<aside>
						  			<p>
						  				<?php echo $rowpack['description'] ?>
						  			</p>
						  		</aside>
						  		<div class="btn_suscribe">
								<div style="float:left;">
                                <form action="pay2.php" method="POST">
                                <input type="hidden" name="price" value="<?php echo $rowpack['price']; ?>">
                                <input type="hidden" name="id" value="<?php echo $rowpack['id']; ?>">
                                <input type="hidden" name="u_id" value="<?php echo $_SESSION['user_id']; ?>">
						  			<input type="submit" value="Paypal" name="pack" class="subscribe_bttn" />
								</form>
								</div>
								<div style="float:right;">
								<form action="stripe.php" method="POST">
								<input type="hidden" name="u_id" value="<?php echo $_SESSION['user_id']; ?>">
								<input type="hidden" name="price" value="<?php echo $rowpack['price']; ?>">
                                <input type="hidden" name="id" value="<?php echo $rowpack['id']; ?>">
									<input type="submit" value="Card Payment" name="stripe" class="subscribe_bttn" />
								</form>
								</div>
						  		</div>
						  	</li>
                         <?php
                            if($i%2==0)
                            { ?>
                                                        <div style="clear: both"></div>
                                                        <?php
                                
                            }
                            $i++;
						 } } 
						 ?> 
            </ul>
        </div>
        <div class="cr_card_dtl">
        	<ul>
            	<li>
                	<div class="card_no">
                    	<p class="desc">Credit Card No.</p>
                        <div class="input_bx">
                        	<input type="text">
                            <div class="card_icon"><img src="images/card_icon.png" alt=""></div>
                        </div>
                    </div>
                    <div class="security_code">
                    	<p class="desc">Security Code</p>
                        <div class="input_bx">
                        	<input type="text">
                        </div>
                    </div>
                </li>
                <li>
                	<p class="desc">Expiration Date</p>
                    <div class="month">
                        <div class="select_bx">
                        	<select><option>Month</option></select>
                        </div>
                    </div>
                    <div class="slash">
						<p>/</p>
                    </div>
                    <div class="date">
                        <div class="select_bx">
                        	<select><option>Year</option></select>
                        </div>
                    </div>
                </li>
                <li>
                	<div class="card_no">
                    	<p class="desc">Name On Card</p>
                        <div class="input_bx">
                        	<input type="text">
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="cr_card_dtl" style="border:0">
        	<ul>
            	<li>
                	<input type="submit" value="Continue">
                    <span class="cancel"><a href="javascript:void(0);" id="closee">Cancel</a></span>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
		    <div class="clearfix"></div>
		    <?php include('includes/footer.php');?>
		</div>
	</div> 
<script>
        $(document).ready(function(){
          $("#login").click(function(){
            $("#loginpage").show();
	        $(".overlay_outer").show();
	
          });
         
          $("#closee").click(function(){
            $("#loginpage").hide();
	        $(".overlay_outer").hide();
          });
        });

function open_forgot()
{
  $('#forgot_password_ul').slideToggle('slow');
}
function send_forgot_password()
{
  var forgot_email=$('#forgot_email').val();
  if(forgot_email=='')
  {
    $('#email_err').html('<font color="red">Please enter your email.</font>');
  }
  else
  {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(forgot_email)) 
    {
            $('#email_err').html('Please wait...');
            $.post('ajax_forgot.php?email='+forgot_email, function(data){
                if(data=='success'){
	              $('#email_err').html('<font color="green">A new password has been sent to your mail. Please check your mail.</font>');
                } else if(data=='fail'){ 
	            $('#email_err').html('<font color="red">Email id not exist.</font>');
                }
             });
     }
     else
     {
        $('#email_err').html('<font color="red">Please enter valid email.</font>');
     }
  }
  
}
</script>
</body>
</html>

























































